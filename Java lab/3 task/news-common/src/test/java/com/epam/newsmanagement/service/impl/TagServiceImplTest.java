package com.epam.newsmanagement.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;

import static org.mockito.Mockito.*;

import org.hibernate.HibernateException;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(profiles = "hibernate")
@ContextConfiguration("classpath:testContext.xml")
public class TagServiceImplTest {

	@Mock
	private TagDAO tagDao;
	
	@Autowired
	@InjectMocks
	private TagService tagService;
	
	public TagServiceImplTest() {
	}
	
	@Before
	public void setUp() throws DAOException {
		 MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testFindAllTags() throws DAOException, ServiceException  {
		tagService.findAllTags();
		verify(tagDao).findAll();
	}
	
	@Test(expected=ServiceException.class)
	public void testFindAllTagsException() throws DAOException, ServiceException  {
		doThrow(HibernateException.class).when(tagDao).findAll();
		tagService.findAllTags();
		
	}
	
	@Test 
	public void testFindTag() throws DAOException, ServiceException {
		when (tagDao.findById(2)).thenReturn(new Tag(2, "tag2"));
		
		Tag expected = new Tag(2, "tag2");
		Tag actual = tagService.findTagById(2);
		verify(tagDao).findById(2);	
		assertEquals(expected, actual);
	}
	
	@Test(expected=ServiceException.class)
	public void testFindTagException() throws DAOException, ServiceException {
		doThrow(HibernateException.class).when(tagDao).findById(3);
		tagService.findTagById(3);
	}
	
	@Test
	public void testSaveTag() throws DAOException, ServiceException {
		Tag tag = new Tag("tag");
		when (tagDao.save(tag)).thenReturn(1L);
		
		long expected = 1;
		
		long actual = tagService.saveTag(tag).getId();
		verify(tagDao).save(tag);
		assertEquals(expected, actual);	
	}
	
	@Test(expected=ServiceException.class)
	public void testSaveTagException() throws DAOException, ServiceException {
		Tag tag = new Tag("tag");
		doThrow(HibernateException.class).when(tagDao).save(tag);
		tagService.saveTag(tag);
	}
	
	@Test
	public void testEditTag() throws DAOException, ServiceException {
		
		Tag tag = new Tag(1, "tag");
		tagService.editTag(tag);
		verify(tagDao).update(tag);
		
	}
	
	@Test(expected=ServiceException.class)
	public void testEditTagException() throws DAOException, ServiceException {
		Tag tag = new Tag(1, "tag");
		doThrow(HibernateException.class).when(tagDao).update(tag);
		tagService.editTag(tag);
	}
	
	@Test
	public void testDeleteTag() throws DAOException, ServiceException {
		long id = 1L;
		tagService.deleteTag(id);
		verify(tagDao).delete(id);
		
		
	}
	
	@Test(expected=ServiceException.class)
	public void testDeleteTagException() throws DAOException, ServiceException {
		long id=1L;
		doThrow(HibernateException.class).when(tagDao).delete(id);
		tagService.deleteTag(id);
	}
	
	@Test
	public void testFindTagsByNews() throws DAOException, ServiceException {
		tagService.findTagsByNews(1);
		verify(tagDao).findByNews(1);
	}
	
	@Test(expected=ServiceException.class)
	public void testFindTagsByNewsException() throws DAOException, ServiceException {
		long id=1L;
		doThrow(HibernateException.class).when(tagDao).findByNews(id);
		tagService.findTagsByNews(id);
	}

}