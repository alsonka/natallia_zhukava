package com.epam.newsmanagement.dao.hibernate.impl;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;

import org.dbunit.DatabaseUnitException;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;


import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class UserDAOImplTest extends AbstractTest {

	
	
	@Autowired
	private UserDAO userDAO;
	
	public UserDAOImplTest()   {
		
	} 

	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		 return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/user-data.xml"));
	}

	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());
	}

	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void testFindUserByLogin() throws DAOException {
		User expected = new User(1, "user", "first", "password");
		String login = "first";
		User actual = userDAO.findUserByLogin(login);
		assertEquals(expected, actual);
	}
	
	
}
