package com.epam.newsmanagement.dao.hibernate.impl;

	import java.io.FileInputStream;
	import java.io.FileNotFoundException;
	import java.sql.Connection;
	import java.sql.DatabaseMetaData;
	import java.sql.SQLException;

	import javax.sql.DataSource;

	import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
	import org.dbunit.database.IDatabaseConnection;
	import org.dbunit.dataset.DataSetException;
	import org.dbunit.dataset.IDataSet;
	import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.Oracle10DataTypeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
    
	@ContextConfiguration("classpath:testContext.xml")
	@ActiveProfiles(profiles = {"JPA","eclipselinkJPA" })
	public abstract class AbstractTest {
		@Autowired
		DataSource datasource;
		
		public DataSource getDatasource() {
			return datasource;
		}

		public void setDatasource(DataSource datasource) {
			this.datasource = datasource;
		}
		

		
		public IDatabaseConnection getConnection() throws Exception {
			Connection con = datasource.getConnection();
			DatabaseMetaData databaseMetaData = con.getMetaData();
			IDatabaseConnection connection = new DatabaseConnection(con, databaseMetaData.getUserName().toUpperCase());
			DatabaseConfig config = connection.getConfig();  
		        // oracle 10g   
		    config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new Oracle10DataTypeFactory());  
		        // recycle bin  
		    config.setProperty(DatabaseConfig.FEATURE_SKIP_ORACLE_RECYCLEBIN_TABLES, Boolean.TRUE);  
			return connection;
		}
		
		public abstract IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException;

		public IDataSet getDataSetForDelete() throws DataSetException, FileNotFoundException {
			return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/deleteData.xml"));
		}

		public abstract void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception;

		public abstract void cleanDataBase() throws DatabaseUnitException, SQLException, Exception;
	}

