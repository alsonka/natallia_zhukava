package com.epam.newsmanagement.dao.hibernate.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TimeZone;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;


import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteriaVO;

import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class NewsDAOImplTest extends AbstractTest {

	@Autowired
	private NewsDAO newsDAO;


	public NewsDAOImplTest() {

	}

	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news-data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/comment-data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/tag-data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/author-data.xml"))

		};
		return new CompositeDataSet(datasets);
	}

	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

	}

	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void testFindAll() throws DAOException {

		List<News> news = newsDAO.findAll();
		System.out.println(news.toString());
		assertThat(news.size(), is(10));
	}

	@Test
	public void testFindById() throws DAOException, ParseException {
		long id = 3;

		News expected = new News(id, "News1", "ShortText", "Full text",  Timestamp.valueOf("2015-08-14 09:28:29.113"),
				java.sql.Date.valueOf("2015-08-14"));
		News actual = newsDAO.findById(id);
		assertEquals(expected, actual);
	}

	@Test
	public void testFindByTag() throws DAOException {
		List<News> expected = new ArrayList<News>();
		expected.add(newsDAO.findById(1));
		expected.add(newsDAO.findById(4));
		Collections.sort(expected, new ListNewsComparator());

		List<News> actual = newsDAO.findByTag(5);
		Collections.sort(actual, new ListNewsComparator());
		assertEquals(expected, actual);
	}

	@Test
	public void testFindByAuthor() throws DAOException {
		List<News> expected = new ArrayList<News>();
		expected.add(newsDAO.findById(1));
		expected.add(newsDAO.findById(3));
		Collections.sort(expected, new ListNewsComparator());
		
		List<News> actual = newsDAO.findByAuthor(2);
		Collections.sort(actual, new ListNewsComparator());
		assertEquals(expected, actual);
	}
	
	@Test
	public void testFindByEmptyCriteria() throws DAOException {
		SearchCriteriaVO obj = new SearchCriteriaVO();
		List<News> list = newsDAO.findByCriteria(obj);
		assertThat(list.size(), is(10));
	}

	@Test
	public void testFindByCriteria() throws DAOException {
		SearchCriteriaVO obj = new SearchCriteriaVO();
		obj.setAuthorId(2);
		List<Long> tags = new ArrayList<Long>();
		tags.add(2L);
		tags.add(5L);
		obj.setTagIdList(tags);

		News n1 = newsDAO.findById(1);

		List<News> expected = new ArrayList<News>();
		expected.add(n1);

		List<News> actual = newsDAO.findByCriteria(obj);

		assertEquals(expected, actual);
	}

	@Test
	public void testFindBySearchCriteriaWithTagsOnly() throws DAOException {
		SearchCriteriaVO obj = new SearchCriteriaVO();

		List<Long> tags = new ArrayList<Long>();
		tags.add(2L);
		tags.add(5L);
		obj.setTagIdList(tags);

		News n1 = newsDAO.findById(1);
		News n2 = newsDAO.findById(4);

		List<News> expected = new ArrayList<News>();
		expected.add(n1);
		expected.add(n2);
		Collections.sort(expected, new ListNewsComparator());

		List<News> actual = newsDAO.findByCriteria(obj);
		Collections.sort(actual, new ListNewsComparator());
		assertEquals(expected, actual);
	}

	@Test
	public void testFindBySearchCriteriaWithAuthorOnly() throws DAOException {
		SearchCriteriaVO obj = new SearchCriteriaVO();
		obj.setAuthorId(2);

		News n1 = newsDAO.findById(1);
		News n2 = newsDAO.findById(3);

		List<News> expected = new ArrayList<News>();
		expected.add(n1);
		expected.add(n2);
		Collections.sort(expected, new ListNewsComparator());

		List<News> actual = newsDAO.findByCriteria(obj);
		
		Collections.sort(actual, new ListNewsComparator());
		assertEquals(expected, actual);
	}

	@Test
	public void testSaveNews() throws DAOException, ParseException {
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		Timestamp currentTimestamp = new Timestamp(date.getTime());
		News news =  new News("News6", "ShortText", "Full text", currentTimestamp,
				date);
		long id = newsDAO.save(news);

		News savedNews = newsDAO.findById(id);
		String expected = news.getTitle();
		String actual = savedNews.getTitle();

		assertEquals(expected, actual);
	}
	
	
	

	@Test
	public void testDelete() throws DAOException {

		News expected = newsDAO.findById(9);
		newsDAO.delete(9);
		News actual = newsDAO.findById(9);
		assertNotEquals(expected, actual);
	}
	
	@Test
	public void testCountNews() throws DAOException {
		long expected = 10;
		long actual = newsDAO.countNews();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testFindNewsOnPage() throws DAOException {	
		List<News> list = newsDAO.findNewsOnCurrentPage(0, 5);
		assertThat(list.size(), is(5));
	}
	
	@Test
	public void testFindNewsOnPageWithSearchCriteria() throws DAOException {
		SearchCriteriaVO obj = new SearchCriteriaVO();

		List<Long> tags = new ArrayList<Long>();
		tags.add(2L);
		tags.add(5L);
		tags.add(3L);
		obj.setTagIdList(tags);
		
		List<News> list = newsDAO.findNewsOnCurrentPage(0, 5, obj);
		assertThat(list.size(), is(4));
	}
	
	@Test
	public void testCountNewsWithSearchCriteria() throws DAOException {
		SearchCriteriaVO obj = new SearchCriteriaVO();

		List<Long> tags = new ArrayList<Long>();
		tags.add(2L);
		tags.add(5L);
		obj.setTagIdList(tags);
		
		long expected = 2; 
		long actual = newsDAO.countNews(obj);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testFindPreviousAndNextNews() throws DAOException {
		long[] expected = {1, 11};
		long[] actual = newsDAO.findPreviousAndNextNews(5);
		
		assertArrayEquals(expected, actual);
	}
	


	@Test
	public void testFindPreviousAndNextNewsWithSearchCriteria() throws DAOException {
		SearchCriteriaVO obj = new SearchCriteriaVO();

		List<Long> tags = new ArrayList<Long>();
		tags.add(2L);
		tags.add(5L);
		tags.add(3L);
		obj.setTagIdList(tags);

		
		long[] expected = {1, 3};
		long[] actual = newsDAO.findPreviousAndNextNews(4, obj);
		
		assertArrayEquals(expected, actual);
	}
	
	@Test
	public void testDeleteNewsList() throws DAOException {
		News news1 = newsDAO.findById(10);
		News news2 = newsDAO.findById(11);
		News[] expected = {news1, news2};
		long[] ids = {10L, 11L};
		
		newsDAO.delete(ids);
		News news3 = newsDAO.findById(10);
		News news4 = newsDAO.findById(11);
		News[] actual = {news3, news4};
		assertNotEquals(expected, actual);
	}
	
	@Test
	public void testGetVersion() throws DAOException {
		News first = newsDAO.findById(1);
		first.setTitle("New Title");
		newsDAO.update(first);
		News second = newsDAO.findById(1);
		int version = second.getVersion();
		assertThat(version, is(2));
	}
	
	
	
	private class ListNewsComparator implements Comparator<News> {

		public int compare(News o1, News o2) {
			if (o1.getId() > o2.getId()) {
				return 1;
			} else if (o1.getId() < o2.getId()) {
				return -1;
			} else {
				return 0;
			}
		}
	}



}
