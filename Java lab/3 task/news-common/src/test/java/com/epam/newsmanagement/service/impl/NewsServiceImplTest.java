package com.epam.newsmanagement.service.impl;


import org.hibernate.HibernateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;

import static org.mockito.Mockito.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;


import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(profiles = "hibernate")
@ContextConfiguration("classpath:testContext.xml")
public class NewsServiceImplTest {

	@Mock
	private NewsDAO newsDao;
	
	@Mock
	private SearchCriteriaVO obj;
	
	@Autowired
	@InjectMocks
	private NewsService newsService;
	
	public NewsServiceImplTest() {		
	}
	
	@Before
	public void setUp() throws DAOException {
        MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testFindAllNews() throws DAOException, ServiceException  {
		newsService.findAllNews();
		verify(newsDao).findAll();
	}
	
	@Test(expected=ServiceException.class)
	public void testFindAllNewsException() throws DAOException, ServiceException {
		doThrow(HibernateException.class).when(newsDao).findAll();
		newsService.findAllNews();
	}
	
	@Test 
	public void testFindNewsById() throws DAOException, ServiceException {
		when (newsDao.findById(2)).thenReturn(new News(2, "title", "short_text", "full_text", null, null));
		
		News expected =  new News(2, "title", "short_text", "full_text", null, null);
		News actual = newsService.findNewsById(2);
		verify(newsDao).findById(2);
		assertEquals(expected, actual);
	}
	
	@Test(expected=ServiceException.class)
	public void testFindNewsByIdException() throws DAOException, ServiceException {
		long id = 5;
		doThrow(HibernateException.class).when(newsDao).findById(id);
		newsService.findNewsById(id);
	}
	
	
	@Test
	public void testSaveNews() throws DAOException, ServiceException {
		News news = new News("title", "short_text", "full_text", new Timestamp(Calendar.getInstance().getTimeInMillis()), null);
		when (newsDao.save(news)).thenReturn(1L);
		
		long expected = 1;
		long actual = newsService.saveNews(news);
		verify(newsDao).save(news);
		assertEquals(expected, actual);	
	}
	
	@Test(expected=ServiceException.class)
	public void testSaveNewsException() throws DAOException, ServiceException {
		News news = new News("title", "short_text", "full_text", new Timestamp(Calendar.getInstance().getTimeInMillis()), null);
		doThrow(HibernateException.class).when(newsDao).save(news);
		newsService.saveNews(news);
	}
	
	@Test
	public void testEditNews() throws DAOException, ServiceException {
		News news = new News("title", "short_text", "full_text", null, new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
		newsService.editNews(news);
		verify(newsDao).update(news);
		
	}
	
	@Test(expected=ServiceException.class)
	public void testEditNewsException() throws DAOException, ServiceException {
		News news = new News("title", "short_text", "full_text", null, new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
		doThrow(HibernateException.class).when(newsDao).update(news);
		newsService.editNews(news);
	}
	
	@Test
	public void testDeleteNews() throws DAOException, ServiceException {
		long id = 1L;
		newsService.deleteNews(id);
		verify(newsDao).delete(id);
	}
	
	@Test(expected=ServiceException.class)
	public void testDeleteNewsException() throws DAOException, ServiceException {
		long id = 5;
		doThrow(HibernateException.class).when(newsDao).delete(id);
		newsService.deleteNews(id);
	}
	
	@Test
	public void testFindNewsByAuthor() throws DAOException, ServiceException {
		long authorId=4L;
		newsService.findNewsByAuthor(authorId);
		verify(newsDao).findByAuthor(authorId);
	}
	
	@Test(expected=ServiceException.class)
	public void testFindNewsByAuthorException() throws DAOException, ServiceException {
		long id = 5;
		doThrow(HibernateException.class).when(newsDao).findByAuthor(id);
		newsService.findNewsByAuthor(id);
	}
	
	@Test
	public void testCountNews() throws DAOException, ServiceException {
		when (newsDao.countNews()).thenReturn(15L);
		
		long expected = 15L;
		long actual = newsService.countAllNews();
		verify(newsDao).countNews();
		assertEquals(expected, actual);
	}
	
	@Test(expected=ServiceException.class)
	public void testCountNewsException() throws DAOException, ServiceException {
		doThrow(HibernateException.class).when(newsDao).countNews();
		newsService.countAllNews();
	}
	
	
	
	@Test
	public void testFindPreviousAndNext() throws ServiceException, DAOException {
		long newsId = 1L;
		
		newsService.findPreviousAndNext(newsId);
		verify(newsDao).findPreviousAndNextNews(newsId);
	}
	
	@Test(expected=ServiceException.class)
	public void testFindPreviousAndNextException() throws DAOException, ServiceException {
		long newsId = 1L;
		doThrow(HibernateException.class).when(newsDao).findPreviousAndNextNews(newsId);
		newsService.findPreviousAndNext(newsId);
	}
	
	@Test
	public void testFindPreviousAndNextWithSearch() throws ServiceException, DAOException {
		long newsId = 1L;
		
		newsService.findPreviousAndNext(newsId, obj);
		verify(newsDao).findPreviousAndNextNews(newsId, obj);
	}
	
	@Test(expected=ServiceException.class)
	public void testFindPreviousAndNextWithSearchException() throws DAOException, ServiceException {
		long newsId = 1L;
		doThrow(HibernateException.class).when(newsDao).findPreviousAndNextNews(newsId, obj);
		newsService.findPreviousAndNext(newsId, obj);
	}
	
	@Test
	public void testFindNewsOnPage() throws ServiceException, DAOException {
		when (newsDao.countNews()).thenReturn(5L);
		when (newsDao.findNewsOnCurrentPage(0, 5)).thenReturn(new ArrayList<News>());
		
		newsService.findNewsOnPage(1, 5);
		verify(newsDao).findNewsOnCurrentPage(0, 5);
	}
	
	@Test
	public void testFindNewsOnPageMoreNews() throws ServiceException, DAOException {
		when (newsDao.countNews()).thenReturn(8L);
		when (newsDao.findNewsOnCurrentPage(0, 5)).thenReturn(new ArrayList<News>());
		
		newsService.findNewsOnPage(1, 5);
		verify(newsDao).findNewsOnCurrentPage(0, 5);
	}
	
	@Test
	public void testFindNewsOnPageIncorrectPage() throws ServiceException, DAOException {
		when (newsDao.countNews()).thenReturn(8L);
		when (newsDao.findNewsOnCurrentPage(0, 5)).thenReturn(new ArrayList<News>());
		
		newsService.findNewsOnPage(5, 5);
		verify(newsDao).findNewsOnCurrentPage(0, 5);
	}
	
	@Test
	public void testFindNewsOnPageLessThanOnPage() throws ServiceException, DAOException {
		when (newsDao.countNews()).thenReturn(4L);
		when (newsDao.findNewsOnCurrentPage(0, 4)).thenReturn(new ArrayList<News>());
		
		newsService.findNewsOnPage(1, 5);
		verify(newsDao).findNewsOnCurrentPage(0, 4);
	}
	
	@Test(expected=ServiceException.class)
	public void testFindNewsOnPageException() throws DAOException, ServiceException {
		when (newsDao.countNews()).thenReturn(5L);
		doThrow(HibernateException.class).when(newsDao).findNewsOnCurrentPage(0, 5);
		newsService.findNewsOnPage(1, 5);
	}
	
	@Test
	public void testDeleteNewsList() throws DAOException, ServiceException {
		long[] ids = {1L, 2L, 3L};
		newsService.deleteNews(ids);
		verify(newsDao).delete(ids);
	}
	
	@Test(expected=ServiceException.class)
	public void testDeleteNewsListException() throws DAOException, ServiceException {
		long[] ids = {1L, 2L, 3L};
		doThrow(HibernateException.class).when(newsDao).delete(ids);
		newsService.deleteNews(ids);
		
	}
	

	

}
