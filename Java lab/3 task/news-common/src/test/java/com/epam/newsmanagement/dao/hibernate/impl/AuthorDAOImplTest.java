package com.epam.newsmanagement.dao.hibernate.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;
import java.util.TimeZone;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;

import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class AuthorDAOImplTest extends AbstractTest {

	@Autowired
	private AuthorDAO authorDAO;


	public AuthorDAOImplTest() {

	}

	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news-data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/author-data.xml"))
				
				};
		return new CompositeDataSet(datasets);
	}

	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		
	}

	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void testFindAll()   {

		List<Author> authors = authorDAO.findAll();
		assertThat(authors.size(), is(14));
	}

	@Test
	public void testFindById() throws DAOException {
		Author expected = new Author(3, "Julia Ivanova", null);
		
		Author actual = authorDAO.findById(3);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testFindByIdNotExists() throws DAOException {
		Author expected = null;
		Author actual = authorDAO.findById(322);
		assertEquals(expected, actual);
	}

	@Test
	public void testSave() throws DAOException {

		Author expected = new Author("New Author", null);
		long id = authorDAO.save(expected);
		expected.setId(id);
		Author actual = authorDAO.findById(id);
		assertEquals(expected, actual);
	}
	


	@Test
	public void testUpdate() throws DAOException {

		Author expected = new Author(6, "Changed name", null);
		Author author = authorDAO.findById(6);
		author.setName("Changed name");
		authorDAO.update(author);
		Author actual = authorDAO.findById(6);
		assertEquals(expected, actual);
	}
	
	

	@Test
	public void testDelete() throws DAOException {

		Author expected = authorDAO.findById(8);
		authorDAO.delete(8);
		Author actual = authorDAO.findById(8);
		assertNotEquals(expected, actual);
	}

	@Test
	public void testFindByNews() throws DAOException {
		Author expected = authorDAO.findById(3);
		Author actual = authorDAO.findByNews(5);
		assertEquals(expected, actual);
	}
	
	
	
	
	@Test
	public void testFindActualAuthors() throws DAOException {
		List<Author> authors = authorDAO.findActualAuthors();
		assertThat(authors.size(), is(9));
		
	}
	


}
