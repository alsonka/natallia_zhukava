package com.epam.newsmanagement.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(profiles = "hibernate")
@ContextConfiguration("classpath:testContext.xml")
public class NewsManagerImplTest {

	@Mock
	private NewsService newsService;
	@Mock
	private AuthorService authorService;
	@Mock
	private TagService tagService;
	@Mock
	private CommentService commentService;
	
	@Autowired
	@InjectMocks
	private NewsManager newsManager;
	
	private NewsVO complexNews;
	
	public NewsManagerImplTest() {
		complexNews = new NewsVO();
		Author author = new Author(2, "author", null);
		complexNews.setAuthor(author);
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag(1, "tag1"));
		tags.add(new Tag(2, "tag2"));
		complexNews.setTagList(tags);
	}
	
	@Before
	public void setUp() throws DAOException, ServiceException {
		MockitoAnnotations.initMocks(this);		
	}
	

	
	@Test
	public void testViewNewsList() throws ServiceException, DAOException {
		List<News> list = new ArrayList<News>();
		list.add(new News(1L, null, null, null, null, null));
		
		when (newsService.findAllNews()).thenReturn(list);
		when (newsService.findNewsById(1L)).thenReturn(new News());
		when (authorService.findAuthorByNews(1L)).thenReturn(new Author());
		when (tagService.findTagsByNews(1L)).thenReturn(new ArrayList<Tag>());
		when (commentService.findCommentsByNews(1L)).thenReturn(new ArrayList<Comment>());
		newsManager.viewNewsList();
		verify(newsService).findAllNews();
	}
	
	@Test
	public void testViewSingleNews() throws DAOException, ServiceException {
		News expected = new News();
		expected.setId(2);
		when (newsService.findNewsById(2)).thenReturn(expected);
		News actual = newsManager.viewSingleNews(2).getNews();
		verify(newsService).findNewsById(2);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testCreateNews() throws DAOException, ServiceException {
		
		long expected = 1;
		News news = new News();
		news.setId(expected);
		
		when (newsService.saveNews(news)).thenReturn(1L);
		
		complexNews.setNews(news);
		long actual = newsManager.saveNews(complexNews);
		verify(newsService).saveNews(news);
		assertEquals(expected, actual);	
	}
	
	@Test
	public void testEditNews() throws DAOException, ServiceException {
		
		News news = new News();
		complexNews.setNews(news);
		newsManager.editNews(complexNews);
		verify(newsService).editNews(news);
		
	}
	
	@Test
	public void testDeleteNews() throws DAOException, ServiceException {
		
		long id = 1L;
		newsManager.deleteNews(id);	
		verify(newsService).deleteNews(id);
		
		
	}
	
	@Test
	public void testDeleteNewsList() throws ServiceException {
		long[] ids = {1L, 2L, 3L};
		newsManager.deleteNews(ids);
		verify(newsService).deleteNews(ids);
	}
	
	@Test (expected=ServiceException.class)
	public void testDeleteNewsException() throws ServiceException {
		long id = 1L;
		
		doThrow(ServiceException.class).when(newsService).deleteNews(1);
		newsManager.deleteNews(id);
		
	}
	
	
	
	@Test
	public void testViewNewsListOnPage() throws ServiceException {
		List<News> list = new ArrayList<News>();
		list.add(new News(1L, null, null, null, null, null));
		
		when (newsService.findNewsOnPage(1, 5)).thenReturn(list);
		when (newsService.findNewsById(1L)).thenReturn(new News());
		when (authorService.findAuthorByNews(1L)).thenReturn(new Author());
		when (tagService.findTagsByNews(1L)).thenReturn(new ArrayList<Tag>());
		when (commentService.findCommentsByNews(1L)).thenReturn(new ArrayList<Comment>());
		
		newsManager.viewNewsListOnPage(1, 5);
		
		verify(newsService).findNewsOnPage(1, 5);
	}
	
	

}