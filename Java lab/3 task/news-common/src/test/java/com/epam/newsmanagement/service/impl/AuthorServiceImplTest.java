package com.epam.newsmanagement.service.impl;


import org.hibernate.HibernateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;

import static org.mockito.Mockito.*;

import java.sql.Timestamp;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(profiles = "hibernate")
@ContextConfiguration("classpath:testContext.xml")
public class AuthorServiceImplTest {

	@Mock
	private AuthorDAO authorDao;
	
	@Autowired
	@InjectMocks
	private AuthorService authorService;
	
	public AuthorServiceImplTest() {		
	}
	
	@Before
	public void setUp() throws DAOException {
        MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testFindAllAuthors() throws DAOException, ServiceException  {
		authorService.findAllAuthors();
		verify(authorDao).findAll();
	}
	
	@Test(expected=ServiceException.class)
	public void testFindAllAuthorsException() throws DAOException, ServiceException {
		doThrow(HibernateException.class).when(authorDao).findAll();
		authorService.findAllAuthors();
	}
	
	@Test 
	public void testFindAuthorById() throws DAOException, ServiceException {
		when (authorDao.findById(2)).thenReturn(new Author(2, "author", null));
		
		Author expected = new Author(2, "author", null);
		Author actual = authorService.findAuthorById(2);
		verify(authorDao).findById(2);
		assertEquals(expected, actual);
	}
	
	@Test(expected=ServiceException.class)
	public void testFindAuthorByIdException() throws DAOException, ServiceException {
		long id = 3L;
		doThrow(HibernateException.class).when(authorDao).findById(id);
		authorService.findAuthorById(id);
		
	}
	
	@Test 
	public void testFindAuthorByNews() throws DAOException, ServiceException {
		when (authorDao.findByNews(2)).thenReturn(new Author (2, "second author", null));
		
		Author expected = new Author (2, "second author", null);
		Author actual  = authorService.findAuthorByNews(2);
		verify(authorDao).findByNews(2);
		assertEquals(expected, actual);
	}
	
	@Test(expected=ServiceException.class)
	public void testFindAuthorByNewsException() throws DAOException, ServiceException {
		long id = 5L;
		doThrow(HibernateException.class).when(authorDao).findByNews(id);
		authorService.findAuthorByNews(id);
	}
	
	@Test
	public void testSaveAuthor() throws DAOException, ServiceException {
		Author author = new Author("author", null);
		when (authorDao.save(author)).thenReturn(1L);
		
		long expected = 1;
		long actual = authorService.saveAuthor(author).getId();
		verify(authorDao).save(author);
		assertEquals(expected, actual);	
	}
	
	@Test(expected=ServiceException.class)
	public void testSaveAuthorException() throws DAOException, ServiceException	{
		Author author = new Author("author", null);
		doThrow(HibernateException.class).when(authorDao).save(author);
		authorService.saveAuthor(author);
	}
	
	@Test
	public void testEditAuthor() throws DAOException, ServiceException {
		Author author = new Author(1, "author", null);
		authorService.editAuthor(author);
		verify(authorDao).update(author);
		
	}
	
	@Test(expected=ServiceException.class)
	public void testEditAuthorException() throws DAOException, ServiceException	{
		Author author = new Author(1, "author", null);
		doThrow(HibernateException.class).when(authorDao).update(author);
		authorService.editAuthor(author);
	}
	
	@Test
	public void testDeleteAuthor() throws DAOException, ServiceException {
		
		long id = 1L;
		authorService.deleteAuthor(id);
		verify(authorDao).delete(id);
	}
	
	@Test(expected=ServiceException.class)
	public void testDeleteAuthorException() throws DAOException, ServiceException {
		long id = 32L;
		doThrow(HibernateException.class).when(authorDao).delete(id);
		authorService.deleteAuthor(id);
	}
	
	@Test
	public void testMakeExpired() throws ServiceException, DAOException {
		
		Author author = new Author(1L, "author", null);
		when (authorDao.findById(1L)).thenReturn(author);
		
		Timestamp expected = null;
		authorService.makeExpired(author.getId());
		Timestamp actual = author.getExpiredDate();
		verify(authorDao).findById(1L);
		verify(authorDao).update(author);
		assertNotEquals(expected, actual);
	}
	
	@Test(expected=ServiceException.class)
	public void testMakeExpiredException() throws DAOException, ServiceException {
		Author author = new Author(1L, "author", null);
		when (authorDao.findById(1L)).thenReturn(author);
		doThrow(HibernateException.class).when(authorDao).update(author);	
		authorService.makeExpired(1);
	}
	
	@Test
	public void testFindActualAuthors() throws ServiceException, DAOException {
		authorService.findActualAuthors();
		verify(authorDao).findActualAuthors();
	}
	
	@Test(expected=ServiceException.class)
	public void testFindActualAuthorsException() throws DAOException, ServiceException {
		doThrow(HibernateException.class).when(authorDao).findActualAuthors();
		authorService.findActualAuthors();
	}

}
