package com.epam.newsmanagement.dao.hibernate.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;

@Transactional
public class TagDAOHibernateImpl implements TagDAO {

	public TagDAOHibernateImpl() {
		// TODO Auto-generated constructor stub
	}
	
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	@Override
	public List<Tag> findAll() {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(Tag.class);
		@SuppressWarnings("unchecked")
		List<Tag> list = cr.list();
		return list;
	}

	@Override
	public Tag findById(long id) {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(Tag.class);
		cr.add(Restrictions.idEq(id));
		Tag tag = (Tag) cr.uniqueResult();
		return tag;
	}

	@Override
	public long save(Tag entity) {
		Session session = sessionFactory.getCurrentSession();
		long id = (long) session.save(entity);
		return id;
	}

	@Override
	public void update(Tag entity) {
		Session session = sessionFactory.getCurrentSession();
		session.update(entity);

	}

	@Override
	public void delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		Tag tag = findById(id);
		session.delete(tag);

	}

	@Override
	public List<Tag> findByNews(long newsId) {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(News.class);
		cr.add(Restrictions.idEq(newsId));
		News news = (News) cr.uniqueResult();
		return news.getTags();
	}

	@Override
	public Tag findByTagName(String name) {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(Tag.class);
		cr.add(Restrictions.eq("name", name));
		@SuppressWarnings("unchecked")
		List<Tag> tags = cr.list();
		return tags.get(0);
	}



}
