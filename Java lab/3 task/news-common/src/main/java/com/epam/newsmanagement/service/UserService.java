package com.epam.newsmanagement.service;

import com.epam.newsmanagement.exception.ServiceException;


/**
 * The Interface UserService contains methods which work with database table User.
 */
public interface UserService {
	
	/**
	 * The method finds name of user by login.
	 *
	 * @param login the login
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	String findUserNameByLogin(String login) throws ServiceException;

}
