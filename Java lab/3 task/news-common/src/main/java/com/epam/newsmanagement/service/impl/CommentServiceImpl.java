package com.epam.newsmanagement.service.impl;



import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;



/**
 * The Class CommentServiceImpl implements method from CommentService interface.
 * 
 * @author Natallia_Zhukava
 */
@Transactional
public class CommentServiceImpl implements CommentService {

	private CommentDAO commentDao;
	
	private NewsDAO newsDao;

	
	public void setCommentDao(CommentDAO commentDao) {
		this.commentDao = commentDao;
	}

	public void setNewsDao(NewsDAO newsDao) {
		this.newsDao = newsDao;
	}


	
	public CommentServiceImpl() {

	}


	
	@Override
	public List<Comment> findAllComments() throws ServiceException  {
		List<Comment> list = new ArrayList<Comment>();
		try {
			list = commentDao.findAll();
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		return list;
	}



	@Override
	public List<Comment> findCommentsByNews(long newsId) throws ServiceException  {
		List<Comment> list = new ArrayList<Comment>();
		try {
			list = commentDao.findByNewsId(newsId);
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		return list;
	}

	

	@Override
	public Comment findCommentById(long id) throws ServiceException {
		Comment comment = null;
		try {
			comment = commentDao.findById(id);
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		return comment;
	}



	@Override
	public long saveComment(Comment comment) throws ServiceException{
		
		Calendar calendar = Calendar.getInstance();
		comment.setCreationDate(new Timestamp(calendar.getTime().getTime()));
		long id=0;
		try {
		//	News news = newsDao.findById(comment.getNewsId());
		//	comment.setNews(news);
			id = commentDao.save(comment);
			
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		return id;

	}



	@Override
	public void editComment(Comment comment) throws ServiceException  {

		try {

			commentDao.update(comment);

		} catch (HibernateException e) {
			throw new ServiceException(e);
		}

	}


	
	@Override
	public void deleteComment(long id) throws ServiceException{
		
		try {

			commentDao.delete(id);

		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
	
	}

	

}
