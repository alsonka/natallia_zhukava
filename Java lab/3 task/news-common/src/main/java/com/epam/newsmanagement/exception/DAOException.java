package com.epam.newsmanagement.exception;


/**
 * The Class DAOException is exception of DAO layer
 */
public class DAOException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new DAO exception.
	 */
	public DAOException() {
		
	}

	/**
	 * Instantiates a new DAO exception.
	 *
	 * @param arg0 the arg0
	 */
	public DAOException(String arg0) {
		super(arg0);
		
	}

	/**
	 * Instantiates a new DAO exception.
	 *
	 * @param arg0 the arg0
	 */
	public DAOException(Throwable arg0) {
		super(arg0);
		
	}

	/**
	 * Instantiates a new DAO exception.
	 *
	 * @param arg0 the arg0
	 * @param arg1 the arg1
	 */
	public DAOException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		
	}

	/**
	 * Instantiates a new DAO exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 * @param enableSuppression the enable suppression
	 * @param writableStackTrace the writable stack trace
	 */
	public DAOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

}
