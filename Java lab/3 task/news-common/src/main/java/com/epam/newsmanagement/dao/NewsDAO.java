package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.exception.DAOException;



/**
 * The Interface NewsDAO contains the methods which should work with {@code News} database table and tables {@code News_Tag} and {@code News_Author}.
 * 
 * @author Natallia_Zhukava
 */
public interface NewsDAO extends CommonDAO<News>{

	/**
	 * The method finds all news which attached to given tag.
	 *
	 * @param tagId the tag id
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<News> findByTag(long tagId);

	/**
	 * The method finds all news which attached to given author.
	 *
	 * @param authorId the author id
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<News> findByAuthor(long authorId);

	/**
	 * The method finds all news which match given criteria.
	 *
	 * @param obj the SearchCriteria obj
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<News> findByCriteria(SearchCriteriaVO obj);

	/**
	 * The method finds news on current page.
	 *
	 * @param lastOnPrevPage the last number on previous page
	 * @param lastOnPage the last number on page
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<News> findNewsOnCurrentPage(int lastOnPrevPage, int lastOnPage);
	/**
	 * The method finds news on current page of search results.
	 *
	 * @param lastOnPrevPage the last number on previous page
	 * @param lastOnPage the last number on page
	 * @param obj - the search criteria which news are filtered
	 * @return the list
	 * @throws DAOException the DAO exception
	 */	
	List<News> findNewsOnCurrentPage(int lastOnPrevPage, int lastOnPage, SearchCriteriaVO obj);

	
	
	/**
	 * The method counts quantity of news.
	 *
	 * @return the long the quantity of news
	 * @throws DAOException the DAO exception
	 */
	long countNews();

	
	/**
	 * The method finds previous and next news by given news id.
	 *
	 * @param newsId the news id
	 * @return array with previous and next news id;
	 * @throws DAOException the DAO exception
	 */
	long[] findPreviousAndNextNews(long newsId);
	
	/**
	 * The method finds previous and next news by given news id in list of search results.
	 *
	 * @param newsId the news id
	 * @param obj the searchCriteria object
	 * @return array with   previous and next news id
	 * @throws DAOException the DAO exception
	 */
	long[] findPreviousAndNextNews(long newsId, SearchCriteriaVO obj);

	/**
	 * Count news which are found by given search criteria .
	 *
	 * @param obj the obj
	 * @return number of news
	 * @throws DAOException the DAO exception
	 */
	long countNews(SearchCriteriaVO obj);
	
	/**
	 * Deletes list of news.
	 *
	 * @param newsIds the news ids
	 * @throws DAOException the DAO exception
	 */
	void delete(long[] newsIds);
	



}