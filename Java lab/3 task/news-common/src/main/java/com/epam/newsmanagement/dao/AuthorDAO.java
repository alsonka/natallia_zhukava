package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Interface AuthorDAO contains the methods which should work with database table {@code Author}
 * 
 * @author Natallia_Zhukava
 */
public interface  AuthorDAO extends CommonDAO<Author> {

	/**
	 * The method finds the author attached to given news.
	 *
	 * @param newsId the news id
	 * @return the {@code Author} object
	 * @throws DAOException the DAO exception
	 */
	Author findByNews(long newsId);
		
	/**
	 * The method finds authors with actual status (not expired).
	 *
	 * @return the list of authors
	 * @throws DAOException the DAO exception
	 */
	List<Author> findActualAuthors();


}