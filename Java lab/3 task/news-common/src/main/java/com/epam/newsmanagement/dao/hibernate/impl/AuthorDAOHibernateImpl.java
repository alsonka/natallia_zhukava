package com.epam.newsmanagement.dao.hibernate.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;

@Transactional
public class AuthorDAOHibernateImpl implements AuthorDAO {


	public AuthorDAOHibernateImpl() {
		
	}
	
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<Author> findAll() {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(Author.class);
		@SuppressWarnings("unchecked")
		List<Author> list = cr.list();
		return list;
	}

	@Override
	public Author findById(long id) {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(Author.class);
		cr.add(Restrictions.idEq(id));
		Author author = (Author) cr.uniqueResult();
		return author;
	}

	@Override
	public long save(Author entity) {
		Session session = sessionFactory.getCurrentSession();
		long id = (long) session.save(entity);	
		return id;
	}

	@Override
	public void update(Author entity) {
		Session session = sessionFactory.getCurrentSession();
		session.update(entity);

	}

	@Override
	public void delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		Author author = findById(id);
		session.delete(author);

	}

	@Override
	public Author findByNews(long newsId) {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(News.class);
		cr.add(Restrictions.idEq(newsId));
		News news = (News) cr.uniqueResult();
		return news.getAuthor();
	}



	@Override
	public List<Author> findActualAuthors() {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(Author.class);
		cr.add(Restrictions.isNull("expiredDate"));
		@SuppressWarnings("unchecked")
		List<Author> list = cr.list();
		return list;
	}

}
