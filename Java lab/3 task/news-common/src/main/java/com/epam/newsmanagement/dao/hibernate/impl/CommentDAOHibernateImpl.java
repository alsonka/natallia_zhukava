package com.epam.newsmanagement.dao.hibernate.impl;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;

@Transactional
public class CommentDAOHibernateImpl implements CommentDAO {

	public CommentDAOHibernateImpl() {
	}
	
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	@Override
	public List<Comment> findAll() {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(Comment.class);
		@SuppressWarnings("unchecked")
		List<Comment> list = cr.list();
		return list;
	}

	@Override
	public Comment findById(long id) {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(Comment.class);
		cr.add(Restrictions.idEq(id));
		Comment comment = (Comment) cr.uniqueResult();
		return comment;
	}

	@Override
	public long save(Comment entity) {
		Session session = sessionFactory.getCurrentSession();
		long newsId = entity.getNewsId();
		News news = (News) session.load(News.class, newsId);
		entity.setNews(news);
		long id = (long) session.save(entity);
		return id;
	}

	@Override
	public void update(Comment entity) {
		Session session = sessionFactory.getCurrentSession();
		session.update(entity);

	}

	@Override
	public void delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		Comment comment = findById(id);
		session.delete(comment);

	}

	@Override
	public List<Comment> findByNewsId(long id) {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(News.class);
		cr.add(Restrictions.idEq(id));
		News news = (News) cr.uniqueResult();
		List<Comment> list = news.getComments();
		list.size();
		return list;
	}



}
