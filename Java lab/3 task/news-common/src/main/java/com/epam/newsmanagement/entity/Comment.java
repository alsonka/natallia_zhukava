package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



/**
 * The class Comment contains data from table Comments.
 *
 * @author Natallia_Zhukava
 */

@Entity
@Table(name="comments")
public class Comment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="COMMENT_ID")
	@SequenceGenerator(name = "COMMENT_SEQ", sequenceName = "comment_sequence",  allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMMENT_SEQ")
	private long id;
	
	@Column(name="NEWS_ID", insertable=false, updatable=false)
	private long newsId;
	
	@Column(name="COMMENT_TEXT")
	private String text;
	

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE")
	private Date creationDate;
	
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	 @JoinColumn(name = "NEWS_ID", nullable = false)
    private News news;
	
	public News getNews() {
    return news;
    	}
	public void setNews(News news) {
		this.news = news;
	}

	
	public Comment() {
	}

	
	public Comment(long id, long newsId, String text, Date creationDate) {
		this.id = id;
		this.newsId = newsId;
		this.text = text;
		this.creationDate = creationDate;
	}
	
	public Comment(String text, Date creationDate) {
		this.text = text;
		this.creationDate = creationDate;
	}
	
	public Comment(String text) {
		this.text = text;
	}

	
	public Comment(long newsId, String text, Date creationDate) {
		this.newsId = newsId;
		this.text = text;
		this.creationDate = creationDate;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public long getNewsId() {
		return newsId;
	}


	public void setNewsId(long newsId) {
		this.newsId = newsId;
	}


	public String getText() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public Date getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}


	@Override
	public String toString() {
		return "Comment [id="+ getId()+", newsId=" + newsId + ", text=" + text + ", creationDate=" + creationDate + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + (int) (newsId ^ (newsId >>> 32));
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Comment))
			return false;
		Comment other = (Comment) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (id != other.id)
			return false;
		if (newsId != other.newsId)
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}


}
