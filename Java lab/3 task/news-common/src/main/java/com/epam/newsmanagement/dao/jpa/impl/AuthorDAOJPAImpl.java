package com.epam.newsmanagement.dao.jpa.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;

public class AuthorDAOJPAImpl implements AuthorDAO {

	public AuthorDAOJPAImpl() {
	}
	
	private EntityManager em;

	public void setEm(EntityManager em) {
		this.em = em;
	}
	

	@Override
	@Transactional(readOnly = true)
	public List<Author> findAll()  {
		String query = "SELECT a FROM Author a";
		TypedQuery<Author> tq = em.createQuery(query, Author.class);
		List<Author> list = tq.getResultList();
		return list;
	}

	@Override
	@Transactional 
	public Author findById(long id)  {
	Author author=em.find(Author.class, id);
	return author;
	
	}

	@Override
	@Transactional
	public long save(Author entity) {
		em.persist(entity);
		em.flush();	
		return entity.getId();
	}

	@Override
	@Transactional
	public void update(Author entity)  {
		em.merge(entity);

	}

	@Override
	@Transactional
	public void delete(long id)  {
		Author author = em.find(Author.class, id);
		if (author!=null) {
		em.remove(author); 
		}

	}

	@Override
	@Transactional
	public Author findByNews(long newsId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<News> cq = cb.createQuery(News.class);
		Root<News> news = cq.from(News.class);
		cq.select(news).where(cb.equal(news.get("id"), newsId));
		Query query = em.createQuery(cq);
		News result = (News) query.getSingleResult();
		return result.getAuthor();
	}



	@Override
	@Transactional (readOnly = true)
	public List<Author> findActualAuthors()  {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Author> cq = cb.createQuery(Author.class);
		Root<Author> author = cq.from(Author.class);		
		cq.select(author).where(author.get("expiredDate").isNull());
		TypedQuery<Author> query = em.createQuery(cq);
		List<Author> result = query.getResultList();
		return result;
		
	}

}
