package com.epam.newsmanagement.dao.jpa.impl;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;


@NamedQuery(
		name = "findAllComments",
		query = "SELECT c FROM Comment c"
		)

public class CommentDAOJPAImpl implements CommentDAO {

	public CommentDAOJPAImpl() {
	}
	
	private EntityManager em;

	public void setEm(EntityManager em) {
		this.em = em;
	}
	

	@Override
	@Transactional (readOnly = true)
	public List<Comment> findAll() {
		
		TypedQuery<Comment> tq = em.createNamedQuery("findAllComments", Comment.class);
		List<Comment> list = tq.getResultList();
		return list;
	}

	@Override
	@Transactional
	public Comment findById(long id) {
		Comment comment = em.find(Comment.class, id);
		return comment;
	}

	@Override
	@Transactional
	public long save(Comment entity)  {
		em.persist(entity);
		return entity.getId();
	}

	@Override
	@Transactional
	public void update(Comment entity) {
		em.merge(entity);

	}

	@Override
	@Transactional
	public void delete(long id)  {
		Comment comment = em.find(Comment.class, id);
		if (comment!=null) {
		em.remove(comment); 
		}

	}

	@Override
	@Transactional
	public List<Comment> findByNewsId(long id)  {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<News> cq = cb.createQuery(News.class);
		Root<News> news = cq.from(News.class);
		cq.select(news).where(cb.equal(news.get("id"), id));
		Query query = em.createQuery(cq);
		News result = (News) query.getSingleResult();
		List<Comment> comments = result.getComments();
		comments.size();
		return comments;
	}



}
