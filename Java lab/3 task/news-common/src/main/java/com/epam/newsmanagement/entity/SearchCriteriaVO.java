package com.epam.newsmanagement.entity;

import java.util.List;

/**
 * The Class SearchCriteria contains {@code Author} and {@code Tag} identifiers for
 * search in news.
 *
 * @author Natallia_Zhukava
 */
public class SearchCriteriaVO {


	private long authorId;
	
	
	private List<Long> tagIdList;

	
	public SearchCriteriaVO() {

	}


	public long getAuthorId() {
		return authorId;
	}


	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}


	public List<Long> getTagIdList() {
		return tagIdList;
	}

	
	public void setTagIdList(List<Long> tagIdList) {
		this.tagIdList = tagIdList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (authorId ^ (authorId >>> 32));
		result = prime * result + ((tagIdList == null) ? 0 : tagIdList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof SearchCriteriaVO))
			return false;
		SearchCriteriaVO other = (SearchCriteriaVO) obj;
		if (authorId != other.authorId)
			return false;
		if (tagIdList == null) {
			if (other.tagIdList != null)
				return false;
		} else if (!tagIdList.equals(other.tagIdList))
			return false;
		return true;
	}
	

}