package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.service.SearchService;


/**
 * The Class SearchServiceImpl implements method from SearchService interface.
 */
public class SearchServiceImpl implements SearchService {
	static Logger logger = Logger.getLogger(SearchServiceImpl.class);
	
	private NewsDAO newsDao;
	
	
	public void setNewsDao(NewsDAO newsDao) {
		this.newsDao = newsDao;
	}

	private NewsManager newsManager;
	

	public void setNewsManager(NewsManager newsManager) {
		this.newsManager = newsManager;
	}


	


	public SearchServiceImpl() {
	}

	

	@Override
	@Transactional(rollbackFor={RuntimeException.class, ServiceException.class})
	public List<NewsVO> searchBy(SearchCriteriaVO search) throws ServiceException {
		List<News> list = new ArrayList<News>();
		List<NewsVO> newsList = new ArrayList<NewsVO>();

		try {
		
			list = newsDao.findByCriteria(search);

			for (News news : list) {
				NewsVO cNews = newsManager.viewSingleNews(news.getId());
				newsList.add(cNews);
			}
		
			
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}

		return newsList;
	}





	@Override
	@Transactional
	public long countNews(SearchCriteriaVO search) throws ServiceException {
		long num = 0;
		try {
			num = newsDao.countNews(search);
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		
		return num;
	}





	@Override
	public int calculateLastPageNum(long num, int onPage) {
		int numPages;

		if (num % onPage != 0) {
			numPages = (int) (num / onPage) + 1;
		} else {
			numPages = (int) (num / onPage);
		}
		return numPages;
	}





	@Override
	public List<NewsVO> findNewsOnPage(int p, int onPage, SearchCriteriaVO obj) throws ServiceException {
		long newsNum = countNews(obj);
		int lastPageNum = calculateLastPageNum(newsNum, onPage);

		if ((p > lastPageNum) || (p < 1)) {
			p = 1;
		}
		
		int lastOnPrevPage = (p-1)*onPage;
		int lastOnPage;
		
		if (newsNum < onPage) {
			lastOnPage = (int) newsNum;
		} else {
			lastOnPage = p * onPage;
		}
		if ((p == lastPageNum) && (newsNum % onPage > 0)) {
			lastOnPage = (int) ((p - 1) * onPage + newsNum % onPage);
		}

		List<News> list = null;
		List<NewsVO> listPage = new ArrayList<NewsVO>();
		try {
			list = newsDao.findNewsOnCurrentPage(lastOnPrevPage, lastOnPage, obj);
			for (News news : list) {
				NewsVO cNews = newsManager.viewSingleNews(news.getId());
				listPage.add(cNews);
			}
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}

		return listPage;

	}
	
	

}
