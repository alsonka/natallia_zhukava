package com.epam.newsmanagement.dao.jpa.impl;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.entity.User;

public class UserDAOJPAImpl implements UserDAO {

	public UserDAOJPAImpl() {
		
	}
	
	private EntityManager em;

	public void setEm(EntityManager em) {
		this.em = em;
	}
	

	@Override
	public User findUserByLogin(String login)  {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<User> cq = cb.createQuery(User.class);
		Root<User> user = cq.from(User.class);
		cq.select(user).where(cb.equal(user.get("login"), login));
		Query query = em.createQuery(cq);
		User result = (User) query.getSingleResult();
		System.out.println(result.toString());
		return result;
	}

}
