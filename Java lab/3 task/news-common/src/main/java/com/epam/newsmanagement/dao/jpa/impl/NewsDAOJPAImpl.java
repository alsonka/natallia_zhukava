package com.epam.newsmanagement.dao.jpa.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.entity.Tag;

@Transactional
public class NewsDAOJPAImpl implements NewsDAO {

	public NewsDAOJPAImpl() {

	}
	
	private static final String JPQL_COUNT = "Select count(f.id) from News f";
	private static final String JPQL_SELECT_NEWS = "Select  news from News news ";
	private static final String JPQL_SELECT_NEWS_ID = "Select  news.id from News news ";
	private static final String JPQL_JOIN_COMMENTS = " LEFT JOIN news.comments as com ";
	private static final String JPQL_GROUP_BY = "GROUP BY news.id, news.title, news.shortText, news.fullText, news.creationDate,"
			+ "news.modificationDate, news.version";
	private static final String JPQL_ORDER_BY = " ORDER BY count(com) desc, news.modificationDate desc, news.id desc";
	private static final String JPQL_JOIN_TAGS = "LEFT JOIN news.tags as tags ";
	private static final String JPQL_WHERE_AUTHOR_ID = " WHERE news.author.id=:authorId ";
	private static final String JPQL_AND_AUTHOR_ID = " AND news.author.id=:authorId ";
	private static final String JPQL_DELETE_NEWS = "DELETE FROM News news WHERE news.id in :newsIds";
	
	private EntityManager em;

	public void setEm(EntityManager em) {
		this.em = em;
	}
	
	@SuppressWarnings("unchecked")
	private void addSortingOrder(CriteriaBuilder cb, @SuppressWarnings("rawtypes") CriteriaQuery cq, Root<News> news ) {
		Join<News, Comment> comments = news.join("comments", JoinType.LEFT);
		Expression<Long> count = cb.count(comments.get("id"));
		
		cq.groupBy(news.get("id"), news.get("title"), news.get("shortText"), news.get("fullText"),
				news.get("creationDate"), news.get("modificationDate"), news.get("author"), news.get("version"));
		
		List<Order> order = new ArrayList<Order>();

		order.add(cb.desc(count));
		order.add(cb.desc(news.get("modificationDate")));
		order.add(cb.desc(news.get("id")));

		cq.orderBy(order);
	}

	@Override
	public List<News> findAll() {
		String query = JPQL_SELECT_NEWS + JPQL_JOIN_COMMENTS + JPQL_GROUP_BY + JPQL_ORDER_BY;
		TypedQuery<News> q = em.createQuery(query, News.class);
		List<News> result = q.getResultList();
		return result;
	}

	@Override
	public News findById(long id) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<News> cq = cb.createQuery(News.class);
		Root<News> news = cq.from(News.class);
		cq.select(news).where(cb.equal(news.get("id"), id));
		TypedQuery<News> query = em.createQuery(cq);
		News result = null;
		try {
			result = query.getSingleResult();
		} catch (NoResultException e) {
		}
		return result;
	}

	@Override
	public long save(News entity) {
		News news = em.merge(entity);
		em.flush();
		return news.getId();
	}

	@Override
	public void update(News entity) {
		em.merge(entity);

	}

	@Override
	public void delete(long id) {

		News result = findById(id);

		if (result != null) {
			em.remove(result);
		}

	}

	@Override
	public List<News> findByTag(long tagId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<News> cq = cb.createQuery(News.class);
		Root<News> news = cq.from(News.class);
		Join<News, Tag> tag = news.join("tags");
		cq.select(news).where(tag.get("id").in(tagId));
		TypedQuery<News> query = em.createQuery(cq);
		List<News> result = new ArrayList<News>();
		result = query.getResultList();
		return result;
	}

	@Override
	public List<News> findByAuthor(long authorId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<News> cq = cb.createQuery(News.class);
		Root<News> news = cq.from(News.class);
		Join<News, Author> author = news.join("author");
		cq.select(news).where(cb.equal(author.get("id"), authorId));
		TypedQuery<News> query = em.createQuery(cq);
		List<News> result = new ArrayList<News>();
		result = query.getResultList();
		return result;
	}

	@Override
	public List<News> findByCriteria(SearchCriteriaVO obj) {
		if ((obj == null) || ((obj.getAuthorId() == 0) && (obj.getTagIdList() == null))) {
			return findAll();
		}
		String query = JPQL_SELECT_NEWS + generateCriteriaQuery(obj);
		TypedQuery<News> q = insertCriteriaQueryParams(obj, query); 
		List<News> result = new ArrayList<News>();
		result = q.getResultList();
		return result;
	}

	@Override
	public List<News> findNewsOnCurrentPage(int lastOnPrevPage, int lastOnPage) {
		String query = JPQL_SELECT_NEWS + JPQL_JOIN_COMMENTS + JPQL_GROUP_BY + JPQL_ORDER_BY;
		TypedQuery<News> q = em.createQuery(query, News.class);
		int onPage = lastOnPage - lastOnPrevPage;
		q.setFirstResult(lastOnPrevPage);
		q.setMaxResults(onPage);
		List<News> result = q.getResultList();
		return result;
	}
	

	@Override
	public List<News> findNewsOnCurrentPage(int lastOnPrevPage, int lastOnPage, SearchCriteriaVO obj) {
		if ((obj == null) || ((obj.getAuthorId() == 0) && (obj.getTagIdList() == null))) {
			return findNewsOnCurrentPage(lastOnPrevPage, lastOnPage);
		}
		String query = JPQL_SELECT_NEWS + generateCriteriaQuery(obj);
		TypedQuery<News> q = insertCriteriaQueryParams(obj, query); 
		 q.setFirstResult(lastOnPrevPage);
		 q.setMaxResults(lastOnPage-lastOnPrevPage);
		List<News> result = new ArrayList<News>();
		result = q.getResultList();
		return result;
	}



	@Override
	public long countNews() {
		Query queryTotal = em.createQuery(JPQL_COUNT);
		long result = (long) queryTotal.getSingleResult();
		return result;
	}

	@Override
	public long[] findPreviousAndNextNews(long newsId) {
		String query = JPQL_SELECT_NEWS_ID + JPQL_JOIN_COMMENTS + JPQL_GROUP_BY + JPQL_ORDER_BY;
		TypedQuery<Long> q = em.createQuery(query, Long.class);
		List<Long> result = q.getResultList();

		int id = result.indexOf(new Long(newsId));
		long prev=0, next=0;
		if (id!=0) {
		 prev = result.get(id-1);
		}
		if (id<result.size()) {
		next = result.get(id+1);
		}
		long[] ids = {prev, next};

		return ids;
	}

	@Override
	public long[] findPreviousAndNextNews(long newsId, SearchCriteriaVO obj) {
		if ((obj == null) || ((obj.getAuthorId() == 0) && (obj.getTagIdList() == null))) {
			return findPreviousAndNextNews(newsId);
		}
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<News> news = cq.from(News.class);
		List<Predicate> predicates = getPredicates(obj, news, cb);
		cq.select(news.get("id"));
		cq.where(predicates.toArray(new Predicate[] {}));

		addSortingOrder(cb, cq, news);

		TypedQuery<Long> query = em.createQuery(cq);
		List<Long> result = query.getResultList();
		
		int id = result.indexOf(new Long(newsId));
		long prev=0, next=0;
		if (id!=0) {
		 prev = result.get(id-1);
		}
		if (id<result.size()) {
		next = result.get(id+1);
		}
		long[] ids = {prev, next};

		return ids;
	}

	@Override
	public long countNews(SearchCriteriaVO obj) {
		if ((obj == null) || ((obj.getAuthorId() == 0) && (obj.getTagIdList() == null))) {
			return countNews();
		}
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<News> cq = cb.createQuery(News.class);
		Root<News> news = cq.from(News.class);

		List<Predicate> predicates = getPredicates(obj, news, cb);

		cq.where(predicates.toArray(new Predicate[] {}));
		cq.distinct(true);
		TypedQuery<News> query = em.createQuery(cq);

		long result = 0;
		result = query.getResultList().size();
		return result;
	}

	private List<Predicate> getPredicates(SearchCriteriaVO obj, Root<News> news, CriteriaBuilder cb) {
		List<Predicate> predicates = new ArrayList<>();
		if (obj.getAuthorId() != 0) {
			Join<News, Author> newsAuthors = news.join("author");
			predicates.add(cb.equal(newsAuthors.get("id"), obj.getAuthorId()));
		}
		if (obj.getTagIdList() != null) {
			Join<News, Tag> newsTags = news.join("tags");
			predicates.add(newsTags.get("id").in(obj.getTagIdList()));
		}
		return predicates;
	}

	@Override
	public void delete(long[] newsIds) {
		String query = JPQL_DELETE_NEWS;
		Query q = em.createQuery(query);
		q.setParameter("newsIds", newsIds);
		q.executeUpdate();

	}
	
	private String generateCriteriaQuery(SearchCriteriaVO obj) {
		StringBuilder query = new StringBuilder(JPQL_JOIN_COMMENTS);
			
			if ((obj.getTagIdList()!=null) && (obj.getTagIdList().size()!=0)){
				 query.append(JPQL_JOIN_TAGS).append(" WHERE tags.id in :tags ");
				 if (obj.getAuthorId()!=0) {
					 query.append(JPQL_AND_AUTHOR_ID);
				 }
			 }
			else {
				query.append(JPQL_WHERE_AUTHOR_ID);
			}
			
			
	
		query.append(JPQL_GROUP_BY).append(JPQL_ORDER_BY);
		return query.toString();
	}
	
	private TypedQuery<News> insertCriteriaQueryParams(SearchCriteriaVO obj, String query) {
		 TypedQuery<News> q = em.createQuery(query, News.class);
		 if ((obj.getTagIdList()!=null) && (obj.getTagIdList().size()!=0)){
			 List<Long> tags = obj.getTagIdList();
			 q.setParameter("tags", tags);
		 }
		 
		 if (obj.getAuthorId()!=0) {
				q.setParameter("authorId", obj.getAuthorId());
			}
		 
		 return q;
	}



}
