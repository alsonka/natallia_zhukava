package com.epam.newsmanagement.service.impl;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.UserService;


/**
 * The Class UserServiceImpl implements methods from UserService interface.
 */
@Transactional
public class UserServiceImpl implements UserService {
	
	private UserDAO userDao;
	

	
	public void setUserDao(UserDAO userDao) {
		this.userDao = userDao;
	}
	
	static Logger logger = Logger.getLogger(TagServiceImpl.class);



	@Override
	public String findUserNameByLogin(String login) throws ServiceException  {
		String username = null;
		try {
			username = userDao.findUserByLogin(login).getName();
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		return username;
	}

}
