package com.epam.newsmanagement.dao.jpa.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;


public class TagDAOJPAImpl implements TagDAO {

	public TagDAOJPAImpl() {
	}
	
	private EntityManager em;

	public void setEm(EntityManager em) {
		this.em = em;
	}

	@Override
	@Transactional (readOnly = true)
	public List<Tag> findAll()  {
		String query = "SELECT t FROM Tag t";
		TypedQuery<Tag> tq = em.createQuery(query, Tag.class);
		List<Tag> list = tq.getResultList();
		return list;

	}

	@Override
	@Transactional
	public Tag findById(long id)  {
		Tag tag = em.find(Tag.class, id);
		return tag;
	}

	@Override
	@Transactional
	public long save(Tag entity)  {
		em.persist(entity);
		em.flush();	

		return entity.getId();
	}

	@Override
	@Transactional
	public void update(Tag entity)  {
		em.merge(entity);

	}

	@Override
	@Transactional
	public void delete(long id) {
		Tag tag = em.find(Tag.class, id);
		if (tag!=null) {
		em.remove(tag); 
		}

	}

	@Override
	@Transactional
	public List<Tag> findByNews(long newsId)  {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<News> cq = cb.createQuery(News.class);
		Root<News> news = cq.from(News.class);
		cq.select(news).where(cb.equal(news.get("id"), newsId));
		Query query = em.createQuery(cq);
		News result = (News) query.getSingleResult();
		return result.getTags();
	}

	@Override
	@Transactional
	public Tag findByTagName(String name)  {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Tag> cq = cb.createQuery(Tag.class);
		Root<Tag> tag = cq.from(Tag.class);
		cq.select(tag).where(cb.equal(tag.get("name"), name));
		TypedQuery<Tag> query = em.createQuery(cq);
		Tag result = query.getSingleResult();
		return result;

	}



}
