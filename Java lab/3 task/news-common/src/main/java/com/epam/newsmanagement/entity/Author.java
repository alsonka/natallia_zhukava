package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



/**
 * The class Author contains data from table Author.
 *
 * @author Natallia_Zhukava
 */

@Entity
@Table(name = "author")
public class Author implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "AUTHOR_SEQ", sequenceName = "id_sequence",  allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AUTHOR_SEQ")
	@Column(name = "AUTHOR_ID")
	private long id;
	
	
	
	@OneToMany(fetch=FetchType.LAZY)
	@JoinTable(
			name = "news_author",
			joinColumns = @JoinColumn(name = "author_id"),
			inverseJoinColumns = @JoinColumn(name = "news_id")
	)
	private List<News> news = new ArrayList<News>();
	
	public List<News> getNews() {
		return news;
	}
	

	@Column(name = "AUTHOR_NAME")
	private String name;

	@Column(name = "EXPIRED")
	private Timestamp expiredDate;

	
	public Author() {
	}

	
	public Author(long id, String name, Timestamp expiredDate) {
		this.setId(id);
		this.name = name;
		this.expiredDate = expiredDate;
	}


	public Author(String name, Timestamp expiredDate) {
		this.name = name;
		this.expiredDate = expiredDate;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Timestamp getExpiredDate() {
		return expiredDate;
	}


	public void setExpiredDate(Timestamp expiredDate) {
		this.expiredDate = expiredDate;
	}


	@Override
	public String toString() {
		return "Author [id= " + id + " name=" + name + ", expiredDate=" + expiredDate + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expiredDate == null) ? 0 : expiredDate.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Author))
			return false;
		Author other = (Author) obj;
		if (expiredDate == null) {
			if (other.expiredDate != null)
				return false;
		} else if (!expiredDate.equals(other.expiredDate))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
