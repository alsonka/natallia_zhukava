package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Interface CommentDAO contains the methods which should work with {@code Comments} database table.
 * 
 * @author Natallia_Zhukava
 */
public interface CommentDAO extends CommonDAO<Comment> {

	/**
	 * The method finds the comments attached to given news.
	 *
	 * @param id the id
	 * @return the list of {@code Comment} objects
	 * @throws DAOException the DAO exception
	 */
	List<Comment> findByNewsId(long id);
	
}

 	