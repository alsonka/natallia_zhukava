package com.epam.newsmanagement.dao.hibernate.impl;


import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.entity.Tag;


@Transactional
public class NewsDAOHibernateImpl implements NewsDAO {

	public NewsDAOHibernateImpl() {
		// TODO Auto-generated constructor stub
	}
	
	private static final String HQL_SELECT_NEWS = "Select  news from News news ";
	private static final String HQL_SELECT_NEWS_ID = "Select news.id from News news ";
	private static final String HQL_JOIN_COMMENTS = " LEFT JOIN news.comments as com ";
	private static final String HQL_JOIN_TAGS = "LEFT JOIN news.tags as tags ";
	private static final String HQL_WHERE_AUTHOR_ID = " WHERE news.author.id=:authorId ";
	private static final String HQL_AND_AUTHOR_ID = " AND news.author.id=:authorId ";
	private static final String HQL_GROUP_BY = "GROUP BY news.id, news.title, news.shortText, news.fullText, news.creationDate,"
			+ "news.modificationDate, news.version, news.author";
	private static final String HQL_ORDER_BY = " ORDER BY count(com) desc, news.modificationDate desc, news.id desc";
	private static final String HQL_DELETE_NEWS = "DELETE FROM News news WHERE news.id in :newsIds";
	
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	@Override
	public List<News> findAll() {
		Session session = sessionFactory.getCurrentSession();
		String query = HQL_SELECT_NEWS
				+ HQL_JOIN_COMMENTS
			+ HQL_GROUP_BY + HQL_ORDER_BY;
		
	
	    Query q = session.createQuery(query);
	    @SuppressWarnings("unchecked")
		List<News> list = q.list();
		return list;
	}

	@Override
	public News findById(long id) {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(News.class);
		cr.add(Restrictions.idEq(id));
		News news = (News) cr.uniqueResult();
		return news;
		
	}

	@Override
	public long save(News entity) {
		Session session = sessionFactory.getCurrentSession();
		long id = (long) session.save(entity);
		return id;
	}

	@Override
	public void update(News entity) {
		Session session = sessionFactory.getCurrentSession();
		session.update(entity);

	}

	@Override
	public void delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		News news = findById(id);
		session.delete(news);

	}

	@Override
	public List<News> findByTag(long tagId) {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(Tag.class);
		cr.add(Restrictions.idEq(tagId));
		Tag tag = (Tag) cr.uniqueResult();
		tag.getNews().size();
		List<News> news = tag.getNews();
		return news;
		
	}

	@Override
	public List<News> findByAuthor(long authorId) {
		Session session = sessionFactory.getCurrentSession();
		String query = HQL_SELECT_NEWS  + HQL_WHERE_AUTHOR_ID ;
		Query q = session.createQuery(query);
		q.setParameter("authorId", authorId);
		@SuppressWarnings("unchecked")
		List<News> list = q.list();
		return list;
	}

	private String generateCriteriaQuery(SearchCriteriaVO obj) {
		StringBuilder query = new StringBuilder(HQL_JOIN_COMMENTS);
			
			if ((obj.getTagIdList()!=null) && (obj.getTagIdList().size()!=0)){
				 query.append(HQL_JOIN_TAGS);
				 query.append(HQL_JOIN_TAGS).append(" WHERE tags.id in (:tags) ");
				 if (obj.getAuthorId()!=0) {
					 query.append(HQL_AND_AUTHOR_ID);
				 }
			 }
			else {
				query.append(HQL_WHERE_AUTHOR_ID);
			}
			
			
	
		query.append(HQL_GROUP_BY).append(HQL_ORDER_BY);
		return query.toString();
	}
	
	private Query insertCriteriaQueryParams(SearchCriteriaVO obj, Session session, String query) {
		 Query q = session.createQuery(query);
		 if ((obj.getTagIdList()!=null) && (obj.getTagIdList().size()!=0)){
			 List<Long> tags = obj.getTagIdList();
			 q.setParameterList("tags", tags);
		 }
		 
		 if (obj.getAuthorId()!=0) {
				q.setParameter("authorId", obj.getAuthorId());
			}
		 
		 return q;
	}
	
	@Override
	public List<News> findByCriteria(SearchCriteriaVO obj) {
		Session session = sessionFactory.getCurrentSession();
		
		if ((obj==null) || ((obj.getAuthorId()==0) && (obj.getTagIdList()==null)) ) {
			return findAll();
		} 
		String query = HQL_SELECT_NEWS + generateCriteriaQuery(obj);
		Query q = insertCriteriaQueryParams(obj, session, query);
		 
		 @SuppressWarnings("unchecked")
		 List<News> news = q.list();
		
		return news;
	}

	@Override
	public List<News> findNewsOnCurrentPage(int lastOnPrevPage, int lastOnPage) {
		Session session = sessionFactory.getCurrentSession();
		String query = HQL_SELECT_NEWS
				+ HQL_JOIN_COMMENTS
			+ HQL_GROUP_BY + HQL_ORDER_BY;
		
	
	    Query q = session.createQuery(query);
	    q.setFirstResult(lastOnPrevPage+1);
	    q.setMaxResults(lastOnPage-lastOnPrevPage);
	    @SuppressWarnings("unchecked")
		List<News> list = q.list();
		
		
		return list;
	}

	@Override
	public List<News> findNewsOnCurrentPage(int lastOnPrevPage, int lastOnPage, SearchCriteriaVO obj) {
		Session session = sessionFactory.getCurrentSession();
		
		if ((obj==null) || ((obj.getAuthorId()==0) && (obj.getTagIdList()==null)) ) {
			return findNewsOnCurrentPage(lastOnPrevPage, lastOnPage);
		} 
		String query = HQL_SELECT_NEWS + generateCriteriaQuery(obj);
		Query q = insertCriteriaQueryParams(obj, session, query); 
		 q.setFirstResult(lastOnPrevPage);
		 q.setMaxResults(lastOnPage-lastOnPrevPage);
		 
		 @SuppressWarnings("unchecked")
		List<News> list = q.list();
		 
		 return list;
	}



	@Override
	public long countNews() {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(News.class);
		cr.setProjection(Projections.rowCount());
		long num = (long) cr.uniqueResult();
		return num;
	}

	@Override
	public long[] findPreviousAndNextNews(long newsId) {
		Session session = sessionFactory.getCurrentSession();
		String query = HQL_SELECT_NEWS_ID + HQL_JOIN_COMMENTS
				+ HQL_GROUP_BY + HQL_ORDER_BY;
		Query q = session.createQuery(query);
		@SuppressWarnings("unchecked")
		List<Long> list = q.list();
	
		int cur = list.indexOf(newsId);
		long prev = list.get(cur-1);
		if (cur==0) {prev=0;}
		long next = list.get(cur+1);
		if (next==list.size()-1) {next=0;}
		long[] ids = {prev, next};
		return ids;
	}

	@Override
	public long[] findPreviousAndNextNews(long newsId, SearchCriteriaVO obj) {
		Session session = sessionFactory.getCurrentSession();
		
		if ((obj==null) || ((obj.getAuthorId()==0) && (obj.getTagIdList()==null)) ) {
			return findPreviousAndNextNews(newsId);
		} 
		String query = HQL_SELECT_NEWS_ID + generateCriteriaQuery(obj);
		Query q = insertCriteriaQueryParams(obj, session, query);
		
		@SuppressWarnings("unchecked")
		List<Long> list = q.list();
	
		int cur = list.indexOf(newsId);
		long prev = list.get(cur-1);
		if (cur==0) {prev=0;}
		long next = list.get(cur+1);
		if (cur==list.size()-1) {next=0;}
		long[] ids = {prev, next};
		return ids;
		
	}

	@Override
	public long countNews(SearchCriteriaVO obj) {
		Session session = sessionFactory.getCurrentSession();
		String query = HQL_SELECT_NEWS + generateCriteriaQuery(obj);
		Query q = insertCriteriaQueryParams(obj, session, query);
		long num = (long) q.list().size();
		return num;
	}

	@Override
	public void delete(long[] newsIds) {
		Session session = sessionFactory.getCurrentSession();
		Query q = session.createQuery(HQL_DELETE_NEWS);
		List<Long> list = new ArrayList<Long>(newsIds.length);
	    for (long id : newsIds) {
	        list.add(id);
	    }
		q.setParameterList("newsIds", list);
		q.executeUpdate();

	}

	
}
