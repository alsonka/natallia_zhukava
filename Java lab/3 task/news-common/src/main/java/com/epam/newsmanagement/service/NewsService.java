package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;


/**
 * The Interface NewsService contains the service methods which should work with {@code News} objects.
 * 
 * @author Natallia_Zhukava
 */
public interface NewsService {

	/**
	 * The method saves the given news.
	 *
	 * @param news the news
	 * @return the newsTO with generated identifier
	 * @throws ServiceException the service exception
	 */
	long saveNews(News news) throws ServiceException;
	
	
	/**
	 * The method edits given news.
	 *
	 * @param news the news
	 * @throws ServiceException the service exception
	 */
	void editNews(News news) throws ServiceException;

	/**
	 * The method deletes given news.
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 */
	void deleteNews(long newsId) throws ServiceException;

	/**
	 * The method finds all news and returns list of them .
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	List<News> findAllNews() throws ServiceException;

	/**
	 * The method finds the news by given id.
	 *
	 * @param newsId the news id
	 * @return the news to
	 * @throws ServiceException the service exception
	 */
	
	News findNewsById(long newsId) throws ServiceException;
	
	
	
	/**
	 * Find news on page.
	 *
	 * @param p the current page number
	 * @param onPage number of news on page
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	List<News> findNewsOnPage(int p, int onPage) throws ServiceException;
	
	
	/**
	 * Calculate last page number for all news in database.
	 *
	 * @param num the num
	 * @param onPage the on page
	 * @return the int
	 */
	int calculateLastPageNum(long num, int onPage) throws ServiceException;
	/**
	 * The method finds all news which attached to given author .
	 *
	 * @param authorId the author id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	
	List<News> findNewsByAuthor(long authorId) throws ServiceException;

	/**
	 * The method counts all news.
	 *
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	long countAllNews() throws ServiceException;

	
	
	/**
	 * Find previous and next news by given news id.
	 *
	 * @param newsId the news id
	 * @return the long[] array with ids of previous and next news
	 * @throws ServiceException the service exception
	 */
	long[] findPreviousAndNext(long newsId) throws ServiceException;

	/**
	 * Find previous and next news by given news id in search results.
	 *
	 * @param newsId the news id
	 * @param obj the SearchCriteria obj
	 * @return the long[] array with ids of previous and next news
	 * @throws ServiceException the service exception
	 */
	long[] findPreviousAndNext(long newsId, SearchCriteriaVO obj) throws ServiceException;
	
	/**
	 * Deletes list of news.
	 *
	 * @param newsIds the news ids
	 * @throws ServiceException the service exception
	 */
	void deleteNews(long[] newsIds) throws ServiceException;
	
	


}