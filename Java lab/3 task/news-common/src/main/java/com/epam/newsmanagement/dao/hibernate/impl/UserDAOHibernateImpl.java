package com.epam.newsmanagement.dao.hibernate.impl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.entity.User;

@Transactional
public class UserDAOHibernateImpl implements UserDAO{
	
	public UserDAOHibernateImpl() {
		// TODO Auto-generated constructor stub
	}
	
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	@Override
	public User findUserByLogin(String login) {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(User.class);
		cr.add(Restrictions.eq("login", login));
		User user = (User) cr.uniqueResult();
		return user;
	}

}
