package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;


/**
 * The Interface TagService contains the service methods which should work with {@code Tag} objects.
 */
public interface TagService {

	/**
	 * The method finds all tags.
	 *
	 * @return the list
	 * @throws ServiceException 
	 */
	List<Tag> findAllTags() throws ServiceException;

	/**
	 * The method finds tag by given id.
	 *
	 * @param tagId the id
	 * @return the tagTO
	 * @throws ServiceException 
	 */
	Tag findTagById(long tagId) throws ServiceException;

	/**
	 * The method finds tags by given news.
	 *
	 * @param newsId the news id
	 * @return the list
	 * @throws ServiceException 
	 */
	List<Tag> findTagsByNews(long newsId) throws ServiceException;

	/**
	 * The method saves the tag
	 *
	 * @param tag the tag
	 * @return the tagTO
	 * @throws ServiceException 
	 */
	Tag saveTag(Tag tag) throws ServiceException;

	/**
	 * The method edits the tag.
	 *
	 * @param tag the tag
	 * @throws ServiceException 
	 */
	void editTag(Tag tag) throws ServiceException;

	/**
	 * The method deletes the tag.
	 *
	 * @param id the id
	 * @throws ServiceException 
	 */
	void deleteTag(long id) throws ServiceException;

}