package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Interface TagDAO contains methods which should work with {@code Tags} database table.
 */
public interface TagDAO extends CommonDAO<Tag> {



	/**
	 * The methods finds all tags which attached to given news
	 *
	 * @param newsId the news id
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<Tag> findByNews(long newsId);

	/**
	 * The method finds tag by given tag name.
	 *
	 * @param name the name
	 * @return the tag to
	 * @throws DAOException the DAO exception
	 */
	Tag findByTagName(String name);
	


	

}