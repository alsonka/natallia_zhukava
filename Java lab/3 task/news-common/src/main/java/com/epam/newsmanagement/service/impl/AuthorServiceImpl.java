package com.epam.newsmanagement.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;

/**
 * The Class AuthorServiceImpl implements methods from AuthorService interface.
 * 
 * @author Natallia_Zhukava
 */
@Transactional
public class AuthorServiceImpl implements AuthorService {


	private AuthorDAO authorDao;
	
	

	
	public void setAuthorDao(AuthorDAO authorDao) {
		this.authorDao = authorDao;
	}



	public AuthorServiceImpl() {
	}

	
	@Override
	public List<Author> findAllAuthors() throws ServiceException  {
		List<Author> list = new ArrayList<Author>();
		try {
			list = authorDao.findAll();
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		return list;
	}

	

	@Override
	public Author findAuthorById(long authorId) throws ServiceException {
		Author author = null;
		try {

			author = authorDao.findById(authorId);

		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		return author;
	}

	

	@Override
	public Author findAuthorByNews(long newsId) throws ServiceException  {
		Author author = null;
		try {

			author = authorDao.findByNews(newsId);

		} catch (HibernateException e) {
				throw new ServiceException(e);
		}
		return author;
	}

	
	
	@Override
	public Author saveAuthor(Author author) throws ServiceException  {

		try {

		long id = authorDao.save(author);
		author.setId(id);	

		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		return author;
	}


	@Override
	public void editAuthor(Author author) throws ServiceException  {
		try {
			authorDao.update(author);	
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		
	}

	

	@Transactional
	@Override
	public Author makeExpired(long authorId) throws ServiceException  {
		Calendar calendar = Calendar.getInstance();
		Date now = calendar.getTime();
		Timestamp currentTimestamp = new Timestamp(now.getTime());
		Author author = null;
		try {
			author = authorDao.findById(authorId);
			author.setExpiredDate(currentTimestamp);
			authorDao.update(author);
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		
		return author;
	}

	/**
	 * The method deletes author
	 * Not planned to use!
	 * @throws ServiceException 
	 * @see com.epam.newsmanagement.service.AuthorService#deleteAuthor(long)
	 */
	@Override
	@Transactional
	public void deleteAuthor(long authorId) throws ServiceException  {

		try {
		authorDao.delete(authorId);
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
	}



	@Override
	public List<Author> findActualAuthors() throws ServiceException {
		List<Author> list = new ArrayList<Author>();
		
		try {
			list = authorDao.findActualAuthors();
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		
		return list;
		
	}

	

}
