package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;


/**
 * The Class NewsServiceImpl implements method from NewsService interface.
 * 
 * @author Natallia_Zhukava
 */
@Transactional 
public class NewsServiceImpl implements NewsService {
	
	static Logger logger = Logger.getLogger(AuthorServiceImpl.class);
	private NewsDAO newsDao;
	

	public void setNewsDao(NewsDAO newsDao) {
		this.newsDao = newsDao;
	}

	
	public NewsServiceImpl() {
		
	}


	@Override
	public long saveNews(News news) throws ServiceException {
		long id=0;
		try {
			id = newsDao.save(news);
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		return id;
	}


	@Override
	public void editNews(News news) throws ServiceException {
		try {
			newsDao.update(news);
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		
	}
	

	@Override
	public List<News> findAllNews() throws ServiceException {
		List<News> list = null;
		try {
			 list = newsDao.findAll();
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		return list;
	}


	@Override
	public News findNewsById(long newsId) throws ServiceException  {
		News news = null;
		try {
			news = newsDao.findById(newsId);
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		return news;
	}
	


	@Override
	public long countAllNews() throws ServiceException  {
		long num = 0;
		try {
			num = newsDao.countNews();
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		return num;
	}





	


	@Override
	public List<News> findNewsByAuthor(long authorId) throws ServiceException  {
		List<News> list = null;
		try {
			 list = newsDao.findByAuthor(authorId);
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		return list;
	}


	@Override
	public void deleteNews(long newsId) throws ServiceException {
		try {
			newsDao.delete(newsId);
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		
	}





	@Override
	public List<News> findNewsOnPage(int p, int onPage) throws ServiceException {
		long newsNum = countAllNews();
		int lastPageNum = calculateLastPageNum(newsNum, onPage);

		if ((p > lastPageNum) || (p < 1)) {
			p = 1;
		}
		
		int firstOnPage = (p-1)*onPage;
		int lastOnPage;
		
		if (newsNum < onPage) {
			lastOnPage =  (int) newsNum;
		} else {
			lastOnPage = p * onPage;
		}
		if ((p == lastPageNum) && (newsNum % onPage > 0)) {
			lastOnPage = (int) ((p - 1) * onPage + newsNum % onPage);
		}

		List<News> listPage = null;
		try {
			listPage = newsDao.findNewsOnCurrentPage(firstOnPage, lastOnPage);
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}

		return listPage;


	}
	
	

	@Override
	public int calculateLastPageNum(long num, int onPage) {
		int numPages;

		if (num % onPage != 0) {
			numPages = (int) (num / onPage) + 1;
		} else {
			numPages = (int) (num / onPage);
		}
		return numPages;
	}

	

	@Override
	public long[] findPreviousAndNext(long newsId) throws ServiceException  {
		long[] idList={};
		try {
			idList = newsDao.findPreviousAndNextNews(newsId);
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		return idList;
	}

	
	@Override
	public long[] findPreviousAndNext(long id, SearchCriteriaVO obj) throws ServiceException  {
		long[] idList={};
		try {
			idList = newsDao.findPreviousAndNextNews(id, obj);
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		return idList;
	}




	
	@Override
	public void deleteNews(long[] newsIds) throws ServiceException  {
		try {
			newsDao.delete(newsIds);
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		
	}


	

}
