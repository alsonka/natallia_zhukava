package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The class Tag contains data from table Tag
 * 
 * @author Natallia_Zhukava
 */
@Entity
@Table(name="tag")
public class Tag implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name = "TAG_SEQ", sequenceName = "id_sequence",  allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAG_SEQ")
	@Column(name="TAG_ID")
	private long id;
	@Column(name="TAG_NAME")
	private String name;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.MERGE, mappedBy="tags")
	private List<News> news = new ArrayList<News>();
	
	public List<News> getNews() {
		return news;
	}
	

	public Tag() {

	}


	public Tag(long id, String name) {
		this.setId(id);
		this.name = name;
	}

	public Tag(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Tag))
			return false;
		Tag other = (Tag) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Tag [Id()=" + id + "; name=" + name + "]";
	}

}
