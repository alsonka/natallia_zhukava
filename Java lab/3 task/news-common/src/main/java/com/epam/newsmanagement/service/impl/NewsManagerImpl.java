package com.epam.newsmanagement.service.impl;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.service.NewsService;

/**
 * The Class NewsManagerImpl implements methods from NewsManager interface.
 * 
 * @author Natallia_Zhukava
 */
public class NewsManagerImpl implements NewsManager {

	private NewsService newsService;
	private AuthorService authorService;	

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}


	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}
	

	static Logger logger = Logger.getLogger(NewsManagerImpl.class);


	
	public NewsManagerImpl() {

	}


	@Override
	@Transactional(rollbackFor={RuntimeException.class, ServiceException.class})
	public long saveNews(NewsVO complexNews) throws ServiceException  {
		News news = complexNews.getNews();
		Author author = complexNews.getAuthor();
		List<Tag> list = complexNews.getTagList();
		
		Author fullAuthor = authorService.findAuthorById(author.getId());
		news.setAuthor(fullAuthor);
		news.setTags(list);

		long newsId = newsService.saveNews(news);

		return newsId;

	}


	@Override
	@Transactional(rollbackFor={RuntimeException.class, ServiceException.class})
	public void editNews(NewsVO complexNews) throws ServiceException  {
		News news = complexNews.getNews();
		Author author = complexNews.getAuthor();
		List<Tag> list = complexNews.getTagList();
		news.setAuthor(author);
		news.setTags(list);
		
		newsService.editNews(news);
	}



	@Override
	@Transactional(rollbackFor={RuntimeException.class, ServiceException.class})
	public void deleteNews(long newsId) throws ServiceException  {
		newsService.deleteNews(newsId);

	}




	@Override
	@Transactional(rollbackFor={RuntimeException.class, ServiceException.class})
	public List<NewsVO> viewNewsList() throws ServiceException{
		List<NewsVO> newsList = new ArrayList<NewsVO>();
					
			List<News> list = newsService.findAllNews();
			for (News news : list) {
				NewsVO cNews = viewSingleNews(news.getId());
				newsList.add(cNews);
			}
			
		return newsList;

	}
	
	



	@Override
	@Transactional(rollbackFor={RuntimeException.class, ServiceException.class})
	public NewsVO viewSingleNews(long newsId) throws ServiceException  {
		NewsVO complexNews = new NewsVO();

		News news = null;
		Author author = null;
		List<Tag> tagList = new ArrayList<Tag>();
		List<Comment> commentList = new ArrayList<Comment>();

			
			news = newsService.findNewsById(newsId);
			author = news.getAuthor();
			tagList = news.getTags();
			commentList = news.getComments();
			commentList.size();

			complexNews.setNews(news);
			complexNews.setAuthor(author);
			complexNews.setTagList(tagList);
			complexNews.setCommentList(commentList);
			
		
		return complexNews;
	}


	@Override
	@Transactional(rollbackFor={RuntimeException.class, ServiceException.class})
	public List<NewsVO> viewNewsListOnPage(int p, int onPage) throws ServiceException  {
		List<NewsVO> newsList = new ArrayList<NewsVO>();
		
		List<News> list = newsService.findNewsOnPage(p, onPage);
		for (News news : list) {
			NewsVO cNews = viewSingleNews(news.getId());
			newsList.add(cNews);
		}
		return newsList;
		
	}

	@Override
	@Transactional(rollbackFor={RuntimeException.class, ServiceException.class})
	public void deleteNews(long[] ids) throws ServiceException  {
		newsService.deleteNews(ids);

		
	}
	

}
