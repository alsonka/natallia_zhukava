package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;


/**
 * The Class TagServiceImpl implements methods from TagService interface.
 */
@Transactional
public class TagServiceImpl implements TagService {

	static Logger logger = Logger.getLogger(TagServiceImpl.class);

	private TagDAO tagDao;
	

	
	public void setTagDao(TagDAO tagDao) {
		this.tagDao = tagDao;
	}
	

	public TagServiceImpl() {

	}


	@Override
	public List<Tag> findAllTags() throws ServiceException  {
		List<Tag> list = new ArrayList<Tag>();
		try {

			list = tagDao.findAll();

		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		return list;
	}


	@Override
	public Tag findTagById(long id) throws ServiceException  {
		Tag tag = null;
		try {
			tag = tagDao.findById(id);
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}
		return tag;
	}


	@Override
	public List<Tag> findTagsByNews(long newsId) throws ServiceException  {
		List<Tag> list = new ArrayList<Tag>();
		try {

			list = tagDao.findByNews(newsId);

		} catch (HibernateException e) {
			throw new ServiceException(e);
		}

		return list;
	}


	@Override
	public Tag saveTag(Tag tag) throws ServiceException  {
		try {
		long id = tagDao.save(tag);
		tag.setId(id);
		} catch (HibernateException e) {
			throw new ServiceException(e);
		}

		return tag;
	}


	@Override
	public void editTag(Tag tag) throws ServiceException {

		try {

			tagDao.update(tag);

		} catch (HibernateException e) {
			throw new ServiceException(e);
		}

	}


	@Override
	@Transactional(rollbackFor={RuntimeException.class, ServiceException.class})
	public void deleteTag(long id) throws ServiceException  {

		try {
			tagDao.delete(id);
		

		} catch (HibernateException e) {
			throw new ServiceException(e);
		}

	}

}
