<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>




<tiles:importAttribute name="stylesheets" />
<tiles:importAttribute name="javascripts" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<c:forEach var="css" items="${stylesheets}">
 <link rel="stylesheet" type="text/css" href="<c:url value="${css}"/>">
</c:forEach>
<c:forEach var="script" items="${javascripts}">
<script src="<c:url value="${script}"/>"></script>
</c:forEach>
<title>List of news</title>

</head>
<body>
	<div class="wrapper">
			<tiles:insertAttribute name="header" />
		<div class="content-wrapper">
			<div class="content-w-menu">
			<tiles:insertAttribute name="content" />
		</div>
		</div>
		<tiles:insertAttribute name="footer" />
</div>
	
</body>
</html>