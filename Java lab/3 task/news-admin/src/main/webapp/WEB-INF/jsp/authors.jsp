<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="authors">

<c:forEach var="author" items="${authorList}">
<c:choose>
<c:when test="${author.id==editAuthor.id }">
<div class="author">
<form:form action="updauthor" modelAttribute="editAuthor" method="POST" id="editAuthor" class="formwithtext">
<form:errors path="name" cssStyle="color:red;"/><br>
<label><spring:message code="authors.author" />: </label> 
<form:input type="hidden" path="id" />
<form:input type="text"  path="name" id="editAuthorName" /> 
<input type="submit" value="<spring:message code='authors.update' />" />
</form:form>
<form action="expire" method="post" class="formbutton">
<input type="submit" value="<spring:message code='authors.expire' />" />
<input type="hidden" name="id" value="${author.id }">
<a href="<c:url value='/admin/authors' /> "><spring:message code="authors.cancel" /></a><br>
</form>
<span class="error" id="editError"></span>

</div>
</c:when>
<c:otherwise>
<div class="author">
<form action="authors" method="post" class="blocked">
<input type="hidden" name="id" value="${author.id }">
<label><spring:message code="authors.author" />: </label> 
<input type="text" value="${author.name}" name="${author.id}" disabled> 
<input type="submit" value="<spring:message code='authors.edit' />" />
</form>
</div>
</c:otherwise>
</c:choose>

</c:forEach>


</div>
<div class="saveauthor">
<form:form modelAttribute="newAuthor" id="newAuthor" action="saveauthor" method="POST"> 
<label><spring:message code="authors.addauthor" />: </label> 
<form:input path="name" id="saveAuthorName"  />
<input type="submit" value="<spring:message code="authors.save" />"><br>
<form:errors path="name" cssStyle="color:red;"/>
<span class="error" id="saveError"></span>
<br>


</form:form>

</div>