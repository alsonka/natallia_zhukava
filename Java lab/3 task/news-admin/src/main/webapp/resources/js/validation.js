var authorName = "[A-Z\u0410-\u042F\u0401][a-z\u0430-\u044F\u0451]{1,29}[\\s][A-Z\u0410-\u042F\u0401][a-z\u0430-\u044F\u0451]{1,29}";
var tagName = "[A-Z\u0410-\u042F\u0401(\\d][A-Za-z\u0410-\u042F\u0401\u0430-\u044F\u0451\\d\\s&.!\\-]{1,29}";
	
$(function() {     
    $('#newsForm').submit(function(){
		 var title = $('#title').val();
		 var shortText = $('#shortText').val();
		 var fullText = $('#fullText').val();
		 if (title.length===0) {
			 $('#titleError').html(titleEmpty);
		     return false; }
		if (title.length>30) {
			 $('#titleError').html(titleTooLong);
		     return false; }
		 if (shortText.length===0) {
			 $('#shortError').html(shortEmpty);
		     return false;}
		 if (fullText==="") {
			 $('#fullError').html(fullEmpty);
		     return false;}
		 
	});
          
          
          $('#editAuthor').submit(function() {
        	  var author = $('#editAuthorName').val();
        	  if (author.length===0) {
        		  $('#editError').html(authorEmpty);
        		  return false;
        	  }
        	  if (author.length>30) {
        		  $('#editError').html(authorTooLong);
        		  return false;
        	  }
        	  if (!author.match(authorName)) {
        		  $('#editError').html(authorFormat);
        		  return false;
        	  }
          });
          
          $('#newAuthor').submit(function() {
        	  var name = $('#saveAuthorName').val();
        	  if (name.length===0) {
        		  $('#saveError').html(authorEmpty);
        		  return false;
        	  }
        	  if (name.length>30) {
        		  $('#saveError').html(authorTooLong);
        		  return false;
        	  }
        	  if (!name.match(authorName)) {
        		  $('#saveError').html(authorFormat);
        		  return false;
        	  }
          });
          
          $('#editTag').submit(function() {
        	  var name = $('#editTagName').val();
        	  if (name.length===0) {
        		  $('#editError').html(tagEmpty);
        		  return false;
        	  }
        	  if (name.length>30) {
        		  $('#editError').html(tagTooLong);
        		  return false;
        	  }
        	  if (!name.match(tagName)) {
        		  $('#editError').html(tagFormat);
        		  return false;
        	  }
          });
          
          $('#newTag').submit(function() {
        	  var name = $('#saveTagName').val();
        	  if (name.length===0) {
        		  $('#saveError').html(tagEmpty);
        		  return false;
        	  }
        	  if (name.length>30) {
        		  $('#saveError').html(tagTooLong);
        		  return false;
        	  }
        	  if (!name.match(tagName)) {
        		  $('#saveError').html(tagFormat);
        		  return false;
        	  }
          });
      });