$(function(){
	  var tz = jstz.determine();
	  var currentCookie = Cookies.get("timezone");
	  var timezone = tz.name();
	  $("#timezone").html(timezone);
	  $("#cookie").html(currentCookie);
	  Cookies.set("timezone", timezone, { path:'/'});	  
	 if (currentCookie!==timezone) {
		  location.reload();
		  }
});

