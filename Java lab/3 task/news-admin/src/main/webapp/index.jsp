<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<body>
<c:if test="${user!=null }">
<jsp:forward page="/admin" />
</c:if>
 <jsp:forward page="/login"></jsp:forward>
</body>
</html>