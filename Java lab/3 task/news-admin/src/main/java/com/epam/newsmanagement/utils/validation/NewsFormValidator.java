package com.epam.newsmanagement.utils.validation;


import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.epam.newsmanagement.entity.NewsVO;


/**
 * The Class NewsFormValidator is designed to validate form "add or edit news".
 * 
 * @author Natallia_Zhukava
 */
@Component
public class NewsFormValidator implements Validator{
	
	private static final String FIELD_NEWS_TITLE = "news.title";
	private static final String FIELD_NEWS_SHORT_TEXT = "news.shortText";
	private static final String FIELD_NEWS_FULL_TEXT = "news.fullText";
	private static final String MESSAGE_TITLE_EMPTY = "validation.title.empty";
	private static final String MESSAGE_TITLE_TOO_LONG = "validation.title.tooLong";
	private static final String MESSAGE_SHORT_TEXT_EMPTY = "validation.shortText.empty";
	private static final String MESSAGE_SHORT_TEXT_TOO_LONG = "validation.shortText.tooLong";
	private static final String MESSAGE_FULL_TEXT_EMPTY = "validation.fullText.empty";
	private static final String MESSAGE_FULL_TEXT_TOO_LONG = "validation.fullText.tooLong";

	
	/**
	 * 
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	public boolean supports(Class<?> clazz) {
		return NewsVO.class.isAssignableFrom(clazz);
	}

	/**
	 * The method validates fields of NewsVO object and its parts
	 * 
	 * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
	 */
	public void validate(Object target, Errors errors) {
		NewsVO newsForm = (NewsVO) target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, FIELD_NEWS_TITLE, MESSAGE_TITLE_EMPTY);
		String title = newsForm.getNews().getTitle();
		if ((title.length()) > 30) {
			errors.rejectValue(FIELD_NEWS_TITLE, MESSAGE_TITLE_TOO_LONG);
		}
		

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, FIELD_NEWS_SHORT_TEXT, MESSAGE_SHORT_TEXT_EMPTY);
		String shortText = newsForm.getNews().getShortText();
		if ((shortText.length()) > 100) {
			errors.rejectValue(FIELD_NEWS_SHORT_TEXT, MESSAGE_SHORT_TEXT_TOO_LONG);
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, FIELD_NEWS_FULL_TEXT, MESSAGE_FULL_TEXT_EMPTY);
		String fullText = newsForm.getNews().getFullText();
		if ((fullText.length()) > 2000) {
			errors.rejectValue(FIELD_NEWS_FULL_TEXT, MESSAGE_FULL_TEXT_TOO_LONG);
		}
		
		
		
	}
}