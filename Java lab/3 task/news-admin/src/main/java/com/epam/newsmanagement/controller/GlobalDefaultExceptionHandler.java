package com.epam.newsmanagement.controller;

import javax.persistence.OptimisticLockException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.orm.hibernate5.HibernateOptimisticLockingFailureException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.NotFoundException;

/**
 * The Class GlobalDefaultExceptionHandler handles all exceptions which were
 * thrown by all controllers.
 */
@ControllerAdvice
public class GlobalDefaultExceptionHandler {
	private static final String MESSAGE_LOCK_ERROR = "error.lockerror";
	private static final String LOCK_ERROR_ATTR = "lockError";
	private static final String REDIRECT_EDITNEWS = "redirect:/admin/editnews/";
	private static final String DEFAULT_ERROR_VIEW = "error";
	private static final String NOT_FOUND_VIEW = "404";

	// private static final String ATTR_EXCEPTION = "exception";
	static Logger logger = Logger.getLogger(GlobalDefaultExceptionHandler.class);

	/**
	 * Default error handler.
	 *
	 * @param req
	 *            the request
	 * @param e
	 *            the exception
	 * @return the model and view
	 * @throws Exception
	 *             the exception
	 */
	@ExceptionHandler(value = Exception.class)
	public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {

		if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null) {
			throw e;
		}
		
		logger.error(e);
		ModelAndView mav = new ModelAndView();
		mav.setViewName(DEFAULT_ERROR_VIEW);
		return mav;
	}

	/**
	 * Handle NotFoundException
	 *
	 * @return the string
	 */
	@ExceptionHandler(NotFoundException.class)
	public String handleNotFoundException() {
		return NOT_FOUND_VIEW;
	}

	@ExceptionHandler(OptimisticLockException.class)
	public ModelAndView handleOptimisticLockException(OptimisticLockException e, HttpServletRequest request) {
		News news = (News) e.getEntity();
		ModelAndView model = new ModelAndView(REDIRECT_EDITNEWS + news.getId());
		FlashMap map = RequestContextUtils.getOutputFlashMap(request);
		map.put(LOCK_ERROR_ATTR, MESSAGE_LOCK_ERROR);
		return model;
	}
	@ExceptionHandler(HibernateOptimisticLockingFailureException.class)
	public ModelAndView handleOptimisticLockHibernateException(HibernateOptimisticLockingFailureException e, HttpServletRequest request) {
		long newsId = (long) e.getIdentifier();
		ModelAndView model = new ModelAndView(REDIRECT_EDITNEWS + newsId);
		FlashMap map = RequestContextUtils.getOutputFlashMap(request);
		map.put(LOCK_ERROR_ATTR, MESSAGE_LOCK_ERROR);
		return model;
	}
	

}