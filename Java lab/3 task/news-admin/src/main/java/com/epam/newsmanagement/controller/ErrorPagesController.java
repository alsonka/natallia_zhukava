package com.epam.newsmanagement.controller;



import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * The Class ErrorPagesController is designed to redirect errors with code of responce status 
 * to required error page.
 * 
 * @author Natallia_Zhukava
 */
@Controller
public class ErrorPagesController {
	
	private static final String VIEW_404 = "404";

	/**
	 * Shows the 404 page
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/404", method = RequestMethod.GET)
	public String show404(ModelMap model)  {			
		return VIEW_404;

		}
	

}
