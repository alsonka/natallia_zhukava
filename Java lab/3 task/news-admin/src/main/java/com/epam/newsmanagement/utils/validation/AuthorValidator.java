package com.epam.newsmanagement.utils.validation;


import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.entity.Author;

/**
 * The Class AuthorValidator is designed to validate author form.
 */
@Component
public class AuthorValidator implements Validator{
	private static final String REGEX_AUTHOR_FORMAT = "[A-Z\u0410-\u042F\u0401][a-z\u0430-\u044F\u0451]{1,29}[\\s][A-Z\u0410-\u042F\u0401][a-z\u0430-\u044F\u0451]{1,29}";
	private static final String FIELD_NAME = "name";
	
	private static final String MESSAGE_NAME_EMPTY = "validation.author.name.empty";
	private static final String MESSAGE_NAME_TOO_LONG = "validation.author.name.tooLong";
	private static final String MESSAGE_NAME_FORMAT = "validation.author.name.format";
	/**
	 * 
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	public boolean supports(Class<?> clazz) {
		return Author.class.isAssignableFrom(clazz);
	}

	/**
	 * The method validates fields of Author object
	 * 
	 * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
	 */
	public void validate(Object target, Errors errors) {
		Author authorForm = (Author) target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, FIELD_NAME, MESSAGE_NAME_EMPTY);
		String name = authorForm.getName();
		if ((name.length()) > 30) {
			errors.rejectValue(FIELD_NAME, MESSAGE_NAME_TOO_LONG);
		}
		
		if (!name.matches(REGEX_AUTHOR_FORMAT)) {
			errors.rejectValue(FIELD_NAME, MESSAGE_NAME_FORMAT);
		}
		
		
	}
}