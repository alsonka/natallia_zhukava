package com.epam.newsmanagement.controller;


import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.utils.validation.TagValidator;

import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;


/**
 * The Class TagsController handles requests about tags.
 * 
 * @author Natallia_Zhukava
 */
@Controller
@RequestMapping(value = "/admin")
public class TagsController{
	
	private static final String TAGS_VIEW = "tags";
	private static final String TAGS_REDIRECT = "redirect:/admin/tags";
	private final static String PARAM_TAG_ID = "id";
	private final static String ATTR_CURRENT = "current";
	private final static String ATTR_TAGS_LIST = "tagsList";
	private final static String ATTR_NEW_TAG = "newTag";
	private final static String ATTR_EDITED_TAG = "editTag";
	
	@Autowired
	private TagService tagService;
	
	@Autowired
	private TagValidator tagValidator;
	
	/**
	 * Views all tags.
	 *
	 * @param model the model
	 * @param session the session
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@RequestMapping(value = "/tags", method = RequestMethod.GET)
	public String viewTags(ModelMap model, HttpSession session) throws ServiceException {
		session.setAttribute(ATTR_CURRENT, TAGS_VIEW);
		List<Tag> tagsList = tagService.findAllTags();
		Tag tag = new Tag();
				
		model.addAttribute(ATTR_TAGS_LIST, tagsList);
		model.addAttribute(ATTR_NEW_TAG, tag);
		
		return TAGS_VIEW;

	}
	
	/**
	 * Edits the tag.
	 *
	 * @param model the model
	 * @param tagId the tag id
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@RequestMapping(value = "/tags", method = RequestMethod.POST)
	public String editTag(ModelMap model,
			@RequestParam(value=PARAM_TAG_ID) long tagId
			) throws ServiceException {
		List<Tag> tagsList = tagService.findAllTags();;
		Tag tag = tagService.findTagById(tagId);
		Tag newTag = new Tag();

		
		model.addAttribute(ATTR_TAGS_LIST, tagsList);
		model.addAttribute(ATTR_EDITED_TAG, tag);
		model.addAttribute(ATTR_NEW_TAG, newTag);
		
		return TAGS_VIEW;

	}
	
	/**
	 * Deletes tag.
	 *
	 * @param model the model
	 * @param tagId the tag id
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@RequestMapping(value = "/del", method = RequestMethod.POST)
	public String deleteTag(ModelMap model,
			@RequestParam(value=PARAM_TAG_ID) long tagId
			) throws ServiceException {
		
		
			tagService.deleteTag(tagId);
		
		return TAGS_REDIRECT;

	}
	
	/**
	 * Saves tag.
	 *
	 * @param model the model
	 * @param tag the tag
	 * @param result the result
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@RequestMapping(value = "/savetag", method = RequestMethod.POST)
	public String saveTag(ModelMap model,
			@ModelAttribute(ATTR_NEW_TAG) Tag tag,
			BindingResult result
			) throws ServiceException {
		
		tagValidator.validate(tag, result);
		if (result.hasErrors()) {
			List<Tag> tagList = tagService.findAllTags();
			
		
			model.addAttribute(ATTR_TAGS_LIST, tagList);
			model.addAttribute(ATTR_NEW_TAG, tag);
			return TAGS_VIEW;
		}
		
		tagService.saveTag(tag);
		
		return TAGS_REDIRECT;

	}
	
	/**
	 * Updates tag.
	 *
	 * @param model the model
	 * @param tag the tag
	 * @param result the result
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@RequestMapping(value = "/updtag", method = RequestMethod.POST)
	public String updateTag(ModelMap model,
			@ModelAttribute(ATTR_EDITED_TAG) Tag tag,
			BindingResult result
			) throws ServiceException {
		
		tagValidator.validate(tag, result);
		if (result.hasErrors()) {
			List<Tag> tagList = tagService.findAllTags();
			Tag newTag = new Tag();
		
			model.addAttribute(ATTR_TAGS_LIST, tagList);
			model.addAttribute(ATTR_EDITED_TAG, tag);
			model.addAttribute(ATTR_NEW_TAG, newTag);
			return TAGS_VIEW;
		}
		
			tagService.editTag(tag);
		
		
		return TAGS_REDIRECT;

	}
	
	/**
	 * if language was switched while tag form was edited - redirect to Tags page
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/savetag", method = RequestMethod.GET)
	public String saveTagLangSwitch(ModelMap model) {
		
		return TAGS_REDIRECT;

	}
	
	/**
	 * if language was switched while tag form was edited - redirect to Tags page
	 *
	 * @param model the model
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@RequestMapping(value = "/updtag", method = RequestMethod.GET)
	public String updateTagLangSwitch(ModelMap model) throws ServiceException {
		
		return TAGS_REDIRECT;

	}
	
}