package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.UserService;

import java.security.Principal;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.ModelMap;


/**
 * The Class LoginController handles login logic.
 */
@Controller
public class LoginController{
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private NewsController newsController;
	
	private static final String LOGIN_VIEW = "login";
	private static final String ATTR_USERNAME = "user";
	private static final String PARAM_ERROR = "error";
	private static final String PARAM_ERROR_VALUE_TRUE = "true";
	private static final String ATTR_ERROR_MESSAGE = "loginerror";
	private static final String MESSAGE_ERROR = "error.login";
	private static final String PARAM_PAGE_NUM = "1";
 
	/**
	 * Sets the user service.
	 *
	 * @param userService the new user service
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * To login page
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(ModelMap model) {
		
		return LOGIN_VIEW;

	}
	
	/**
	 * If there incorrect login or password
	 *
	 * @param model the model
	 * @param error the error
	 * @return the string
	 */
	@RequestMapping(value = "/login", params={PARAM_ERROR}, method = RequestMethod.GET)
	public String loginError(ModelMap model, @RequestParam(PARAM_ERROR) String error) {
		
		if ((error!=null) && (error.equals(PARAM_ERROR_VALUE_TRUE))) {
			model.addAttribute(ATTR_ERROR_MESSAGE, MESSAGE_ERROR);
		}

		return LOGIN_VIEW;
	}
	
	
	
	/**
	 * After login.
	 * The method gets name of user from database and sets it to session
	 *
	 * @param model the model
	 * @param principal the principal
	 * @param session the session
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String afterLogin(ModelMap model, Principal principal, HttpSession session) throws ServiceException {
		if (principal!=null) {
		 String login = principal.getName();
		 String name=null;
		 name = userService.findUserNameByLogin(login);
			
	      session.setAttribute(ATTR_USERNAME, name); }
	      
		return newsController.showNews(model, PARAM_PAGE_NUM, session);
	}
	
}