package com.epam.newsmanagement.utiltest;

import static org.junit.Assert.*;
import org.junit.Test;

import com.epam.newsmanagement.utils.ParseFieldsUtils;

public class ParseFieldsUtilsTest {
	
	@Test
	public void testParseId() {
		String forParse = "Tag [Id()=432; name=Tag 3]";
		

		String expected = "432";
		String actual = ParseFieldsUtils.parseId(forParse);

		assertEquals(expected, actual);

	}
	
	@Test
	public void testParseName() {
		String forParse = "Tag [Id()=3; name=Tag 3]";
		

		String expected = "Tag 3";
		String actual = ParseFieldsUtils.parseName(forParse);

		assertEquals(expected, actual);

	}

}


