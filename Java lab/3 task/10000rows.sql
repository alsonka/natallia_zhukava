BEGIN
 FOR i in 1 .. 10000 LOOP
	INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE, NEWS_VERSION) 
	VALUES (
	id_sequence.nextval, 
	concat('News title', i), 
	concat('Short', i), 
	concat('Full text', i), 
	TO_TIMESTAMP('2015-08-25 07:52:52.619000000', 'YYYY-MM-DD HH24:MI:SS.FF'), 
	TO_DATE('2015-08-25 07:53:01', 'YYYY-MM-DD HH24:MI:SS'),
	'1');

	INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) 
	VALUES (
	id_sequence.currval, 
	(SELECT AUTHOR_ID FROM ( SELECT AUTHOR_ID FROM AUTHOR ORDER BY dbms_random.value ) WHERE rownum = 1)
	); 
  
  FOR j in 1..dbms_random.value(1,3) LOOP 
  INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_TAG.NEWS_ID, NEWS_TAG.TAG_ID) 
    VALUES (
    id_sequence.currval,
    (SELECT TAG_ID FROM ( SELECT TAG_ID FROM TAG  ORDER BY dbms_random.value ) WHERE rownum = 1)
    );
   END LOOP; 
 
 FOR b in 1..dbms_random.value(1,5) LOOP
 INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) 
 VALUES (
 comment_sequence.nextval, 
 id_sequence.currval, 
 concat ('Comment', b),
 TO_TIMESTAMP('2015-08-25 08:47:37.429000000', 'YYYY-MM-DD HH24:MI:SS.FF')
 );
 END LOOP;
 
  END LOOP;
END;