package com.epam.newsmanagement.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class PaginationTag extends TagSupport {

	private static final long serialVersionUID = 1L;
	private String current;
	private String numOfPages;
	private static final int NUMS_ON_PAGE = 10;

	public PaginationTag() {
		// TODO Auto-generated constructor stub
	}

	public void setCurrent(String current) {
		this.current = current;
	}

	public void setNumOfPages(String numOfPages) {
		this.numOfPages = numOfPages;
	}

	@Override
	public int doStartTag() throws JspException {
		try {
			int cur;
			int lastPage = Integer.parseInt(numOfPages);
			StringBuilder sb = new StringBuilder("<div class='pages'>");
			
			
			
			if (current == null) {
				cur = 1;
			} else {
				try {
					cur = Integer.parseInt(current);
				} catch (NumberFormatException e) {
					cur = 1;
				}
			}
			
			int firstNumOnCurPage = (cur/NUMS_ON_PAGE)*NUMS_ON_PAGE+1;
			if (cur%10==0) {
				firstNumOnCurPage = (cur/NUMS_ON_PAGE-1)*NUMS_ON_PAGE+1;
			}
			int lastNumOnCurPage = firstNumOnCurPage + NUMS_ON_PAGE-1;
			
			if (lastNumOnCurPage>lastPage) {
				lastNumOnCurPage = lastPage;
			}
			
			sb.append("<a href='news?act=showall&p=1' class='pagenum'>&lt;&lt;</a>");
			
			if (firstNumOnCurPage>NUMS_ON_PAGE) {
				sb.append("<a class='arrow pagenum' href='news?act=showall&p=")
						.append(firstNumOnCurPage-1).append("'>&lt;</a>");
			}
			
			
			for (int i = firstNumOnCurPage; i <= lastNumOnCurPage; i++) {
				if (i == cur) {
					sb.append("<span class='active pagenum'>").append(i).append("</span>");
				} else {
					sb.append("<a href='news?act=showall&p=").append(i).append("' class='pagenum'>").append(i)
							.append("</a>");
				}
			}
			
			if (lastPage-lastNumOnCurPage>0) {
				sb.append("<a class='arrow pagenum' href='news?act=showall&p=")
						.append(lastNumOnCurPage+1).append("'>&gt;</a>");
			}
			
			sb.append("<a href='news?act=showall&p=").append(lastPage).append("' class='pagenum'>&gt;&gt;</a>");
			sb.append("</div>");

			pageContext.getOut().write(sb.toString());
		} catch (IOException e) {
			throw new JspException(e.getMessage());
		}

		return SKIP_BODY;

	}

}
