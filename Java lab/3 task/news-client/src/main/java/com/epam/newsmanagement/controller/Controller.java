
package com.epam.newsmanagement.controller;

import java.io.IOException;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.epam.newsmanagement.command.ActionCommand;
import com.epam.newsmanagement.command.ActionFactory;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.utils.ConfigurationManager;


/**
 * The Class Controller manages user's requests and sends it to required views.
 * 
 * @author Natallia_Zhukava
 */
@WebServlet(name="news-client", urlPatterns = "/news")
public class Controller extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private WebApplicationContext ctx;
	private ActionFactory client;
	private static final String PAGE_ERROR = "page.path.customError";
	 static Logger logger = Logger.getLogger(Controller.class);
	/**
	 * Instantiates a new controller.
	 */
	public Controller() {

	}


	/**
	 * The methods initializes Spring context
	 * 
	 * @see javax.servlet.GenericServlet#init()
	 */
	public void init() throws ServletException {
		super.init();
		ctx = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		client = new ActionFactory(ctx);
	}
		
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	/**
	 * The method gets parameters from user's requests, defines which command should handle it.
	 * Then it sends results of processing request to required page.
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
	    String page = null;
	    ActionCommand command = null;
	    command = client.defineCommand(request);
	    try {
			page = command.execute(request);
		} catch (ServiceException e) {
			logger.error(e);
			page = ConfigurationManager.getProperty(PAGE_ERROR);
		}
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
		dispatcher.forward(request, response);
		
		
	}


}
