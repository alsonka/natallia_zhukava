package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagement.utils.ConfigurationManager;

import static com.epam.newsmanagement.command.Constants.*;

/**
 * The Class ChangeLanguageCommand changes language of application.
 * 
 * @author Natallia_Zhukava
 */
public class ChangeLanguageCommand implements ActionCommand {
	
	private static final String PARAM_LOCALE = "l";
	private static final String ATTR_LOCALE = "loc";
	private static final String PAGE_INDEX = "/news?null";

	

	/**
	 * The method takes locale as parameter from request and set it to session. 
	 * 
	 * @see com.epam.newsmanagement.command.ActionCommand#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		HttpSession session = request.getSession();
		String lang = request.getParameter(PARAM_LOCALE);		
		
		page = (String)session.getAttribute(ATTR_CURRENT);

		session.setAttribute(ATTR_LOCALE, lang);
		if ((page==null) || (page.equals(PAGE_INDEX))){
			String current = ConfigurationManager.getProperty(PAGE_MAIN);
			session.setAttribute(ATTR_CURRENT, current);
				page = current;
			
		}

		return page;
	}

}
