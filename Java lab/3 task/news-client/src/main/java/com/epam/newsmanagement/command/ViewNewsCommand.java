package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.utils.ConfigurationManager;
import static com.epam.newsmanagement.command.Constants.*;

import java.util.Date;

/**
 * The Class ViewNewsCommand allows to show single news on page.
 * 
 * @author Natallia_Zhukava
 */
public class ViewNewsCommand implements ActionCommand {
	
	private NewsManager newsManager;
	
	private NewsService newsService;
	

	private static final String PAGE_VIEW = "page.path.newspage";
	private static final String PARAM_NEWS_ID = "id";
	private static final String ATTR_COMPLEX_NEWS = "newsVO";
	private static final String ATTR_PREV_NEXT = "prevNext";
	
	
	/**
	 * Sets the news manager.
	 *
	 * @param newsManager the new news manager
	 */
	public void setNewsManager(NewsManager newsManager) {
		this.newsManager = newsManager;
	}


	/**
	 * Sets the news service.
	 *
	 * @param newsService the new news service
	 */
	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}


	/**
	 * The method takes identifier of news, finds news with this id and also previous and next news in database.
	 * Then it shows in on page. 
	 * @throws ServiceException 
	 * @see com.epam.newsmanagement.command.ActionCommand#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		String page = ConfigurationManager.getProperty(PAGE_VIEW);
		
		HttpSession session = request.getSession();
		String current = request.getServletPath()+"?"+request.getQueryString();
		session.setAttribute(ATTR_CURRENT, current);
	
		
	    long id = Long.parseLong(request.getParameter(PARAM_NEWS_ID));
		
		NewsVO news=null;
		long[] prevNext = null;
		SearchCriteriaVO obj = (SearchCriteriaVO) session.getAttribute(ATTR_SEARCH);
		
		
			news = newsManager.viewSingleNews(id);
			
			if (obj!=null) {
				prevNext = newsService.findPreviousAndNext(id, obj);} 
			else {
			prevNext = newsService.findPreviousAndNext(id); }
			
			
		
		String randForm = String.valueOf(session.getId()) + String.valueOf(new Date().getTime());
		request.setAttribute(ATTR_COMPLEX_NEWS, news);
		request.setAttribute(ATTR_PREV_NEXT, prevNext);
		request.setAttribute(ATTR_UNIQUE_FORM, randForm);
		
		
		return page;
	}




}
