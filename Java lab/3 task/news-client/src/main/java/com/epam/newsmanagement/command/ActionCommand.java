package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagement.exception.ServiceException;




/**
 * The Interface ActionCommand is template for commands.
 *
 * @author Natallya Zhukova
 * 
 */
public interface ActionCommand {

	/**
	 * Execute.
	 *
	 * @param request the request
	 * @return the string - the page path
	 * @throws ServiceException 
	 * 
	 */
	String execute(HttpServletRequest request) throws ServiceException;
}