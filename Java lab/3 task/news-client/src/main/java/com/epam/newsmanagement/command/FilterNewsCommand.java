package com.epam.newsmanagement.command;

import java.util.ArrayList;
import java.util.List;
import static com.epam.newsmanagement.command.Constants.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;

import com.epam.newsmanagement.service.SearchService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.utils.ConfigurationManager;

/**
 * The Class FilterNewsCommand is designed to search news by given criteria and show search results.
 * 
 * @author Natallia_Zhukava
 */
public class FilterNewsCommand implements ActionCommand {
	
		
	private static final String PAGE_VIEW = "page.path.newslist";
	private static final String PARAM_AUTHOR = "author";
	private static final String PARAM_TAGS = "tags";
	private static final String ATTR_SEARCH_MESSAGE = "searchMessage";
	private static final String MESSAGE_NOT_FOUND = "news.notfound";

	private AuthorService authorService;

	private TagService tagService;

	/**
	 * Sets the author service.
	 *
	 * @param authorService the new author service
	 */
	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	/**
	 * Sets the tag service.
	 *
	 * @param tagService the new tag service
	 */
	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	private SearchService searchService;

	/**
	 * Sets the search service.
	 *
	 * @param searchService the new search service
	 */
	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}

	/**
	 * The method gets SearchCriteria object and searches news by given criteria.
	 * Then it shows the list of found news on page.
	 * @throws ServiceException 
	 * @see com.epam.newsmanagement.command.ActionCommand#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		String page = ConfigurationManager.getProperty(PAGE_VIEW);
		
		HttpSession session = request.getSession(true);
		String current = request.getServletPath()+"?"+request.getQueryString();
		session.setAttribute(ATTR_CURRENT, current);
		
		
		List<Author> authors = null;
		List<Tag> tagList = null;

		String author = request.getParameter(PARAM_AUTHOR);
		String[] tags = request.getParameterValues(PARAM_TAGS);

		long authorId;
		if (author == null) {
			authorId = 0;
		}
		try {
			authorId = Long.parseLong(author);
		} catch (NumberFormatException e) {
			authorId = 0;
		}

		List<Long> searchTags = new ArrayList<Long>();

		if (tags != null) {
			for (String tag : tags) {
				Long tagId = Long.parseLong(tag);
				searchTags.add(tagId);
			}
		}

		SearchCriteriaVO obj = new SearchCriteriaVO();
		obj.setAuthorId(authorId);
		if (searchTags.size() != 0) {
			obj.setTagIdList(searchTags);
		}

		if ((authorId == 0) && (searchTags.size() == 0)) {
			session.removeAttribute(ATTR_SEARCH);
			page = ConfigurationManager.getProperty(PAGE_MAIN);
		} else {

			List<NewsVO> news = new ArrayList<NewsVO>();
			long newsNum;
			int pageNum = 1;
			int lastPage = 1;
		
				
					authors = authorService.findAllAuthors();
					tagList = tagService.findAllTags();
					newsNum = searchService.countNews(obj);
					lastPage = searchService.calculateLastPageNum(newsNum, ONPAGE);
					news = searchService.findNewsOnPage(pageNum, ONPAGE, obj);
				

			
			session.setAttribute(ATTR_SEARCH, obj);
			request.setAttribute(ATTR_NEWS_LIST, news);
			request.setAttribute(ATTR_AUTHORS_LIST, authors);
			request.setAttribute(ATTR_TAGS_LIST, tagList);
			request.setAttribute(ATTR_LAST_PAGE_NUM, lastPage);
			if (news.size() == 0) {
				request.setAttribute(ATTR_SEARCH_MESSAGE, MESSAGE_NOT_FOUND);
			}
		}

		return page;
	}

}
