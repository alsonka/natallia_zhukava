package com.epam.newsmanagement.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.SearchService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.utils.ConfigurationManager;

import static com.epam.newsmanagement.command.Constants.*;


/**
 * The Class ShowAllNewsCommand allows to show list of news.
 * 
 * @author Natallia_Zhukava
 */
public class ShowAllNewsCommand implements ActionCommand {

	private static final String PAGE_VIEW = "page.path.newslist";
	private static final String PAGE_WITH_NEWS = "/news?act=showall&p=";
	private static final String PARAM_PAGE_NUM = "p";


	private AuthorService authorService;
	private NewsManager newsManager;
	private NewsService newsService;
	private TagService tagService;
	private SearchService searchService;

	/**
	 * Sets the author service.
	 *
	 * @param authorService the new author service
	 */
	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	/**
	 * Sets the news manager.
	 *
	 * @param newsManager the new news manager
	 */
	public void setNewsManager(NewsManager newsManager) {
		this.newsManager = newsManager;
	}

	/**
	 * Sets the news service.
	 *
	 * @param newsService the new news service
	 */
	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	/**
	 * Sets the tag service.
	 *
	 * @param tagService the new tag service
	 */
	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	/**
	 * Sets the search service.
	 *
	 * @param searchService the new search service
	 */
	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}

	/**
	 * The method gets list of news (with or without SearchCriteria) and shows it on page.
	 * @throws ServiceException 
	 * 
	 * @see com.epam.newsmanagement.command.ActionCommand#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		String page =  ConfigurationManager.getProperty(PAGE_VIEW);
		
		HttpSession session = request.getSession();
		String current = request.getServletPath()+"?"+request.getQueryString();
		session.setAttribute(ATTR_CURRENT, current);

		List<Author> authors = null;
		List<NewsVO> news = null;
		List<Tag> tags = null;

		String currPageNum = request.getParameter(PARAM_PAGE_NUM);

		int pageNum;
		if (currPageNum == null) {
			pageNum = 1;
		}
		try {
			pageNum = Integer.parseInt(currPageNum);
		} catch (NumberFormatException e) {
			pageNum = 1;
		}

		SearchCriteriaVO obj = (SearchCriteriaVO) session.getAttribute(ATTR_SEARCH);
		if (obj != null) {
			
			long authorId = obj.getAuthorId();
			List<Long> searchTags = obj.getTagIdList();
			if ((authorId == 0) && (searchTags.size() == 0)) {
				session.removeAttribute(ATTR_SEARCH);
				page = PAGE_WITH_NEWS + pageNum;
			} else {
				long newsNum = 0;
				int lastPageNum = 1;
				List<NewsVO> newsOnPage = new ArrayList<NewsVO>();
				

					newsNum = searchService.countNews(obj);
					lastPageNum = searchService.calculateLastPageNum(newsNum, ONPAGE);
					newsOnPage = searchService.findNewsOnPage(pageNum, ONPAGE, obj);
				

				
				request.setAttribute(ATTR_LAST_PAGE_NUM, lastPageNum);
				request.setAttribute(ATTR_NEWS_LIST, newsOnPage);
			}
		} else {

			long newsNum;
			int lastPageNum = 1;
			
				newsNum = newsService.countAllNews();
				lastPageNum = newsService.calculateLastPageNum(newsNum, ONPAGE);
				news = newsManager.viewNewsListOnPage(pageNum, ONPAGE);
		

			request.setAttribute(ATTR_LAST_PAGE_NUM, lastPageNum);
			request.setAttribute(ATTR_NEWS_LIST, news);
		}

		
			authors = authorService.findAllAuthors();
			tags = tagService.findAllTags();
		

		request.setAttribute(ATTR_AUTHORS_LIST, authors);
		request.setAttribute(ATTR_TAGS_LIST, tags);

		request.setAttribute(PARAM_PAGE_NUM, pageNum);

		return page;
	}

}
