package com.epam.newsmanagement.utils;

import java.util.ResourceBundle;

/**
 * The Class ConfigurationManager is designed to work with pages resource
 *
 */
public class ConfigurationManager {
	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("config");

	private ConfigurationManager() {
	}

	/**
	 * The method gets property value by given key
	 * 
	 * @param key
	 *            property key
	 * @return string
	 */
	public static String getProperty(String key) {
		return resourceBundle.getString(key);
	}
}
