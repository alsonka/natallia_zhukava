package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagement.utils.ConfigurationManager;
import static com.epam.newsmanagement.command.Constants.*;

/**
 * The Class ResetSearchCommand is designed to reset search criteria.
 * 
 * @author Natallia_Zhukava
 */
public class ResetSearchCommand implements ActionCommand {
	

	/**
	 * The method removes SearchCriteria object from session
	 * 
	 * @see com.epam.newsmanagement.command.ActionCommand#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) {
		HttpSession session = request.getSession();
		String current = request.getServletPath()+"?"+request.getQueryString();
		session.setAttribute(ATTR_CURRENT, current);
		
		session.removeAttribute(ATTR_SEARCH);
		String page = ConfigurationManager.getProperty(PAGE_MAIN);
		
		
		return page;
	}

}
