package com.epam.newsmanagement.command;

final class Constants {

	private Constants() {
	}
	
	public static final int ONPAGE = 4; // number of news on single page
	
	public static final String ATTR_EXCEPTION = "exception";
	public static final String ATTR_CURRENT = "current"; 
	public static final String ATTR_NEWS_LIST = "newsList";
	public static final String ATTR_AUTHORS_LIST = "authorList";
	public static final String ATTR_TAGS_LIST = "tagList";
	public static final String ATTR_SEARCH = "searchCriteria";
	public static final String ATTR_LAST_PAGE_NUM = "lastPage";
	public static final String ATTR_UNIQUE_FORM = "unique";
	
	public static final String PAGE_MAIN = "page.path.main";
	public static final String PAGE_ERROR = "page.path.customError";

}
