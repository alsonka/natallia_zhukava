<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="current" value="/news" scope="session"></c:set>

<html>
<body>
 <jsp:forward page="/news"></jsp:forward>
</body>
</html>
