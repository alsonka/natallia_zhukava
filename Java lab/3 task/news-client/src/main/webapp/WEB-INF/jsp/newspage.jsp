<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:choose>

<c:when test="${not empty loc }">
<fmt:setLocale value="${loc}" />
</c:when>
<c:otherwise>
<fmt:setLocale value="ru_RU" />
</c:otherwise>
</c:choose>

<fmt:setBundle basename="locale" var="rb" />

<c:set var="timezone" value="${cookie.timezone.value }" />
<fmt:setTimeZone value="${timezone}" />

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/styles.css">
<script src="js/jstz-1.0.4.min.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/js.cookie.js"></script>
<script src="js/validation_message.jsp"></script>
<script src="js/validation.js"></script>
<script src="js/timezone.js"></script>
<title>News</title>
</head>
<body>
<div class="wrapper">
		<div class="header">
			<h1><fmt:message key="header.title" bundle="${ rb }" /></h1>

			<div class="lang">
				<a href="news?act=lang&l=en_US">EN</a> | <a href="news?act=lang&l=ru_RU">RU</a>
			</div>
		</div>
		

	<div class="content-wrapper">
	
	<div class="content">

<a href="news?act=showall"><fmt:message key='keys.back' bundle='${ rb }' /></a>
	
	<div class="fullnews news">
	<span class="title"><c:out value="${newsVO.news.title}"/></span> 
	<span class="author">(<fmt:message key='news.author' bundle='${ rb }' /> <c:out value="${newsVO.author.name}"/>)</span>
	<span class="date"><fmt:formatDate dateStyle="Medium" value="${newsVO.news.modificationDate}" /></span>
	<br>
	
	<p class="short"><c:out value="${newsVO.news.shortText}"/></p>
	<p class="full"><c:out value="${newsVO.news.fullText}"/></p>
	</div>
	
	<div class="comments">
	<c:forEach var="comment" items="${newsVO.commentList}">
	<div class="comment">
	 <span class="date"><fmt:formatDate dateStyle="Medium"  value="${comment.creationDate}"/>	</span>
	<br>
	<span class="text">
	<c:out value="${comment.text }" />
	</span>
	
	</div>
	</c:forEach>
	
	<form action="news" method="post" class="postcomment" id="postComment">
	<input type="hidden" value="postcomment" name="act">
	<input type="hidden" name="unique" value="${unique}">
	<input type="hidden" name="page" value="/news?act=viewnews&id=${newsVO.news.id}">
	<input type="hidden" value="${newsVO.news.id}" name="newsId">
	<textarea class="text" name="commentText" maxlength="100" id="commentText"></textarea>
	
	<span id="commentError" class="error"></span>
	
	<input type="submit" value="<fmt:message key='comment.post' bundle='${ rb }' />"> 
	</form>
	
	</div>		
	
	<div class="controls">
	<c:if test="${prevNext[0]!=0 }">
	<a class="prev" href="news?act=viewnews&id=${prevNext[0]}"><fmt:message key='keys.previous' bundle='${ rb }' /></a>
	</c:if>
	<c:if test="${prevNext[1]!=0 }">
	<a class="next" href="news?act=viewnews&id=${prevNext[1]}"><fmt:message key='keys.next' bundle='${ rb }' /></a>
	</c:if>
	</div>

	</div>
	</div>
	<div class="footer">
		Copyright @ Epam 2015. All rights reserved.
	</div>
</div>
</body>
</html>