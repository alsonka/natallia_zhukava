$(function(){
	  var tz = jstz.determine();
	  var currentCookie = $.getCookie("timezone");
	  var timezone = tz.name();
	  $("#timezone").html(timezone);
	  $("#cookie").html(currentCookie);
	  $.setCookie("timezone", timezone, { path:"/"});	  
	 if (currentCookie!==timezone) {
		  location.reload();
		  }
	  
});