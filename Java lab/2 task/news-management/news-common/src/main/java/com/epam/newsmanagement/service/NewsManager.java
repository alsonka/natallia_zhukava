package com.epam.newsmanagement.service;

import java.util.List;


import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.exception.ServiceException;


/**
 * The Interface NewsManager contains service method which should work with News Value Object.
 * 
 * @author Natallia_Zhukava
 */
public interface NewsManager {

	/**
	 * The method saves the NewsVO object.
	 *
	 * @param complexNews the complex news
	 * @return id the generated identifier
	 * @throws ServiceException the service exception
	 */
	long saveNews(NewsVO complexNews) throws ServiceException;

	/**
	 * The method edits the NewsVO object.
	 *
	 * @param complexNews the complex news
	 * @return 
	 * @throws ServiceException the service exception
	 */
	void editNews(NewsVO complexNews) throws ServiceException;

	/**
	 * The method deletes the NewsVO object by given identifier.
	 *
	 * @param id the id
	 * @throws ServiceException the service exception
	 */
	void deleteNews(long id) throws ServiceException;
	
	/**
	 * The method deletes the list of the NewsVO objects by given identifiers.
	 *
	 * @param ids the ids
	 * @throws ServiceException the service exception
	 */
	void deleteNews(long[] ids) throws ServiceException;
	
	
	/**
	 * The method allow to view list of all news sorted by comments number and modification date.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	List<NewsVO> viewNewsList() throws ServiceException;

	/**
	 * The method allows to view single news with given id.
	 *
	 * @param newsId the news id
	 * @return the newsVO
	 * @throws ServiceException the service exception
	 */
	NewsVO viewSingleNews(long newsId) throws ServiceException;

	/**
	 * The method allows to view list of news on current page.
	 * @param p the current page
	 * @param onPage the number of news on page
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	List<NewsVO> viewNewsListOnPage(int p, int onPage) throws ServiceException;

}