package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.exception.ServiceException;


/**
 * The Interface SearchService contains method which works with search in database.
 * 
 * @author Natallia_Zhukava
 */
public interface SearchService {

	/**
	 * The method searches by search criteria.
	 *
	 * @param search the search criteria
	 * @return the list the list of news
	 * @throws ServiceException the service exception
	 */
	List<NewsVO> searchBy(SearchCriteriaVO search) throws ServiceException;
	
	/**
	 * Count news which were found by search criteria.
	 *
	 * @param search the search
	 * @return the long - quantity of news
	 * @throws ServiceException the service exception
	 */
	long countNews(SearchCriteriaVO search) throws ServiceException;
	
	/**
	 * Calculate last page num.
	 *
	 * @param num the overall number of news
	 * @param onPage the number of news on single page
	 * @return the int number of last page
	 */
	int calculateLastPageNum(long num, int onPage);

	/**
	 * Find news on page.
	 *
	 * @param p the page number
	 * @param onPage the quantity of news on page
	 * @param obj the SearchCriteria object
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	List<NewsVO> findNewsOnPage(int p, int onPage, SearchCriteriaVO obj) throws ServiceException;

}