package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;


/**
 * The Class NewsServiceImpl implements method from NewsService interface.
 * 
 * @author Natallia_Zhukava
 */
public class NewsServiceImpl implements NewsService {
	
	static Logger logger = Logger.getLogger(AuthorServiceImpl.class);
	private NewsDAO newsDao;
	
	/**
	 * Sets the newsDAO.
	 *
	 * @param newsDao the new news dao
	 */
	public void setNewsDao(NewsDAO newsDao) {
		this.newsDao = newsDao;
	}

	/**
	 * Instantiates a new news service implementation object.
	 */
	public NewsServiceImpl() {
		
	}

	/**
	 * The method saves news.
	 *
	 * @param news the news
	 * @return id the generated id
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsService#saveNews(com.epam.newsmanagement.entity.NewsTO)
	 */
	@Override
	public long saveNews(NewsTO news) throws ServiceException {
		long id=0;
		try {
			id = newsDao.save(news);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return id;
	}

	/**
	 * The method edits news and sets new modification date.
	 *
	 * @param news the news
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsService#editNews(com.epam.newsmanagement.entity.NewsTO)
	 */
	@Override
	public void editNews(NewsTO news) throws ServiceException {
		try {
			
			newsDao.update(news);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	}
	




	/**
	 * The method finds all news sorted by comments number and modification date.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsService#findAllNews()
	 */
	@Override
	public List<NewsTO> findAllNews() throws ServiceException {
		List<NewsTO> list = null;
		try {
			 list = newsDao.findAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return list;
	}

	/**
	 * The method finds the news by given id.
	 *
	 * @param newsId the news id
	 * @return the news to
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsService#findNewsById(long)
	 */
	@Override
	public NewsTO findNewsById(long newsId) throws ServiceException {
		NewsTO news = null;
		try {
			news = newsDao.findById(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return news;
	}
	

	/**
	 * The method counts all news.
	 *
	 * @return the long
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsService#countAllNews()
	 */
	@Override
	public long countAllNews() throws ServiceException {
		long num = 0;
		try {
			num = newsDao.countNews();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return num;
	}

	/**
	 * The method attaches list of tags to given news.
	 *
	 * @param newsId the news id
	 * @param list the list
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsService#attachNewsTags(long, java.util.List)
	 */
	@Override
	public void attachNewsTags(long newsId, List<TagTO> list) throws ServiceException {
		if (list!=null) {
		try {
			newsDao.attachTagsToNews(newsId, list);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		}
	}

	/**
	 *  
	 * The method attaches author to given news.
	 *
	 * @param newsId the news id
	 * @param authorId the author id
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsService#attachNewsAuthor(long, long)
	 */
	@Override
	public void attachNewsAuthor(long newsId, long authorId) throws ServiceException {
		try {
			newsDao.attachAuthorToNews(newsId, authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	}

	/**
	 * The method finds all news attached to given author.
	 *
	 * @param authorId the author id
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsService#findNewsByAuthor(long)
	 */
	@Override
	public List<NewsTO> findNewsByAuthor(long authorId) throws ServiceException {
		List<NewsTO> list = null;
		try {
			 list = newsDao.findByAuthor(authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return list;
	}

	/**
	 * The method deletes news by given id.
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsService#deleteNews(long)
	 */
	@Override
	public void deleteNews(long newsId) throws ServiceException {
		try {
			newsDao.delete(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	}

	/**
	 * The method detaches tags from given news.
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsService#detachNewsTags(long)
	 */
	@Override
	public void detachNewsTags(long newsId) throws ServiceException {
		try {
			newsDao.detachNewsTags(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	
	}

	/**
	 * The method detaches author from given news.
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsService#detachNewsAuthor(long)
	 */
	@Override
	public void detachNewsAuthor(long newsId) throws ServiceException {
		try {
			newsDao.detachNewsAuthor(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	}

	/**
	 * Finds news on current page.
	 *
	 * @param p the page number
	 * @param onPage the quantify of news  on single page
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@Override
	public List<NewsTO> findNewsOnPage(int p, int onPage) throws ServiceException {
		long newsNum = countAllNews();
		int lastPageNum = calculateLastPageNum(newsNum, onPage);

		if ((p > lastPageNum) || (p < 1)) {
			p = 1;
		}
		
		int firstOnPage = (p-1)*onPage;
		int lastOnPage;
		
		if (newsNum < onPage) {
			lastOnPage =  (int) newsNum;
		} else {
			lastOnPage = p * onPage;
		}
		if ((p == lastPageNum) && (newsNum % onPage > 0)) {
			lastOnPage = (int) ((p - 1) * onPage + newsNum % onPage);
		}

		List<NewsTO> listPage = null;
		try {
			listPage = newsDao.findNewsOnCurrentPage(firstOnPage, lastOnPage);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

		return listPage;


	}
	
	
	/**
	 * Calculates last page number.
	 *
	 * @param num the number of all news
	 * @param onPage the number of news on page
	 * @return the int number of last page
	 */
	@Override
	public int calculateLastPageNum(long num, int onPage) {
		int numPages;

		if (num % onPage != 0) {
			numPages = (int) (num / onPage) + 1;
		} else {
			numPages = (int) (num / onPage);
		}
		return numPages;
	}

	
	/**
	 * Find previous and next news by given news id.
	 *
	 * @param newsId the news id
	 * @return the long[] array with ids of previous and next news
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsService#findPreviousAndNext(long)
	 */
	@Override
	public long[] findPreviousAndNext(long newsId) throws ServiceException {
		long[] idList;
		try {
			idList = newsDao.findPreviousAndNextNews(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return idList;
	}

	/**
	 * Finds previous and next news by given news id in search results list.
	 *
	 * @param id the id
	 * @param obj the SearchCriteria object
	 * @return the long[] array with ids of previous and next news
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsService#findPreviousAndNext(long, com.epam.newsmanagement.entity.SearchCriteriaVO)
	 */
	@Override
	public long[] findPreviousAndNext(long id, SearchCriteriaVO obj) throws ServiceException {
		long[] idList;
		try {
			idList = newsDao.findPreviousAndNextNews(id, obj);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return idList;
	}

	/**
	 * Updates author to given news.
	 *
	 * @param newsId the news id
	 * @param authorId the author id
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsService#updateAuthorNews(long, long)
	 */
	@Override
	public void updateAuthorNews(long newsId, long authorId) throws ServiceException {
		try {
			newsDao.updateNewsAuthor(newsId, authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	}

	/**
	 * The method deletes list of news by their identifiers
	 * 
	 * @see com.epam.newsmanagement.service.NewsService#deleteNews(long[])
	 */
	@Override
	public void deleteNews(long[] newsIds) throws ServiceException {
		try {
			newsDao.delete(newsIds);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	}

	/**
	 * The method detaches authors from list of news
	 * @throws ServiceException 
	 * @see com.epam.newsmanagement.service.NewsService#detachNewsAuthors(long[])
	 */
	@Override
	public void detachNewsAuthors(long[] newsIds) throws ServiceException {
		try {
			newsDao.detachNewsAuthor(newsIds);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	}

	/**
	 * The method detaches tags from list of news
	 * @throws ServiceException 
	 * @see com.epam.newsmanagement.service.NewsService#detachNewsTags(long[])
	 */
	@Override
	public void detachNewsTags(long[] newsIds) throws ServiceException {
		try {
			newsDao.detachNewsTags(newsIds);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	} 

}
