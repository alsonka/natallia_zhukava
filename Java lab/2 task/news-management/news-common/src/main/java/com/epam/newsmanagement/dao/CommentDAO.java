package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Interface CommentDAO contains the methods which should work with {@code Comments} database table.
 * 
 * @author Natallia_Zhukava
 */
public interface CommentDAO extends CommonDAO<CommentTO> {

	/**
	 * The method finds the comments attached to given news.
	 *
	 * @param id the id
	 * @return the list of {@code CommentTO} objects
	 * @throws DAOException the DAO exception
	 */
	List<CommentTO> findByNewsId(long id) throws DAOException;
	
	/**
	 * The method deletes all comments attached to given news  by given news id.
	 *
	 * @param newsId the news id
	 * @throws DAOException the DAO exception
	 */
	void deleteByNewsId(long newsId) throws DAOException;

	/**
	 * The method deletes all comments attached to given news  by given news ids.
	 *
	 * @param newsIds the news ids array
	 * @throws DAOException the DAO exception
	 */
	void deleteByNewsId(long[] newsIds) throws DAOException;
	}

 	