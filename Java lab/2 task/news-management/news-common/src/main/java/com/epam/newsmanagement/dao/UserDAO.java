package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.UserTO;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Interface UserDAO contains methods which work with database tables Users and Roles.
 */
public interface UserDAO {

	/**
	 * The method finds User entity by login.
	 *
	 * @param login the login
	 * @return user
	 * @throws DAOException the DAO exception
	 */
	
	public UserTO findUserByLogin(String login) throws DAOException;
}
