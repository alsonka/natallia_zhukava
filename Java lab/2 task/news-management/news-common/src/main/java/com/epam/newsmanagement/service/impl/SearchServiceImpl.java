package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.service.SearchService;


/**
 * The Class SearchServiceImpl implements method from SearchService interface.
 */
public class SearchServiceImpl implements SearchService {
	static Logger logger = Logger.getLogger(SearchServiceImpl.class);
	
	private NewsDAO newsDao;
	
	/**
	 * Sets the newsDAO.
	 *
	 * @param newsDao the new news dao
	 */
	public void setNewsDao(NewsDAO newsDao) {
		this.newsDao = newsDao;
	}

	private NewsManager newsManager;
	
	/**
	 * Sets the news manager.
	 *
	 * @param newsManager the new news manager
	 */
	public void setNewsManager(NewsManager newsManager) {
		this.newsManager = newsManager;
	}


	

	/**
	 * Instantiates a new search service implementation object.
	 */
	public SearchServiceImpl() {
	}

	
	/**
	 * The method searches by given criteria and forms list of NewsVO objects.
	 *
	 * @param search the search
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.SearchService#searchBy(com.epam.newsmanagement.entity.SearchCriteriaVO)
	 */
	@Override
	@Transactional(rollbackFor={RuntimeException.class, ServiceException.class})
	public List<NewsVO> searchBy(SearchCriteriaVO search) throws ServiceException {
		List<NewsTO> list = new ArrayList<NewsTO>();
		List<NewsVO> newsList = new ArrayList<NewsVO>();

		try {
		
			list = newsDao.findByCriteria(search);

			for (NewsTO news : list) {
				NewsVO cNews = newsManager.viewSingleNews(news.getId());
				newsList.add(cNews);
			}
		
			
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return newsList;
	}




	/**
	 * Counts news in search results.
	 *
	 * @param search the searchCriteria object
	 * @return the long quantity of news
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.SearchService#countNews(com.epam.newsmanagement.entity.SearchCriteriaVO)
	 */
	@Override
	public long countNews(SearchCriteriaVO search) throws ServiceException {
		long num = 0;
		try {
			num = newsDao.countNews(search);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
		return num;
	}




	/**
	 * Calculate last page number.
	 *
	 * @param num the number of news
	 * @param onPage the quantity of news on single page
	 * @return the int the last page number
	 * @see com.epam.newsmanagement.service.SearchService#calculateLastPageNum(long, int)
	 */
	@Override
	public int calculateLastPageNum(long num, int onPage) {
		int numPages;

		if (num % onPage != 0) {
			numPages = (int) (num / onPage) + 1;
		} else {
			numPages = (int) (num / onPage);
		}
		return numPages;
	}




	/**
	 * Find news on page.
	 *
	 * @param p the page number
	 * @param onPage the quantity of news on single page
	 * @param obj the SearchCriteria obj
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.SearchService#findNewsOnPage(int, int, com.epam.newsmanagement.entity.SearchCriteriaVO)
	 */
	@Override
	public List<NewsVO> findNewsOnPage(int p, int onPage, SearchCriteriaVO obj) throws ServiceException {
		long newsNum = countNews(obj);
		int lastPageNum = calculateLastPageNum(newsNum, onPage);

		if ((p > lastPageNum) || (p < 1)) {
			p = 1;
		}
		
		int lastOnPrevPage = (p-1)*onPage;
		int lastOnPage;
		
		if (newsNum < onPage) {
			lastOnPage = (int) newsNum;
		} else {
			lastOnPage = p * onPage;
		}
		if ((p == lastPageNum) && (newsNum % onPage > 0)) {
			lastOnPage = (int) ((p - 1) * onPage + newsNum % onPage);
		}

		List<NewsTO> list = null;
		List<NewsVO> listPage = new ArrayList<NewsVO>();
		try {
			list = newsDao.findNewsOnCurrentPage(lastOnPrevPage, lastOnPage, obj);
			for (NewsTO news : list) {
				NewsVO cNews = newsManager.viewSingleNews(news.getId());
				listPage.add(cNews);
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return listPage;

	}
	
	

}
