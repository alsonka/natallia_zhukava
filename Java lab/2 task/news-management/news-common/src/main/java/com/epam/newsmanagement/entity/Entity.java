package com.epam.newsmanagement.entity;

import java.io.Serializable;

/**
 * The class Entity is on top of hierarchy of classes which represent database
 * tables.
 *
 * @author Natallia_Zhukava
 */
public abstract class Entity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new entity.
	 */
	public Entity() {

	}

	/**
	 * Instantiates a new entity.
	 *
	 * @param id
	 *            the id
	 */
	public Entity(long id) {
		this.id = id;
	}

	private long id;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Entity))
			return false;
		Entity other = (Entity) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
