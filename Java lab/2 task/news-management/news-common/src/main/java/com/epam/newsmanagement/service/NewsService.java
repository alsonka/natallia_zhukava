package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.ServiceException;


/**
 * The Interface NewsService contains the service methods which should work with {@code NewsTO} objects.
 * 
 * @author Natallia_Zhukava
 */
public interface NewsService {

	/**
	 * The method saves the given news.
	 *
	 * @param news the news
	 * @return the newsTO with generated identifier
	 * @throws ServiceException the service exception
	 */
	long saveNews(NewsTO news) throws ServiceException;
	
	/**
	 * The method attaches list of tag to given news.
	 *
	 * @param newsId the news id
	 * @param list the list
	 * @throws ServiceException the service exception
	 */
	void attachNewsTags(long newsId, List<TagTO> list) throws ServiceException;
	
	/**
	 * The method attaches the author to given news.
	 *
	 * @param newsId the news id
	 * @param authorId the author id
	 * @throws ServiceException the service exception
	 */
	void attachNewsAuthor(long newsId, long authorId) throws ServiceException;

	/**
	 * The method edits given news.
	 *
	 * @param news the news
	 * @throws ServiceException the service exception
	 */
	void editNews(NewsTO news) throws ServiceException;

	/**
	 * The method deletes given news.
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 */
	void deleteNews(long newsId) throws ServiceException;

	/**
	 * The method finds all news and returns list of them .
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	List<NewsTO> findAllNews() throws ServiceException;

	/**
	 * The method finds the news by given id.
	 *
	 * @param newsId the news id
	 * @return the news to
	 * @throws ServiceException the service exception
	 */
	
	NewsTO findNewsById(long newsId) throws ServiceException;
	
	
	
	/**
	 * Find news on page.
	 *
	 * @param p the current page number
	 * @param onPage number of news on page
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	List<NewsTO> findNewsOnPage(int p, int onPage) throws ServiceException;
	
	
	/**
	 * Calculate last page number for all news in database.
	 *
	 * @param num the num
	 * @param onPage the on page
	 * @return the int
	 */
	int calculateLastPageNum(long num, int onPage);
	/**
	 * The method finds all news which attached to given author .
	 *
	 * @param authorId the author id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	
	List<NewsTO> findNewsByAuthor(long authorId) throws ServiceException;

	/**
	 * The method counts all news.
	 *
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	long countAllNews() throws ServiceException;

	/**
	 * The method detaches all tags from given news.
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 */
	void detachNewsTags(long newsId) throws ServiceException;
	
	/**
	 * The method detaches author from given news.
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 */
	void detachNewsAuthor(long newsId) throws ServiceException;
	
	/**
	 * The method updates author of given news.
	 *
	 * @param newsId the news id
	 * @param authorId the author id
	 * @throws ServiceException the service exception
	 */
	void updateAuthorNews(long newsId, long authorId) throws ServiceException;
	
	/**
	 * Find previous and next news by given news id.
	 *
	 * @param newsId the news id
	 * @return the long[] array with ids of previous and next news
	 * @throws ServiceException the service exception
	 */
	long[] findPreviousAndNext(long newsId) throws ServiceException;

	/**
	 * Find previous and next news by given news id in search results.
	 *
	 * @param newsId the news id
	 * @param obj the SearchCriteria obj
	 * @return the long[] array with ids of previous and next news
	 * @throws ServiceException the service exception
	 */
	long[] findPreviousAndNext(long newsId, SearchCriteriaVO obj) throws ServiceException;
	
	/**
	 * Deletes list of news.
	 *
	 * @param newsIds the news ids
	 * @throws ServiceException the service exception
	 */
	void deleteNews(long[] newsIds) throws ServiceException;
	
	/**
	 * Detach  authors from list of news.
	 *
	 * @param newsIds the news ids
	 * @throws ServiceException 
	 */
	void detachNewsAuthors(long[] newsIds) throws ServiceException;
	
	/**
	 * Detaches  tags from list of news.
	 *
	 * @param newsIds the news ids
	 * @throws ServiceException 
	 */
	void detachNewsTags(long[] newsIds) throws ServiceException;


}