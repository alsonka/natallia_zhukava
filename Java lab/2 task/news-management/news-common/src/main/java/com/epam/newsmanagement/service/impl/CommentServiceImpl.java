package com.epam.newsmanagement.service.impl;



import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;



/**
 * The Class CommentServiceImpl implements method from CommentService interface.
 * 
 * @author Natallia_Zhukava
 */
public class CommentServiceImpl implements CommentService {

	private CommentDAO commentDao;

	/**
	 * Sets the commentDAO.
	 *
	 * @param commentDao the new commentDAO
	 */
	public void setCommentDao(CommentDAO commentDao) {
		this.commentDao = commentDao;
	}


	/**
	 * Instantiates a new comment service implementation object.
	 */
	public CommentServiceImpl() {

	}


	/**
	 * The method finds all comments.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.CommentService#findAllComments()
	 */
	@Override
	public List<CommentTO> findAllComments() throws ServiceException {
		List<CommentTO> list = new ArrayList<CommentTO>();
		try {
			list = commentDao.findAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return list;
	}


	/**
	 * The method finds all comments attached to given news.
	 *
	 * @param newsId the news id
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.CommentService#findCommentsByNews(long)
	 */
	@Override
	public List<CommentTO> findCommentsByNews(long newsId) throws ServiceException {
		List<CommentTO> list = new ArrayList<CommentTO>();
		try {
			list = commentDao.findByNewsId(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return list;
	}

	
	/**
	 * The method finds comment by given identifier.
	 *
	 * @param id the id
	 * @return the commentTO
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.CommentService#findCommentById(long)
	 */
	@Override
	public CommentTO findCommentById(long id) throws ServiceException {
		CommentTO comment = null;
		try {
			comment = commentDao.findById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return comment;
	}


	/**
	 *  
	 * The method saves comment with current timestamp as "creation date".
	 *
	 * @param comment the comment
	 * @return the commentTO with generated identifier
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.CommentService#saveComment(com.epam.newsmanagement.entity.CommentTO)
	 */
	@Override
	public long saveComment(CommentTO comment) throws ServiceException {
		
		Calendar calendar = Calendar.getInstance();
		comment.setCreationDate(calendar.getTime());
		long id=0;
		try {
			
			id = commentDao.save(comment);
			
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return id;

	}


	/**
	 * The method edits comment info.
	 *
	 * @param comment the comment
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.CommentService#editComment(com.epam.newsmanagement.entity.CommentTO)
	 */
	@Override
	public void editComment(CommentTO comment) throws ServiceException {

		try {

			commentDao.update(comment);

		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}


	/**
	 * The method deletes comment with given identifier.
	 *
	 * @param id the id
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.CommentService#deleteComment(long)
	 */
	@Override
	public void deleteComment(long id) throws ServiceException {
		
		try {

			commentDao.delete(id);

		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	
	}


	/**
	 * The method deletes all comments attached to given news.
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.CommentService#deleteCommentsByNews(long)
	 */
	@Override
	public void deleteCommentsByNews(long newsId) throws ServiceException {
		try {

			commentDao.deleteByNewsId(newsId);

		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	}


	/**
	 * The method deletes all comments attached to given list of news.
	 *
	 * @param newsIds the news ids
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.CommentService#deleteCommentsByNews(long[])
	 */
	@Override
	public void deleteCommentsByNews(long[] newsIds) throws ServiceException {
		try {

			commentDao.deleteByNewsId(newsIds);

		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	}

	

}
