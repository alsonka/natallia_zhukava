package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.ServiceException;


/**
 * The Interface TagService contains the service methods which should work with {@code TagTO} objects.
 */
public interface TagService {

	/**
	 * The method finds all tags.
	 *
	 * @return the list
	 * @throws ServiceException 
	 */
	List<TagTO> findAllTags() throws ServiceException;

	/**
	 * The method finds tag by given id.
	 *
	 * @param tagId the id
	 * @return the tagTO
	 * @throws ServiceException 
	 */
	TagTO findTagById(long tagId) throws ServiceException;

	/**
	 * The method finds tags by given news.
	 *
	 * @param newsId the news id
	 * @return the list
	 * @throws ServiceException 
	 */
	List<TagTO> findTagsByNews(long newsId) throws ServiceException;

	/**
	 * The method saves the tag
	 *
	 * @param tag the tag
	 * @return the tagTO
	 * @throws ServiceException 
	 */
	TagTO saveTag(TagTO tag) throws ServiceException;

	/**
	 * The method edits the tag.
	 *
	 * @param tag the tag
	 * @throws ServiceException 
	 */
	void editTag(TagTO tag) throws ServiceException;

	/**
	 * The method deletes the tag.
	 *
	 * @param id the id
	 * @throws ServiceException 
	 */
	void deleteTag(long id) throws ServiceException;

}