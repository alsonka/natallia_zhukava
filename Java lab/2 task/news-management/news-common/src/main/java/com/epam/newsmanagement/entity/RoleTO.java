package com.epam.newsmanagement.entity;

/**
 * The class Role contains data from table Roles.
 *
 * @author Natallia_Zhukava
 */
public class RoleTO extends Entity {


	private static final long serialVersionUID = 1L;
	private String name;

	/**
	 * Instantiates a new role.
	 */
	public RoleTO() {

	}

	/**
	 * Instantiates a new role.
	 *
	 * @param id
	 *            the id
	 * @param name
	 *            the name
	 */
	public RoleTO(long id, String name) {
		super(id);
		this.name = name;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof RoleTO))
			return false;
		RoleTO other = (RoleTO) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
