package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.DAOException;



/**
 * The Interface NewsDAO contains the methods which should work with {@code News} database table and tables {@code News_Tag} and {@code News_Author}.
 * 
 * @author Natallia_Zhukava
 */
public interface NewsDAO extends CommonDAO<NewsTO>{

	/**
	 * The method finds all news which attached to given tag.
	 *
	 * @param tagId the tag id
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<NewsTO> findByTag(long tagId) throws DAOException;

	/**
	 * The method finds all news which attached to given author.
	 *
	 * @param authorId the author id
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<NewsTO> findByAuthor(long authorId) throws DAOException;

	/**
	 * The method finds all news which match given criteria.
	 *
	 * @param obj the SearchCriteria obj
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<NewsTO> findByCriteria(SearchCriteriaVO obj) throws DAOException;

	/**
	 * The method finds news on current page.
	 *
	 * @param firstOnPage the last number on previous page
	 * @param lastOnPage the last number on page
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<NewsTO> findNewsOnCurrentPage(int firstOnPage, int lastOnPage) throws DAOException;
	/**
	 * The method finds news on current page of search results.
	 *
	 * @param lastOnPrevPage the last number on previous page
	 * @param lastOnPage the last number on page
	 * @param obj - the search criteria which news are filtered
	 * @return the list
	 * @throws DAOException the DAO exception
	 */	
	List<NewsTO> findNewsOnCurrentPage(int lastOnPrevPage, int lastOnPage, SearchCriteriaVO obj) throws DAOException;

	/**
	 * The method attaches author to news.
	 *
	 * @param newsId the news id
	 * @param authorId the author id
	 * @throws DAOException the DAO exception
	 */
	void attachAuthorToNews(long newsId, long authorId) throws DAOException;

	/**
	 * The method updates author of given news.
	 *
	 * @param newsId the news id
	 * @param authorId the author id
	 * @throws DAOException the DAO exception
	 */
	void updateNewsAuthor(long newsId, long authorId) throws DAOException;

	/**
	 * The method attaches tag to news.
	 *
	 * @param newsId the news id
	 * @param tagId the tag id
	 * @throws DAOException the DAO exception
	 */
	void attachTagToNews(long newsId, long tagId) throws DAOException;

	/**
	 * The method detaches all tags which were attached to given news.
	 *
	 * @param newsId the news id
	 * @throws DAOException the DAO exception
	 */
	void detachNewsTags(long newsId) throws DAOException;
	
	/**
	 * The method detaches the author from the given news.
	 *
	 * @param newsId the news id
	 * @throws DAOException the DAO exception
	 */
	void detachNewsAuthor(long newsId) throws DAOException;

	/**
	 * The method attaches list of tags to given news.
	 *
	 * @param newsId the news id
	 * @param list the list
	 * @throws DAOException the DAO exception
	 */
	void attachTagsToNews(long newsId, List<TagTO> list) throws DAOException;
	
	/**
	 * The method counts quantity of news.
	 *
	 * @return the long the quantity of news
	 * @throws DAOException the DAO exception
	 */
	long countNews() throws DAOException;

	
	/**
	 * The method finds previous and next news by given news id.
	 *
	 * @param newsId the news id
	 * @return array with previous and next news id;
	 * @throws DAOException the DAO exception
	 */
	long[] findPreviousAndNextNews(long newsId) throws DAOException;
	
	/**
	 * The method finds previous and next news by given news id in list of search results.
	 *
	 * @param newsId the news id
	 * @param obj the searchCriteria object
	 * @return array with   previous and next news id
	 * @throws DAOException the DAO exception
	 */
	long[] findPreviousAndNextNews(long newsId, SearchCriteriaVO obj) throws DAOException;

	/**
	 * Count news which are found by given search criteria .
	 *
	 * @param obj the obj
	 * @return number of news
	 * @throws DAOException the DAO exception
	 */
	long countNews(SearchCriteriaVO obj) throws DAOException;
	
	/**
	 * Deletes list of news.
	 *
	 * @param newsIds the news ids
	 * @throws DAOException the DAO exception
	 */
	void delete(long[] newsIds) throws DAOException;
	
	/**
	 * Detaches authors from list of news.
	 *
	 * @param newsIds the news ids
	 * @throws DAOException the DAO exception
	 */
	void detachNewsAuthor(long[] newsIds) throws DAOException;
	
	/**
	 * Detaches  tags from list of news.
	 *
	 * @param newsIds the news ids
	 * @throws DAOException the DAO exception
	 */
	void detachNewsTags(long[] newsIds) throws DAOException;



}