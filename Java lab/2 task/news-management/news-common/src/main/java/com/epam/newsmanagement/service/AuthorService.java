package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.ServiceException;


/**
 * The Interface AuthorService contains the service methods which should work with {@code AuthorTO} objects.
 * 
 * @author Natallia_Zhukava
 */
public interface AuthorService {
	
	/**
	 * The method returns list of all authors from database.
	 *
	 * @return the list of authors
	 * @throws ServiceException the service exception
	 */
	List<AuthorTO> findAllAuthors() throws ServiceException;
	
	/**
	 * The method returns list of actual authors
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	List<AuthorTO> findActualAuthors() throws ServiceException;
	
	/**
	 * The method gets information about concrete author from database by identifier.
	 *
	 * @param authorId identifier of author
	 * @return {@code Author} entity
	 * @throws ServiceException the service exception
	 */

	AuthorTO findAuthorById(long authorId) throws ServiceException;
    
    /**
     * The method gets information about concrete author from database by identifier of news.
     *
     * @param newsId identifier of news
     * @return {@code Author} entity
     * @throws ServiceException the service exception
     */
	AuthorTO findAuthorByNews(long newsId) throws ServiceException;
	
	/**
	 * The method saves information about given author to database.
	 *
	 * @param author the author
	 * @return {@code Author} entity
	 * @throws ServiceException the service exception
	 */

	AuthorTO saveAuthor(AuthorTO author) throws ServiceException;
	
	/**
	 * The method edits information about given author in database.
	 *
	 * @param author the author
	 * @throws ServiceException the service exception
	 */

	void editAuthor(AuthorTO author) throws ServiceException;
    
    /**
     * The method sets status "expired" to given author.
     *
     * @param authorId the author identifier
     * @return {@code Author} entity
     * @throws ServiceException the service exception
     */
	AuthorTO makeExpired(long authorId) throws ServiceException;
	
	/**
	 * The method deletes given author
	 * Not planned to use!.
	 *
	 * @param authorId the author id
	 * @throws ServiceException the service exception
	 */

	void deleteAuthor(long authorId) throws ServiceException;

}