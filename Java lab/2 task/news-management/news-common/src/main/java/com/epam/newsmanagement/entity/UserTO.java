package com.epam.newsmanagement.entity;

/**
 * The class User contains data from database table Users.
 *
 * @author Natallia_Zhukava
 */
public class UserTO extends Entity {

	
	private static final long serialVersionUID = 1L;
	private String name;
	private String login;
	private String password;

	/**
	 * Instantiates a new user.
	 */
	public UserTO() {

	}

	/**
	 * Instantiates a new user.
	 *
	 * @param id
	 *            the id
	 * @param name
	 *            the name
	 * @param login
	 *            the login
	 * @param password
	 *            the password
	 */
	public UserTO(long id, String name, String login, String password) {
		super(id);
		this.name = name;
		this.login = login;
		this.password = password;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the login.
	 *
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * Sets the login.
	 *
	 * @param login
	 *            the new login
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password
	 *            the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof UserTO))
			return false;
		UserTO other = (UserTO) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserTO [id=" + getId() + " name=" + name + ", login=" + login + ", password=" + password + "]";
	}

}
