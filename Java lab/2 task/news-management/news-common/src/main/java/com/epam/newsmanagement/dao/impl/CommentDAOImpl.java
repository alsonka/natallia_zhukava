package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.DBUtils;


/**
 * The Class CommentDAOImpl implements methods from CommentDAO interface.
 */
public class CommentDAOImpl implements CommentDAO {

	private static final String FIELD_COMMENT_ID = "comment_id";
	private static final String TIMEZONE = "UTC";
	private static final String SQL_FIND_ALL = "SELECT comment_id, news_id, comment_text, creation_date FROM comments";
	private static final String SQL_FIND_BY_ID = "SELECT comment_id, news_id, comment_text, creation_date FROM comments WHERE comment_id=? ";
	private static final String SQL_FIND_BY_NEWS_ID = "SELECT comment_id, news_id, comment_text, creation_date FROM comments WHERE news_id=? ORDER BY creation_date";
	private static final String SQL_DELETE = "DELETE FROM comments WHERE comment_id=?";
	private static final String SQL_DELETE_BY_NEWS = "DELETE FROM comments WHERE news_id=?";
	private static final String SQL_CREATE = "INSERT INTO comments (comment_id, news_id, comment_text, creation_date) VALUES"
			+ "(id_sequence.nextval, ?, ?, ? at time zone 'UTC')";
	private static final String SQL_UPDATE = "UPDATE comments SET news_id=?, comment_text=? WHERE comment_id=?";

	/**
	 * Instantiates a new comment dao impl.
	 */
	public CommentDAOImpl() {
		super();

	}

	private DataSource datasource;

	/**
	 * Sets the datasource.
	 *
	 * @param datasource
	 *            the new datasource
	 */
	public void setDatasource(DataSource datasource) {
		this.datasource = datasource;
	}

	/**
	 * The method parses given ResultSet object and returns {@code CommentTO}
	 * object
	 * 
	 * @param res
	 * @return CommentTO object
	 * @throws SQLException
	 */
	private CommentTO parseResultSet(ResultSet res) throws SQLException {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE)); 
				
		CommentTO comment = new CommentTO();
		comment.setId(res.getLong(1));
		comment.setNewsId(res.getLong(2));
		comment.setText(res.getString(3));
		comment.setCreationDate(res.getTimestamp(4, cal));
		return comment;
	}

	/**
	 * The method finds all comments from database table.
	 *
	 * @return the list
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#findAll()
	 */
	@Override
	public List<CommentTO> findAll() throws DAOException {
		List<CommentTO> list = new ArrayList<CommentTO>();
		Statement st = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			st = con.createStatement();
			res = st.executeQuery(SQL_FIND_ALL);
			while (res.next()) {
				CommentTO comment = parseResultSet(res);
				list.add(comment);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(st);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return list;

	}

	/**
	 * The method finds the comment by given id.
	 *
	 * @param id
	 *            the id
	 * @return the comment to
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#findById(long)
	 */
	@Override
	public CommentTO findById(long id) throws DAOException {
		CommentTO comment = null;
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_FIND_BY_ID);
			pst.setLong(1, id);
			res = pst.executeQuery();
			while (res.next()) {
				comment = parseResultSet(res);

			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return comment;
	}

	/**
	 * The method finds all comments attached to given news.
	 *
	 * @param id
	 *            the id
	 * @return the list
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.CommentDAO#findByNewsId(long)
	 */
	@Override
	public List<CommentTO> findByNewsId(long id) throws DAOException {
		List<CommentTO> list = new ArrayList<CommentTO>();
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_FIND_BY_NEWS_ID);
			pst.setLong(1, id);
			res = pst.executeQuery();
			while (res.next()) {
				CommentTO comment = parseResultSet(res);
				list.add(comment);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return list;

	}

	/**
	 * The method saves information from given {@code CommmentTO} object to
	 * database table.
	 *
	 * @param entity
	 *            the entity
	 * @return the long
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#save(com.epam.newsmanagement.entity.Entity)
	 */
	@Override
	public long save(CommentTO entity) throws DAOException {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE)); 
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		long id = 0;
		try {
			con = DataSourceUtils.getConnection(datasource);
			String[] genKeys = { FIELD_COMMENT_ID };
			pst = con.prepareStatement(SQL_CREATE, genKeys);
			pst.setLong(1, entity.getNewsId());
			pst.setString(2, entity.getText());
			pst.setTimestamp(3, new Timestamp(entity.getCreationDate().getTime()), cal);
			pst.executeUpdate();
			res = pst.getGeneratedKeys();
			res.next();
			id = res.getLong(1);

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);

			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}
		return id;
	}

	/**
	 * The method updates information about given comment in database.
	 *
	 * @param entity
	 *            the entity
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#update(com.epam.newsmanagement.entity.Entity)
	 */
	@Override
	public void update(CommentTO entity) throws DAOException {
		PreparedStatement pst = null;
		Connection con = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_UPDATE);
			pst.setLong(3, entity.getId());
			pst.setLong(1, entity.getNewsId());
			pst.setString(2, entity.getText());
			pst.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}
	}

	/**
	 * The method deletes comment with given id from database.
	 *
	 * @param id
	 *            the id
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#delete(long)
	 */
	@Override
	public void delete(long id) throws DAOException {

		PreparedStatement pst = null;
		Connection con = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_DELETE);
			pst.setLong(1, id);
			pst.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

	}

	/**
	 * The method deletes all comments attached to given news.
	 *
	 * @param newsId
	 *            the news id
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.CommentDAO#deleteByNewsId(long)
	 */
	@Override
	public void deleteByNewsId(long newsId) throws DAOException {
		PreparedStatement pst = null;
		Connection con = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_DELETE_BY_NEWS);
			pst.setLong(1, newsId);
			pst.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

	}

	/**
	 * 
	 * The method deletes all comments attached to given list of news.
	 *
	 * @param newsIds
	 *            the news ids
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.CommentDAO#deleteByNewsId(long[])
	 */
	@Override
	public void deleteByNewsId(long[] newsIds) throws DAOException {
		PreparedStatement pst = null;
		Connection con = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			con.setAutoCommit(false);
			pst = con.prepareStatement(SQL_DELETE_BY_NEWS);
			for (long newsId : newsIds) {
				pst.setLong(1, newsId);
				pst.addBatch();
			}
			pst.executeBatch();
			con.commit();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);

		}

	}
}