package com.epam.newsmanagement.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;

/**
 * The Class AuthorServiceImpl implements methods from AuthorService interface.
 * 
 * @author Natallia_Zhukava
 */
public class AuthorServiceImpl implements AuthorService {


	private AuthorDAO authorDao;
	
	

	/**
	 * Sets the authorDAO
	 *
	 * @param authorDao the new authorDAO
	 */
	public void setAuthorDao(AuthorDAO authorDao) {
		this.authorDao = authorDao;
	}


	/**
	 * Instantiates a new author service implementation object.
	 */
	public AuthorServiceImpl() {
	}

	/**
	 * The method finds all authors.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.AuthorService#findAllAuthors()
	 */
	@Override
	public List<AuthorTO> findAllAuthors() throws ServiceException {
		List<AuthorTO> list = new ArrayList<AuthorTO>();
		try {
			list = authorDao.findAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return list;
	}

	
	/**
	 * The method finds author by given identifier.
	 *
	 * @param authorId the author id
	 * @return the authorTO
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.AuthorService#findAuthorById(long)
	 */
	@Override
	public AuthorTO findAuthorById(long authorId) throws ServiceException {
		AuthorTO author = null;
		try {

			author = authorDao.findById(authorId);

		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return author;
	}

	
	/**
	 * The method finds the author of given news.
	 *
	 * @param newsId the news id
	 * @return the author to
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.AuthorService#findAuthorByNews(long)
	 */
	@Override
	public AuthorTO findAuthorByNews(long newsId) throws ServiceException {
		AuthorTO author = null;
		try {

			author = authorDao.findByNews(newsId);

		} catch (DAOException e) {
				throw new ServiceException(e);
		}
		return author;
	}

	
	/**
	 * The method saves new author.
	 *
	 * @param author the author
	 * @return the authorTO with generated identifier
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.AuthorService#saveAuthor(com.epam.newsmanagement.entity.AuthorTO)
	 */
	@Override
	public AuthorTO saveAuthor(AuthorTO author) throws ServiceException {

		try {

		long id = authorDao.save(author);
		author.setId(id);	

		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return author;
	}

	/**
	 * The method edits the information about author.
	 *
	 * @param author the author
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.AuthorService#editAuthor(com.epam.newsmanagement.entity.AuthorTO)
	 */
	@Override
	public void editAuthor(AuthorTO author) throws ServiceException {
		try {
			authorDao.update(author);	
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
	}

	
	/**
	 * The method sets status "expired" to given author .
	 *
	 * @param author the author
	 * @return the authorTO with not null field "expired"
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.AuthorService#makeExpired(com.epam.newsmanagement.entity.AuthorTO)
	 */
	@Transactional
	@Override
	public AuthorTO makeExpired(long authorId) throws ServiceException {
		Calendar calendar = Calendar.getInstance();
		Date now = calendar.getTime();
		Timestamp currentTimestamp = new Timestamp(now.getTime());
		AuthorTO author = null;
		try {
			author = authorDao.findById(authorId);
			author.setExpiredDate(currentTimestamp);
			authorDao.update(author);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
		return author;
	}

	/**
	 * The method deletes author
	 * Not planned to use!
	 * @see com.epam.newsmanagement.service.AuthorService#deleteAuthor(long)
	 */
	@Override
	@Transactional
	public void deleteAuthor(long authorId) throws ServiceException {

		try {
		authorDao.delete(authorId);
		authorDao.detachAuthorNews(authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * The method finds authors with actual status
	 * 
	 * @see com.epam.newsmanagement.service.AuthorService#findActualAuthors()
	 */

	@Override
	public List<AuthorTO> findActualAuthors() throws ServiceException {
		List<AuthorTO> list = new ArrayList<AuthorTO>();
		
		try {
			list = authorDao.findActualAuthors();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
		return list;
		
	}

	

}
