package com.epam.newsmanagement.service.impl;

import org.apache.log4j.Logger;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.UserService;


/**
 * The Class UserServiceImpl implements methods from UserService interface.
 */
public class UserServiceImpl implements UserService {
	
	private UserDAO userDao;
	

	/**
	 * Sets the user dao.
	 *
	 * @param userDao the new user dao
	 */
	public void setUserDao(UserDAO userDao) {
		this.userDao = userDao;
	}
	
	static Logger logger = Logger.getLogger(TagServiceImpl.class);


	/**
	 * Find user name by login.
	 *
	 * @param login the login
	 * @return the string - name of user
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.UserService#findUserNameByLogin(java.lang.String)
	 */
	@Override
	public String findUserNameByLogin(String login) throws ServiceException {
		String username = null;
		try {
			username = userDao.findUserByLogin(login).getName();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return username;
	}

}
