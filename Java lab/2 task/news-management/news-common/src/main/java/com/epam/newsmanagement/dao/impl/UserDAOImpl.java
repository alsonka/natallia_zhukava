package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.entity.UserTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.DBUtils;


/**
 * The Class UserDAOImpl implements methods from UserDAO interface.
 * 
 * @author Natallia_Zhukava
 */
public class UserDAOImpl implements UserDAO {

	private static final String SQL_FIND_BY_LOGIN = "SELECT user_id, user_name, login, password FROM users WHERE login=?";
	
	
	private DataSource datasource;
	
	/**
	 * Sets the datasource.
	 *
	 * @param datasource the new datasource
	 */
	public void setDatasource(DataSource datasource) {
		this.datasource = datasource;
	}

	/**
	 * Finds the user by login.
	 *
	 * @param login the login
	 * @return the User entity
	 * @throws DAOException the DAO exception
	 * @see com.epam.newsmanagement.dao.UserDAO#findUserByLogin(java.lang.String)
	 */
	@Override
	public UserTO findUserByLogin(String login) throws DAOException {
		UserTO user = new UserTO();
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_FIND_BY_LOGIN);
			pst.setString(1, login);
			res = pst.executeQuery();
			while (res.next()) {
				user.setId(res.getLong(1));
				user.setName(res.getString(2));
				user.setLogin(res.getString(3));
				user.setPassword(res.getString(4));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}
		return user;
		
	}

}
