package com.epam.newsmanagement.entity;

import java.util.Date;


/**
 * The class Comment contains data from table Comments.
 *
 * @author Natallia_Zhukava
 */
public class CommentTO extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long newsId;
	private String text;
	private Date creationDate;

	/**
	 * Instantiates a new comment.
	 */
	public CommentTO() {
	}

	/**
	 * Instantiates a new comment.
	 *
	 * @param id
	 *            the id
	 * @param newsId
	 *            the news id
	 * @param text
	 *            the text
	 * @param creationDate
	 *            the creation date
	 */
	public CommentTO(long id, long newsId, String text, Date creationDate) {
		super(id);
		this.newsId = newsId;
		this.text = text;
		this.creationDate = creationDate;
	}

	

	/**
	 * Instantiates a new comment to.
	 *
	 * @param newsId the news id
	 * @param text the text
	 * @param creationDate the creation date
	 */
	public CommentTO(long newsId, String text, Date creationDate) {
		super();
		this.newsId = newsId;
		this.text = text;
		this.creationDate = creationDate;
	}

	/**
	 * Gets the news id.
	 *
	 * @return the news id
	 */
	public long getNewsId() {
		return newsId;
	}

	/**
	 * Sets the news id.
	 *
	 * @param newsId
	 *            the new news id
	 */
	public void setNewsId(long newsId) {
		this.newsId = newsId;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * Sets the text.
	 *
	 * @param text
	 *            the new text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate
	 *            the new creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public String toString() {
		return "CommentTO [id="+ getId()+", newsId=" + newsId + ", text=" + text + ", creationDate=" + creationDate + "]";
	}

	/* (non-Javadoc)
	 * @see by.zhukova.newsapp.entity.Entity#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + (int) (newsId ^ (newsId >>> 32));
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see by.zhukova.newsapp.entity.Entity#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CommentTO))
			return false;
		CommentTO other = (CommentTO) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (newsId != other.newsId)
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

}
