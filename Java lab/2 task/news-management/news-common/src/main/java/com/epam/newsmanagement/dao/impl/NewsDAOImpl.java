package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.DBUtils;


/**
 * The Class NewsDAOImpl implements NewsDAO interface and contains methods which
 * work with database table News, News_Tag, News_Author.
 */
public class NewsDAOImpl implements NewsDAO {

	private static final String FIELD_NEWS_ID = "news_id";
	private static final String TIMEZONE = "UTC";
	private static final String SQL_GROUP = " GROUP BY news.news_id, news.title, news.short_text, news.full_text, "
			+ "news.creation_date, news.modification_date ";
	private static final String SQL_JOIN_AUTHOR = " JOIN news_author ON news.news_id = news_author.news_id ";
	private static final String SQL_JOIN_TAG = "JOIN news_tag ON news.news_id = news_tag.news_id ";

	private static final String SQL_ORDER = "ORDER BY COUNT(comments.comment_id) DESC, news.modification_date DESC";

	private static final String SQL_FIND_ALL = "SELECT news.news_id, news.title, news.short_text, news.full_text, "
			+ "news.creation_date, news.modification_date, COUNT(comments.comment_id) as commnum FROM news LEFT JOIN comments ON news.news_id = comments.news_id "
			+ SQL_GROUP + SQL_ORDER;
	private static final String SQL_FIND_BY_ID = "SELECT news_id, title, short_text, full_text, "
			+ "creation_date, modification_date FROM news WHERE news_id=?";
	private static final String SQL_CREATE = "INSERT INTO news (news_id, title, short_text, full_text, "
			+ "creation_date, modification_date) VALUES(id_sequence.nextval, ?, ?, ?, ?, ?)";
	private static final String SQL_CREATE_AUTHOR_NEWS = "INSERT INTO news_author (news_id, author_id) VALUES (?, ?)";
	private static final String SQL_UPDATE_AUTHOR_NEWS = "UPDATE news_author SET author_id=? WHERE news_id=?";
	private static final String SQL_CREATE_TAG_NEWS = "INSERT INTO news_tag (news_id, tag_id) VALUES (?, ?)";
	private static final String SQL_DELETE_TAG_NEWS = "DELETE FROM news_tag WHERE news_id=?";

	private static final String SQL_DELETE = "DELETE FROM news WHERE news_id=?";
	private static final String SQL_UPDATE = "UPDATE news SET title=?, short_text=?, full_text=?, "
			+ " modification_date=? WHERE news_id=?";
	private static final String SQL_FIND_BY_TAG = "SELECT news.news_id, news.title, news.short_text, news.full_text, "
			+ "news.creation_date, news.modification_date FROM news JOIN news_tag "
			+ "ON news.news_id=news_tag.news_id " + "LEFT JOIN comments ON news.news_id = comments.news_id "
			+ " WHERE news_tag.tag_id=?" + SQL_GROUP + SQL_ORDER;

	private static final String SQL_FIND_BY_AUTHOR = "SELECT news.news_id, news.title, news.short_text, news.full_text, "
			+ "news.creation_date, news.modification_date, COUNT(comments.comment_id) as commnum FROM news JOIN news_author "
			+ "ON news.news_id=news_author.news_id " + "LEFT JOIN comments ON news.news_id = comments.news_id "
			+ "WHERE news_author.author_id=?" + SQL_GROUP + SQL_ORDER;

	private static final String SQL_SEARCH = "SELECT news.news_id, news.title, news.short_text, news.full_text, news.creation_date, news.modification_date, "
			+ "COUNT(comments.comment_id) as commnum  FROM news "
			+ "LEFT JOIN comments ON news.news_id = comments.news_id ";
	private static final String SQL_DELETE_AUTHOR_NEWS = "DELETE FROM news_author WHERE news_id=?";
	private static final String SQL_COUNT = "SELECT COUNT(*) FROM news";
	private static final String SQL_COUNT_FROM = "SELECT COUNT(*) FROM (";
	private static final String SQL_FIND_PREV_NEXT = "SELECT  prev_id, next_id FROM ("
			+ "SELECT news_id, title, short_text, full_text, creation_date, modification_date, "
			+ "LAG (news_id, 1, 0) OVER (ORDER BY commnum DESC, modification_date DESC) as prev_id,"
			+ "LEAD(news_id, 1, 0) OVER (ORDER BY commnum DESC, modification_date DESC) AS next_id " + "FROM (";
	private static final String SQL_FIND_ON_PAGE = "SELECT * FROM ( SELECT  a.*, rownum rnum FROM("
			+ SQL_FIND_ALL	
		    +") a WHERE rownum <=?) WHERE rnum >?";

	/**
	 * Instantiates a new news dao impl.
	 */
	public NewsDAOImpl() {
		super();

	}

	private DataSource datasource;

	/**
	 * Sets the datasource.
	 *
	 * @param datasource
	 *            the new datasource
	 */
	public void setDatasource(DataSource datasource) {
		this.datasource = datasource;
	}

	/**
	 * The method parses given ResultSet object and returns {@code NewsTO}
	 * object
	 * 
	 * @param res
	 * @return
	 * @throws SQLException
	 */
	private NewsTO parseResultSet(ResultSet res) throws SQLException {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE));
		NewsTO news = new NewsTO();
		news.setId(res.getLong(1));
		news.setTitle(res.getString(2));
		news.setShortText(res.getString(3));
		news.setFullText(res.getString(4));
		news.setCreationDate(res.getTimestamp(5, calendar));
		news.setModificationDate(res.getDate(6));
		return news;
	}

	/**
	 * The method finds all news from database table sorted by comments number
	 * and modification date.
	 *
	 * @return the list
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#findAll()
	 */
	@Override
	public List<NewsTO> findAll() throws DAOException {
		List<NewsTO> list = new ArrayList<NewsTO>();
		Statement st = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			st = con.createStatement();
			res = st.executeQuery(SQL_FIND_ALL);
			while (res.next()) {
				NewsTO news = parseResultSet(res);
				list.add(news);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(st);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return list;

	}

	/**
	 * The method finds all news with given tag sorted by comments number and
	 * modification date.
	 *
	 * @param tagId
	 *            the tag id
	 * @return the list
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.NewsDAO#findByTag(long)
	 */
	@Override
	public List<NewsTO> findByTag(long tagId) throws DAOException {
		List<NewsTO> list = new ArrayList<NewsTO>();
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_FIND_BY_TAG);
			pst.setLong(1, tagId);
			res = pst.executeQuery();
			while (res.next()) {
				NewsTO news = parseResultSet(res);
				list.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return list;

	}

	/**
	 * 
	 * The method finds all news with given author sorted by comments number and
	 * modification date.
	 *
	 * @param authorId
	 *            the author id
	 * @return the list
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.NewsDAO#findByAuthor(long)
	 */
	@Override
	public List<NewsTO> findByAuthor(long authorId) throws DAOException {
		List<NewsTO> list = new ArrayList<NewsTO>();
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_FIND_BY_AUTHOR);
			pst.setLong(1, authorId);
			res = pst.executeQuery();
			while (res.next()) {
				NewsTO news = parseResultSet(res);
				list.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return list;
	}

	/**
	 * The method finds the news by given identifier.
	 *
	 * @param id
	 *            the id
	 * @return the newsTO
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#findById(long)
	 */
	@Override
	public NewsTO findById(long id) throws DAOException {

		NewsTO news = null;
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_FIND_BY_ID);
			pst.setLong(1, id);
			res = pst.executeQuery();
			while (res.next()) {
				news = parseResultSet(res);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return news;
	}

	// The method generates search query for search method
	private String generateSearchQuery(SearchCriteriaVO obj) {
		StringBuilder searchQuery = new StringBuilder(SQL_SEARCH);
		long author = obj.getAuthorId();
		List<Long> tagList = obj.getTagIdList();

		if (author != 0) {
			searchQuery.append(SQL_JOIN_AUTHOR);

			if (tagList != null) {
				searchQuery.append(SQL_JOIN_TAG);
			}
			searchQuery.append(" WHERE news_author.author_id=?");
			if ((tagList != null) && (tagList.size() != 0)) {
				searchQuery.append(" AND news_tag.tag_id IN(?");

				for (int i = 0; i < tagList.size() - 1; i++) {
					searchQuery.append(", ?");
				}
				searchQuery.append(")");

			}
			searchQuery.append(SQL_GROUP);

		}

		else {
			if ((tagList != null) && (tagList.size() != 0)) {
				searchQuery.append(SQL_JOIN_TAG).append(" WHERE news_tag.tag_id IN(?");
				for (int i = 0; i < tagList.size() - 1; i++) {
					searchQuery.append(", ?");
				}
				searchQuery.append(")").append(SQL_GROUP);

			}
		}

		searchQuery.append(SQL_ORDER);

		return searchQuery.toString();

	}
	//The method prepares statement for query with search criteria
	private PreparedStatement prepareSearchStatement(Connection con, String query, SearchCriteriaVO obj) throws SQLException {
		long author = obj.getAuthorId();
		List<Long> tagList = obj.getTagIdList();
		PreparedStatement pst = con.prepareStatement(query);
		if (author != 0) {
			pst.setLong(1, author);

			if (tagList != null) {
				for (int i = 0; i < tagList.size(); i++) {
					pst.setLong((i + 2), tagList.get(i));
				}
			}
		} else {
			if (tagList != null) {
				for (int i = 0; i < tagList.size(); i++) {
					pst.setLong((i + 1), tagList.get(i));
				}
			}
		}
		
		return pst;
		
		
	}
	//The method prepares statement for query with search criteria and given news id
	private PreparedStatement prepareSearchStatement(Connection con, String query, SearchCriteriaVO obj, long newsId) throws SQLException {
		long author = obj.getAuthorId();
		List<Long> tagList = obj.getTagIdList();
		PreparedStatement pst = con.prepareStatement(query);
		if (author != 0) {
			pst.setLong(1, author);

			if (tagList != null) {
				for (int i = 0; i < tagList.size(); i++) {
					pst.setLong((i + 2), tagList.get(i));
				}
				pst.setLong(tagList.size() + 2, newsId);
			} else {
				pst.setLong(2, newsId);
			}
		} else {
			if (tagList != null) {
				for (int i = 0; i < tagList.size(); i++) {
					pst.setLong((i + 1), tagList.get(i));
				}
				pst.setLong(tagList.size() + 1, newsId);
			}
		}
		
		return pst;
		
		
	}
	//The method prepares statement for query with search criteria and numbers of news
	private PreparedStatement prepareSearchStatement(Connection con, String query, SearchCriteriaVO obj, int lastOnPrevPage, int lastOnPage) throws SQLException {
		long author = obj.getAuthorId();
		List<Long> tagList = obj.getTagIdList();
		PreparedStatement pst = con.prepareStatement(query);
		if (author != 0) {
			pst.setLong(1, author);

			if (tagList != null) {
				for (int i = 0; i < tagList.size(); i++) {
					pst.setLong((i + 2), tagList.get(i));
				}
				pst.setLong(tagList.size() + 2, lastOnPage);
				pst.setLong(tagList.size()+ 3, lastOnPrevPage);
			}
			else {
				pst.setLong(2, lastOnPage);
				pst.setLong(3, lastOnPrevPage);
			}
		} else {
			if (tagList != null) {
				for (int i = 0; i < tagList.size(); i++) {
					pst.setLong((i + 1), tagList.get(i));
				}
				pst.setLong(tagList.size() + 1, lastOnPage);
				pst.setLong(tagList.size() + 2, lastOnPrevPage);
			}
		}
		
		return pst;
		
		
	}

	/**
	 * The method finds the news by given search criteria.
	 *
	 * @param obj
	 *            the SearchCriteria obj
	 * @return the list
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.NewsDAO#findByCriteria(com.epam.newsmanagement.entity.SearchCriteriaVO)
	 */
	@Override
	public List<NewsTO> findByCriteria(SearchCriteriaVO obj) throws DAOException {
		
		List<NewsTO> list = new ArrayList<NewsTO>();
		
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		if ((obj!=null) && (obj.getAuthorId()!=0) || (obj.getTagIdList()!=null)) {
		String query = generateSearchQuery(obj);

		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = prepareSearchStatement(con, query, obj);
			res = pst.executeQuery();
			while (res.next()) {
				NewsTO news = parseResultSet(res);
				list.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
			} 
		}
		else {
			list = findAll();
		}

		return list;

	}

	/**
	 * The method deletes the news with given id from database.
	 *
	 * @param id
	 *            the id
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#delete(long)
	 */
	@Override
	public void delete(long id) throws DAOException {

		PreparedStatement pst = null;
		Connection con = null;

		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_DELETE);
			pst.setLong(1, id);
			pst.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

	}

	/**
	 * The method saves information from given {@code NewsTO} object to
	 * database.
	 *
	 * @param entity
	 *            the entity
	 * @return the long identifier of inserted row
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#save(com.epam.newsmanagement.entity.Entity)
	 */
	@Override
	public long save(NewsTO entity) throws DAOException {

		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		long id = 0;
		try {
			con = DataSourceUtils.getConnection(datasource);
			Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE));
			String[] genKeys = { FIELD_NEWS_ID };
			pst = con.prepareStatement(SQL_CREATE, genKeys);
			pst.setString(1, entity.getTitle());
			pst.setString(2, entity.getShortText());
			pst.setString(3, entity.getFullText());
			pst.setTimestamp(4, new Timestamp(entity.getCreationDate().getTime()),calendar);
			pst.setDate(5,  new java.sql.Date(entity.getModificationDate().getTime()), calendar);
			pst.executeUpdate();

			res = pst.getGeneratedKeys();
			res.next();
			id = res.getLong(1);

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return id;
	}

	/**
	 * The method attaches the author to the given news.
	 *
	 * @param newsId
	 *            the news id
	 * @param authorId
	 *            the author id
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.NewsDAO#attachAuthorToNews(long, long)
	 */
	@Override
	public void attachAuthorToNews(long newsId, long authorId) throws DAOException {

		PreparedStatement pst = null;
		Connection con = null;

		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_CREATE_AUTHOR_NEWS);
			pst.setLong(1, newsId);
			pst.setLong(2, authorId);
			pst.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

	}

	/**
	 * The method updates author to given news.
	 *
	 * @param newsId
	 *            the news id
	 * @param authorId
	 *            the author id
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.NewsDAO#updateNewsAuthor(long, long)
	 */
	@Override
	public void updateNewsAuthor(long newsId, long authorId) throws DAOException {

		PreparedStatement pst = null;
		Connection con = null;

		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_UPDATE_AUTHOR_NEWS);
			pst.setLong(1, authorId);
			pst.setLong(2, newsId);

			pst.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

	}

	/**
	 * The method attaches the tag to the given news.
	 *
	 * @param newsId
	 *            the news id
	 * @param tagId
	 *            the tag id
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.NewsDAO#attachTagToNews(long, long)
	 */
	@Override
	public void attachTagToNews(long newsId, long tagId) throws DAOException {

		PreparedStatement pst = null;
		Connection con = null;

		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_CREATE_TAG_NEWS);
			pst.setLong(1, newsId);
			pst.setLong(2, tagId);
			pst.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}
	}

	/**
	 * The method detaches all tags which attached to given news.
	 *
	 * @param newsId
	 *            the news id
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.NewsDAO#detachNewsTags(long)
	 */
	@Override
	public void detachNewsTags(long newsId) throws DAOException {

		PreparedStatement pst = null;
		Connection con = null;

		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_DELETE_TAG_NEWS);
			pst.setLong(1, newsId);
			pst.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

	}

	/**
	 * The method attaches list of tags to given news.
	 *
	 * @param newsId
	 *            the news id
	 * @param list
	 *            the list
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.NewsDAO#attachTagsToNews(long,
	 *      java.util.List)
	 */
	@Override
	public void attachTagsToNews(long newsId, List<TagTO> list) throws DAOException {

		PreparedStatement pst = null;
		Connection con = null;

		try {
			con = DataSourceUtils.getConnection(datasource);
			con.setAutoCommit(false);
			pst = con.prepareStatement(SQL_CREATE_TAG_NEWS);
			for (TagTO tag : list) {
				pst.setLong(1, newsId);
				pst.setLong(2, tag.getId());
				pst.addBatch();

			}
			pst.executeBatch();
			con.commit();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

	}

	/**
	 * The method updates information about news in database.
	 *
	 * @param entity
	 *            the entity
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#update(com.epam.newsmanagement.entity.Entity)
	 */
	@Override
	public void update(NewsTO entity) throws DAOException {

		PreparedStatement pst = null;
		Connection con = null;

		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_UPDATE);
			pst.setLong(5, entity.getId());
			pst.setString(1, entity.getTitle());
			pst.setString(2, entity.getShortText());
			pst.setString(3, entity.getFullText());
			pst.setDate(4,  new java.sql.Date(entity.getModificationDate().getTime()));
			pst.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

	}

	/**
	 * The method detaches author from given news.
	 *
	 * @param newsId
	 *            the news id
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.NewsDAO#detachNewsAuthor(long)
	 */
	@Override
	public void detachNewsAuthor(long newsId) throws DAOException {
		PreparedStatement pst = null;
		Connection con = null;

		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_DELETE_AUTHOR_NEWS);
			pst.setLong(1, newsId);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

	}

	/**
	 * 
	 * The method counts number of all news in database.
	 *
	 * @return the long
	 * @throws DAOException
	 *             the DAO exception
	 * @see com.epam.newsmanagement.dao.NewsDAO#countNews()
	 */
	@Override
	public long countNews() throws DAOException {
		long num = 0;
		Statement st = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			st = con.createStatement();
			res = st.executeQuery(SQL_COUNT);
			while (res.next()) {
				num = res.getLong(1);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(st);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return num;
	}
	
	/**
	 * The method counts all news which were found by search criteria.
	 *
	 * @param obj the searchCriteria obj
	 * @return the number of news
	 * @throws DAOException the DAO exception
	 */
	@Override
	public long countNews(SearchCriteriaVO obj) throws DAOException {
		long num = 0;
		PreparedStatement st = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			String query = SQL_COUNT_FROM + generateSearchQuery(obj) + ")";
			st = prepareSearchStatement(con, query, obj);
			res = st.executeQuery();
			while (res.next()) {
				num = res.getLong(1);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(st);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return num;
	}
	
	/**
	 * The method find news on current page.
	 *
	 * @param lastOnPrevPage the last news on previous page
	 * @param lastOnPage the last news on page
	 * @return the list
	 * @throws DAOException the DAO exception
	 */

	@Override
	public List<NewsTO> findNewsOnCurrentPage(int lastOnPrevPage, int lastOnPage) throws DAOException {
		List<NewsTO> list = new ArrayList<NewsTO>();
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_FIND_ON_PAGE);
			pst.setInt(1, lastOnPage);
			pst.setInt(2, lastOnPrevPage);
			res = pst.executeQuery();
			while (res.next()) {
				NewsTO news = parseResultSet(res);
				list.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return list;
	}

	/**
	 * The method finds previous and next news identifier by given news.
	 *
	 * @param newsId the news id
	 * @return the array with previous and next news identifiers
	 * @throws DAOException the DAO exception
	 * @see com.epam.newsmanagement.dao.NewsDAO#findPreviousAndNextNews(long)
	 */
	@Override
	public long[] findPreviousAndNextNews(long newsId) throws DAOException {
		long[] list = new long[2];
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_FIND_PREV_NEXT +  SQL_FIND_ALL +")) WHERE news_id=?");
			pst.setLong(1, newsId);
			res = pst.executeQuery();
			res.next();
			list[0] = res.getLong(1);
			list[1] = res.getLong(2);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return list;
	}

/**
 * The method finds news on current page of search results.
 *
 * @param lastOnPrevPage the last news on previous page
 * @param lastOnPage the last news on page
 * @param obj the searchCriteria obj
 * @return the list
 * @throws DAOException the DAO exception
 * @see com.epam.newsmanagement.dao.NewsDAO#findNewsOnCurrentPage(int, int, com.epam.newsmanagement.entity.SearchCriteriaVO)
 */
	
	@Override
	public List<NewsTO> findNewsOnCurrentPage(int lastOnPrevPage, int lastOnPage, SearchCriteriaVO obj)
			throws DAOException {
	List<NewsTO> list = new ArrayList<NewsTO>();
		
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		String query = generateSearchQuery(obj);
		String fullQuery = "SELECT * FROM ( SELECT  a.*, rownum rnum FROM(" + query + ") a WHERE rownum <=?) WHERE rnum >?";

		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = prepareSearchStatement(con, fullQuery, obj, lastOnPrevPage, lastOnPage);
			res = pst.executeQuery();
			while (res.next()) {
				NewsTO news = parseResultSet(res);
				list.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return list;
	}
	
	/**
	 * The method finds previous and next news identifier by given news in search results list.
	 *
	 * @param newsId the news id
	 * @param obj the searchCriteria object
	 * @return the array with previous and next news identifiers
	 * @throws DAOException the DAO exception
	 * @see com.epam.newsmanagement.dao.NewsDAO#findPreviousAndNextNews(long)
	 */
	@Override
	public long[] findPreviousAndNextNews(long newsId, SearchCriteriaVO obj) throws DAOException {
		long[] list = new long[2];
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			String query = generateSearchQuery(obj);
			String fullQuery = SQL_FIND_PREV_NEXT + query +")) WHERE news_id=?";
			
			pst = prepareSearchStatement(con, fullQuery, obj, newsId);
			res = pst.executeQuery();
			res.next();
			list[0] = res.getLong(1);
			list[1] = res.getLong(2);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return list;
	}

	/**
	 * The method deletes several news
	 * 
	 * @see com.epam.newsmanagement.dao.NewsDAO#delete(long[])
	 */
	@Override
	public void delete(long[] newsIds) throws DAOException {
		PreparedStatement pst = null;
		Connection con = null;

		try {
			con = DataSourceUtils.getConnection(datasource);
			con.setAutoCommit(false);
			pst = con.prepareStatement(SQL_DELETE);
			for (long id : newsIds) {
			pst.setLong(1, id);
			pst.addBatch();
			}
			pst.executeBatch();
			con.commit();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}
		
	}

	/**
	 * The method detaches authors from list of news
	 * 
	 * @see com.epam.newsmanagement.dao.NewsDAO#detachNewsAuthor(long[])
	 */
	@Override
	public void detachNewsAuthor(long[] newsIds) throws DAOException {
		PreparedStatement pst = null;
		Connection con = null;

		try {
			con = DataSourceUtils.getConnection(datasource);
			con.setAutoCommit(false);
			pst = con.prepareStatement(SQL_DELETE_AUTHOR_NEWS);
			for (long newsId: newsIds) {
			pst.setLong(1, newsId);
			pst.addBatch();
			}
			pst.executeBatch();
			con.commit();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}
		
	}

	/**
	 * The method detaches tags from list of news
	 * @see com.epam.newsmanagement.dao.NewsDAO#detachNewsTags(long[])
	 */
	@Override
	public void detachNewsTags(long[] newsIds) throws DAOException {

		PreparedStatement pst = null;
		Connection con = null;

		try {
			con = DataSourceUtils.getConnection(datasource);
			con.setAutoCommit(false);
			pst = con.prepareStatement(SQL_DELETE_TAG_NEWS);
			for (long newsId: newsIds) {
			pst.setLong(1, newsId);
			pst.addBatch(); 
			}
			pst.executeBatch();
			con.commit();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}
		
	}

}