package com.epam.newsmanagement.service.impl;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

/**
 * The Class NewsManagerImpl implements methods from NewsManager interface.
 * 
 * @author Natallia_Zhukava
 */
public class NewsManagerImpl implements NewsManager {

	private NewsService newsService;
	private AuthorService authorService;
	private TagService tagService;
	private CommentService commentService;
	
	/**
	 * Sets the news service.
	 *
	 * @param newsService the new news service
	 */
	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	/**
	 * Sets the author service.
	 *
	 * @param authorService the new author service
	 */
	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	/**
	 * Sets the tag service.
	 *
	 * @param tagService the new tag service
	 */
	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	/**
	 * Sets the comment service.
	 *
	 * @param commentService the new comment service
	 */
	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}


	

	static Logger logger = Logger.getLogger(NewsManagerImpl.class);


	/**
	 * Instantiates a new news manager implementation object.
	 */
	public NewsManagerImpl() {

	}

	/**
	 * The method saves news with author and tags.
	 *
	 * @param complexNews the complex news
	 * @return the newsTO with generated id
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsManager#saveNews(com.epam.newsmanagement.entity.NewsVO)
	 */
	@Override
	@Transactional(rollbackFor={RuntimeException.class, ServiceException.class})
	public long saveNews(NewsVO complexNews) throws ServiceException {
		NewsTO news = complexNews.getNews();
		AuthorTO author = complexNews.getAuthor();
		List<TagTO> list = complexNews.getTagList();

		long newsId = newsService.saveNews(news);
		newsService.attachNewsAuthor(newsId, author.getId());

		if ( (list!=null) && (list.size() != 0)) {
			newsService.attachNewsTags(newsId, list);
		}

		return newsId;

	}

	/**
	 * The method edits news with author and tags.
	 *
	 * @param complexNews the complex news
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsManager#editNews(com.epam.newsmanagement.entity.NewsVO)
	 */
	@Override
	@Transactional(rollbackFor={RuntimeException.class, ServiceException.class})
	public void editNews(NewsVO complexNews) throws ServiceException {
		NewsTO news = complexNews.getNews();
		AuthorTO author = complexNews.getAuthor();
		List<TagTO> list = complexNews.getTagList();
		
			newsService.editNews(news);
			newsService.updateAuthorNews(news.getId(), author.getId());
			newsService.detachNewsTags(news.getId());
			newsService.attachNewsTags(news.getId(), list);

	}


	/**
	 * The method deletes the news by given identifier and detaches author and tags.
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsManager#deleteNews(long)
	 */
	@Override
	@Transactional(rollbackFor={RuntimeException.class, ServiceException.class})
	public void deleteNews(long newsId) throws ServiceException {
		commentService.deleteCommentsByNews(newsId);
		newsService.detachNewsTags(newsId);
		newsService.detachNewsAuthor(newsId);
		newsService.deleteNews(newsId);


		
	}



	/**
	 * The method allows to view news list sorted by comments number and modification date.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsManager#viewNewsList()
	 */
	@Override
	@Transactional(rollbackFor={RuntimeException.class, ServiceException.class})
	public List<NewsVO> viewNewsList() throws ServiceException {
		List<NewsVO> newsList = new ArrayList<NewsVO>();
					
			List<NewsTO> list = newsService.findAllNews();
			for (NewsTO news : list) {
				NewsVO cNews = viewSingleNews(news.getId());
				newsList.add(cNews);
			}
			
		return newsList;

	}
	
	


	/**
	 *  
	 * The method allows to view single news with author, tags and comments.
	 *
	 * @param newsId the news id
	 * @return the newsVO
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsManager#viewSingleNews(long)
	 */
	@Override
	@Transactional(rollbackFor={RuntimeException.class, ServiceException.class})
	public NewsVO viewSingleNews(long newsId) throws ServiceException {
		NewsVO complexNews = new NewsVO();

		NewsTO news = null;
		AuthorTO author = null;
		List<TagTO> tagList = new ArrayList<TagTO>();
		List<CommentTO> commentList = new ArrayList<CommentTO>();

			
			news = newsService.findNewsById(newsId);
			author = authorService.findAuthorByNews(newsId);
			tagList = tagService.findTagsByNews(newsId);
			commentList = commentService.findCommentsByNews(newsId);

			complexNews.setNews(news);
			complexNews.setAuthor(author);
			complexNews.setTagList(tagList);
			complexNews.setCommentList(commentList);
			
		
		return complexNews;
	}

	/**
	 * The method allows to view news list on given  page.
	 *
	 * @param p the page number
	 * @param onPage the quantity of news on single page
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see com.epam.newsmanagement.service.NewsManager#viewNewsListOnPage(int, int)
	 */
	@Override
	@Transactional(rollbackFor={RuntimeException.class, ServiceException.class})
	public List<NewsVO> viewNewsListOnPage(int p, int onPage) throws ServiceException {
		List<NewsVO> newsList = new ArrayList<NewsVO>();
		
		List<NewsTO> list = newsService.findNewsOnPage(p, onPage);
		for (NewsTO news : list) {
			NewsVO cNews = viewSingleNews(news.getId());
			newsList.add(cNews);
		}
		return newsList;
		
	}

	@Override
	@Transactional(rollbackFor={RuntimeException.class, ServiceException.class})
	public void deleteNews(long[] ids) throws ServiceException {
		commentService.deleteCommentsByNews(ids);
		newsService.detachNewsTags(ids);
		newsService.detachNewsAuthors(ids);
		newsService.deleteNews(ids);

		
	}
	

}
