package com.epam.newsmanagement.entity;

import java.util.List;

/**
 * The Class SearchCriteria contains {@code Author} and {@code Tag} identifiers for
 * search in news.
 *
 * @author Natallia_Zhukava
 */
public class SearchCriteriaVO {

	/** The author id. */
	private long authorId;
	
	/** The tag id list. */
	private List<Long> tagIdList;

	/**
	 * Instantiates a new search criteria.
	 */
	public SearchCriteriaVO() {

	}

	/**
	 * Gets the author id.
	 *
	 * @return the author id
	 */
	public long getAuthorId() {
		return authorId;
	}

	/**
	 * Sets the author id.
	 *
	 * @param authorId the new author id
	 */
	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}

	/**
	 * Gets the tag id list.
	 *
	 * @return the tag id list
	 */
	public List<Long> getTagIdList() {
		return tagIdList;
	}

	/**
	 * Sets the tag id list.
	 *
	 * @param tagIdList the new tag id list
	 */
	public void setTagIdList(List<Long> tagIdList) {
		this.tagIdList = tagIdList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (authorId ^ (authorId >>> 32));
		result = prime * result + ((tagIdList == null) ? 0 : tagIdList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof SearchCriteriaVO))
			return false;
		SearchCriteriaVO other = (SearchCriteriaVO) obj;
		if (authorId != other.authorId)
			return false;
		if (tagIdList == null) {
			if (other.tagIdList != null)
				return false;
		} else if (!tagIdList.equals(other.tagIdList))
			return false;
		return true;
	}
	

}