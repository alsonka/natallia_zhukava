package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.DBUtils;


/**
 * The Class AuthorDAOImpl implements AuthorDAO interface and contains the methods which works with database.
 *
 * @author Natallia_Zhukava
 */
public class AuthorDAOImpl implements AuthorDAO {

	private static final String TIMEZONE = "UTC";
	private static final String SQL_FIND_ALL = "SELECT author_id, author_name, expired FROM author";
	private static final String SQL_FIND_ACTUAL = "SELECT author_id, author_name, expired FROM author WHERE expired IS null";
	private static final String SQL_FIND_BY_ID = "SELECT author_id, author_name, expired FROM author WHERE author_id=?";
	private static final String SQL_FIND_BY_NEWS = "SELECT author.author_id, author_name, expired FROM author "
			+ "JOIN news_author ON author.author_id=news_author.author_id WHERE  news_author.news_id=?";
	private static final String SQL_DELETE = "DELETE FROM author WHERE author_id=?";
	private static final String SQL_CREATE = "INSERT INTO author (author_id, author_name, expired) VALUES"
			+ "(id_sequence.nextval, ?, ?) ";
	private static final String SQL_UPDATE = "UPDATE author SET author_name=?, expired=? WHERE author_id=?";
	private static final String SQL_DELETE_AUTHOR_NEWS = "DELETE FROM news_author WHERE author_id=?";

	private DataSource datasource;



	/**
	 * Sets the datasource.
	 *
	 * @param datasource the new datasource
	 */
	public void setDatasource(DataSource datasource) {
		this.datasource = datasource;
	}

	/**
	 * Instantiates a new authorDAO implementantion.
	 */
	public AuthorDAOImpl() {
		super();

	}
	
	/**
	 * The method parses given ResultSet object and returns {@code AuthorTO} object
	 * @param res
	 * @return {@code AuthorTO} object
	 * @throws SQLException
	 */
	private AuthorTO parseResultSet(ResultSet res) throws SQLException {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE));
		AuthorTO author = new AuthorTO();
		author.setId(res.getLong(1));
		author.setName(res.getString(2));
		author.setExpiredDate(res.getTimestamp(3, calendar));
		return author;
	}

	/**
	 * The method finds all authors from table {@code Author}.
	 *
	 * @return the list
	 * @throws DAOException the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#findAll()
	 */
	@Override
	public List<AuthorTO> findAll() throws DAOException {

		List<AuthorTO> list = new ArrayList<AuthorTO>();
		Statement st = null;
		ResultSet res = null;
		Connection con = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			st = con.createStatement();
			res = st.executeQuery(SQL_FIND_ALL);
			while (res.next()) {
				AuthorTO author = parseResultSet(res);
				list.add(author);
			}
		} catch (SQLException e) {
			throw new DAOException(e);                                                           
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(st);
			DataSourceUtils.releaseConnection(con, datasource);
		}
		return list;
	}

	/**
	 * The method finds the author by given id.
	 *
	 * @param id the id
	 * @return the author to
	 * @throws DAOException the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#findById(long)
	 */
	@Override
	public AuthorTO findById(long id) throws DAOException {
		AuthorTO author = null;
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_FIND_BY_ID);
			pst.setLong(1, id);
			res = pst.executeQuery();
			while (res.next()) {
				author = parseResultSet(res);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			con = DataSourceUtils.getConnection(datasource);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return author;

	}

	/**
	 * The method finds the author of the given news.
	 *
	 * @param newsId the news id
	 * @return the author to
	 * @throws DAOException the DAO exception
	 * @see com.epam.newsmanagement.dao.AuthorDAO#findByNews(long)
	 */
	@Override
	public AuthorTO findByNews(long newsId) throws DAOException {
		AuthorTO author = null;
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_FIND_BY_NEWS);
			pst.setLong(1, newsId);
			res = pst.executeQuery();
			while (res.next()) {
				author = parseResultSet(res);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return author;
	}

	/**
	 * The method saves the information from {@code AuthorTO} object to database.
	 *
	 * @param entity the entity
	 * @return the long
	 * @throws DAOException the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#save(com.epam.newsmanagement.entity.Entity)
	 */
	@Override
	public long save(AuthorTO entity) throws DAOException {

		PreparedStatement pst = null;
		Connection con = null;
		
		ResultSet res = null;
		long id = 0;
		try {
			con = DataSourceUtils.getConnection(datasource);
			String[] genKeys = {"author_id"};
			pst = con.prepareStatement(SQL_CREATE, genKeys);
			
			pst.setString(1, entity.getName());
			pst.setTimestamp(2, entity.getExpiredDate());
			pst.executeUpdate();
			
			res = pst.getGeneratedKeys();
			res.next();
			id = res.getLong(1);

		} catch (SQLException e) {
			throw new DAOException(e.toString());
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return id;

	}

	/**
	 * The method updates information about given {@code AuthorTO} object in database.
	 *
	 * @param entity the entity
	 * @throws DAOException the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#update(com.epam.newsmanagement.entity.Entity)
	 */
	@Override
	public void update(AuthorTO entity) throws DAOException {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(TIMEZONE));
		PreparedStatement pst = null;
		Connection con = null;
		
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_UPDATE);
			pst.setLong(3, entity.getId());
			pst.setString(1, entity.getName());
			pst.setTimestamp(2, entity.getExpiredDate(), calendar);
			pst.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

	}

	/**
	 * The method deletes author with given id from database.
	 * Not planned to use.
	 *
	 * @param id the id
	 * @throws DAOException the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#delete(long)
	 */
	@Override
	public void delete(long id) throws DAOException {
		PreparedStatement pst = null;
		Connection con = null;
	
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_DELETE);
			pst.setLong(1, id);
			pst.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
		
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

	}


	/**
	 * The method detaches news from given author
	 * @param authorId
	 * @see com.epam.newsmanagement.dao.AuthorDAO#detachAuthorNews(long)
	 */
	@Override
	public void detachAuthorNews(long authorId) throws DAOException {
		PreparedStatement pst = null;
		Connection con = null;
	
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_DELETE_AUTHOR_NEWS);
			pst.setLong(1, authorId);
			pst.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
		
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}
		
	}

	/**
	 *  The method finds authors with actual status (who hasn't the date of expiration).
	 *
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	@Override
	public List<AuthorTO> findActualAuthors() throws DAOException {
		List<AuthorTO> list = new ArrayList<AuthorTO>();
		Statement st = null;
		ResultSet res = null;
		Connection con = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			st = con.createStatement();
			res = st.executeQuery(SQL_FIND_ACTUAL);
			while (res.next()) {
				AuthorTO author = parseResultSet(res);
				list.add(author);
			}
		} catch (SQLException e) {
			throw new DAOException(e);                                                           
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(st);
			DataSourceUtils.releaseConnection(con, datasource);
		}
		return list;
	}

}