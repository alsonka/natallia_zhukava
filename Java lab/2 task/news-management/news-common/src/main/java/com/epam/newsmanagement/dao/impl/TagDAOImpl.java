package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.DBUtils;


/**
 * The Class TagDAOImpl implements methods from TagDAO interface.
 * 
 * @author Natallia_Zhukava
 */
public class TagDAOImpl  implements TagDAO {

	private static final String SQL_FIND_ALL = "SELECT tag_id, tag_name FROM tag";
	private static final String SQL_FIND_BY_ID = "SELECT tag_id, tag_name FROM tag WHERE tag_id=?";
	private static final String SQL_FIND_BY_NEWS = "SELECT tag.tag_id, tag_name FROM tag "
			+ "JOIN news_tag ON tag.tag_id=news_tag.tag_id WHERE news_tag.news_id=?";
	private static final String SQL_FIND_BY_TAG_NAME = "SELECT tag_id, tag_name FROM tag WHERE tag_name=?";
	private static final String SQL_DELETE = "DELETE FROM tag WHERE tag_id=?";
	private static final String SQL_CREATE = "INSERT INTO tag (tag_id, tag_name) VALUES (id_sequence.nextval, ?)";
	private static final String SQL_UPDATE = "UPDATE tag SET tag_name=? WHERE tag_id=?";
	private static final String SQL_DELETE_TAG_NEWS = "DELETE FROM news_tag WHERE tag_id=?";

	/**
	 * Instantiates a new tagDAO implementation.
	 */
	public TagDAOImpl() {
		super();
	}
	
	private DataSource datasource;
	
	/**
	 * Sets the datasource.
	 *
	 * @param datasource the new datasource
	 */
	public void setDatasource(DataSource datasource) {
		this.datasource = datasource;
	}

	/**
	 * The method parses given ResultSet object and returns {@code TagTO} object
	 * @param res
	 * @return TagTO object
	 * @throws SQLException
	 */
	private TagTO parseResultSet(ResultSet res) throws SQLException {
		TagTO tag = new TagTO();
		tag.setId(res.getLong(1));
		tag.setName(res.getString(2));
		return tag;
	}

	
	/**
	 *  
	 * The method finds all tags from database.
	 *
	 * @return the list
	 * @throws DAOException the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#findAll()
	 */
	@Override
	public List<TagTO> findAll() throws DAOException {
		List<TagTO> list = new ArrayList<TagTO>();
		Statement st = null;
		Connection con = null;
		ResultSet res = null;
		
		try {
			con = DataSourceUtils.getConnection(datasource);
			st = con.createStatement();
			res = st.executeQuery(SQL_FIND_ALL);
			while (res.next()) {
				TagTO tag = parseResultSet(res);
				list.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(st);
			DataSourceUtils.releaseConnection(con, datasource);
		}
		return list;
	}


	/**
	 * The method finds all tags attached to given news.
	 *
	 * @param newsId the news id
	 * @return the list
	 * @throws DAOException the DAO exception
	 * @see com.epam.newsmanagement.dao.TagDAO#findByNews(long)
	 */
	@Override
	public List<TagTO> findByNews(long newsId) throws DAOException {
		List<TagTO> list = new ArrayList<TagTO>();
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_FIND_BY_NEWS);
			pst.setLong(1, newsId);
			res = pst.executeQuery();
			while (res.next()) {
				TagTO tag = parseResultSet(res);
				list.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}
		return list;
	}

	
	/**
	 * The method finds the tag by given id.
	 *
	 * @param id the id
	 * @return the tag to
	 * @throws DAOException the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#findById(long)
	 */
	@Override
	public TagTO findById(long id) throws DAOException {
		TagTO tag = null;
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_FIND_BY_ID);
			pst.setLong(1, id);
			res = pst.executeQuery();
			while (res.next()) {
				tag = parseResultSet(res);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}
		return tag;
	}

	/**
	 *  
	 * The method finds the tag by given tag name.
	 *
	 * @param name the name
	 * @return the tag to
	 * @throws DAOException the DAO exception
	 * @see com.epam.newsmanagement.dao.TagDAO#findByTagName(java.lang.String)
	 */
	@Override	
	public TagTO findByTagName(String name) throws DAOException {
		TagTO tag = null;
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_FIND_BY_TAG_NAME);
			pst.setString(1, name);
			res = pst.executeQuery();
			while (res.next()) {
				tag = parseResultSet(res);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}
		return tag;
	}


	/**
	 * The method saves information from given {@code TagTO} object to database.
	 *
	 * @param entity the entity
	 * @return the long
	 * @throws DAOException the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#save(com.epam.newsmanagement.entity.Entity)
	 */
	@Override
	public long save(TagTO entity) throws DAOException {
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		long id=0; 
		try {
			con = DataSourceUtils.getConnection(datasource);
			String[] genKeys = {"tag_id"};
			pst = con.prepareStatement(SQL_CREATE, genKeys);
			pst.setString(1, entity.getName());
			pst.executeUpdate();
			
			res = pst.getGeneratedKeys();
			res.next();
			id = res.getLong(1);
			
			
			
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}
		return id;
	}


	/**
	 * The method updates information about given tag in database.
	 *
	 * @param entity the entity
	 * @throws DAOException the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#update(com.epam.newsmanagement.entity.Entity)
	 */
	@Override
	public void update(TagTO entity) throws DAOException {
		
		PreparedStatement pst = null;
		Connection con = null;
		
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_UPDATE);
			pst.setLong(2, entity.getId());
			pst.setString(1, entity.getName());
			pst.executeUpdate();
			
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
		
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}
	}

	
	/**
	 * The method deletes the tag with given identifier from database.
	 *
	 * @param id the id
	 * @throws DAOException the DAO exception
	 * @see com.epam.newsmanagement.dao.CommonDAO#delete(long)
	 */
	@Override
	public void delete(long id) throws DAOException {
		
		PreparedStatement pst = null;
		Connection con = null;
	
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_DELETE);
			pst.setLong(1, id);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
	
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}
		
	}

	/**
	 * The method detaches the tag with given identifier from all news which it attached to.
	 *
	 * @param tagId the tag id
	 * @throws DAOException the DAO exception
	 * @see com.epam.newsmanagement.dao.TagDAO#detachTagNews(long)
	 */
	@Override
	public void detachTagNews(long tagId) throws DAOException {
		PreparedStatement pst = null;
		Connection con = null;
	
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_DELETE_TAG_NEWS);
			pst.setLong(1, tagId);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
	
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}
		
	}

}