package com.epam.newsmanagement.service.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.impl.NewsDAOImpl;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.service.SearchService;






@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
public class SearchServiceImplTest {
	
	@Mock
	private NewsDAOImpl newsDao;
	@Mock
	private SearchCriteriaVO obj;
	@Mock
	private NewsManager newsManager;

	@Autowired
	@InjectMocks
	private SearchService searchService;

	public SearchServiceImplTest() {
		
	}
	
	@Before
	public void setUp() throws DAOException {
        MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSearchBy() throws DAOException, ServiceException   {
		List<NewsTO> list = new ArrayList<NewsTO>();
		list.add(new NewsTO(1L, null, null, null, null, null));
		list.add(new NewsTO(2L, null, null, null, null, null));
		when (newsDao.findByCriteria(obj)).thenReturn(list);
		when (newsManager.viewSingleNews(1L)).thenReturn(new NewsVO());
		when (newsManager.viewSingleNews(2L)).thenReturn(new NewsVO());
		
		searchService.searchBy(obj);
		verify(newsDao).findByCriteria(obj);


	}
	
	@Test(expected=ServiceException.class)
	public void testSearchByException() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(newsDao).findByCriteria(obj);
		searchService.searchBy(obj);
	}
	
	@Test
	public void testCountNews() throws DAOException, ServiceException {
		
		when (newsDao.countNews(obj)).thenReturn(5L);
		long expected = 5;
		long actual = searchService.countNews(obj);
		verify(newsDao).countNews(obj);
		assertEquals(expected, actual);
	}
	
	@Test(expected=ServiceException.class)
	public void testCountNewsException() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(newsDao).countNews(obj);
		searchService.countNews(obj);
	}
	
	@Test
	public void testFindNewsOnPage() throws DAOException, ServiceException {
		when (newsDao.countNews(obj)).thenReturn(15L);
		List<NewsTO> list = new ArrayList<NewsTO>();
		list.add(new NewsTO(1L, null, null, null, null, null));
		list.add(new NewsTO(2L, null, null, null, null, null));
		
		when (newsDao.findNewsOnCurrentPage(0, 5, obj)).thenReturn(list);
		when (newsManager.viewSingleNews(1L)).thenReturn(new NewsVO());
		when (newsManager.viewSingleNews(2L)).thenReturn(new NewsVO());
		
		searchService.findNewsOnPage(1, 5, obj);
		verify(newsDao).findNewsOnCurrentPage(0, 5, obj);
	}
	
	@Test
	public void testFindNewsOnPageLessOnPage() throws DAOException, ServiceException {
		when (newsDao.countNews(obj)).thenReturn(4L);
		List<NewsTO> list = new ArrayList<NewsTO>();
		list.add(new NewsTO(1L, null, null, null, null, null));
		list.add(new NewsTO(2L, null, null, null, null, null));
		
		when (newsDao.findNewsOnCurrentPage(0, 4, obj)).thenReturn(list);
		when (newsManager.viewSingleNews(1L)).thenReturn(new NewsVO());
		when (newsManager.viewSingleNews(2L)).thenReturn(new NewsVO());
		
		searchService.findNewsOnPage(1, 5, obj);
		verify(newsDao).findNewsOnCurrentPage(0, 4, obj);
	}
	
	@Test
	public void testFindNewsOnPageIncorrectPage() throws DAOException, ServiceException {
		when (newsDao.countNews(obj)).thenReturn(11L);
		List<NewsTO> list = new ArrayList<NewsTO>();
		list.add(new NewsTO(1L, null, null, null, null, null));
		list.add(new NewsTO(2L, null, null, null, null, null));
		
		when (newsDao.findNewsOnCurrentPage(0, 5, obj)).thenReturn(list);
		when (newsManager.viewSingleNews(1L)).thenReturn(new NewsVO());
		when (newsManager.viewSingleNews(2L)).thenReturn(new NewsVO());
		
		searchService.findNewsOnPage(7, 5, obj);
		verify(newsDao).findNewsOnCurrentPage(0, 5, obj);
	}
	
	@Test(expected=ServiceException.class)
	public void testFindNewsOnPageException() throws DAOException, ServiceException {
		when (newsDao.countNews(obj)).thenReturn(15L);
		doThrow(DAOException.class).when(newsDao).findNewsOnCurrentPage(0, 5, obj);
		searchService.findNewsOnPage(1, 5, obj);
	}
	

}
