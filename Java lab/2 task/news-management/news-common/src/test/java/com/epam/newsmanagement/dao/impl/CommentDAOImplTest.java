package com.epam.newsmanagement.dao.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;



import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class CommentDAOImplTest extends AbstractTest {

	@Autowired
	private CommentDAO commentDAO;

	public CommentDAOImplTest() {

	}

	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news-data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/comment-data.xml"))
				
				};
		return new CompositeDataSet(datasets);
	}

	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());
		
	}

	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void testFindAll() throws DAOException {

		List<CommentTO> comments = commentDAO.findAll();
		assertThat(comments.size(), is(12));
	}

	@Test
	public void testFindById() throws DAOException, ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
		Date date = df.parse("2015-08-14 10:28:29.123 UTC");
		CommentTO expected = new CommentTO(3, 2, "Third",  new Timestamp(date.getTime()));
		CommentTO actual = commentDAO.findById(3);
		assertEquals(expected, actual);
	}

	@Test
	public void testSave() throws DAOException {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		Date now = calendar.getTime();
		Timestamp currentTimestamp = new Timestamp(now.getTime());
		
		CommentTO expected = new CommentTO(4, "New Comment", currentTimestamp);
		long id = commentDAO.save(expected);
		expected.setId(id);
		CommentTO actual = commentDAO.findById(id);
		assertEquals(expected, actual);
	}
	
	@Test(expected=DAOException.class)
	public void testSaveException() throws DAOException {
		CommentTO comment=new CommentTO();
		comment.setCreationDate(Calendar.getInstance().getTime());
		commentDAO.save(comment);
	}

	@Test
	public void testUpdate() throws DAOException, ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
		Date date = df.parse("2015-08-14 11:28:29.123 GMT");
		CommentTO expected = new CommentTO(6, 3,  "Changed", new Timestamp(date.getTime()));
		CommentTO comment = commentDAO.findById(6);
		comment.setText("Changed");
		commentDAO.update(comment);
		CommentTO actual = commentDAO.findById(6);
		assertEquals(expected, actual);
	}
	
	@Test(expected=DAOException.class)
	public void testUpdateException() throws DAOException {
		CommentTO comment=new CommentTO(1, 3, null, null);
		commentDAO.update(comment);
	}

	@Test
	public void testDelete() throws DAOException {

		CommentTO expected = commentDAO.findById(8);
		commentDAO.delete(8);
		CommentTO actual = commentDAO.findById(8);
		assertNotEquals(expected, actual);
	}

	@Test
	public void testFindByNews() throws DAOException {
		List<CommentTO> list = new ArrayList<CommentTO>();
		list.add(commentDAO.findById(4));
		list.add(commentDAO.findById(6));
		Object[] expected = list.toArray();
		List<CommentTO> list2 = commentDAO.findByNewsId(3);
		Object[] actual = list2.toArray();
		assertArrayEquals(expected, actual);
	}
	
	@Test
	public void testDeleteByNews() throws DAOException {
		commentDAO.deleteByNewsId(1);
		int expected = 0;
		List<CommentTO> list = commentDAO.findByNewsId(1);
		int actual = list.size();
		assertEquals(expected, actual);
		
	}
	
	@Test
	public void testDeleteByNewsList() throws DAOException {
		long[] newsIds = {1L, 2L};
		commentDAO.deleteByNewsId(newsIds);
		List<CommentTO> list = commentDAO.findByNewsId(1);
		List<CommentTO> list2 = commentDAO.findByNewsId(2);
		assertThat(list.size(), is(0));
		assertThat(list2.size(), is(0));
	}
	
	

	


}
