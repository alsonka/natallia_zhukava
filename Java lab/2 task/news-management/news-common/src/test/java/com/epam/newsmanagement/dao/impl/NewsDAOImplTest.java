package com.epam.newsmanagement.dao.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.dao.impl.AuthorDAOImpl;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class NewsDAOImplTest extends AbstractTest {

	@Autowired
	private NewsDAO newsDAO;
	@Autowired
	private AuthorDAOImpl authorDAO;
	@Autowired
	private TagDAO tagDAO;

	public NewsDAOImplTest() {

	}

	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news-data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/comment-data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/tag-data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/author-data.xml"))

		};
		return new CompositeDataSet(datasets);
	}

	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());

	}

	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void testFindAll() throws DAOException {

		List<NewsTO> news = newsDAO.findAll();
		assertThat(news.size(), is(10));
	}

	@Test
	public void testFindById() throws DAOException, ParseException {
		long id = 3;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
		Date date = df.parse("2015-08-14 09:28:29.113 GMT");
		NewsTO expected = new NewsTO(id, "News1", "ShortText", "Full text", new Timestamp(date.getTime()),
				java.sql.Date.valueOf("2015-08-14"));
		NewsTO actual = newsDAO.findById(id);
		assertEquals(expected, actual);
	}

	@Test
	public void testFindByTag() throws DAOException {
		List<NewsTO> expected = new ArrayList<NewsTO>();
		expected.add(newsDAO.findById(1));
		expected.add(newsDAO.findById(4));
		Collections.sort(expected, new ListNewsComparator());

		List<NewsTO> actual = newsDAO.findByTag(5);
		Collections.sort(actual, new ListNewsComparator());
		assertEquals(expected, actual);
	}

	@Test
	public void testFindByAuthor() throws DAOException {
		List<NewsTO> expected = new ArrayList<NewsTO>();
		expected.add(newsDAO.findById(1));
		expected.add(newsDAO.findById(3));
		Collections.sort(expected, new ListNewsComparator());
		
		List<NewsTO> actual = newsDAO.findByAuthor(2);
		Collections.sort(actual, new ListNewsComparator());
		assertEquals(expected, actual);
	}
	
	@Test
	public void testFindByEmptyCriteria() throws DAOException {
		SearchCriteriaVO obj = new SearchCriteriaVO();
		List<NewsTO> list = newsDAO.findByCriteria(obj);
		assertThat(list.size(), is(10));
	}

	@Test
	public void testFindByCriteria() throws DAOException {
		SearchCriteriaVO obj = new SearchCriteriaVO();
		obj.setAuthorId(2);
		List<Long> tags = new ArrayList<Long>();
		tags.add(2L);
		tags.add(5L);
		obj.setTagIdList(tags);

		NewsTO n1 = newsDAO.findById(1);

		List<NewsTO> expected = new ArrayList<NewsTO>();
		expected.add(n1);

		List<NewsTO> actual = newsDAO.findByCriteria(obj);

		assertEquals(expected, actual);
	}

	@Test
	public void testFindBySearchCriteriaWithTagsOnly() throws DAOException {
		SearchCriteriaVO obj = new SearchCriteriaVO();

		List<Long> tags = new ArrayList<Long>();
		tags.add(2L);
		tags.add(5L);
		obj.setTagIdList(tags);

		NewsTO n1 = newsDAO.findById(1);
		NewsTO n2 = newsDAO.findById(4);

		List<NewsTO> expected = new ArrayList<NewsTO>();
		expected.add(n1);
		expected.add(n2);
		Collections.sort(expected, new ListNewsComparator());

		List<NewsTO> actual = newsDAO.findByCriteria(obj);
		Collections.sort(actual, new ListNewsComparator());
		assertEquals(expected, actual);
	}

	@Test
	public void testFindBySearchCriteriaWithAuthorOnly() throws DAOException {
		SearchCriteriaVO obj = new SearchCriteriaVO();
		obj.setAuthorId(2);

		NewsTO n1 = newsDAO.findById(1);
		NewsTO n2 = newsDAO.findById(3);

		List<NewsTO> expected = new ArrayList<NewsTO>();
		expected.add(n1);
		expected.add(n2);
		Collections.sort(expected, new ListNewsComparator());

		List<NewsTO> actual = newsDAO.findByCriteria(obj);
		
		Collections.sort(actual, new ListNewsComparator());
		assertEquals(expected, actual);
	}

	@Test
	public void testSaveNews() throws DAOException, ParseException {
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		Timestamp currentTimestamp = new Timestamp(date.getTime());
		NewsTO news =  new NewsTO("News6", "ShortText", "Full text", currentTimestamp,
				date);
		long id = newsDAO.save(news);

		NewsTO savedNews = newsDAO.findById(id);
		String expected = news.getTitle();
		String actual = savedNews.getTitle();

		assertEquals(expected, actual);
	}
	
	@Test(expected=DAOException.class)
	public void testSaveNewsException() throws DAOException {
		NewsTO news = new NewsTO();
		Calendar calendar = Calendar.getInstance();
		
		news.setCreationDate(new Timestamp(calendar.getTimeInMillis()));
		news.setModificationDate(new java.sql.Date(calendar.getTimeInMillis()));
		
		newsDAO.save(news);
	} 

	@Test
	public void testAttachAuthorToNews() throws DAOException {
		Calendar calendar = Calendar.getInstance();
		java.util.Date now = calendar.getTime();
		Timestamp currentTimestamp = new Timestamp(now.getTime());

		NewsTO news = new NewsTO("News7", "ShortText", "Full text", currentTimestamp, new java.sql.Date(now.getTime()));
		long newsId = newsDAO.save(news);

		newsDAO.attachAuthorToNews(newsId, 5);
		AuthorTO expected = new AuthorTO(5, "Julia Ryabuhina", null);
		AuthorTO actual = authorDAO.findByNews(newsId);

		assertEquals(expected, actual);
	}
	

	

	@Test
	public void testAttachTagToNews() throws DAOException {
		NewsTO news = newsDAO.findById(5);
		TagTO tag = tagDAO.findById(1);
		boolean expected = true;
		newsDAO.attachTagToNews(news.getId(), 1);

		List<TagTO> tags = tagDAO.findByNews(news.getId());
		boolean actual = tags.contains(tag);

		assertEquals(expected, actual);
	}
	
	
	@Test
	public void testUpdateAuthorNews() throws DAOException {
		NewsTO news = newsDAO.findById(2);
		AuthorTO expected = new AuthorTO(5, "Julia Ryabuhina", null);
		newsDAO.updateNewsAuthor(news.getId(), 5);
		AuthorTO actual = authorDAO.findByNews(news.getId());

		assertEquals(expected, actual);

	}

	@Test
	public void testUpdateNews() throws DAOException, ParseException {
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
		Date date = df.parse("2015-08-14 09:28:29.111 GMT");

		NewsTO expected = new NewsTO(2, "News2", "ShortText", "Full text", new Timestamp(date.getTime()),
				java.sql.Date.valueOf("2015-08-14"));
		NewsTO news = newsDAO.findById(2);
		news.setTitle("News2");
		newsDAO.update(news);
		NewsTO actual = newsDAO.findById(2);
		assertEquals(expected, actual);
	}
	
	@Test(expected=DAOException.class) 
	public void testUpdateNewsException() throws DAOException {
		NewsTO news = newsDAO.findById(3);
		news.setTitle(null);
		newsDAO.update(news);
	}

	@Test
	public void testDelete() throws DAOException {

		NewsTO expected = newsDAO.findById(9);
		newsDAO.delete(9);
		NewsTO actual = newsDAO.findById(9);
		assertNotEquals(expected, actual);
	}

	@Test
	public void testAddNewsTags() throws DAOException {
		List<TagTO> expected = new ArrayList<TagTO>();
		expected.add(tagDAO.findById(4));
		expected.add(tagDAO.findById(7));
		Collections.sort(expected, new ListTagComparator());
		Calendar calendar = Calendar.getInstance();
		Date now = calendar.getTime();
		Timestamp currentTimestamp = new Timestamp(now.getTime());

		NewsTO news = new NewsTO("News7", "ShortText", "Full text", currentTimestamp, new java.sql.Date(now.getTime()));
		long newsId=newsDAO.save(news);

		newsDAO.attachTagsToNews(newsId, expected);

		List<TagTO> actual = tagDAO.findByNews(newsId);
		Collections.sort(expected, new ListTagComparator());
		
		assertEquals(expected, actual);
	}

	@Test
	public void testDetachNewsTags() throws DAOException {
		newsDAO.detachNewsTags(4);
		List<TagTO> list = tagDAO.findByNews(4);
		assertThat(list.size(), is(0));
	}
	
	@Test
	public void testDetachNewsAuthor() throws DAOException {
		AuthorTO expected = new AuthorTO(7, "Nastassia Demidova", null);
		newsDAO.detachNewsAuthor(6);
		AuthorTO actual = authorDAO.findByNews(6);
		
		assertNotEquals(expected, actual);
	}
	
	@Test
	public void testCountNews() throws DAOException {
		long expected = 10;
		long actual = newsDAO.countNews();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testFindNewsOnPage() throws DAOException {	
		List<NewsTO> list = newsDAO.findNewsOnCurrentPage(0, 5);
		assertThat(list.size(), is(5));
	}
	
	@Test
	public void testFindNewsOnPageWithSearchCriteria() throws DAOException {
		SearchCriteriaVO obj = new SearchCriteriaVO();

		List<Long> tags = new ArrayList<Long>();
		tags.add(2L);
		tags.add(5L);
		tags.add(3L);
		obj.setTagIdList(tags);
		
		List<NewsTO> list = newsDAO.findNewsOnCurrentPage(0, 5, obj);
		assertThat(list.size(), is(4));
	}
	
	@Test
	public void testCountNewsWithSearchCriteria() throws DAOException {
		SearchCriteriaVO obj = new SearchCriteriaVO();

		List<Long> tags = new ArrayList<Long>();
		tags.add(2L);
		tags.add(5L);
		obj.setTagIdList(tags);
		
		long expected = 2; 
		long actual = newsDAO.countNews(obj);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testFindPreviousAndNextNews() throws DAOException {
		long[] expected = {4, 9};
		long[] actual = newsDAO.findPreviousAndNextNews(5);
		
		assertArrayEquals(expected, actual);
	}
	


	@Test
	public void testFindPreviousAndNextNewsWithSearchCriteria() throws DAOException {
		SearchCriteriaVO obj = new SearchCriteriaVO();

		List<Long> tags = new ArrayList<Long>();
		tags.add(2L);
		tags.add(5L);
		tags.add(3L);
		obj.setTagIdList(tags);

		
		long[] expected = {3, 5};
		long[] actual = newsDAO.findPreviousAndNextNews(4, obj);
		
		assertArrayEquals(expected, actual);
	}
	
	@Test
	public void testDeleteNewsList() throws DAOException {
		NewsTO news1 = newsDAO.findById(10);
		NewsTO news2 = newsDAO.findById(11);
		NewsTO[] expected = {news1, news2};
		long[] ids = {10L, 11L};
		
		newsDAO.delete(ids);
		NewsTO news3 = newsDAO.findById(10);
		NewsTO news4 = newsDAO.findById(11);
		NewsTO[] actual = {news3, news4};
		assertNotEquals(expected, actual);
	}
	
	@Test
	public void testDetachAuthorsNews() throws DAOException {
		
		AuthorTO auth1 = new AuthorTO(7, "Nastassia Demidova", Timestamp.valueOf("2015-08-14 19:28:29.66"));
		AuthorTO auth2 = new AuthorTO(4, "Vasily Bodrov", null);
		AuthorTO[] expected = {auth1, auth2};
		long[] ids = {6L, 8L};
		newsDAO.detachNewsAuthor(ids);
		AuthorTO auth3 = authorDAO.findByNews(6);
		AuthorTO auth4 = authorDAO.findByNews(8);
		AuthorTO[] actual = {auth3, auth4};
		assertNotEquals(expected, actual);
	}
	
	@Test
	public void testDetachTagsNews() throws DAOException {
		long[] ids = {4L, 3L};
		newsDAO.detachNewsTags(ids);
		
		List<TagTO> list = tagDAO.findByNews(4);
		List<TagTO> list2 = tagDAO.findByNews(3);
		assertThat(list2.size(), is(0));
		assertThat(list.size(), is(0));
	}
	
	
	
	private class ListTagComparator implements Comparator<TagTO> {

		public int compare(TagTO o1, TagTO o2) {
			if (o1.getId() > o2.getId()) {
				return 1;
			} else if (o1.getId() < o2.getId()) {
				return -1;
			} else {
				return 0;
			}
		}
	}
	
	private class ListNewsComparator implements Comparator<NewsTO> {

		public int compare(NewsTO o1, NewsTO o2) {
			if (o1.getId() > o2.getId()) {
				return 1;
			} else if (o1.getId() < o2.getId()) {
				return -1;
			} else {
				return 0;
			}
		}
	}



}
