package com.epam.newsmanagement.dao.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class AuthorDAOImplTest extends AbstractTest {

	@Autowired
	private AuthorDAO authorDAO;
	@Autowired
	private NewsDAO newsDAO;

	public AuthorDAOImplTest() {

	}

	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news-data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/author-data.xml"))
				
				};
		return new CompositeDataSet(datasets);
	}

	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());
		
	}

	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void testFindAll() throws DAOException {

		List<AuthorTO> authors = authorDAO.findAll();
		assertThat(authors.size(), is(14));
	}

	@Test
	public void testFindById() throws DAOException {
		AuthorTO expected = new AuthorTO(3, "Julia Ivanova", null);
		
		AuthorTO actual = authorDAO.findById(3);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testFindByIdNotExists() throws DAOException {
		AuthorTO expected = null;
		AuthorTO actual = authorDAO.findById(322);
		assertEquals(expected, actual);
	}

	@Test
	public void testSave() throws DAOException {

		AuthorTO expected = new AuthorTO("New Author", null);
		long id = authorDAO.save(expected);
		expected.setId(id);
		AuthorTO actual = authorDAO.findById(id);
		assertEquals(expected, actual);
	}
	
	@Test(expected=DAOException.class)
	public void testSaveException() throws DAOException {
		AuthorTO author = new AuthorTO(null, null);
		authorDAO.save(author);
	}

	@Test
	public void testUpdate() throws DAOException {

		AuthorTO expected = new AuthorTO(6, "Changed name", null);
		AuthorTO author = authorDAO.findById(6);
		author.setName("Changed name");
		authorDAO.update(author);
		AuthorTO actual = authorDAO.findById(6);
		assertEquals(expected, actual);
	}
	
	@Test(expected=DAOException.class)
	public void testUpdateException() throws DAOException {
		AuthorTO author = new AuthorTO(6, "Changed name fsdfsdfewfsfafasdfasdfasdfasd", null);
		authorDAO.update(author);
	}

	@Test
	public void testDelete() throws DAOException {

		AuthorTO expected = authorDAO.findById(8);
		authorDAO.delete(8);
		AuthorTO actual = authorDAO.findById(8);
		assertNotEquals(expected, actual);
	}

	@Test
	public void testfindByNews() throws DAOException {
		AuthorTO expected = authorDAO.findById(3);
		AuthorTO actual = authorDAO.findByNews(5);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testDetachAuthorNews() throws DAOException {
		long authorId = 4;
		authorDAO.detachAuthorNews(authorId);
		List<NewsTO> list = newsDAO.findByAuthor(authorId);
		assertThat(list.size(), is(0));
	}
	
	
	@Test
	public void testFindActualAuthors() throws DAOException {
		List<AuthorTO> authors = authorDAO.findActualAuthors();
		assertThat(authors.size(), is(9));
		
	}
	


}
