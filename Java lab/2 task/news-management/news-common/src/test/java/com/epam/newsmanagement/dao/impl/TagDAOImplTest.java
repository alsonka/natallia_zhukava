package com.epam.newsmanagement.dao.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;


import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class TagDAOImplTest extends AbstractTest {

	@Autowired
	private TagDAO tagDAO;
	
	@Autowired
	private NewsDAO newsDAO;
	
	public TagDAOImplTest()   {
		
	} 

	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news-data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/tag-data.xml"))
				
				};
		return new CompositeDataSet(datasets);
	}

	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());
	}

	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void testFindAll() throws DAOException {
		
		List<TagTO> tags = tagDAO.findAll();
		assertThat(tags.size(), is(14));
	}
	
	@Test
	public void testFindById() throws DAOException {
		TagTO expected = new TagTO();
		expected.setId(3);
		expected.setName("Android");
		TagTO actual = tagDAO.findById(3);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testFindByName() throws DAOException {
		TagTO expected = new TagTO();
		expected.setId(12);
		expected.setName("Google Android M");
		TagTO actual = tagDAO.findByTagName("Google Android M");
		assertEquals(expected, actual);
	}
	
	@Test
	public void testSave() throws DAOException {
				
		TagTO expected = new TagTO("Test tag");
		long id = tagDAO.save(expected);
		expected.setId(id);
		TagTO actual = tagDAO.findById(id);
		assertEquals(expected, actual);
	}
	
	@Test(expected=NullPointerException.class) 
	public void testSaveNullException() throws DAOException {
		TagTO tag = null;
		tagDAO.save(tag);
	}
	
	@Test(expected=DAOException.class)
	public void testSaveDAOException() throws DAOException {
		TagTO tag = new TagTO();
		tagDAO.save(tag);
	}
	
	@Test
	public void testUpdate() throws DAOException {
		
		TagTO expected = new TagTO(6, "Changed name");
		TagTO tag = tagDAO.findById(6);
		tag.setName("Changed name");
		tagDAO.update(tag);
		TagTO actual = tagDAO.findById(6);
		assertEquals(expected, actual);
	}
	
	@Test(expected=DAOException.class)
	public void testUpdateException() throws DAOException {
		TagTO tag = new TagTO(1, null);
		tagDAO.update(tag);
	}
	
	@Test
	public void testDelete() throws DAOException {
		
		TagTO expected = tagDAO.findById(8);
		tagDAO.delete(8);
		TagTO actual = tagDAO.findById(8);
		assertNotEquals(expected, actual);
	}
	@Test
	public void testFindByNews() throws DAOException {
		List<TagTO> expected = new LinkedList<TagTO>();
		expected.add(tagDAO.findById(2));
		expected.add(tagDAO.findById(3));
		expected.add(tagDAO.findById(5));
		Collections.sort(expected, new ListComparator());
		List<TagTO> actual = tagDAO.findByNews(1);
		Collections.sort(actual, new ListComparator());
		assertEquals(expected, actual);
	}
	
	@Test
	public void testDeleteTagNews() throws DAOException {
		tagDAO.detachTagNews(7);
		List<NewsTO> list = newsDAO.findByTag(7);
		assertThat(list.size(), is(0));
		
	}
	
	private  class ListComparator implements Comparator<TagTO> {
		
		public  int compare(TagTO o1, TagTO o2) {
			if (o1.getId() > o2.getId()) {
				return 1;
			} else if (o1.getId() < o2.getId()) {
				return -1;
			} else {
				return 0;
			}
		}
}
	

}

