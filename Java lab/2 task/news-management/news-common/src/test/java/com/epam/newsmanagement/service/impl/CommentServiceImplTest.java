package com.epam.newsmanagement.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;

import static org.mockito.Mockito.*;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
public class CommentServiceImplTest {

	@Mock
	private CommentDAO commentDao;
	@Mock
	CommentTO comment;
	
	@Autowired
	@InjectMocks
	private CommentService commentService;
	
	public CommentServiceImplTest() {		
	}
	
	@Before
	public void setUp() throws DAOException {
        MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testFindAllComments() throws DAOException, ServiceException  {
		commentService.findAllComments();
		verify(commentDao).findAll();
	}
	
	@Test(expected=ServiceException.class)
	public void testFindAllCommentsException() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(commentDao).findAll();
		commentService.findAllComments();
	}
	
	@Test 
	public void testFindCommentById() throws DAOException, ServiceException {
		long id = 2;
		commentService.findCommentById(id);
		verify(commentDao).findById(id);
	}
	
	@Test(expected=ServiceException.class)
	public void testFindCommentByIdException() throws DAOException, ServiceException {
		long id = 3;
		doThrow(DAOException.class).when(commentDao).findById(id);
		commentService.findCommentById(id);
	}
	
	@Test 
	public void testFindCommentsByNews() throws DAOException, ServiceException {
		long id = 2;
		commentService.findCommentsByNews(id);
		verify(commentDao).findByNewsId(id);
	}
	
	@Test(expected=ServiceException.class)
	public void testFindCommentsByNewsException() throws DAOException, ServiceException {
		long id=3;
		doThrow(DAOException.class).when(commentDao).findByNewsId(id);
		commentService.findCommentsByNews(id);
	}
	
	@Test
	public void testSaveComment() throws DAOException, ServiceException {
		CommentTO comment = new CommentTO(2, "comment", null);
		when (commentDao.save(comment)).thenReturn(1L);
		
		long expected = 1L;
		long actual = commentService.saveComment(comment);
		verify(commentDao).save(comment);
		assertEquals(expected, actual);	
	}
	
	@Test(expected=ServiceException.class)
	public void testSaveCommentException() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(commentDao).save(comment);
		commentService.saveComment(comment);
	}
	
	@Test
	public void testEditComment() throws DAOException, ServiceException {
		
		commentService.editComment(comment);
		verify(commentDao).update(comment);
		
	}
	
	@Test(expected=ServiceException.class)
	public void testEditCommentException() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(commentDao).update(comment);
		commentService.editComment(comment);
	}
	
	@Test
	public void testDeleteComment() throws DAOException, ServiceException {
		long id = 2L;
		commentService.deleteComment(id);
		verify(commentDao).delete(id);
		
	}
	
	@Test(expected=ServiceException.class)
	public void testDeleteCommentException() throws DAOException, ServiceException {
		long id=3L;
		doThrow(DAOException.class).when(commentDao).delete(id);
		commentService.deleteComment(id);
	}
	
	@Test
	public void testDeleteCommentsByNews() throws  DAOException, ServiceException {
		long id = 2L;
		commentService.deleteCommentsByNews(id);
		verify(commentDao).deleteByNewsId(id);
	}
	
	@Test(expected=ServiceException.class)
	public void testDeleteCommentsByNewsException() throws DAOException, ServiceException {
		long id=2L;
		doThrow(DAOException.class).when(commentDao).deleteByNewsId(id);
		commentService.deleteCommentsByNews(id);
	}
	
	@Test
	public void testDeleteCommentsByNewsList() throws DAOException, ServiceException {
		long[] ids = {2L, 3L, 4L};
		commentService.deleteCommentsByNews(ids);
		verify(commentDao).deleteByNewsId(ids);
	}
	
	@Test(expected=ServiceException.class)
	public void testDeleteCommentsByNewsListException() throws DAOException, ServiceException {
		long[] ids = {2L, 3L, 4L};
		doThrow(DAOException.class).when(commentDao).deleteByNewsId(ids);
		commentService.deleteCommentsByNews(ids);
		
	}
	


}
