package com.epam.newsmanagement.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
public class NewsManagerImplTest {

	@Mock
	private NewsService newsService;
	@Mock
	private AuthorService authorService;
	@Mock
	private TagService tagService;
	@Mock
	private CommentService commentService;
	
	@Autowired
	@InjectMocks
	private NewsManager newsManager;
	
	private NewsVO complexNews;
	
	public NewsManagerImplTest() {
		complexNews = new NewsVO();
		AuthorTO author = new AuthorTO(2, "author", null);
		complexNews.setAuthor(author);
		List<TagTO> tags = new ArrayList<TagTO>();
		tags.add(new TagTO(1, "tag1"));
		tags.add(new TagTO(2, "tag2"));
		complexNews.setTagList(tags);
	}
	
	@Before
	public void setUp() throws DAOException, ServiceException {
		MockitoAnnotations.initMocks(this);		
	}
	

	
	@Test
	public void testViewNewsList() throws ServiceException, DAOException {
		List<NewsTO> list = new ArrayList<NewsTO>();
		list.add(new NewsTO(1L, null, null, null, null, null));
		
		when (newsService.findAllNews()).thenReturn(list);
		when (newsService.findNewsById(1L)).thenReturn(new NewsTO());
		when (authorService.findAuthorByNews(1L)).thenReturn(new AuthorTO());
		when (tagService.findTagsByNews(1L)).thenReturn(new ArrayList<TagTO>());
		when (commentService.findCommentsByNews(1L)).thenReturn(new ArrayList<CommentTO>());
		newsManager.viewNewsList();
		verify(newsService).findAllNews();
	}
	
	@Test
	public void testViewSingleNews() throws DAOException, ServiceException {
		NewsTO expected = new NewsTO();
		expected.setId(2);
		when (newsService.findNewsById(2)).thenReturn(expected);
		NewsTO actual = newsManager.viewSingleNews(2).getNews();
		verify(newsService).findNewsById(2);
		verify(authorService).findAuthorByNews(2);
		verify(tagService).findTagsByNews(2);
		verify(commentService).findCommentsByNews(2);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testCreateNews() throws DAOException, ServiceException {
		
		long expected = 1;
		NewsTO news = new NewsTO();
		news.setId(expected);
		
		when (newsService.saveNews(news)).thenReturn(1L);
		
		complexNews.setNews(news);
		long actual = newsManager.saveNews(complexNews);
		verify(newsService).saveNews(news);
		verify(newsService).attachNewsAuthor(news.getId(), complexNews.getAuthor().getId());
		verify(newsService).attachNewsTags(news.getId(), complexNews.getTagList());
		assertEquals(expected, actual);	
	}
	
	@Test
	public void testEditNews() throws DAOException, ServiceException {
		
		NewsTO news = new NewsTO();
		complexNews.setNews(news);
		newsManager.editNews(complexNews);
		verify(newsService).editNews(news);
		
	}
	
	@Test
	public void testDeleteNews() throws DAOException, ServiceException {
		
		long id = 1L;
		newsManager.deleteNews(id);
		verify(commentService).deleteCommentsByNews(id);
		verify(newsService).detachNewsAuthor(id);
		verify(newsService).detachNewsTags(id);	
		verify(newsService).deleteNews(id);
		
		
	}
	
	@Test
	public void testDeleteNewsList() throws ServiceException {
		long[] ids = {1L, 2L, 3L};
		newsManager.deleteNews(ids);
		verify(commentService).deleteCommentsByNews(ids);
		verify(newsService).detachNewsAuthors(ids);
		verify(newsService).detachNewsTags(ids);
		verify(newsService).deleteNews(ids);
	}
	
	@Test (expected=ServiceException.class)
	public void testDeleteNewsException() throws ServiceException {
		long id = 1L;
		
		doThrow(ServiceException.class).when(newsService).deleteNews(1);
		newsManager.deleteNews(id);
		
	}
	
	
	
	@Test
	public void testViewNewsListOnPage() throws ServiceException {
		List<NewsTO> list = new ArrayList<NewsTO>();
		list.add(new NewsTO(1L, null, null, null, null, null));
		
		when (newsService.findNewsOnPage(1, 5)).thenReturn(list);
		when (newsService.findNewsById(1L)).thenReturn(new NewsTO());
		when (authorService.findAuthorByNews(1L)).thenReturn(new AuthorTO());
		when (tagService.findTagsByNews(1L)).thenReturn(new ArrayList<TagTO>());
		when (commentService.findCommentsByNews(1L)).thenReturn(new ArrayList<CommentTO>());
		
		newsManager.viewNewsListOnPage(1, 5);
		
		verify(newsService).findNewsOnPage(1, 5);
	}
	
	

}