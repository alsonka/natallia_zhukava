package com.epam.newsmanagement.service.impl;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.entity.UserTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.UserService;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
public class UserServiceImplTest {

	@Mock
	private UserDAO userDao;
	
	@Autowired
	@InjectMocks
	private UserService userService;
	
	public UserServiceImplTest() {		
	}
	
	@Before
	public void setUp() throws DAOException {
        MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testFindUserNameByLogin() throws DAOException, ServiceException  {
		when (userDao.findUserByLogin("login")).thenReturn(new UserTO(1L, "user", "login", "password"));
		String expected = "user";
		String actual = userService.findUserNameByLogin("login");
		assertEquals(expected, actual);
	}
	
	@Test(expected=ServiceException.class)
	public void testFindUserByLoginException() throws DAOException, ServiceException {
		doThrow(DAOException.class).when(userDao).findUserByLogin("user");
		
		String login = "user";
		userService.findUserNameByLogin(login);
	}
	
	
	
	

}