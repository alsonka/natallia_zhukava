<%@ page language="java" contentType="text/javascript; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<fmt:setLocale value="${loc}" />
<fmt:setBundle basename="locale" var="rb" />
<c:set var="commentEmpty"> 
<fmt:message key='validation.comment.empty' bundle='${rb }'/>
</c:set>
var locale = '${loc}';
var commentEmpty = '${commentEmpty}';