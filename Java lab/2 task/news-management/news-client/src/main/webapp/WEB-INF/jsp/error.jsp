<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:choose>
<c:when test="${not empty loc }">
<fmt:setLocale value="${loc}" />
</c:when>
<c:otherwise>
<fmt:setLocale value="ru_RU" />
</c:otherwise>
</c:choose>

<fmt:setBundle basename="locale" var="rb" />
<jsp:useBean id="authorList"
	class="com.epam.newsmanagement.entity.AuthorTO" scope="session"></jsp:useBean>
<jsp:useBean id="newsList" class="com.epam.newsmanagement.entity.NewsVO"
	scope="session"></jsp:useBean>
<jsp:useBean id="tagList" class="com.epam.newsmanagement.entity.TagTO"
	scope="session"></jsp:useBean>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/css/styles.css" rel="stylesheet">
<link href="/css/multiple-select.css" rel="stylesheet" />

<title>List of news</title>

</head>
<body>
	<div class="wrapper">
		<div class="header">
			<h1><fmt:message key="header.title" bundle="${ rb }" /></h1>
			
			<div class="lang">
				<a href="news?act=lang&l=en_US">EN</a> | <a href="news?act=lang&l=ru_RU">RU</a>
			</div>
		</div>
		<div class="content-wrapper">

			<div class="content">
	Request from ${pageContext.errorData.requestURI} is failed
  <br/>
  Servlet name : <c:out value="${pageContext.errorData.servletName }"></c:out>
  ${pageContext.errorData.servletName}
  <br/>
  Status code : ${pageContext.errorData.statusCode}
  <br/>
  Exception: ${pageContext.exception}
  <br/>
    Message from exception: ${pageContext.exception.message}
		
		</div>
		</div>
		<div class="footer">Copyright @ Epam 2015. All rights reserved.
		</div>
</div>
	
</body>
</html>