<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags" %>

<c:choose>
<c:when test="${not empty loc }">
<fmt:setLocale value="${loc}" />
</c:when>
<c:otherwise>
<fmt:setLocale value="ru_RU" />
</c:otherwise>
</c:choose>

<fmt:setBundle basename="locale" var="rb" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/styles.css" rel="stylesheet">
<link href="css/multiple-select.css" rel="stylesheet" />
<script src="js/jquery.min.js"></script>
<script src="js/jstz-1.0.4.min.js"></script>
<script src="js/jquery.multiple.select.js"></script>
<title>List of news</title>

</head>
<body>
	<div class="wrapper">
		<div class="header">
			<h1><fmt:message key="header.title" bundle="${ rb }" /></h1>
			<div class="lang">
				<a href="news?act=lang&l=en_US">EN</a> | <a href="news?act=lang&l=ru_RU">RU</a>
			</div>
		</div>
		<div class="content-wrapper">

			<div class="content">
				<div class="filter">
					<div id="criteria">
					<form action="news" method="POST" >
						<input type="hidden" name="act" value="filter"> 
						<select	name="author" class="ms-choice author">
							<option value="" disabled selected><fmt:message key="author.select" bundle="${ rb }" /></option>
							<c:forEach var="author" items="${authorList}">
								<option value="${author.id}">${author.name}</option>
							</c:forEach>
						</select>
						<div class="multiselect">
							<select id="ms" multiple="multiple" name="tags">
								<c:forEach var="tag" items="${tagList}">
									<option value="${tag.id}">${tag.name}</option>
								</c:forEach>
							</select>

							
							<script>
								$(function() {
									$('#ms').change(function() {
										console.log($(this).val());
									}).multipleSelect({
										width : '100%',
										selectAll : false,
										placeholder : "<fmt:message key='tags.select' bundle='${ rb }' />"
									});
								});
							</script>
						</div>
						<input type="submit" value="<fmt:message key='form.filter' bundle='${ rb }' />"> </form></div>
					<div id="resetform">
					<form action="news" method="POST">
						<input type="hidden" name="act" value="resetsearch">
						<input type="submit" value="<fmt:message key='form.reset' bundle='${ rb }' />">
					</form>
					</div>
			
				</div>
				
				<div class="newslist">
						<c:if test="${!empty searchMessage }"> 
						<fmt:message key='${searchMessage}' bundle='${ rb }' />
						</c:if>
						
						<c:forEach var="newsVO" items="${newsList}">
							<div class="news">
								<span class="title"><c:out value="${newsVO.news.title}"/></span> <span
									class="author">(<fmt:message key='news.author' bundle='${ rb }' /> <c:out value="${newsVO.author.name}"/>)</span> 
									<span class="date">
									<fmt:formatDate dateStyle="MEDIUM" value="${newsVO.news.modificationDate}"/></span> 
									<br>
								<p class="short"><c:out value="${newsVO.news.shortText}" /></p>
								<span class="additional"> 
								<c:forEach var="tag" items="${newsVO.tagList}">
										<span class="tag"><c:out value="${tag.name}"/></span>, 
								</c:forEach> 
								<span class="commentsnum"> <fmt:message key='news.comments' bundle='${ rb }' />(${newsVO.commentList.size() }) </span> 
								<a href="news?act=viewnews&id=${newsVO.news.id}"><fmt:message key='news.view' bundle='${ rb }' /></a>
								</span>
							</div>
						</c:forEach>
					</div>
			
					<ctg:pagination current="${p}" numOfPages="${lastPage}" /> 
		</div>
		
	
		
		
		</div>
		<div class="footer">Copyright @ Epam 2015. All rights reserved.
		</div>
</div>
	
</body>
</html>