package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import static com.epam.newsmanagement.command.Constants.*;


/**
 * The Class PostCommentCommand allows to post comments to news.
 * 
 * @author Natallia_Zhukava
 */
public class PostCommentCommand implements ActionCommand {
	
	private static final String PARAM_NEWS_ID = "newsId";
	private static final String PARAM_COMMENT_TEXT = "commentText";
	private static final String PARAM_UNIQUE_FORM = "unique";

	private CommentService commentService;
	
	/**
	 * Sets the comment service.
	 *
	 * @param commentService the new comment service
	 */
	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	/**
	 * The method gets comment text from form and adds it to database, then forwards to news page
	 * @throws ServiceException 
	 * 
	 * @see com.epam.newsmanagement.command.ActionCommand#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		
		
		HttpSession session = request.getSession();
		String page = (String) session.getAttribute(ATTR_CURRENT);
		
		String uniqueForm = (String) session.getAttribute(ATTR_UNIQUE_FORM);
		String unique = request.getParameter(PARAM_UNIQUE_FORM);
		
		if ((uniqueForm==null) || (!unique.equals(uniqueForm)) ) {
		long newsId = Long.parseLong(request.getParameter(PARAM_NEWS_ID));
		String text = request.getParameter(PARAM_COMMENT_TEXT);
		if ((text!=null) && (!text.equals(""))) {
			
			CommentTO comment = new CommentTO();
			comment.setNewsId(newsId);
			comment.setText(text);
			
			commentService.saveComment(comment);
			session.setAttribute(ATTR_UNIQUE_FORM, unique);
			
		}
	
		}
		
		return page;
	}





}
