package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;



/**
 * A factory for creating ActionCommand objects.
 * 
 */
public class ActionFactory {
	
	
	private static final String PARAM_COMMAND = "act";
	
	
	private WebApplicationContext ctx;
	static Logger logger = Logger.getLogger(ActionFactory.class);

	
	/**
	 * Instantiates a new action factory.
	 *
	 * @param ctx the ctx
	 */
	public ActionFactory(WebApplicationContext ctx) {
		this.ctx=ctx;
	}
	
	/**
	 * Define command.
	 *
	 * @param request the request
	 * @return the action command
	 */
	public ActionCommand defineCommand(HttpServletRequest request)  {
		
		
		ActionCommand current = (ActionCommand)ctx.getBean(CommandEnum.SHOWALL.getName());
		
		String action = request.getParameter(PARAM_COMMAND);
		if (action == null || action.isEmpty()) {
			return current;
		}

		CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
		current = (ActionCommand) ctx.getBean(currentEnum.getName());
	
		return current;
	}
}
