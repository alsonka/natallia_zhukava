
package com.epam.newsmanagement.command;

/**
 * The Enum CommandEnum contains command names.
 *
 */
public enum CommandEnum {

	SHOWALL("showAllNews"), 
	VIEWNEWS("viewNews"),
	POSTCOMMENT("postComment"),
	FILTER("filterNews"),
	RESETSEARCH("resetSearch"),
	LANG("changeLanguage");
	
	private  String  name;
	
	CommandEnum(String name) {
	   this.name = name;
	}
	
	public  String getName() {
		return name;
	}
}