package com.epam.newsmanagement.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class PaginationTag extends TagSupport {

	private static final long serialVersionUID = 1L;
	private String current;
	private String numOfPages;

	public PaginationTag() {
		// TODO Auto-generated constructor stub
	}

	public void setCurrent(String current) {
		this.current = current;
	}

	public void setNumOfPages(String numOfPages) {
		this.numOfPages = numOfPages;
	}

	@Override
	public int doStartTag() throws JspException {
		try {
			int cur;
			int lastPage = Integer.parseInt(numOfPages);
			StringBuilder sb = new StringBuilder("<div class='pages'>");
			if (current == null) {
				cur = 1;
			} else {
				try {
					cur = Integer.parseInt(current);
				} catch (NumberFormatException e) {
					cur = 1;
				}
			}
			for (int i = 1; i <= lastPage; i++) {
				if (i == cur) {
					sb.append("<span class='active pagenum'>").append(i).append("</span>");
				} else {
					sb.append("<a href='news?act=showall&p=").append(i).append("' class='pagenum'>").append(i)
							.append("</a>");
				}
			}
			sb.append("</div>");

			pageContext.getOut().write(sb.toString());
		} catch (IOException e) {
			throw new JspException(e.getMessage());
		}

		return SKIP_BODY;

	}

}
