package com.epam.newsmanagement.utils.validation;


import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.entity.TagTO;

/**
 * The Class TagValidator is designed to validate tag form.
 * 
 * @author Natallia_Zhukava
 */
@Component
public class TagValidator implements Validator{
	
	private static final String REGEX_TAG_FORMAT = "[A-Z\u0410-\u042F\u0401(\\d][A-Za-z\u0410-\u042F\u0401\u0430-\u044F\u0451\\d\\s&.!\\-]{1,29}";
	private static final String FIELD_NAME = "name";
	private static final String MESSAGE_NAME_EMPTY = "validation.tag.name.empty";
	private static final String MESSAGE_NAME_TOO_LONG = "validation.tag.name.tooLong";
	private static final String MESSAGE_NAME_FORMAT = "validation.tag.name.format";
	
	/**
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	public boolean supports(Class<?> clazz) {
		return TagTO.class.isAssignableFrom(clazz);
	}

	/**
	 * The method validates fields of TagTO object
	 * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
	 */
	public void validate(Object target, Errors errors) {
		TagTO tagForm = (TagTO) target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, FIELD_NAME, MESSAGE_NAME_EMPTY);
		String name = tagForm.getName();
		if ((name.length()) > 30) {
			errors.rejectValue(FIELD_NAME, MESSAGE_NAME_TOO_LONG);
		}
		
		if (!name.matches(REGEX_TAG_FORMAT)) {
			errors.rejectValue(FIELD_NAME, MESSAGE_NAME_FORMAT);
		}
		
		
		
		
	}
}