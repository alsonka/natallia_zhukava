package com.epam.newsmanagement.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagement.exception.NotFoundException;



/**
 * The Class GlobalDefaultExceptionHandler handles all  exceptions which were thrown by all controllers.
 */
@ControllerAdvice
public class GlobalDefaultExceptionHandler {
    private static final String DEFAULT_ERROR_VIEW = "error";
    private static final String NOT_FOUND_VIEW = "404";
 //   private static final String ATTR_EXCEPTION = "exception";
    static Logger logger = Logger.getLogger(GlobalDefaultExceptionHandler.class);

    /**
     * Default error handler.
     *
     * @param req the request
     * @param e the exception
     * @return the model and view
     * @throws Exception the exception
     */
    @ExceptionHandler(value = Exception.class)
    public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        // If the exception is annotated with @ResponseStatus rethrow it and let
        // the framework handle it.
        // AnnotationUtils is a Spring Framework utility class.
        if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null)
            throw e;

        // Otherwise setup and send the user to a default error-view.
        ModelAndView mav = new ModelAndView();
        //mav.addObject(ATTR_EXCEPTION, e.getLocalizedMessage());
        logger.error(e);
        mav.setViewName(DEFAULT_ERROR_VIEW);
        return mav;
    }
    
    /**
     * Handle NotFoundException
     *
     * @return the string
     */
    @ExceptionHandler(NotFoundException.class)
        public String handleNotFoundException() {
            return NOT_FOUND_VIEW;
        }

}