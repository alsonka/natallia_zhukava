package com.epam.newsmanagement.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class ParseFieldsUtils is designed to extract fields information from text string.
 * 
 * @author Natallia_Zhukava
 */
public class ParseFieldsUtils {
	
	private static final String PARSE_ID = "=\\d{1,20};";
	private static final String PARSE_NAME = "e=.{1,60}]";

	/**
	 * Parses the string to find id.
	 *
	 * @param string the string
	 * @return the string
	 */
	public static String parseId(String string) {
		String parseID = PARSE_ID;
		Pattern idPattern = Pattern.compile(parseID);
		Matcher m = idPattern.matcher(string);
		m.find();
		String id = m.group();
		id=id.substring(1, id.length()-1);
		return id;
		
	}
	
	/**
	 * Parses the string to find the name.
	 *
	 * @param string the string
	 * @return the string
	 */
	public static String parseName(String string) {
		String parseName = PARSE_NAME;
		Pattern namePattern = Pattern.compile(parseName);
		Matcher m = namePattern.matcher(string);
		m.find();
		String name = m.group();
		name=name.substring(2, name.length()-1);
		return name;
	}
}
