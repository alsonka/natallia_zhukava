package com.epam.newsmanagement.controller;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;

import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.SearchCriteriaVO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.NotFoundException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.SearchService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.utils.ParseFieldsUtils;
import com.epam.newsmanagement.utils.validation.NewsFormValidator;

import org.springframework.ui.ModelMap;
import org.springframework.util.AutoPopulatingList;
import org.springframework.validation.BindingResult;


/**
 * The Class NewsController handles requests about news and comments.
 * 
 * @author Natallia_Zhukava
 */
@Controller
@RequestMapping(value = "/admin")
public class NewsController {

	private static final String DEFAULT_TIMEZONE = "UTC";
	private static final String COOKIE_CLIENT_TIMEZONE = "timezone";
	private static final String LOCALE_EN = "en";
	private static final int ONPAGE = 4;
	private static final String ACTION_ADD = "add";
	private static final String ACTION_POST = "post";
	private static final String ACTION_DEL = "delete";
	private static final String ACTION_UPD = "update";
	private static final String ATTR_CURRENT = "current";
	private static final String ATTR_SEARCH = "searchCriteria";
	private static final String ATTR_NEWS_LIST = "newsList";
	private static final String ATTR_AUTHORS_LIST = "authorList";
	private static final String ATTR_TAGS_LIST = "tagsList";
	private static final String ATTR_LAST_PAGE_NUM = "lastPage";
	private static final String ATTR_CURR_PAGE_NUM = "p";
	private static final String ATTR_SEARCH_MESSAGE = "searchMessage";
	private static final String ATTR_COMPLEX_NEWS = "newsVO";
	private static final String ATTR_COMPLEX_NEWS_FORM = "newsForm";
	private static final String ATTR_PREV_NEXT = "prevNext";
	
	private static final String MESSAGE_NOT_FOUND = "news.notfound";
	
	private static final String NEWS_VIEW = "newslist";
	private static final String NEWS_PAGE_VIEW = "newspage";
	private static final String SHOW_NEWS_REDIRECT = "redirect:/admin/shownews/";
	private static final String ADD_NEWS_VIEW = "addnews";
	private static final String MAIN_PAGE_REDIRECT = "redirect:/admin/news";
	private static final String PARAM_PAGE_NUM = "p";
	private static final String PARAM_AUTHOR = "author";
	private static final String PARAM_TAGS = "tags";
	private static final String PARAM_ID = "id";
	private static final String PARAM_COMMENT_ACT = "commentact";
	private static final String PARAM_NEWS_ACT = "act";
	private static final String PARAM_NEWS_ID = "newsId";
	private static final String PARAM_COMMENT_ID = "commentId";
	private static final String PARAM_COMMENT_TEXT = "commentText";
	private static final String PARAM_NEWS_TO_DELETE = "checkedNews";
	private static final String PAGE_NUM = "1";
	
	private static final String FIELD_WITH_LIST_NAME = "tagList";
	private static final String DATE_FORMAT_US = "MM/dd/yyyy";
	private static final String DATE_FORMAT_RU ="dd/MM/yyyy";
	

	@Autowired
	private AuthorService authorService;
	@Autowired
	private NewsManager newsManager;
	@Autowired
	private NewsService newsService;
	@Autowired
	private TagService tagService;
	@Autowired
	private SearchService searchService;
	@Autowired
	private CommentService commentService;
	@Autowired
	private NewsFormValidator newsFormValidator;

	/**
	 * Shows all news.
	 *
	 * @param model the model
	 * @param page the page
	 * @param session the session
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@RequestMapping(value = "/news", method = RequestMethod.GET)
	public String showNews(ModelMap model, @RequestParam(value = PARAM_PAGE_NUM, required = false) String page,
			HttpSession session) throws ServiceException {

		session.setAttribute(ATTR_CURRENT, NEWS_VIEW);
		List<AuthorTO> authors = null;
		List<NewsVO> news = null;
		List<TagTO> tags = null;

		int pageNum;
		if (page == null) {
			pageNum = 1;
		}
		try {
			pageNum = Integer.parseInt(page);
		} catch (NumberFormatException e) {
			pageNum = 1;
		}

		SearchCriteriaVO obj = (SearchCriteriaVO) session.getAttribute(ATTR_SEARCH);
		if (obj != null) {

			long authorId = obj.getAuthorId();
			List<Long> searchTags = obj.getTagIdList();
			if ((authorId == 0) && (searchTags.size() == 0)) {
				session.removeAttribute(ATTR_SEARCH);
				return this.showNews(model, PAGE_NUM, session);
			} else {
				long newsNum = 0;
				int lastPageNum = 1;
				List<NewsVO> newsOnPage = new ArrayList<NewsVO>();
				

					newsNum = searchService.countNews(obj);
					lastPageNum = searchService.calculateLastPageNum(newsNum, ONPAGE);
					newsOnPage = searchService.findNewsOnPage(pageNum, ONPAGE, obj);
				

				model.addAttribute(ATTR_LAST_PAGE_NUM, lastPageNum);
				model.addAttribute(ATTR_NEWS_LIST, newsOnPage);
			}
		} else {

			long newsNum;
			int lastPageNum = 1;
			
				newsNum = newsService.countAllNews();
				lastPageNum = newsService.calculateLastPageNum(newsNum, ONPAGE);
				news = newsManager.viewNewsListOnPage(pageNum, ONPAGE);
			

			model.addAttribute(ATTR_LAST_PAGE_NUM, lastPageNum);
			model.addAttribute(ATTR_NEWS_LIST, news);
		}

		
			authors = authorService.findAllAuthors();
			tags = tagService.findAllTags();
		

		model.addAttribute(ATTR_AUTHORS_LIST, authors);
		model.addAttribute(ATTR_TAGS_LIST, tags);

		model.addAttribute(ATTR_CURR_PAGE_NUM, pageNum);

		return NEWS_VIEW;

	}

	/**
	 * Searches news.
	 *
	 * @param model the model
	 * @param author the author
	 * @param tags the tags
	 * @param session the session
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@RequestMapping(value = "/news", method = RequestMethod.POST)
	public String searchNews(ModelMap model, @RequestParam(value = PARAM_AUTHOR, required = false) String author,
			@RequestParam(value = PARAM_TAGS, required = false) String[] tags, HttpSession session) throws ServiceException {

		session.setAttribute(ATTR_CURRENT, NEWS_VIEW);
		List<AuthorTO> authors = null;
		List<TagTO> tagList = null;

		long authorId;
		if (author == null) {
			authorId = 0;
		}
		try {
			authorId = Long.parseLong(author);
		} catch (NumberFormatException e) {
			authorId = 0;
		}

		List<Long> searchTags = new ArrayList<Long>();

		if (tags != null) {
			for (String tag : tags) {
				Long tagId = Long.parseLong(tag);
				searchTags.add(tagId);
			}
		}

		SearchCriteriaVO obj = new SearchCriteriaVO();
		obj.setAuthorId(authorId);
		if (searchTags.size() != 0) {
			obj.setTagIdList(searchTags);
		}

		if ((authorId == 0) && (searchTags.size() == 0)) {
			session.removeAttribute(ATTR_SEARCH);
			return this.showNews(model, PAGE_NUM, session);
		} else {

			List<NewsVO> news = new ArrayList<NewsVO>();
			long newsNum;
			int pageNum = 1;
			int lastPage = 1;
	
				authors = authorService.findAllAuthors();
				tagList = tagService.findAllTags();
				newsNum = searchService.countNews(obj);
				lastPage = searchService.calculateLastPageNum(newsNum, ONPAGE);
				news = searchService.findNewsOnPage(pageNum, ONPAGE, obj);

			

			session.setAttribute(ATTR_SEARCH, obj);
			model.addAttribute(ATTR_NEWS_LIST, news);
			model.addAttribute(ATTR_AUTHORS_LIST, authors);
			model.addAttribute(ATTR_TAGS_LIST, tagList);
			model.addAttribute(ATTR_LAST_PAGE_NUM, lastPage);
			if (news.size() == 0) {
				model.addAttribute(ATTR_SEARCH_MESSAGE, MESSAGE_NOT_FOUND);
			}
		}

		return NEWS_VIEW;

	}

	/**
	 * Shows single news.
	 *
	 * @param model the model
	 * @param id the id
	 * @param session the session
	 * @return the string
	 * @throws ServiceException the service exception
	 * @throws NotFoundException the not found exception
	 */
	@RequestMapping(value = "/shownews/{id}", method = RequestMethod.GET)
	public String showSingleNews(ModelMap model, @PathVariable(value = PARAM_ID) String id, HttpSession session) throws ServiceException, NotFoundException {

		long newsId=0;
		try {
			 newsId = Long.parseLong(id);
		}
		catch(NumberFormatException e) {
			throw new NotFoundException();
		}
		
		session.setAttribute(ATTR_CURRENT, "");
		NewsVO news = null;
		long[] prevNext = null;
		SearchCriteriaVO obj = (SearchCriteriaVO) session.getAttribute(ATTR_SEARCH);
 
	
			news = newsManager.viewSingleNews(newsId);
			if (news.getNews()==null) {throw new NotFoundException(); }
			if (obj != null) {
				prevNext = newsService.findPreviousAndNext(newsId, obj);
			} else {
				prevNext = newsService.findPreviousAndNext(newsId);
			}

		
		model.addAttribute(ATTR_COMPLEX_NEWS, news);
		model.addAttribute(ATTR_PREV_NEXT, prevNext);

		return NEWS_PAGE_VIEW;

	}

	/**
	 * Actions with comment.
	 *
	 * @param model the model
	 * @param action the action
	 * @param newsId the news id
	 * @param commentId the comment id
	 * @param text the text
	 * @param session the session
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@RequestMapping(value = "/comments", method = RequestMethod.POST)
	public String actionsComment(ModelMap model, @RequestParam(value = PARAM_COMMENT_ACT) String action,
			@RequestParam(value = PARAM_NEWS_ID) long newsId,
			@RequestParam(value = PARAM_COMMENT_ID, required = false) Long commentId,
			@RequestParam(value = PARAM_COMMENT_TEXT, required = false) String text, HttpSession session,
			HttpServletRequest request) throws ServiceException {

		if (action.equals(ACTION_POST)) {
			if ((text != null) && (!text.equals(""))) {
				text = StringEscapeUtils.escapeHtml4(text);

				CommentTO comment = new CommentTO();
				comment.setNewsId(newsId);
				comment.setText(text);

				commentService.saveComment(comment);

			}
		} else if (action.equals(ACTION_DEL)) {
		
				commentService.deleteComment(commentId);
		}

		return SHOW_NEWS_REDIRECT + newsId;

	}

	/**
	 * Adds the news.
	 *
	 * @param model the model
	 * @param session the session
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@RequestMapping(value = "/addnews", method = RequestMethod.GET)
	public String addNews(ModelMap model, HttpServletRequest request,  HttpSession session) throws ServiceException {
		List<AuthorTO> authors = null;
		List<TagTO> tags = null;
		session.setAttribute(ATTR_CURRENT, ADD_NEWS_VIEW);

		NewsTO news = new NewsTO();
		
		Calendar calendar = Calendar.getInstance();
		news.setCreationDate(calendar.getTime());

		AuthorTO author = new AuthorTO();
		List<TagTO> tagList = new AutoPopulatingList<TagTO>(TagTO.class);

		NewsVO complexNews = new NewsVO();
		complexNews.setAuthor(author);
		complexNews.setNews(news);
		complexNews.setTagList(tagList);

		
			authors = authorService.findActualAuthors();
			tags = tagService.findAllTags();
		

		model.addAttribute(ATTR_AUTHORS_LIST, authors);
		model.addAttribute(ATTR_TAGS_LIST, tags);
		model.addAttribute(ATTR_COMPLEX_NEWS_FORM, complexNews);

		return ADD_NEWS_VIEW;

	}

	/**
	 * Saves news.
	 *
	 * @param model the model
	 * @param action the action
	 * @param news the news
	 * @param result the result
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@RequestMapping(value = "/savenews", method = RequestMethod.POST)
	public String saveNews(ModelMap model, @RequestParam(value = PARAM_NEWS_ACT) String action,
			@ModelAttribute(ATTR_COMPLEX_NEWS_FORM) NewsVO news, BindingResult result,
			HttpServletRequest request) throws ServiceException {

		newsFormValidator.validate(news, result);
		if (result.hasErrors()) {
			List<AuthorTO> authors = authorService.findActualAuthors();
			List<TagTO> tags = tagService.findAllTags();
			
			model.addAttribute(ATTR_AUTHORS_LIST, authors);
			model.addAttribute(ATTR_TAGS_LIST, tags);
			model.addAttribute(ATTR_COMPLEX_NEWS_FORM, news);
			return ADD_NEWS_VIEW;
		} 
		
		long resultNews = 0;
		TimeZone clientTimezone = getClientTimezone(request);

		if (action.equals(ACTION_ADD)) {
			NewsTO newNews = news.getNews();
			Date date = newNews.getCreationDate();
			Date dateWithTime = addTimeToDate(date, clientTimezone);
			newNews.setCreationDate(dateWithTime);
			newNews.setModificationDate(dateWithTime);
			news.setNews(newNews);
			resultNews = newsManager.saveNews(news);
		}
		if (action.equals(ACTION_UPD)) {
			NewsTO editedNews = news.getNews();
			Date date = editedNews.getModificationDate();
			Date dateWithTime = addTimeToDate(date, clientTimezone);
			editedNews.setModificationDate(dateWithTime);
			news.setNews(editedNews);
			newsManager.editNews(news);

			resultNews = news.getNews().getId();
		}

		return SHOW_NEWS_REDIRECT + resultNews;

	}

	/**
	 * Deletes news.
	 *
	 * @param model the model
	 * @param newsId the news id
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@RequestMapping(value = "/deletenews", method = RequestMethod.POST)
	public String deleteNews(ModelMap model, @RequestParam(value = PARAM_NEWS_TO_DELETE, required = false) long[] newsIds)
			throws ServiceException {

		if (newsIds.length != 0) {
			newsManager.deleteNews(newsIds);
			}
		

		return MAIN_PAGE_REDIRECT;
	}
	
	private Date addTimeToDate(Date date, TimeZone timezone) {
		Calendar cal = Calendar.getInstance(timezone);
		cal.setTime(date);
			
		Calendar curCal = Calendar.getInstance(timezone);
		int hour = curCal.get(Calendar.HOUR_OF_DAY);
		int minute = curCal.get(Calendar.MINUTE);
		
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, minute);
		Date dateWithTime = cal.getTime();
		
		return dateWithTime;
	}

	private TimeZone getClientTimezone(HttpServletRequest request) {
		String cookieName = COOKIE_CLIENT_TIMEZONE;
		String timezone = DEFAULT_TIMEZONE;
		Cookie[] cookies = request.getCookies();
		for ( int i=0; i<cookies.length; i++) {
			  Cookie cookie = cookies[i];
			  if (cookieName.equals(cookie.getName()))
				  timezone = cookie.getValue();
			  }
		TimeZone clientTimezone = TimeZone.getTimeZone(timezone);
		return clientTimezone;
	}
	/**
	 * Inits the binder.
	 *
	 * @param webDataBinder the web data binder
	 */
	@InitBinder
	public void initBinder(WebDataBinder webDataBinder, HttpServletRequest request) {
		Locale locale = LocaleContextHolder.getLocale();
		String chosenFormat;
		if (locale.toLanguageTag().equals(LOCALE_EN)) {
			chosenFormat = DATE_FORMAT_US;
		}
		else {
			chosenFormat = DATE_FORMAT_RU;
		}
				
		TimeZone clientTimezone = getClientTimezone(request);
		SimpleDateFormat dateFormat = new SimpleDateFormat(chosenFormat);
		dateFormat.setTimeZone(clientTimezone);
		dateFormat.setLenient(false);
		webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
		
		webDataBinder.registerCustomEditor(List.class, FIELD_WITH_LIST_NAME, new CustomCollectionEditor(List.class) {
			protected Object convertElement(Object element) {
				if (element instanceof TagTO) {
					return element;
				}
				if (element instanceof String) {
					TagTO tag = new TagTO();
					String stringId = ParseFieldsUtils.parseId((String) element);
					long id = Long.parseLong(stringId);
					String name = ParseFieldsUtils.parseName((String) element);
					tag.setId(id);
					tag.setName(name);

					return tag;
				} else {

					return null;
				}

			}
		});

	}

	/**
	 * Edits the news.
	 *
	 * @param model the model
	 * @param id the id
	 * @param session the session
	 * @return the string
	 * @throws ServiceException the service exception
	 * @throws NotFoundException the not found exception
	 */
	@RequestMapping(value = "/editnews/{id}", method = RequestMethod.GET)
	public String editNews(ModelMap model, @PathVariable(value = PARAM_ID) String id, HttpSession session) throws ServiceException, NotFoundException {
		List<AuthorTO> authors = null;
		List<TagTO> tags = null;
		session.setAttribute(ATTR_CURRENT, ADD_NEWS_VIEW);

		long newsId=0;
		try {
			 newsId = Long.parseLong(id);
		}
		catch(NumberFormatException e) {
			throw new NotFoundException();
		}
		
		NewsVO complexNews = null;

		
			complexNews = newsManager.viewSingleNews(newsId);
			if (complexNews.getNews()==null){ throw new NotFoundException(); }
			authors = authorService.findActualAuthors();
			AuthorTO author = complexNews.getAuthor();
			if (author.getExpiredDate()!=null) {
				authors.add(author);
			}
			tags = tagService.findAllTags();

			Calendar calendar = Calendar.getInstance();
			Date now = calendar.getTime();
     		complexNews.getNews().setModificationDate(now);
		

		model.addAttribute(ATTR_AUTHORS_LIST, authors);
		model.addAttribute(ATTR_TAGS_LIST, tags);
		model.addAttribute(ATTR_COMPLEX_NEWS_FORM, complexNews);

		return ADD_NEWS_VIEW;

	}
}
