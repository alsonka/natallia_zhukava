package com.epam.newsmanagement.controller;


import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.utils.validation.AuthorValidator;

import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;


/**
 * The Class AuthorsController manages requests about AuthorTO entities.
 * 
 * @author Natallia_Zhukava
 */
@Controller
@RequestMapping(value = "/admin")
public class AuthorsController{
	
	private final static String AUTHORS_VIEW = "authors";
	private final static String AUTHORS_REDIRECT = "redirect:/admin/authors";
	private final static String PARAM_AUTHOR_ID = "id";
	private final static String ATTR_CURRENT = "current";
	private final static String ATTR_AUTHORS_LIST = "authorList";
	private final static String ATTR_NEW_AUTHOR = "newAuthor";
	private final static String ATTR_EDITED_AUTHOR = "editAuthor";
	
	@Autowired
	private AuthorService authorService;
	
	@Autowired
	private AuthorValidator authorValidator;
	
	/**
	 * Views all authors.
	 *
	 * @param model the model
	 * @param session the session
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@RequestMapping(value = "/authors", method = RequestMethod.GET)
	public String viewAuthors(ModelMap model, HttpSession session) throws ServiceException {
		session.setAttribute(ATTR_CURRENT, AUTHORS_VIEW);
		List<AuthorTO> authorList = authorService.findActualAuthors();
		AuthorTO author = new AuthorTO();
		
		
		model.addAttribute(ATTR_AUTHORS_LIST, authorList);
		model.addAttribute(ATTR_NEW_AUTHOR, author);
		return AUTHORS_VIEW;

	}
	
	/**
	 * Edits the author.
	 *
	 * @param model the model
	 * @param authorId the author id
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@RequestMapping(value = "/authors", method = RequestMethod.POST)
	public String editAuthor(ModelMap model,
			@RequestParam(value=PARAM_AUTHOR_ID) long authorId
			) throws ServiceException {
		List<AuthorTO> authorList =  authorService.findActualAuthors();
		AuthorTO author = authorService.findAuthorById(authorId);
		AuthorTO newAuthor = new AuthorTO();
	
	
		
		model.addAttribute(ATTR_AUTHORS_LIST, authorList);
		model.addAttribute(ATTR_EDITED_AUTHOR, author);
		model.addAttribute(ATTR_NEW_AUTHOR, newAuthor);
		
		return AUTHORS_VIEW;

	}
	
	/**
	 * Expires author.
	 *
	 * @param model the model
	 * @param authorId the author id
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@RequestMapping(value = "/expire", method = RequestMethod.POST)
	public String expireAuthor(ModelMap model,
			@RequestParam(value=PARAM_AUTHOR_ID) long authorId
			) throws ServiceException {
		
			authorService.makeExpired(authorId);
				
		return AUTHORS_REDIRECT;

	}
	
	/**
	 * Saves author.
	 *
	 * @param model the model
	 * @param author the author
	 * @param result the result
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@RequestMapping(value = "/saveauthor", method = RequestMethod.POST)
	public String saveAuthor(ModelMap model,
			@ModelAttribute(ATTR_NEW_AUTHOR) AuthorTO author,
			BindingResult result
			) throws ServiceException {
		
			authorValidator.validate(author, result);
			if (result.hasErrors()) {
			List<AuthorTO> authorList = authorService.findActualAuthors();
			model.addAttribute(ATTR_AUTHORS_LIST, authorList);
			model.addAttribute(ATTR_NEW_AUTHOR, author);
			return AUTHORS_VIEW;
		}
		
			authorService.saveAuthor(author);
			
		return AUTHORS_REDIRECT;
	}
	
	/**
	 * Updates author.
	 *
	 * @param model the model
	 * @param author the author
	 * @param result the result
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@RequestMapping(value = "/updauthor", method = RequestMethod.POST)
	public String updateAuthor(ModelMap model,
			@ModelAttribute(ATTR_EDITED_AUTHOR) AuthorTO author,
			BindingResult result
			) throws ServiceException {
		authorValidator.validate(author, result);
		if (result.hasErrors()) {
			List<AuthorTO> authorList = authorService.findActualAuthors();
			AuthorTO newAuthor = new AuthorTO();
		
			model.addAttribute(ATTR_AUTHORS_LIST, authorList);
			model.addAttribute(ATTR_EDITED_AUTHOR, author);
			model.addAttribute(ATTR_NEW_AUTHOR, newAuthor);
			return AUTHORS_VIEW;
		}
		
			authorService.editAuthor(author);
		
		return AUTHORS_REDIRECT;

	}
	
	/**
	 * if language was switched while author form was edited - redirect to Authors page
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/updauthor", method = RequestMethod.GET)
	public String updAuthorLangSwitch(ModelMap model)  {
		
		return AUTHORS_REDIRECT;

	}
	
	/**
	 * if language was switched while author form was edited - redirect to Authors page
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/saveauthor", method = RequestMethod.GET)
	public String saveAuthorLangSwitch(ModelMap model)  {
		return AUTHORS_REDIRECT;
	}
	

	

	
	
}