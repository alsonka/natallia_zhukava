package com.epam.newsmanagement.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 * The Class NotFoundException is runtime exception which is thrown when requested page cannot be found.
 * 
 * @author Natallia_Zhukava
 */
@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Page not found")
public class NotFoundException extends RuntimeException {


	private static final long serialVersionUID = -2105468709651071532L;

	/**
	 *  Instantiates a new NotFound exception.
	 */
	public NotFoundException() {
		
	}

	/**
	 *  Instantiates a new NotFound exception.
	 *
	 * @param message the message
	 */
	public NotFoundException(String message) {
		super(message);
		
	}

	/**
	 *  Instantiates a new NotFound exception.
	 *
	 * @param cause the cause
	 */
	public NotFoundException(Throwable cause) {
		super(cause);
		
	}

	/**
	 *  Instantiates a new NotFound exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public NotFoundException(String message, Throwable cause) {
		super(message, cause);
		
	}

	/**
	 * Instantiates a new NotFound exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 * @param enableSuppression the enable suppression
	 * @param writableStackTrace the writable stack trace
	 */
	public NotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

}
