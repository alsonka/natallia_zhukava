<%@ page language="java" contentType="text/javascript; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<c:set var="locale" value="${cookie['localizationCookie'].value}" />
<fmt:setLocale value="${locale}" />
<c:set var="titleEmpty"><spring:message code="validation.title.empty" /></c:set>
<c:set var="titleTooLong"><spring:message code="validation.title.tooLong" /></c:set>
<c:set var="shortEmpty"><spring:message code="validation.shortText.empty" /></c:set>
<c:set var="fullEmpty"><spring:message code="validation.fullText.empty" /></c:set>

<c:set var="authorEmpty" ><spring:message code='validation.author.name.empty' /></c:set>
<c:set var="authorFormat"><spring:message code='validation.author.name.format' /></c:set>
<c:set var="authorTooLong"><spring:message code='validation.author.name.tooLong' /></c:set>

<c:set var="tagEmpty" ><spring:message code='validation.tag.name.empty' /></c:set>
<c:set var="tagFormat"><spring:message code='validation.tag.name.format' /></c:set>
<c:set var="tagTooLong"><spring:message code='validation.tag.name.tooLong' /></c:set>

var titleEmpty = '${titleEmpty}';
var titleTooLong = '${titleTooLong}';
var shortEmpty = '${shortEmpty}';
var fullEmpty = '${fullEmpty}';

var authorEmpty = '${authorEmpty}';
var authorFormat = '${authorFormat}';
var authorTooLong = '${authorTooLong}';

var tagEmpty = '${tagEmpty}';
var tagFormat = '${tagFormat}';
var tagTooLong = '${tagTooLong}';


