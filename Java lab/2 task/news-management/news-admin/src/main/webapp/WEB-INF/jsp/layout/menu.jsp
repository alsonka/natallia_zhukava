<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="menu">
<ul>
<li><a href="<c:url value='/admin/news' />" 
<c:if test='${current=="newslist" }'>class="highlight"</c:if> ><spring:message code="menu.newslist" />
</a></li>

<li><a href="<c:url value='/admin/addnews'/>" <c:if test='${current=="addnews" }'>class="highlight"</c:if> ><spring:message code="menu.addnews" /></a></li>
<li><a href="<c:url value='/admin/authors'/>" <c:if test='${current=="authors" }'>class="highlight"</c:if> ><spring:message code="menu.authors" /></a></li>
<li><a href="<c:url value='/admin/tags' /> " <c:if test='${current=="tags" }'>class="highlight"</c:if> ><spring:message code="menu.tags" /></a></li>
</ul>
</div>