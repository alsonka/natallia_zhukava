<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
     
   
   <form class="login" action="j_spring_security_check" method="post">
   <div class="left">
   <label for="login"><spring:message code="loginform.login" />: </label> <br>
   <label for="password"><spring:message code="loginform.password" />: </label>
   </div>
   <div class="right">
   <input type="text" name="username" /> <br>
   <input type="password" name="password"> <br>
    <input type="submit" value="<spring:message code='loginform.log.in' />">
   </div>
   <c:if test="${loginerror!=null }">
   <div class="error">
 <spring:message code="${loginerror}" />
   </div>
   ${message}
   </c:if>
   </form>
