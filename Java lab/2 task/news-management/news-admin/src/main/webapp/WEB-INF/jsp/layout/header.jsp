
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<div class="header">
			<h1><spring:message code="header.title" /></h1>
			<!--   <span id="timezone"></span>
			<span id="cookie"></span>--> 
			<div class="right">
			<span class="logout">
			<c:if test="${user!=null }">
			<span class="welcome"><spring:message code="header.welcome" />, <span class="username">${user}</span> </span>       
			<a href="<c:url value='/logout' /> "><spring:message code="header.logout" /></a>
			</c:if>
			</span>
			
			<span class="lang">
				<a href="?lang=en">EN</a> | <a href="?lang=ru">RU</a>
			</span>
			</div>
		</div>