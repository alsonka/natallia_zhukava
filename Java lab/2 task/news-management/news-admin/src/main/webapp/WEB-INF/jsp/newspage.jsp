<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:set var="timezone" value="${cookie.timezone.value }" />
<fmt:setTimeZone value="${timezone }" />
<c:set var="locale" value="${cookie.org.springframework.web.servlet.i18n.CookieLocaleResolver.LOCALE.value }" />
<fmt:setLocale value="${locale }" />

<a href="<c:url value="/admin/news" />"><spring:message code='keys.back' /></a>
	<div class="fullnews news">
	<span class="title"><c:out value="${newsVO.news.title}" /></span> 
	<span class="author">(<spring:message code='news.author'  /> ${newsVO.author.name})</span>
	<span class="date"><fmt:formatDate dateStyle="MEDIUM" value="${newsVO.news.modificationDate}" /></span>
	<br>
	
	<p class="short"><c:out value="${newsVO.news.shortText}" /></p>
	<p class="full"><c:out value="${newsVO.news.fullText}" /></p>
	</div>
	
	<div class="comments">
	<c:forEach var="comment" items="${newsVO.commentList}">
	<div class="comment">
	 <span class="date"><fmt:formatDate type="both"  dateStyle="MEDIUM" timeStyle="SHORT" value="${comment.creationDate}"/>	</span>
	<br>
	<div class="text">
	<form action="<c:url value='/admin/comments' />" method="POST" class="deletecomment">
	<input type="hidden" name="commentact" value="delete">
	<input type="hidden" name="commentId" value="${comment.id}">
	<input type="hidden" value="${newsVO.news.id}" name="newsId">
	<input type="submit" value="X">
	</form>
	<c:out value="${comment.text }" />
	</div>
	
	</div>
	</c:forEach>
	<form action="<c:url value='/admin/comments' />" method="post" class="postcomment">
	<input type="hidden" name="commentact" value="post">
	<input type="hidden" value="${newsVO.news.id}" name="newsId">
	<textarea class="text" maxlength="100" name="commentText"  required ></textarea>
	<input type="submit" value="<spring:message code='comment.post'  />"> 
	</form>
	
	</div>		
	
	<div class="controls">
	<c:if test="${prevNext[0]!=0 }">
	<a class="prev" href="<c:url value='/admin/shownews/${prevNext[0]}/'/>"><spring:message code='keys.previous'  /></a>
	</c:if>
	<c:if test="${prevNext[1]!=0 }">
	<a class="next" href="<c:url value='/admin/shownews/${prevNext[1]}/'/>"><spring:message code='keys.next'  /></a>
	</c:if>
	</div>
