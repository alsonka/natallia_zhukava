<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="tags">

<c:forEach var="tag" items="${tagsList}">
<c:choose>
<c:when test="${tag.id==editTag.id }">
<div class="tag">
<form:form action="updtag" modelAttribute="editTag" id="editTag" method="POST" class="formwithtext">
<form:errors path="name" cssStyle="color:red;" /><br>
<label><spring:message code="tags.tag" />: </label> 
<form:input type="hidden" path="id" />
<form:input type="text"  path="name" id="editTagName" /> 
<input type="submit" value="<spring:message code='tags.update' />" />
</form:form>
<form action="del" method="post" class="formbutton">
<input type="submit" value="<spring:message code='tags.delete' />" />
<input type="hidden" name="id" value="${tag.id }">
<a href="<c:url value='/admin/tags' /> "><spring:message code="tags.cancel" /></a> <br>
</form>
<span id="editError" class="error"></span>

</div>
</c:when>
<c:otherwise>
<div class="tag">
<form action="tags" method="post" class="blocked">
<input type="hidden" name="id" value="${tag.id }">
<label><spring:message code="tags.tag" />: </label> 
<input type="text" value="${tag.name}" name="${tags.id}" disabled> 
<input type="submit" value="<spring:message code='tags.edit' />" />
</form>
</div>
</c:otherwise>
</c:choose>

</c:forEach>


</div>
<div class="savetag">
<form:form action="savetag" modelAttribute="newTag" id="newTag" method="POST"> 
<label><spring:message code="tags.addtag" />: </label> 
<form:input path="name" id="saveTagName" />
<input type="submit" value="<spring:message code="tags.save" />"><br>
<form:errors path="name" cssStyle="color:red;" />
<span id="saveError" class="error"></span>
</form:form>

</div>