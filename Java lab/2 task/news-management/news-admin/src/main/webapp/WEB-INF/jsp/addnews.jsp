<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<c:set var="timezone" value="${cookie.timezone.value }" />
<fmt:setTimeZone value="${timezone}" />	


<script src="<c:url value='/resources/js/jquery-ui.js'/>" /></script>
<script src="<c:url value='/resources/js/datepicker-us.js'/>" /></script>
<script src="<c:url value='/resources/js/datepicker-ru.js'/>" /></script>
<script src="<c:url value='/resources/js/datepicker.js'/>" /></script>

<form:form action=" ${pageContext.request.contextPath}/admin/savenews" method="POST" 
modelAttribute="newsForm" class="addnews" id="newsForm">
<c:choose>
<c:when test="${newsForm.news.id==0}">
<input type="hidden" name="act" value="add">
</c:when>
<c:otherwise>
<input type="hidden" name="act" value="update">
<form:input path="news.id" type="hidden"/> 
<form:input path="news.creationDate" type="hidden"/> 
</c:otherwise>
</c:choose>
	<div class="field">
		<label for="title"><spring:message code="addnews.title" />: </label>
		<form:input type="text" id="title" path="news.title" class="title" reqiured="" />
		
		<form:errors path="news.title" cssStyle="color:red;"/>
		<span id="titleError" class="error">
		</span>
	</div>
	<div class="field">
		<label for="date"><spring:message code="addnews.date" />: </label>
		<c:choose>
		<c:when test="${newsForm.news.id==0}">
		<form:input path="news.creationDate" class="date" required="required" id="datePicker" />
		<form:errors path="news.creationDate" cssStyle="color:red;"/>
		</c:when>
		<c:otherwise>
		<form:input path="news.modificationDate" class="date" required="required" id="datePicker"/>
		<form:errors path="news.modificationDate" cssStyle="color:red;"/>
		</c:otherwise>
		</c:choose>
		

	</div>
	<div class="field">
		<label for="short"><spring:message code="addnews.short" />: </label>
		<form:textarea id="shortText" path="news.shortText" name="short" class="short" maxlength="100" reqiured="required"  ></form:textarea>
	     <form:errors path="news.shortText" cssStyle="color:red;"/>
	     <span id="shortError" class="error"></span>
	</div>
	<div class="field">
		<label for="full"><spring:message code="addnews.full" />: </label>
		<form:textarea id="fullText" name="full" path="news.fullText" class="full" maxlength="2000" reqiured="required" ></form:textarea>
		<form:errors path="news.fullText" cssStyle="color:red;"/>
		<span id="fullError" class="error"></span>
		<br>
	</div>

	<br>
	<div class='filter'>
		<form:select name="author" path="author.id" class="ms-choice author" items="${authorList}"
		itemLabel='name' itemValue='id' required="required"> 
		</form:select>
		<form:errors path="author.id" cssStyle="color:red;"/>	
		<div class="multiselect">
		
	 	
			
			 <form:select id="ms" items="${tagsList}" 
			itemLabel='name' path="tagList" multiple="multiple" >
			</form:select> 
 			<form:errors path="tagList" cssStyle="color:red;"/>
 		
			<script>
				$(function() {
					$('#ms').change(function() {
						console.log($(this).val());
					}).multipleSelect({
						width : '100%',
						selectAll : false,
					});
				});
			</script>
		</div>	
	</div>
	<br>
	<input type="submit" value="<spring:message code='addnews.save'/> ">

</form:form>