<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div style="text-align:middle">
<h2><spring:message code="error.404" /></h2>
<c:url value='/images/cat_eyes.png' var="cat" />
<img src="${cat}">
</div>