<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="timezone" value="${cookie.timezone.value }" />
<fmt:setTimeZone value="${timezone }" scope="session"/>
<c:set var="locale" value="${cookie.org.springframework.web.servlet.i18n.CookieLocaleResolver.LOCALE.value }" />
<fmt:setLocale value="${locale }" />


<div class="filter">
					<div id="criteria">
					<form action="news" method="POST" >
						<input type="hidden" name="act" value="filter"> 
						<select	name="author" class="ms-choice author">
							<option value="" disabled selected><spring:message code="author.select" /></option>
							<c:forEach var="author" items="${authorList}">
								<option value="${author.id}">${author.name}</option>
							</c:forEach>
						</select>
						<div class="multiselect">
							<select id="ms" multiple="multiple" name="tags">
								<c:forEach var="tag" items="${tagsList}">
									<option value="${tag.id}">${tag.name}</option>
								</c:forEach>
							</select>

							
							<script>
								$(function() {
									$('#ms').change(function() {
										console.log($(this).val());
									}).multipleSelect({
										width : '100%',
										selectAll : false,
										placeholder : "<spring:message code='tags.select' />"
									});
								});
							</script>
						</div>
						<input type="submit" value="<spring:message code='form.filter' />"> </form></div>
					<div id="resetform">
					<form action="news" method="POST">
						<input type="submit" value="<spring:message code='form.reset'  />">
					</form>
					</div>
			
				</div>
				<div class="newslist">
						<c:if test="${!empty searchMessage }"> 
						<spring:message code='${searchMessage}' />
						</c:if>
						<form action="deletenews" method="post" class="delete">
								<c:forEach var="newsVO" items="${newsList}">
							<div class="news">
								<span class="title"><a href="<c:url value='/admin/shownews/${newsVO.news.id}/' /> "><c:out value="${newsVO.news.title}" /></a></span> <span
									class="author">(<spring:message code='news.author' /> <c:out value="${newsVO.author.name}" />)</span> 
									<span class="date">
									<fmt:formatDate dateStyle="MEDIUM" value="${newsVO.news.modificationDate}"/></span> 
									<br>
								<p class="short"><c:out value="${newsVO.news.shortText}" /></p>
								<span class="additional"> 
								<c:forEach var="tag" items="${newsVO.tagList}">
										<span class="tag"><c:out value="${tag.name}" /></span>, 
								</c:forEach> 
								<span class="commentsnum"> <spring:message code='news.comments'  />(${newsVO.commentList.size() }) </span> 
								<a href="<c:url value='/admin/editnews/${newsVO.news.id}/' />" ><spring:message code='news.edit'  /></a>
								<input type="checkbox" name="checkedNews" value="${newsVO.news.id}">
								</span>
							</div>
						</c:forEach>
						<input type="submit" value="<spring:message code='news.delete' />" >
						</form>
					</div>
				
					<ctg:pagination numOfPages="${lastPage}" current="${p}" />