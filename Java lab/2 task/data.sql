
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('1', 'Oleg Kril');
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('2', 'Alex Barchuk');INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('3', 'Julia Ivanova');
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('4', 'Vasily Bodrov');
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('5', 'Julia Ryabuhina');
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('6', 'Alex Lyapota');
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('7', 'Nastassia Demidova');
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('8', 'Olga Gusakova');
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('9', 'Kirill Scherbakov');
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('10', 'Denis Fedorov');
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('11', 'Semyon Sokolov');
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('12', 'Alex Medvedev');
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('13', 'Sofia Kovaleva');
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('14', 'Alina Dorofeeva');
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('15', 'Mikhail Korotkevich');
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('16', 'Nastassia Komarova');
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('17', 'Nikolai Luskovich');
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('18', 'Maria Sherbina');
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('19', 'Oleg Petrov');
INSERT INTO "NEWS_MANAGEMENT"."AUTHOR" (AUTHOR_ID, AUTHOR_NAME) VALUES ('20', 'Marina Petuhova');



INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('1', 'C�������� � 3D-�������', 'Microsoft ��������� ��������� � 3D-�������', 'Microsoft �������� ��� ����� ��������, ������� �������� ��������� ������������������ 3D-����������� � ������ ��������� ������� ��� ������ ������� ����������. �������� �����, ����� ��� ���� ��� �� ������ � �����, ��� ������ ���������� ��� �����.
 �� ������ ������ ��������, ������� ������������ ������������, �������� ��������������� ���������� ��� ���� �������� �� ����� ���������� � Microsoft Research, ��� ������� �������� ������������� ������� ���-��, ������� �� ������� ��������, �� �������������� ����� ����������� 3D-������.
������������ ������� ��� ������� MobileFusion � ����������, ��� ���� ����� �������� 3D-�������� ����� ���� ������������ ������ ����� ������, ��� �� �� ������� ������� �������������� ���������� ������� � ����������� � ����.', TO_TIMESTAMP('2015-08-25 07:52:52.619000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-25 07:53:01', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('2', 'C����� ��� ������ ���� ���', 'Google ���������� ������������� ������ ��� ������ ������������ ���', '� ������ ����� ���� ����� ��������, ��� �������� Google ���� ���������� ����� ���������� ���� Tablescape, ������� �������� ������������ ������������ ���. ������ ������ ����� ������ Google+, �� ����� �������� � ���� ���������� ����������. � Tablescape ��� ���������� ������ �������������� �� ����������, � ����� �� ����������� � �����. ������ ��������� ������� ������ ��� ������, �� Google �������, ��� �� ���������� ������ ��������� Tablescape � ����� ��.

������� ��� ������� �������� �����-�� ��������, ��� ��� ������ Google ��������� ����� �������, ��������� � ��������� ���������� ����, � ���� ���������������� �������. �������� ���������� � ���� ���������, ����� �� ������ ������� ���� � �����-�� ������������ �����, ��� ����� ��������� ����������� � ������������ ������������ ��� ���������� � ���������� "�����", ����� ������ ������������ ������ � �������.

', TO_TIMESTAMP('2015-08-24 07:54:19.164000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-24 07:54:25', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('3', '20-����� Windows 95', 'Windows 95 ��������� ��� 20-�����', '����� 20 ��� �����, 24 ������� 1995 ����, �������� Microsoft ��������� ������������ ������� Windows 95, ������� ����������� ����������� MS-DOS � Windows, ������� �� ����� ���������������� �������� ���� �� �����. ����� Windows 95 - ��� ������ ������ Windows, � ������� ��������� ���� "����" � ��� ������ �������� �������, ������� ��������� �������������� ���������������� ���������, ��������� ������� ��� ������, ������ �����, ��������� 32-��������� ����������, ����������� ������� ���� � ������ ������. ����� ��������, ��� ����� �� ���� �������� ������ �������, � � ����� ���� ���������� ���� �� ������.', TO_TIMESTAMP('2015-08-05 07:54:31.001000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-17 07:54:36', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('4', '������ �� ������', '��������� ZTE ����� ���������������� ������������� �� ������', '����������� ��������� ����� ������������ ������ ����� �����. � ��� �� ������ ������ ��������, ���������, ���������, ����������, ����� � ������, ������� ����� ��������� �������� � ��������� � ������ ������ ������. ������ ������� ����������� �������������� ������� �� ������ �������� ������ ����������� ������������ ������ ������. ����� ������� �������� ���������� ����� �������� ����������� ������� ��������� ����� � ���������� � ������� ��� ����������. �������� ZTE ���������� � ����� ������������ � ���� �����������.

ZTE ������� ����� �������� ������� ������������� � ����������� � ����� ����������� ������������ ������������ ������. ��������� ������������� ����� 18 ����������������� �������, �������� ���������� ���������� � ���������� � ���������, ������� ����� 4 500 ����������� ���������, �� ������� 200 ���������� �� ���������� ����������� ������������, ��� � ��������� ZTE ����������� � ����� ����������� ��������� ������� ������ ������.

� ���������� �������� ZTE ��� ����������� ������������ ������� Android ����������� ��� ����������� ������� ����������:

����������� ����;
PIN-��� - �������� ������ ������� �� 4 ����;
������ � �������������� ��� ����, ��� � ����.
��� ���� ������ �� ���� �������� ����� ��� ���� ������������, ��� � ����������. ������ ������� ������ ������������� �� ���� ���������� �������������� ����������. ����� �� ����� ���������� �������� ������ ����� ������������� �������� ���������� �������, �� � ���� ����� ����� ���� �����������. ������ ������� ZTE ������ ����� ������ � ��������� ���� ��������� ������, ����� ��� Blade S6, ����������� ������������� ������������ �� ������. ������ ���������� ���������� Eyeprint IDTM � �������� ��������������� ����������� �������� EyeVerify Inc. ���� ������� ������ ������������� ����������� � ������������ ������� ����������� ������� ������ ����.', TO_TIMESTAMP('2015-08-20 07:54:44.803000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-21 07:54:50', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('5', '�� Pavilion x360', '�� Pavilion x360 � ���������� �������� �������� �� ������', '�������� HP ���������� �������� � ������ ������ � ������ ������ �������� Pavilion x360. ������������� ����� �������� ������� ����� �����������, ����������� ���� � �������, ��� �������� ������� ��������� ������������ � ����������� �� ��� ����� � ������������. ��������� ����������� ����������� HP Pavilion x360 �������� �������� � ������� ��������, ��������, ������� � ��������. ������������� ����� ��������, ��� ������ ������� ���� ������ ��������� � ������ ����������� ���������������� ������ �� HP, ������� ������� ������������� Bang and Olufsen � BO Play.

', TO_TIMESTAMP('2015-08-21 07:54:56.630000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-21 07:55:02', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('6', '��� ������?', 'Microsoft �� ����� ���������� ���������� ���������� ��� Windows 10', 'Microsoft ����� ������ �������� ������ ��������� ������ � ������������ ��� ������������ ������� - ������ �������� ����������� ������������� �� ���������� ������ ���. �� ������ ����������� ������ ���������� �� ���������� ���������� ���������, �������� � Windows 10. ��� ������, ��� �� ����� �������� ���������� ��� �������������� �����������, ����������, ��� ������ ����������� ������. ��������� ����� ���� �������� ��� � ��������� ����������� ���������: ����� �������� � ������� ������������ � ������� ������, ���������� ������������������, ������ ������ ��������� ��������� �����������.
Microsoft ����� ���������� ��������� ���������� ��� Windows 10 ���� � ��� ������, ���� ���� ��� � ���������� �������������� ������������ �������. �, ����� ��, ������������ ����� ���� ����������, ����� �� ����� ����������� ������ ���������, � ����� ����� ������������ ������������� "���������, ������������� ���������������� Windows 10".', TO_TIMESTAMP('2015-08-16 07:55:05.964000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-17 07:55:10', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('7', 'Lumia ������', '���������� Lumia ������ ����� �������� ��� ���� ���������� �� Windows Phone
', '�� ��������� ������� ���������� Lumia Camera ���� ����������� ��� ���������� ������� Lumia, � ������� ��������� �������� 97% ����������, ���������� �� Windows Phone. � ��������� ����� �������� ����������, �, �� ������ ��������� �������������, ���������� �������� �� Huawei Ascend W1 � Samsung ATIV SE, ���� � � ��������: ��� ������� ������ ��������� ����������� � ���������� ���������� � ��������� ������������� �����������.
��� �� ����� ������ ����� � ������ ������������ ����������, ��� ��������� �������� ��������� ��� ���� ������������� Windows Phone.', TO_TIMESTAMP('2015-08-27 07:55:14.386000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-27 07:55:20', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('8', 'Google Play Store', '�� Windows 10 Mobile ������� Google Play Store', '���������� � ������ XDA Developers ����� �� �������� ���������� Android �� Windows 10 Mobile ������ ��������� �� ��������� ������������ ������� �� Microsoft ������� ���������� Google Play Store. �������� ��������� �������� ��������� ���������� ���� ���������� ��������� �������� Google, ��� ���� ���� ��������� ���������� ��������� �� ��������� ��������� �����-������������ ���������. ������ �������� ���������� ��������������� � ���, ��� �������� � Google Play Services ���� �� ��������, �� ������.

', TO_TIMESTAMP('2015-08-23 07:55:23.652000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-25 07:55:29', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('9', '� ��� �������?', '� ���������� Samsung ��������� �������', '���� �� ��������� ���������� ����������, �� ������ � ����, ��� � ��� �� ������ ��������� ���������, ������� ����� ��������� ������ � ������� ������. �� ����� �� �������� �������� �� ��������� ����� ��������, ��� �� ������� ������ � ��� ���� �����-�� �������. � ������ ����� ������ �������� ����������� ���������� � ���, ��� ����������� ��������� HTC ������ ��������������� ������� � ������ �����������. ������ �� ���������� ������ Android Police ��������, ��� � ���������� Samsung ����� ��������� ��������� �������.

��������� �� ���������� ��������� �� Samsung ��������, ��� �� ������ ��������� ��������� � ������ �����������. �������� ������ Android Police, ������� ��������� ��������� Samsung Push Service, ������� �������� �� �������� ����������� �� Samsung Pay, Samsung Link � ������ ��������� ��������.

� ���������, Samsung �� ������ ������� ����������� ��������� �� ����� ������. ��� ��� ��������� ��������� �������� ��������� ����� �������������, �� ������ ��������, ��� �������� ��������� ����� ������ ����� ���������� ��������.

', TO_TIMESTAMP('2015-08-24 07:55:35.202000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-24 07:55:38', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('10', 'No notes!', 'LG ���������� Samsung �� ���������� Galaxy Note 5 � ������', '������� Samsung Galaxy Note 5 � ������ � ��������� ������� ������� �� �����. �� ������ ������ ������������� ��������� ����������� ������ � ������ ������� � ������ ������� � ���������������� ������ ��������� �� ����� ��������� �����. ���� �� �������� ����������� Samsung - �������� LG - ������ ��������������� ������� � ���������� � Twitter ��������� � ��������� ������ � ������� ���������. LG �������, ��� ������ ��������� ��� �� �� ��������� � ����� �������� �� ����������� ��.
', TO_TIMESTAMP('2015-08-24 07:55:50.040000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-26 07:55:55', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('11', 'Marshmallow', 'Android M ����� ���������� Android 6.0 Marshmallow', '�� ������� ������ �������� Google ��������� ����������� ����, ��� ��� ���������, ��� ����������� �������� ��������� ������ Android ������� �� ���� ������. � ��� ������� ����� �������� ������� Google ������������ ����� ������, �����������, ��� � ���������, Android 6.0 Marshmallow. ����� �������� ��� �������� ����������� ���� ��� �������������, ��� ������������ ������ � ��������� ����-������ Android M Developer Preview 3. ������������ ��� ����� ������� ����� ����������� SDK � Android Studio.


', TO_TIMESTAMP('2015-08-21 07:55:59.785000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-22 07:56:04', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('12', '�������� ������', 'Huawei Glory 7i ���������� �������� �������
', '��� �� �������� � ����, ��� � ����������� ���������� ����� ����� ��� ��������, ��� � ����������� ������. ��� �� ����� �� ������������ ������������ ������������� ����� ������������� �������� ����������� ������ ������� ��� ����, ����� �������� �������� ���������� � �������� ������. ����� �� ����� ��������� ������� ����� ��������� ������� N �������� Oppo, ������� �������� ����� ���������� �������. ������� ��������� ����� �� ���������� � �������� Huawei, ���������� ����� �������� Glory 7i � �������� �������.


', TO_TIMESTAMP('2015-08-16 07:56:09.593000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-18 07:56:14', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('13', '��������� �����', 'Sony SmartBand 2 �������� �������� ���������� �����
', '�������� Sony ����������� ����� ������-������ SmartBand 2. ��� � ���������, ������� ��������� ���������� �������� ���������� �����, ���� ��� �� ������� ���������� ������� ��������� ����������. ������ ������ ���������� ����� ��, ��� � SmartBand 2 ������ ��� �������, ��� � ���������� ������. ������ � ����� ������������, �������� ������� � ���������� �� ������� ������ ������������� � ������� �������� � LED-�����������. ��� ���� ������������� ������� �� 2 ���� ������ � ������ ��������� �������������, � ��� ������ ���������� �������� 1 ���.', TO_TIMESTAMP('2015-08-10 07:56:18.353000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-14 07:56:21', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('14', '������� �������', 'Samsung Fast Charge Wireless Charging Pad ������� ������� ����� ��������� ��������
', '������������ �������� ���������� �������� ���������� ������� �����������. ��� ������ ������� ������������� ����� ������ �������� ��������� ���������� �� ����������� ���������, � ����� ������� ������ �����������. �� ��������� ����������� ������������ ������� �������� ������������ ������ �������� �������. � �������� Samsung ������ ��������� ���� ����������. ������������� ������������� ����������� ����� ������������ �������� ���������� ��� ��������� Fast Charge Wireless Charging Pad. 
������� ����������� ������������ ������� �� Samsung - ������� �������� ���������� ������ ������������. ������������ ������� ���������� �������� ������� � 1,4 ���� �� ��������� � ������������ ������������. �� ��� ����� ������� ��������� ��������: �������� ������� ����� ���� ������ � ������ ���������� �������� - Galaxy Note 5 � Galaxy S6 Edge+, �� ������� ����������� ����������� ��.

', TO_TIMESTAMP('2015-08-13 07:56:24.709000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-14 07:56:29', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('15', '��� �� ��������?', '��������� �� ���� ������������ ��� ������
', '�� ����������� ������������ �������������� ������������ ���������� ������������ ������� The Wall Street Journal ������ � ������, ��� ������� ������ ����������� ���� �� �����������. ���� �� � ���, ��� ���� �������� ������������ ��������� ������ ��� ������, ������ �������� ���� ����������� ����������� ������������ ��������, ���� ������ ������� ��� �������� ��� ������������������ "�������". �������� ������ ������������, ������������������ ����������, ����� ��� Amazon Kindle, �� ���� ������������ ��� ������ - � 2012 ���� 50% ���������� ������������ �� ��� ������ ����������� ����, � �� ����� ��� � 2015 ���� ��� ����� ����������� �� 32%. ������������ � ���� ����������� ���������� �����, ������������ �������� ��� ������, - � 30% � 2009 ���� �� 41% � 2015 ����.

�� ������ ��������������, ��������� �� ��� ��������� � �������� ��������� ���������� ��� ������ ���� - ����� ���� 14% ���������� ������� ����� �����. ��� �� ����� � �������� ������� ���������� ��� ������ �� ���� ��������� ������������ � 54% �������. ����������, ��� ����������� ��������� � ����� ���������� ������� ��������� � ����� ������� � ����, ��� ���� � ����� ��������� �� ������������� "�������".

', TO_TIMESTAMP('2015-08-12 07:56:36.637000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-12 07:56:41', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('16', '������ Nemo', 'LG �������� ��������� ��������� �����-����� ������� ����� ������ ��������', 'LG �������� ��� ��������� ���������� �����-�����. ������ ������� ������� ������������ Nemo. ��� ���� �������� ������ ����, �� ���������� � ���������� ������� ��� ��� ��������� ��������, ��� �������� � �������� ����������� �� ����� ����� �������� ����� ��������, ���������� �� Android Wear. �������� ���������� ���������, ���������� ������� ������� ����� �������� 480�480 ��������. �� ��������� ����� ���������� �� �������� ������ ��������� �� ������������, ������ ����� ������, ��� ��������� �������� ��������� ��������� "�����" ����� LG ���������� ����� 1,3 �����. 
���� �����, � �������, LG Watch Urbane, �� ��� ����� ��������� � ���������� 320�320 ��������, ���������� ppi ���������� �������� 240 ����� �� ����. LG Nemo, � ���� �������, ������ ������������ ����������� �������� 400 ����� �� ����.

����� ���������� �������� ���������� �����, ���� ����� ������ ������ �� ������ �����, ������� �� ����� ������� �������������� �����������, �� ������ ��������� ����� ������� ���������� ����� ��������� ����������.

', TO_TIMESTAMP('2015-08-23 07:56:44.026000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-23 07:56:47', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('17', 'microSD ��� Note5', '� "������������" Samsung Galaxy Note 5 ����� ���������� microSD-�����
', '����� ����������� ������� Samsung Galaxy Note 5 � ���� ������������� ����� � ���, ��� ���� �� ������ ��������� � � ����� ������� ��� SIM-���� � ������� ������ ��� ���� ������ microSD. ���� �������� ������, � ������ ����������� ����������, �������������� ���, ����� �������. � ����� ������ Galaxy Note 5 �� ������ ���� ��� SIM-���� ����� ���� ����������� ����� microSD. ��� ����� ������ ������������� ����� ������ �������� � ����������. ����������� ������, �� ������� Samsung ���������� �������� ��������� microSD �� ���� ������� Galaxy Note 5, ����� �� ����������.

', TO_TIMESTAMP('2015-08-25 07:57:00.112000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-25 07:57:03', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('18', '40% ����� �����-�����', 'Apple Watch ����� 40% ����� �����-����� � 2020 ����
', '�� ������ ������������ BI Intelligence, ��� � 2020 ���� �� Apple Watch ����� ����������� ����� 40% ������ ���� �����-����� �������-������. ��� ���������� ������� ����� ���������� ������ $350. � ������� ����� ��������������, ��� Apple � ��������� ���� ����� ���������� 40 ��������� �����-����� ��������. �����������, ��� � ��������� ����� � ������� ��������� ���������� ������ ����� ������������ ����������� �������� �����������, � ����� ���������� ��� ������� � ��������. ������������, ���������� Business Insider, ��������� �� �������� ������� ������� �� ������� ������������� ����������� � �����-����� Apple Watch, �� �� � ��������� �� ���� Android Wear.
', TO_TIMESTAMP('2015-08-24 07:57:06.660000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-29 07:57:10', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('19', 'LG UF950V', '���������� LG UF950V ������� �� ���������� �����', '������� ������������� �������� LG ����������� �� ���������� ����� ����� ��������� �������-������ UF950V �� ������� LG Prime UHD. ������� �������� ��� ����������� webOS 2.0 � ���������� ��������� ������� �����, 4K-�������� � ������ �������������, ��������� ��������� � Harman/Kardon. ��� �� �� �� ����, ����� �� �������� ������������ UF950V �������� �����. ����� 4K IPS-������, ������� ���������� �������� ������ ������, � ����� ���������� �� �������� LG ���� ���������� ������� ��������� ��������� ������� � ���������� ColorPrime, ���������� ��������� ����� �������� � ������������ �����������. 
', TO_TIMESTAMP('2015-08-23 07:57:13.969000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-27 07:57:18', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "NEWS_MANAGEMENT"."NEWS" (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES ('20', 'teXet X-line ', 'teXet X-line �������� ������� ��������� ����������
', '������� ���������� �������� teXet ����������� ����� ������� X-line (��-5006), ������� �������� ������������� ��������������� ������� ������������, ������� �� ����� ������������� �������������� ������ �� �������� ����� � AnTuTu, ����������� � ���������. �������� ������������ ������� ������ IPS-������� � ��������� ���� SIM-����, � ����� ������� ������. � ��������� �������������� ��������� �� ���������� �� ������������ ���� �������������� ���������� �������� ��������.
', TO_TIMESTAMP('2015-08-30 07:57:24.904000000', 'YYYY-MM-DD HH24:MI:SS.FF'), TO_DATE('2015-08-31 07:57:28', 'YYYY-MM-DD HH24:MI:SS'));



INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('1', '1', '�������!', TO_TIMESTAMP('2015-08-25 08:47:37.429000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('2', '3', '���������� �������', TO_TIMESTAMP('2015-08-25 08:47:58.792000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('3', '6', ':()', TO_TIMESTAMP('2015-08-25 08:48:09.108000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('4', '2', '���� �����', TO_TIMESTAMP('2015-08-25 08:48:16.627000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('5', '4', '�� ��??', TO_TIMESTAMP('2015-08-25 08:48:34.896000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('6', '7', ':)', TO_TIMESTAMP('2015-08-25 08:48:41.559000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('7', '13', '��� O_O', TO_TIMESTAMP('2015-08-25 08:48:46.207000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('8', '10', '���������-���������...', TO_TIMESTAMP('2015-08-25 08:48:50.995000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('9', '3', '������� �������', TO_TIMESTAMP('2015-08-25 08:49:01.728000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('10', '15', '�� � �������.', TO_TIMESTAMP('2015-08-25 08:49:06.931000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('11', '3', '� ����� ��� ����?', TO_TIMESTAMP('2015-08-25 08:49:12.572000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('12', '4', '����...', TO_TIMESTAMP('2015-08-25 08:49:16.067000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('13', '1', '��������', TO_TIMESTAMP('2015-08-25 09:47:40.083000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('14', '17', '�������������', TO_TIMESTAMP('2015-08-25 09:49:21.462000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('15', '2', '���������� �������', TO_TIMESTAMP('2015-08-25 09:51:26.685000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('16', '19', '��� ��������!', TO_TIMESTAMP('2015-08-25 09:53:33.566000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('17', '11', '���������, ��� �� ��� �����?', TO_TIMESTAMP('2015-08-25 09:55:38.751000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('18', '4', '������� ��� �������', TO_TIMESTAMP('2015-08-25 09:57:56.198000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('19', '14', ':(', TO_TIMESTAMP('2015-08-25 09:59:06.977000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO "NEWS_MANAGEMENT"."COMMENTS" (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES ('20', '3', '�� ���������', TO_TIMESTAMP('2015-08-25 10:00:15.783000000', 'YYYY-MM-DD HH24:MI:SS.FF'));

INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('1', '3');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('2', '2');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('3', '4');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('4', '5');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('5', '1');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('6', '6');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('7', '4');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('8', '7');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('9', '8');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('10', '9');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('11', '4');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('12', '10');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('13', '11');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('14', '11');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('15', '4');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('16', '12');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('17', '2');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('18', '3');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('19', '13');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_AUTHOR" (NEWS_ID, AUTHOR_ID) VALUES ('20', '14');


INSERT INTO "NEWS_MANAGEMENT"."USERS" (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES ('1', 'Natallia Zhukava', 'admin', '1e6947ac7fb3a9529a9726eb692c8cc5');
insert into "NEWS_MANAGEMENT"."ROLES" ("USER_ID", "ROLE_NAME") values(1, 'ROLE_ADMIN');


INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('1', 'Tag 1');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('2', 'Tag 2');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('3', 'Tag 3');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('4', 'Tag 4');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('5', 'Tag 5');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('6', 'Tag 6');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('7', 'Tag 7');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('8', 'Tag 8');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('9', 'Tag 9');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('10', 'Tag 10');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('11', 'Tag 11');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('12', 'Tag 12');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('13', 'Tag 13');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('14', 'Tag 14');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('15', 'Tag 15');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('16', 'Tag 16');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('17', 'Tag 17');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('18', 'Tag 18');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('19', 'Tag 19');
INSERT INTO "NEWS_MANAGEMENT"."TAG" (TAG_ID, TAG_NAME) VALUES ('20', 'Tag 20');


INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('1', '2');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('1', '5');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('2', '5');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('3', '1');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('4', '3');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('5', '4');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('6', '6');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('7', '7');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('8', '8');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('9', '9');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('10', '10');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('11', '11');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('12', '12');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('13', '13');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('14', '14');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('15', '15');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('16', '16');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('17', '17');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('18', '18');
INSERT INTO "NEWS_MANAGEMENT"."NEWS_TAG" (NEWS_ID, TAG_ID) VALUES ('19', '20');

