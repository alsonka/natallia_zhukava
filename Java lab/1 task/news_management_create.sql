
CREATE USER News_Management IDENTIFIED BY News_Management;
ALTER SESSION SET CURRENT_SCHEMA = News_Management ;

GRANT create session TO News_Management;
GRANT create table TO News_Management;
GRANT create view TO News_Management;
GRANT create any trigger TO News_Management;
GRANT create any procedure TO News_Management;
GRANT create sequence TO News_Management;
GRANT create synonym TO News_Management;

alter user News_Management quota 50m on system;


CREATE TABLE News_Management.News (
  NEWS_ID NUMBER(20) NOT NULL,
  TITLE NVARCHAR2(30) NOT NULL,
  SHORT_TEXT NVARCHAR2(100) NOT NULL,
  FULL_TEXT NVARCHAR2(2000) NOT NULL,
  CREATION_DATE TIMESTAMP NOT NULL,
  MODIFICATION_DATE DATE NOT NULL,
  PRIMARY KEY (NEWS_ID))
;


CREATE TABLE News_Management.Comments (
  COMMENT_ID NUMBER(20) NOT NULL,
  NEWS_ID NUMBER(20) NOT NULL,
  COMMENT_TEXT NVARCHAR2(100) NOT NULL,
  CREATION_DATE TIMESTAMP NOT NULL,
  PRIMARY KEY (COMMENT_ID)
 ,
  CONSTRAINT fk_Comments_News
    FOREIGN KEY (NEWS_ID) 
    REFERENCES News_Management.News (NEWS_ID) 
   )
;

CREATE INDEX fk_Comments_News_idx ON News_Management.Comments (NEWS_ID ASC);


CREATE TABLE News_Management.Author (
  AUTHOR_ID NUMBER(20) NOT NULL,
  AUTHOR_NAME NVARCHAR2(30) NOT NULL,
  EXPIRED TIMESTAMP NULL,
  PRIMARY KEY (AUTHOR_ID))
;


CREATE TABLE News_Management.Tag (
  TAG_ID NUMBER(20) NOT NULL,
  TAG_NAME NVARCHAR2(30) NOT NULL,
  PRIMARY KEY (TAG_ID))
;


CREATE TABLE News_Management.News_Author (
  NEWS_ID NUMBER(20) NOT NULL,
  AUTHOR_ID NUMBER(20) NOT NULL,
  PRIMARY KEY (NEWS_ID, AUTHOR_ID)
 ,
  CONSTRAINT fk_News_has_Author_News1
    FOREIGN KEY (NEWS_ID)
    REFERENCES News_Management.News (NEWS_ID) 
   ,
  CONSTRAINT fk_News_has_Author_Author1
    FOREIGN KEY (AUTHOR_ID)
    REFERENCES News_Management.Author (AUTHOR_ID) 
   )
;

CREATE INDEX fk_News_has_Author_Author1_idx ON News_Management.News_Author (AUTHOR_ID ASC);
CREATE INDEX fk_News_has_Author_News1_idx ON News_Management.News_Author (NEWS_ID ASC);


CREATE TABLE News_Management.News_Tag (
  NEWS_ID NUMBER(20) NOT NULL,
  TAG_ID NUMBER(20) NOT NULL,
  PRIMARY KEY (NEWS_ID, TAG_ID)
 ,
  CONSTRAINT fk_News_has_Tag_News1
    FOREIGN KEY (NEWS_ID)
    REFERENCES News_Management.News (NEWS_ID)
   ,
  CONSTRAINT fk_News_has_Tag_Tag1
    FOREIGN KEY (TAG_ID)
    REFERENCES News_Management.Tag (TAG_ID)
   )
;

CREATE INDEX fk_News_has_Tag_Tag1_idx ON News_Management.News_Tag (TAG_ID ASC);
CREATE INDEX fk_News_has_Tag_News1_idx ON News_Management.News_Tag (NEWS_ID ASC);


CREATE TABLE News_Management.Users (
  USER_ID NUMBER(20) NOT NULL,
  USER_NAME NVARCHAR2(50) NOT NULL,
  LOGIN NVARCHAR2(30) NOT NULL,
  PASSWORD NVARCHAR2(30) NOT NULL,
  PRIMARY KEY (USER_ID))
;


CREATE TABLE News_Management.Roles (
  USER_ID NUMBER(20) NOT NULL,
  ROLE_NAME NVARCHAR2(50) NOT NULL
 ,
  CONSTRAINT fk_Roles_User1
    FOREIGN KEY (USER_ID)
    REFERENCES News_Management.Users (USER_ID)
   )
;

CREATE INDEX fk_Roles_User1_idx ON News_Management.Roles (USER_ID ASC);

