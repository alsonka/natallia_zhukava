package by.zhukova.newsapp.service.impl;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import by.zhukova.newsapp.entity.NewsVO;
import by.zhukova.newsapp.dao.NewsDAO;
import by.zhukova.newsapp.entity.NewsTO;
import by.zhukova.newsapp.entity.SearchCriteriaVO;
import by.zhukova.newsapp.exception.DAOException;
import by.zhukova.newsapp.exception.ServiceException;
import by.zhukova.newsapp.service.NewsManager;
import by.zhukova.newsapp.service.SearchService;

// TODO: Auto-generated Javadoc
/**
 * The Class SearchServiceImpl implements method from SearchService interface.
 */
public class SearchServiceImpl implements SearchService {
	static Logger logger = Logger.getLogger(SearchServiceImpl.class);
	
	private NewsDAO newsDao;
	
	/**
	 * Sets the newsDAO
	 *
	 * @param newsDao the new news dao
	 */
	public void setNewsDao(NewsDAO newsDao) {
		this.newsDao = newsDao;
	}

	private NewsManager newsManager;
	
	/**
	 * Sets the news manager.
	 *
	 * @param newsManager the new news manager
	 */
	public void setNewsManager(NewsManager newsManager) {
		this.newsManager = newsManager;
	}


	

	/**
	 * Instantiates a new search service implementation object.
	 */
	public SearchServiceImpl() {
	}

	
	/**
	 * The method searches by given criteria and forms list of NewsVO objects.
	 *
	 * @param search the search
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.SearchService#searchBy(by.zhukova.newsapp.entity.SearchCriteriaVO)
	 */
	@Override
	@Transactional
	public List<NewsVO> searchBy(SearchCriteriaVO search) throws ServiceException {
		List<NewsTO> list = new LinkedList<NewsTO>();
		List<NewsVO> newsList = new LinkedList<NewsVO>();

		try {
		
			list = newsDao.findByCriteria(search);

			for (NewsTO news : list) {
				NewsVO cNews = newsManager.viewSingleNews(news.getId());
				newsList.add(cNews);
			}
		
			
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

		return newsList;
	}

}
