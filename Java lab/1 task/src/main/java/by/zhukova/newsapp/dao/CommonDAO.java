package by.zhukova.newsapp.dao;

import java.util.List;

import by.zhukova.newsapp.entity.Entity;
import by.zhukova.newsapp.exception.DAOException;

/**
 * The Interface CommonDAO contains common methods which should work with database.
 * 
 * @author Natallia_Zhukava
 *
 * @param <T> the generic type
 */
public interface CommonDAO<T extends Entity> {

	
	/**
	 * The method finds all entries from database table.
	 *
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	public abstract List<T> findAll() throws DAOException;

	/**
	 * The method finds entity in database table by given id.
	 *
	 * @param id the id
	 * @return the T
	 * @throws DAOException the DAO exception
	 */
	public abstract T findById(long id) throws DAOException;

	/**
	 * The method saves information from {@code Entity} object to database.
	 *
	 * @param entity the entity
	 * @return the long id of inserted row
	 * @throws DAOException the DAO exception
	 */
	public abstract long save(T entity) throws DAOException;

	/**
	 * The method updates information about given entity in database.
	 *
	 * @param entity the entity
	 * @throws DAOException the DAO exception
	 */
	public abstract void update(T entity) throws DAOException;

	/**
	 * The method deletes information about entity with given identifier from database table.
	 *
	 * @param id the id
	 * @throws DAOException the DAO exception
	 */
	public abstract void delete(long id) throws DAOException;
}
