package by.zhukova.newsapp.service;

import java.util.List;

import by.zhukova.newsapp.entity.NewsTO;
import by.zhukova.newsapp.entity.TagTO;
import by.zhukova.newsapp.exception.ServiceException;


/**
 * The Interface NewsService contains the service methods which should work with {@code NewsTO} objects.
 * 
 * @author Natallia_Zhukava
 */
public interface NewsService {

	/**
	 * The method saves the given news.
	 *
	 * @param news the news
	 * @return the newsTO with generated identifier
	 * @throws ServiceException the service exception
	 */
	NewsTO saveNews(NewsTO news) throws ServiceException;
	
	/**
	 * The method attaches list of tag to given news
	 *
	 * @param newsId the news id
	 * @param list the list
	 * @throws ServiceException the service exception
	 */
	void attachNewsTags(long newsId, List<TagTO> list) throws ServiceException;
	
	/**
	 * The method attaches the author to given news
	 *
	 * @param newsId the news id
	 * @param authorId the author id
	 * @throws ServiceException the service exception
	 */
	void attachNewsAuthor(long newsId, long authorId) throws ServiceException;

	/**
	 * The method edits given news
	 *
	 * @param news the news
	 * @throws ServiceException the service exception
	 */
	void editNews(NewsTO news) throws ServiceException;

	/**
	 * The method deletes given news
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 */
	void deleteNews(long newsId) throws ServiceException;

	/**
	 * The method finds all news and returns list of them 
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	List<NewsTO> findAllNews() throws ServiceException;

	/**
	 * The method finds the news by given id
	 *
	 * @param newsId the news id
	 * @return the news to
	 * @throws ServiceException the service exception
	 */
	NewsTO findNewsById(long newsId) throws ServiceException;
	
	/**
	 * The method finds all news which attached to given author 
	 *
	 * @param authorId the author id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	List<NewsTO> findNewsByAuthor(long authorId) throws ServiceException;

	/**
	 * The method counts all news
	 *
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	long countAllNews() throws ServiceException;

	/**
	 * The method detaches all tags from given news
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 */
	void detachNewsTags(long newsId) throws ServiceException;
	
	/**
	 * The method detaches author from given news
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 */
	void detachNewsAuthor(long newsId) throws ServiceException;


}