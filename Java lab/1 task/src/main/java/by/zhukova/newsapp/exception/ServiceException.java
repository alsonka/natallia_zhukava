package by.zhukova.newsapp.exception;


/**
 * The Class ServiceException is exception of service layer..
 */
public class ServiceException extends Exception {


	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new service exception.
	 */
	public ServiceException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Instantiates a new service exception.
	 *
	 * @param arg0 the arg0
	 */
	public ServiceException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Instantiates a new service exception.
	 *
	 * @param arg0 the arg0
	 */
	public ServiceException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Instantiates a new service exception.
	 *
	 * @param arg0 the arg0
	 * @param arg1 the arg1
	 */
	public ServiceException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Instantiates a new service exception.
	 *
	 * @param arg0 the arg0
	 * @param arg1 the arg1
	 * @param arg2 the arg2
	 * @param arg3 the arg3
	 */
	public ServiceException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}
