package by.zhukova.newsapp.service.impl;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import by.zhukova.newsapp.dao.NewsDAO;
import by.zhukova.newsapp.entity.NewsTO;
import by.zhukova.newsapp.entity.TagTO;
import by.zhukova.newsapp.exception.DAOException;
import by.zhukova.newsapp.exception.ServiceException;
import by.zhukova.newsapp.service.NewsService;

/**
 * The Class NewsServiceImpl implements method from NewsService interface.
 * 
 * @author Natallia_Zhukava
 */
public class NewsServiceImpl implements NewsService {
	
	static Logger logger = Logger.getLogger(AuthorServiceImpl.class);
	private NewsDAO newsDao;
	
	/**
	 * Sets the newsDAO
	 *
	 * @param newsDao the new news dao
	 */
	public void setNewsDao(NewsDAO newsDao) {
		this.newsDao = newsDao;
	}

	/**
	 * Instantiates a new news service implementation object.
	 */
	public NewsServiceImpl() {
		
	}

	/**
	 * The method saves news with current timestamp as creation date.
	 *
	 * @param news the news
	 * @return the newsTO with generated id
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.NewsService#saveNews(by.zhukova.newsapp.entity.NewsTO)
	 */
	@Override
	public NewsTO saveNews(NewsTO news) throws ServiceException {
		Calendar calendar = Calendar.getInstance();
		Date now = calendar.getTime();
		Timestamp currentTimestamp = new Timestamp(now.getTime());
		try {
			news.setCreationDate(currentTimestamp);
			news.setModificationDate(new java.sql.Date(now.getTime()));
			long id = newsDao.save(news);
			news.setId(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return news;
	}

	/**
	 * The method edits news and sets new modification date.
	 *
	 * @param news the news
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.NewsService#editNews(by.zhukova.newsapp.entity.NewsTO)
	 */
	@Override
	public void editNews(NewsTO news) throws ServiceException {
		try {
			Calendar calendar = Calendar.getInstance();
			Date now = calendar.getTime();
			news.setModificationDate(new java.sql.Date(now.getTime()));
			newsDao.update(news);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		
	}



	/**
	 * The method finds all news sorted by comments number and modification date.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.NewsService#findAllNews()
	 */
	@Override
	public List<NewsTO> findAllNews() throws ServiceException {
		List<NewsTO> list = null;
		try {
			 list = newsDao.findAll();
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return list;
	}

	/**
	 * The method finds the news by given id
	 * 
	 * @see by.zhukova.newsapp.service.NewsService#findNewsById(long)
	 */
	@Override
	public NewsTO findNewsById(long newsId) throws ServiceException {
		NewsTO news = null;
		try {
			news = newsDao.findById(newsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return news;
	}
	

	/**
	 * The method counts all news.
	 *
	 * @return the long
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.NewsService#countAllNews()
	 */
	@Override
	public long countAllNews() throws ServiceException {
		long num = 0;
		try {
			num = newsDao.countNews();
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return num;
	}

	/**
	 * The method attaches list of tags to given news.
	 *
	 * @param newsId the news id
	 * @param list the list
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.NewsService#attachNewsTags(long, java.util.List)
	 */
	@Override
	public void attachNewsTags(long newsId, List<TagTO> list) throws ServiceException {
		try {
			newsDao.attachTagsToNews(newsId, list);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		
	}

	/**
	 *  
	 * The method attaches author to given news.
	 *
	 * @param newsId the news id
	 * @param authorId the author id
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.NewsService#attachNewsAuthor(long, long)
	 */
	@Override
	public void attachNewsAuthor(long newsId, long authorId) throws ServiceException {
		try {
			newsDao.attachAuthorToNews(newsId, authorId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		
	}

	/**
	 * The method finds all news attached to given author.
	 *
	 * @param authorId the author id
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.NewsService#findNewsByAuthor(long)
	 */
	@Override
	public List<NewsTO> findNewsByAuthor(long authorId) throws ServiceException {
		List<NewsTO> list = null;
		try {
			 list = newsDao.findByAuthor(authorId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return list;
	}

	/**
	 * The method deletes news by given id.
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.NewsService#deleteNews(long)
	 */
	@Override
	public void deleteNews(long newsId) throws ServiceException {
		try {
			newsDao.delete(newsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		
	}

	/**
	 * The method detaches tags from given news.
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.NewsService#detachNewsTags(long)
	 */
	@Override
	public void detachNewsTags(long newsId) throws ServiceException {
		try {
			newsDao.detachNewsTags(newsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		
	
	}

	/**
	 * The method detaches author from given news.
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.NewsService#detachNewsAuthor(long)
	 */
	@Override
	public void detachNewsAuthor(long newsId) throws ServiceException {
		try {
			newsDao.detachNewsAuthor(newsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		
	}

}
