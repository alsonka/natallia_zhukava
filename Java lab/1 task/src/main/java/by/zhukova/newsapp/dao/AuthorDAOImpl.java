package by.zhukova.newsapp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import by.zhukova.newsapp.utils.DBUtils;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import by.zhukova.newsapp.entity.AuthorTO;
import by.zhukova.newsapp.exception.DAOException;


/**
 * The Class AuthorDAOImpl implements AuthorDAO interface and contains the methods which works with database.
 *
 * @author Natallia_Zhukava
 */
public class AuthorDAOImpl implements AuthorDAO {

	private static final String SQL_FIND_ALL = "SELECT author_id, author_name, expired FROM author";
	private static final String SQL_FIND_BY_ID = "SELECT author_id, author_name, expired FROM author WHERE author_id=?";
	private static final String SQL_FIND_BY_NEWS = "SELECT author.author_id, author_name, expired FROM author "
			+ "JOIN news_author ON author.author_id=news_author.author_id WHERE  news_author.news_id=?";
	private static final String SQL_DELETE = "DELETE FROM author WHERE author_id=?";
	private static final String SQL_CREATE = "INSERT INTO author (author_id, author_name, expired) VALUES"
			+ "(id_sequence.nextval, ?, ?) ";
	private static final String SQL_UPDATE = "UPDATE author SET author_name=?, expired=? WHERE author_id=?";
	private static final String SQL_DELETE_AUTHOR_NEWS = "DELETE FROM news_author WHERE author_id=?";

	private DataSource datasource;



	/**
	 * Sets the datasource.
	 *
	 * @param datasource the new datasource
	 */
	public void setDatasource(DataSource datasource) {
		this.datasource = datasource;
	}

	/**
	 * Instantiates a new authorDAO implementantion.
	 */
	public AuthorDAOImpl() {
		super();

	}
	
	/**
	 * The method parses given ResultSet object and returns {@code AuthorTO} object
	 * @param res
	 * @return {@code AuthorTO} object
	 * @throws SQLException
	 */

	private AuthorTO parseResultSet(ResultSet res) throws SQLException {
		AuthorTO author = new AuthorTO();
		author.setId(res.getLong(1));
		author.setName(res.getString(2));
		author.setExpiredDate(res.getTimestamp(3));
		return author;
	}

	/**
	 * The method finds all authors from table {@code Author}.
	 *
	 * @return the list
	 * @throws DAOException the DAO exception
	 * @see by.zhukova.newsapp.dao.CommonDAO#findAll()
	 */
	@Override
	public List<AuthorTO> findAll() throws DAOException {

		List<AuthorTO> list = new LinkedList<AuthorTO>();
		Statement st = null;
		ResultSet res = null;
		Connection con = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			st = con.createStatement();
			res = st.executeQuery(SQL_FIND_ALL);
			while (res.next()) {
				AuthorTO author = parseResultSet(res);
				list.add(author);
			}
		} catch (SQLException e) {
			throw new DAOException(e);                                                           
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(st);
			DataSourceUtils.releaseConnection(con, datasource);
		}
		return list;
	}

	/**
	 * The method finds the author by given id.
	 *
	 * @param id the id
	 * @return the author to
	 * @throws DAOException the DAO exception
	 * @see by.zhukova.newsapp.dao.CommonDAO#findById(long)
	 */
	@Override
	public AuthorTO findById(long id) throws DAOException {
		AuthorTO author = null;
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_FIND_BY_ID);
			pst.setLong(1, id);
			res = pst.executeQuery();
			while (res.next()) {
				author = parseResultSet(res);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			con = DataSourceUtils.getConnection(datasource);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return author;

	}

	/**
	 * The method finds the author of the given news.
	 *
	 * @param newsId the news id
	 * @return the author to
	 * @throws DAOException the DAO exception
	 * @see by.zhukova.newsapp.dao.AuthorDAO#findByNews(long)
	 */
	@Override
	public AuthorTO findByNews(long newsId) throws DAOException {
		AuthorTO author = null;
		PreparedStatement pst = null;
		Connection con = null;
		ResultSet res = null;
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_FIND_BY_NEWS);
			pst.setLong(1, newsId);
			res = pst.executeQuery();
			while (res.next()) {
				author = parseResultSet(res);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return author;
	}

	/**
	 * The method saves the information from {@code AuthorTO} object to database.
	 *
	 * @param entity the entity
	 * @return the long
	 * @throws DAOException the DAO exception
	 * @see by.zhukova.newsapp.dao.CommonDAO#save(by.zhukova.newsapp.entity.Entity)
	 */
	@Override
	public long save(AuthorTO entity) throws DAOException {

		PreparedStatement pst = null;
		Connection con = null;
		
		ResultSet res = null;
		long id = 0;
		try {
			con = DataSourceUtils.getConnection(datasource);
			String[] genKeys = {"author_id"};
			pst = con.prepareStatement(SQL_CREATE, genKeys);
			
			pst.setString(1, entity.getName());
			pst.setTimestamp(2, entity.getExpiredDate());
			pst.executeUpdate();
			
			res = pst.getGeneratedKeys();
			res.next();
			id = res.getLong(1);

		} catch (SQLException e) {
			throw new DAOException(e.toString());
		} finally {
			DBUtils.closeResultSet(res);
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

		return id;

	}

	/**
	 * The method updates information about given {@code AuthorTO} object in database.
	 *
	 * @param entity the entity
	 * @throws DAOException the DAO exception
	 * @see by.zhukova.newsapp.dao.CommonDAO#update(by.zhukova.newsapp.entity.Entity)
	 */
	@Override
	public void update(AuthorTO entity) throws DAOException {
		PreparedStatement pst = null;
		Connection con = null;
		
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_UPDATE);
			pst.setLong(3, entity.getId());
			pst.setString(1, entity.getName());
			pst.setTimestamp(2, entity.getExpiredDate());
			pst.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

	}

	/**
	 * The method deletes author with given id from database.
	 * Not planned to use.
	 *
	 * @param id the id
	 * @throws DAOException the DAO exception
	 * @see by.zhukova.newsapp.dao.CommonDAO#delete(long)
	 */
	@Override
	public void delete(long id) throws DAOException {
		PreparedStatement pst = null;
		Connection con = null;
	
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_DELETE);
			pst.setLong(1, id);
			pst.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
		
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}

	}


	/**
	 * The method detaches news from given author
	 * @param authorId
	 * @see by.zhukova.newsapp.dao.AuthorDAO#detachAuthorNews(long)
	 */
	@Override
	public void detachAuthorNews(long authorId) throws DAOException {
		PreparedStatement pst = null;
		Connection con = null;
	
		try {
			con = DataSourceUtils.getConnection(datasource);
			pst = con.prepareStatement(SQL_DELETE_AUTHOR_NEWS);
			pst.setLong(1, authorId);
			pst.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
		
			DBUtils.closeStatement(pst);
			DataSourceUtils.releaseConnection(con, datasource);
		}
		
	}

}