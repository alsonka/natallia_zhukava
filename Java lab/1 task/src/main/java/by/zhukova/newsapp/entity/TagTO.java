package by.zhukova.newsapp.entity;

/**
 * The class Tag contains data from table Tag
 * 
 * @author Natallia_Zhukava
 */
public class TagTO extends Entity {

	private static final long serialVersionUID = 1L;
	private String name;

	/**
	 * Instantiates a new tag.
	 */
	public TagTO() {

	}

	/**
	 * Instantiates a new tag.
	 *
	 * @param id
	 *            the id
	 * @param name
	 *            the name
	 */
	public TagTO(long id, String name) {
		super(id);
		this.name = name;
	}

	public TagTO(String name) {
		super();
		this.name = name;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof TagTO))
			return false;
		TagTO other = (TagTO) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
