package by.zhukova.newsapp.service;

import java.util.List;

import by.zhukova.newsapp.entity.NewsTO;
import by.zhukova.newsapp.entity.NewsVO;
import by.zhukova.newsapp.exception.ServiceException;

/**
 * The Interface NewsManager contains service method which should work with News Value Object.
 * 
 * @author Natallia_Zhukava
 */
public interface NewsManager {

	/**
	 * The method saves the NewsVO object
	 *
	 * @param complexNews the complex news
	 * @return the newsTO object with generated identifier
	 * @throws ServiceException the service exception
	 */
	NewsTO saveNews(NewsVO complexNews) throws ServiceException;

	/**
	 * The method edits the NewsVO object
	 *
	 * @param complexNews the complex news
	 * @throws ServiceException the service exception
	 */
	void editNews(NewsVO complexNews) throws ServiceException;

	/**
	 * The method deletes the NewsVO object by given identifier
	 *
	 * @param id the id
	 * @throws ServiceException the service exception
	 */
	void deleteNews(long id) throws ServiceException;
	
	/**
	 * The method allow to view list of all news sorted by comments number and modification date
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	List<NewsVO> viewNewsList() throws ServiceException;

	/**
	 * The method allows to view single news with given id.
	 *
	 * @param newsId the news id
	 * @return the newsVO
	 * @throws ServiceException the service exception
	 */
	NewsVO viewSingleNews(long newsId) throws ServiceException;


}