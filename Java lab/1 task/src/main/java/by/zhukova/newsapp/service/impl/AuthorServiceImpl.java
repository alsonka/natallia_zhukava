package by.zhukova.newsapp.service.impl;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import by.zhukova.newsapp.dao.AuthorDAO;
import by.zhukova.newsapp.entity.AuthorTO;
import by.zhukova.newsapp.exception.DAOException;
import by.zhukova.newsapp.exception.ServiceException;
import by.zhukova.newsapp.service.AuthorService;

// TODO: Auto-generated Javadoc
/**
 * The Class AuthorServiceImpl implements methods from AuthorService interface.
 * 
 * @author Natallia_Zhukava
 */
public class AuthorServiceImpl implements AuthorService {

	static Logger logger = Logger.getLogger(AuthorServiceImpl.class);
	private AuthorDAO authorDao;
	
	

	/**
	 * Sets the authorDAO
	 *
	 * @param authorDao the new authorDAO
	 */
	public void setAuthorDao(AuthorDAO authorDao) {
		this.authorDao = authorDao;
	}


	/**
	 * Instantiates a new author service implementation object.
	 */
	public AuthorServiceImpl() {
	}

	/**
	 * The method finds all authors.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.AuthorService#findAllAuthors()
	 */
	@Override
	public List<AuthorTO> findAllAuthors() throws ServiceException {
		List<AuthorTO> list = new LinkedList<AuthorTO>();
		try {
			list = authorDao.findAll();
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return list;
	}

	
	/**
	 * The method finds author by given identifier.
	 *
	 * @param authorId the author id
	 * @return the authorTO
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.AuthorService#findAuthorById(long)
	 */
	@Override
	public AuthorTO findAuthorById(long authorId) throws ServiceException {
		AuthorTO author = null;
		try {

			author = authorDao.findById(authorId);

		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return author;
	}

	
	/**
	 * The method finds the author of given news.
	 *
	 * @param newsId the news id
	 * @return the author to
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.AuthorService#findAuthorByNews(long)
	 */
	@Override
	public AuthorTO findAuthorByNews(long newsId) throws ServiceException {
		AuthorTO author = null;
		try {

			author = authorDao.findByNews(newsId);

		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return author;
	}

	
	/**
	 * The method saves new author.
	 *
	 * @param author the author
	 * @return the authorTO with generated identifier
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.AuthorService#saveAuthor(by.zhukova.newsapp.entity.AuthorTO)
	 */
	@Override
	public AuthorTO saveAuthor(AuthorTO author) throws ServiceException {

		try {

		long id = authorDao.save(author);
		author.setId(id);	

		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return author;
	}

	/**
	 * The method edits the information about author.
	 *
	 * @param author the author
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.AuthorService#editAuthor(by.zhukova.newsapp.entity.AuthorTO)
	 */
	@Override
	public void editAuthor(AuthorTO author) throws ServiceException {
		try {
			authorDao.update(author);	
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		
	}

	
	/**
	 * The method sets status "expired" to given author .
	 *
	 * @param author the author
	 * @return the authorTO with not null field "expired"
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.AuthorService#makeExpired(by.zhukova.newsapp.entity.AuthorTO)
	 */
	@Override
	public AuthorTO makeExpired(AuthorTO author) throws ServiceException {
		Calendar calendar = Calendar.getInstance();
		Date now = calendar.getTime();
		Timestamp currentTimestamp = new Timestamp(now.getTime());
		author.setExpiredDate(currentTimestamp);
		try {
			authorDao.update(author);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return author;
	}

	/**
	 * The method deletes author
	 * Not planned to use!
	 * @see by.zhukova.newsapp.service.AuthorService#deleteAuthor(long)
	 */
	@Override
	@Transactional
	public void deleteAuthor(long authorId) throws ServiceException {

		try {
		authorDao.delete(authorId);
		authorDao.detachAuthorNews(authorId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	

}
