package by.zhukova.newsapp.service;

import java.util.List;

import by.zhukova.newsapp.entity.CommentTO;
import by.zhukova.newsapp.exception.ServiceException;


/**
 * The Interface CommentService contains the service methods which should work with {@code AuthorTO} objects.
 * 
 * @author Natallia_Zhukava
 */
public interface CommentService {

	/**
	 * The method finds all comments.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	List<CommentTO> findAllComments() throws ServiceException;

	/**
	 * The method finds all comments attached to given news.
	 *
	 * @param newsId the news id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	List<CommentTO> findCommentsByNews(long newsId) throws ServiceException;

	/**
	 * The method finds comment by given id.
	 *
	 * @param id the id
	 * @return the comment to
	 * @throws ServiceException the service exception
	 */
	CommentTO findCommentById(long id) throws ServiceException;

	/**
	 * The method saves given comment
	 *
	 * @param comment the commentTO
	 * @return the commentTO object with generated id
	 * @throws ServiceException the service exception
	 */
	CommentTO saveComment(CommentTO comment) throws ServiceException;

	/**
	 * The method edits the given comment.
	 *
	 * @param comment the comment
	 * @throws ServiceException the service exception
	 */
	void editComment(CommentTO comment) throws ServiceException;

	/**
	 * The method deletes the comment with given id.
	 *
	 * @param id the id
	 * @throws ServiceException the service exception
	 */
	void deleteComment(long id) throws ServiceException;

}