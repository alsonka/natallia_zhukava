package by.zhukova.newsapp.entity;

import java.sql.Timestamp;

/**
 * The class Author contains data from table Author.
 *
 * @author Natallia_Zhukava
 */
public class AuthorTO extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private Timestamp expiredDate;

	/**
	 * Instantiates a new author.
	 */
	public AuthorTO() {
	}

	/**
	 * Instantiates a new author.
	 *
	 * @param id
	 *            the id
	 * @param name
	 *            the name
	 * @param expiredDate
	 *            the expired date
	 */
	public AuthorTO(int id, String name, Timestamp expiredDate) {
		super(id);
		this.name = name;
		this.expiredDate = expiredDate;
	}
	
	

	/**
	 * Instantiates a new author.
	 *
	 * @param name the name
	 * @param expiredDate the expired date
	 */
	public AuthorTO(String name, Timestamp expiredDate) {
		super();
		this.name = name;
		this.expiredDate = expiredDate;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the expired date.
	 *
	 * @return the expired date
	 */
	public Timestamp getExpiredDate() {
		return expiredDate;
	}

	/**
	 * Sets the expired date.
	 *
	 * @param expiredDate
	 *            the new expired date
	 */
	public void setExpiredDate(Timestamp expiredDate) {
		this.expiredDate = expiredDate;
	}

	@Override
	public String toString() {
		return "AuthorTO [id= " + getId() +  " name=" + name + ", expiredDate=" + expiredDate + "]";
	}

	/* (non-Javadoc)
	 * @see by.zhukova.newsapp.entity.Entity#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((expiredDate == null) ? 0 : expiredDate.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see by.zhukova.newsapp.entity.Entity#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof AuthorTO))
			return false;
		AuthorTO other = (AuthorTO) obj;
		if (expiredDate == null) {
			if (other.expiredDate != null)
				return false;
		} else if (!expiredDate.equals(other.expiredDate))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
