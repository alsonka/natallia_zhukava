package by.zhukova.newsapp.service;

import java.util.List;

import by.zhukova.newsapp.entity.AuthorTO;
import by.zhukova.newsapp.exception.ServiceException;

/**
 * The Interface AuthorService contains the service methods which should work with {@code AuthorTO} objects.
 * 
 * @author Natallia_Zhukava
 */
public interface AuthorService {
	
	/**
	 * The method returns list of all authors from database.
	 *
	 * @return the list of authors
	 * @throws ServiceException the service exception
	 */
	List<AuthorTO> findAllAuthors() throws ServiceException;
	
	/**
	 * The method gets information about concrete author from database by identifier.
	 *
	 * @param authorId identifier of author
	 * @return {@code Author} entity
	 * @throws ServiceException the service exception
	 */

	AuthorTO findAuthorById(long authorId) throws ServiceException;
    /**
     * The method gets information about concrete author from database by identifier of news
     * 
     * @param newsId identifier of news
     * @return {@code Author} entity
     * @throws ServiceException the service exception
     */
	AuthorTO findAuthorByNews(long newsId) throws ServiceException;
	/**
	 * The method saves information about given author to database
	 * 
	 * @param author
	 * @return {@code Author} entity
	 * @throws ServiceException the service exception
	 */

	AuthorTO saveAuthor(AuthorTO author) throws ServiceException;
	/**
	 * The method edits information about given author in database
	 * 
	 * @param author
	 * @throws ServiceException the service exception
	 * 
	 */

	void editAuthor(AuthorTO author) throws ServiceException;
    /**
     * The method sets status "expired" to given author
     * 
     * @param author
     * @return {@code Author} entity
     * @throws ServiceException
     */
	AuthorTO makeExpired(AuthorTO author) throws ServiceException;
	
	/**
	 * The method deletes given author
	 * Not planned to use!
	 * 
	 * @param authorId
	 * @throws ServiceException
	 */

	void deleteAuthor(long authorId) throws ServiceException;

}