package by.zhukova.newsapp.service.impl;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import by.zhukova.newsapp.dao.TagDAO;
import by.zhukova.newsapp.entity.TagTO;
import by.zhukova.newsapp.exception.DAOException;
import by.zhukova.newsapp.exception.ServiceException;
import by.zhukova.newsapp.service.TagService;


/**
 * The Class TagServiceImpl implements methods from TagService interface.
 */
public class TagServiceImpl implements TagService {

	static Logger logger = Logger.getLogger(TagServiceImpl.class);

	private TagDAO tagDao;
	

	/**
	 * Sets the tagDAO.
	 *
	 * @param tagDao the new tagDAO
	 */
	public void setTagDao(TagDAO tagDao) {
		this.tagDao = tagDao;
	}
	

	/**
	 * Instantiates a new tag service implementation object.
	 */
	public TagServiceImpl() {

	}

	/**
	 * The method finds all tags.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.TagService#findAllTags()
	 */
	@Override
	public List<TagTO> findAllTags() throws ServiceException {
		List<TagTO> list = new LinkedList<TagTO>();
		try {

			list = tagDao.findAll();

		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return list;
	}

	/**
	 * The method finds tag by given identifier
	 * @see by.zhukova.newsapp.service.TagService#findTagById(int)
	 */
	@Override
	public TagTO findTagById(int id) throws ServiceException {
		TagTO tag = null;
		try {
			tag = tagDao.findById(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return tag;
	}

	/**
	 * The method finds tags attached to given news.
	 *
	 * @param newsId the news id
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.TagService#findTagsByNews(long)
	 */
	@Override
	public List<TagTO> findTagsByNews(long newsId) throws ServiceException {
		List<TagTO> list = new LinkedList<TagTO>();
		try {

			list = tagDao.findByNews(newsId);

		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

		return list;
	}

	/**
	 * The method saves new tag.
	 *
	 * @param tag the tag
	 * @return the tagTO with generated id
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.TagService#saveTag(by.zhukova.newsapp.entity.TagTO)
	 */
	@Override
	public TagTO saveTag(TagTO tag) throws ServiceException {
		try {
		long id = tagDao.save(tag);
		tag.setId(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

		return tag;
	}

	/**
	 * The method edits given tag.
	 *
	 * @param tag the tag
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.TagService#editTag(by.zhukova.newsapp.entity.TagTO)
	 */
	@Override
	public void editTag(TagTO tag) throws ServiceException {

		try {

			tagDao.update(tag);

		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

	}

	/**
	 * The method deletes tag with given identifier and detaches it from news which it attached to.
	 *
	 * @param id the id
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.TagService#deleteTag(long)
	 */
	@Override
	@Transactional
	public void deleteTag(long id) throws ServiceException {

		try {

			tagDao.delete(id);
			tagDao.detachTagNews(id);

		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

	}

}
