package by.zhukova.newsapp.dao;

import java.util.List;

import by.zhukova.newsapp.entity.NewsTO;
import by.zhukova.newsapp.entity.SearchCriteriaVO;
import by.zhukova.newsapp.entity.TagTO;
import by.zhukova.newsapp.exception.DAOException;


/**
 * The Interface NewsDAO contains the methods which should work with {@code News} database table and tables {@code News_Tag} and {@code News_Author}.
 * 
 * @author Natallia_Zhukava
 */
public interface NewsDAO extends CommonDAO<NewsTO>{

	/**
	 * The method finds all news which attached to given tag
	 *
	 * @param tagId the tag id
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<NewsTO> findByTag(long tagId) throws DAOException;

	/**
	 * The method finds all news which attached to given author
	 *
	 * @param authorId the author id
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<NewsTO> findByAuthor(long authorId) throws DAOException;

	/**
	 * The method finds all news which match given criteria
	 *
	 * @param obj the SearchCriteria obj
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<NewsTO> findByCriteria(SearchCriteriaVO obj) throws DAOException;


	/**
	 * The method attaches author to news.
	 *
	 * @param newsId the news id
	 * @param authorId the author id
	 * @throws DAOException the DAO exception
	 */
	void attachAuthorToNews(long newsId, long authorId) throws DAOException;

	/**
	 * The method updates author of given news.
	 *
	 * @param newsId the news id
	 * @param authorId the author id
	 * @throws DAOException the DAO exception
	 */
	void updateNewsAuthor(long newsId, long authorId) throws DAOException;

	/**
	 * The method attaches tag to news.
	 *
	 * @param newsId the news id
	 * @param tagId the tag id
	 * @throws DAOException the DAO exception
	 */
	void attachTagToNews(long newsId, long tagId) throws DAOException;

	/**
	 * The method detaches all tags which were attached to given news.
	 *
	 * @param newsId the news id
	 * @throws DAOException the DAO exception
	 */
	void detachNewsTags(long newsId) throws DAOException;
	
	/**
	 * The method detaches the author from the given news
	 *
	 * @param newsId the news id
	 * @throws DAOException the DAO exception
	 */
	void detachNewsAuthor(long newsId) throws DAOException;

	/**
	 * The method attaches list of tags to given news.
	 *
	 * @param newsId the news id
	 * @param list the list
	 * @throws DAOException the DAO exception
	 */
	void attachTagsToNews(long newsId, List<TagTO> list) throws DAOException;
	
	/**
	 * The method counts quantity of news.
	 *
	 * @return the long the quantity of news
	 * @throws DAOException the DAO exception
	 */
	long countNews() throws DAOException;

	



}