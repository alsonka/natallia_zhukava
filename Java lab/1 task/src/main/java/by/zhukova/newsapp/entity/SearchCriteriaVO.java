package by.zhukova.newsapp.entity;

import java.util.List;

/**
 * The Class SearchCriteria contains {@code Author} and {@code Tag} entity for
 * search in news
 * 
 * @author Natallia_Zhukava
 */
public class SearchCriteriaVO {

	private AuthorTO author;
	private List<TagTO> tagList;

	/**
	 * Instantiates a new search criteria.
	 */
	public SearchCriteriaVO() {

	}

	/**
	 * Gets the author.
	 *
	 * @return the author
	 */
	public AuthorTO getAuthor() {
		return author;
	}

	/**
	 * Sets the author.
	 *
	 * @param author
	 *            the new author
	 */
	public void setAuthor(AuthorTO author) {
		this.author = author;
	}

	/**
	 * Gets the tag list.
	 *
	 * @return the tag list
	 */
	public List<TagTO> getTagList() {
		return tagList;
	}

	/**
	 * Sets the tag list.
	 *
	 * @param tagList
	 *            the new tag list
	 */
	public void setTagList(List<TagTO> tagList) {
		this.tagList = tagList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((tagList == null) ? 0 : tagList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof SearchCriteriaVO))
			return false;
		SearchCriteriaVO other = (SearchCriteriaVO) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (tagList == null) {
			if (other.tagList != null)
				return false;
		} else if (!tagList.equals(other.tagList))
			return false;
		return true;
	}

}
