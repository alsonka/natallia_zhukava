package by.zhukova.newsapp.dao;

import java.util.List;

import by.zhukova.newsapp.entity.CommentTO;
import by.zhukova.newsapp.exception.DAOException;


/**
 * The Interface CommentDAO contains the methods which should work with {@code Comments} database table.
 * 
 * @author Natallia_Zhukava
 */
public interface CommentDAO extends CommonDAO<CommentTO> {

	/**
	 * The method finds the comments attached to given news
	 *
	 * @param id the id
	 * @return the list of {@code CommentTO} objects
	 * @throws DAOException the DAO exception
	 */
	List<CommentTO> findByNewsId(long id) throws DAOException;

	}