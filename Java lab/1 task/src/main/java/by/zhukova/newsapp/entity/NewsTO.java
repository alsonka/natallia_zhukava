package by.zhukova.newsapp.entity;

import java.sql.Date;
import java.sql.Timestamp;


/**
 * The class News contains data from table News.
 *
 * @author Natallia_Zhukava
 */

public class NewsTO extends Entity {


	private static final long serialVersionUID = 1L;
	private String title;
	private String shortText;
	private String fullText;
	private Timestamp creationDate;
	private Date modificationDate;

	/**
	 * Instantiates a new news.
	 */
	public NewsTO() {
	}

	/**
	 * Instantiates a new news.
	 *
	 * @param id
	 *            the news id
	 * @param title
	 *            the title
	 * @param shortText
	 *            the short text
	 * @param fullText
	 *            the full text
	 * @param creationDate
	 *            the creation date
	 * @param modificationDate
	 *            the modification date
	 */
	public NewsTO(long id, String title, String shortText, String fullText, Timestamp creationDate,
			Date modificationDate) {
		super(id);
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}
	
	

	/**
	 * Instantiates a new news.
	 *
	 * @param title the title
	 * @param shortText the short text
	 * @param fullText the full text
	 * @param creationDate the creation date
	 * @param modificationDate the modification date
	 */
	public NewsTO(String title, String shortText, String fullText, Timestamp creationDate, Date modificationDate) {
		super();
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the short text.
	 *
	 * @return the short text
	 */
	public String getShortText() {
		return shortText;
	}

	/**
	 * Sets the short text.
	 *
	 * @param shortText
	 *            the new short text
	 */
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	/**
	 * Gets the full text.
	 *
	 * @return the full text
	 */
	public String getFullText() {
		return fullText;
	}

	/**
	 * Sets the full text.
	 *
	 * @param fullText
	 *            the new full text
	 */
	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Timestamp getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate
	 *            the new creation date
	 */
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the modification date.
	 *
	 * @return the modification date
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * Sets the modification date.
	 *
	 * @param modificationDate
	 *            the new modification date
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "News [ id =" + getId() + ", title=" + title + ", shortText=" + shortText + ", fullText=" + fullText
				+ ", creationDate=" + creationDate + ", modificationDate=" + modificationDate + "]";
	}

	/* (non-Javadoc)
	 * @see by.zhukova.newsapp.entity.Entity#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see by.zhukova.newsapp.entity.Entity#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof NewsTO))
			return false;
		NewsTO other = (NewsTO) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}
