package by.zhukova.newsapp.service;

import java.util.List;

import by.zhukova.newsapp.entity.NewsVO;
import by.zhukova.newsapp.entity.SearchCriteriaVO;
import by.zhukova.newsapp.exception.ServiceException;


/**
 * The Interface SearchService contains method which works with search in database.
 * 
 * @author Natallia_Zhukava
 */
public interface SearchService {

	/**
	 * The method searches by search criteria
	 *
	 * @param search the search criteria
	 * @return the list the list of news
	 * @throws ServiceException the service exception
	 */
	List<NewsVO> searchBy(SearchCriteriaVO search) throws ServiceException;

}