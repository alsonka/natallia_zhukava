package by.zhukova.newsapp.entity;

import java.util.List;

/**
 * The Class ComplexNews contains {@code News}, {@code Author}, {@code Tag} and
 * {@code Comment} entities which represent concrete news with author, tags and
 * comments
 * 
 * @author Natallia_Zhukava
 *
 */
public class NewsVO {

	private NewsTO news;
	private AuthorTO author;
	private List<TagTO> tagList;
	private List<CommentTO> commentList;

	/**
	 * Instantiates a new complex news.
	 */
	public NewsVO() {

	}

	/**
	 * Gets the news.
	 *
	 * @return the news
	 */
	public NewsTO getNews() {
		return news;
	}

	/**
	 * Sets the news.
	 *
	 * @param news
	 *            the new news
	 */
	public void setNews(NewsTO news) {
		this.news = news;
	}

	/**
	 * Gets the author.
	 *
	 * @return the author
	 */
	public AuthorTO getAuthor() {
		return author;
	}

	/**
	 * Sets the author.
	 *
	 * @param author
	 *            the new author
	 */
	public void setAuthor(AuthorTO author) {
		this.author = author;
	}

	/**
	 * Gets the tag list.
	 *
	 * @return the tag list
	 */
	public List<TagTO> getTagList() {
		return tagList;
	}

	/**
	 * Sets the tag list.
	 *
	 * @param tagList
	 *            the new tag list
	 */
	public void setTagList(List<TagTO> tagList) {
		this.tagList = tagList;
	}

	/**
	 * Gets the comment list.
	 *
	 * @return the comment list
	 */
	public List<CommentTO> getCommentList() {
		return commentList;
	}

	/**
	 * Sets the comment list.
	 *
	 * @param commentList
	 *            the new comment list
	 */
	public void setCommentList(List<CommentTO> commentList) {
		this.commentList = commentList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((commentList == null) ? 0 : commentList.hashCode());
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		result = prime * result + ((tagList == null) ? 0 : tagList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof NewsVO))
			return false;
		NewsVO other = (NewsVO) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (commentList == null) {
			if (other.commentList != null)
				return false;
		} else if (!commentList.equals(other.commentList))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		if (tagList == null) {
			if (other.tagList != null)
				return false;
		} else if (!tagList.equals(other.tagList))
			return false;
		return true;
	}

}
