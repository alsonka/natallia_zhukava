package by.zhukova.newsapp.dao;


import by.zhukova.newsapp.entity.AuthorTO;
import by.zhukova.newsapp.exception.DAOException;


/**
 * The Interface AuthorDAO contains the methods which should work with database {@code Author}
 * 
 * @author Natallia_Zhukava
 */
public interface  AuthorDAO extends CommonDAO<AuthorTO> {

	/**
	 * The method finds the author attached to given news.
	 *
	 * @param newsId the news id
	 * @return the {@code AuthorTO} object
	 * @throws DAOException the DAO exception
	 */
	AuthorTO findByNews(long newsId) throws DAOException;
	
	/**
	 * The method deletes all news attached to given author
	 *
	 * @param authorId the author id
	 * @throws DAOException the DAO exception
	 */
	void detachAuthorNews(long authorId) throws DAOException;


}