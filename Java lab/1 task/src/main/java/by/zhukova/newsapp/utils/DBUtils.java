package by.zhukova.newsapp.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;


/**
 * The Class DBUtils contains utility methods which works with database.
 */
public class DBUtils {
	
	static Logger logger = Logger.getLogger(DBUtils.class);

	/**
	 * Instantiates a new DBUtils object.
	 */
	public DBUtils() {
		
	}
	
	/**
	 * The method closes the ResultSet.
	 *
	 * @param res the res
	 */
	public static void closeResultSet(ResultSet res) {
		try {
			if (res != null) {
				res.close();
			}
		} catch (SQLException e) {
			logger.error(e);
		}
	}
	
	/**
	 * The method closes statement.
	 *
	 * @param st the st
	 */
	public static void closeStatement(Statement st) {
		try {
			if (st != null) {
				st.close();
			}
		} catch (SQLException e) {
			logger.error(e);
		}
	}
	
	/**
	 * The method closes connection.
	 *
	 * @param con the con
	 */
	public static void closeConnection(Connection con) {
		try {
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			logger.error(e);
		}
	}
	
	

}
