package by.zhukova.newsapp.dao;

import java.util.List;

import by.zhukova.newsapp.entity.TagTO;
import by.zhukova.newsapp.exception.DAOException;

/**
 * The Interface TagDAO contains methods which should work with {@code Tags} database table.
 */
public interface TagDAO extends CommonDAO<TagTO> {



	/**
	 * The methods finds all tags which attached to given news
	 *
	 * @param newsId the news id
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<TagTO> findByNews(long newsId) throws DAOException;

	/**
	 * The method finds tag by given tag name.
	 *
	 * @param name the name
	 * @return the tag to
	 * @throws DAOException the DAO exception
	 */
	TagTO findByTagName(String name) throws DAOException;
	
	/**
	 * The method detaches the given tag from all news
	 *
	 * @param tagId the tag id
	 * @throws DAOException the DAO exception
	 */
	void detachTagNews(long tagId) throws DAOException;

	

}