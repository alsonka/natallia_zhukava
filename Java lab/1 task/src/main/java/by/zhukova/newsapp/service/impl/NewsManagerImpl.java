package by.zhukova.newsapp.service.impl;


import java.util.ArrayList;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import by.zhukova.newsapp.entity.AuthorTO;
import by.zhukova.newsapp.entity.CommentTO;
import by.zhukova.newsapp.entity.NewsVO;
import by.zhukova.newsapp.entity.NewsTO;
import by.zhukova.newsapp.entity.TagTO;
import by.zhukova.newsapp.exception.ServiceException;
import by.zhukova.newsapp.service.AuthorService;
import by.zhukova.newsapp.service.CommentService;
import by.zhukova.newsapp.service.NewsManager;
import by.zhukova.newsapp.service.NewsService;
import by.zhukova.newsapp.service.TagService;

// TODO: Auto-generated Javadoc
/**
 * The Class NewsManagerImpl implements methods from NewsManager interface.
 * 
 * @author Natallia_Zhukava
 */
public class NewsManagerImpl implements NewsManager {

	private NewsService newsService;
	private AuthorService authorService;
	private TagService tagService;
	private CommentService commentService;
	
	/**
	 * Sets the news service.
	 *
	 * @param newsService the new news service
	 */
	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	/**
	 * Sets the author service.
	 *
	 * @param authorService the new author service
	 */
	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	/**
	 * Sets the tag service.
	 *
	 * @param tagService the new tag service
	 */
	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	/**
	 * Sets the comment service.
	 *
	 * @param commentService the new comment service
	 */
	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}


	

	static Logger logger = Logger.getLogger(NewsManagerImpl.class);


	/**
	 * Instantiates a new news manager implementation object.
	 */
	public NewsManagerImpl() {

	}

	/**
	 * The method saves news with author and tags.
	 *
	 * @param complexNews the complex news
	 * @return the newsTO with generated id
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.NewsManager#saveNews(by.zhukova.newsapp.entity.NewsVO)
	 */
	@Override
	@Transactional
	public NewsTO saveNews(NewsVO complexNews) throws ServiceException {
		NewsTO news = complexNews.getNews();
		AuthorTO author = complexNews.getAuthor();
		List<TagTO> list = complexNews.getTagList();

		newsService.saveNews(news);
		newsService.attachNewsAuthor(news.getId(), author.getId());

		if (list.size() != 0) {
			newsService.attachNewsTags(news.getId(), list);
		}

		return news;

	}

	/**
	 * The method edits news with author and tags.
	 *
	 * @param complexNews the complex news
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.NewsManager#editNews(by.zhukova.newsapp.entity.NewsVO)
	 */
	@Override
	@Transactional
	public void editNews(NewsVO complexNews) throws ServiceException {
		NewsTO news = complexNews.getNews();
		AuthorTO author = complexNews.getAuthor();
		List<TagTO> list = complexNews.getTagList();
		
			newsService.editNews(news);
			newsService.attachNewsAuthor(news.getId(), author.getId());
			newsService.detachNewsTags(news.getId());
			newsService.attachNewsTags(news.getId(), list);

	}


	/**
	 * The method deletes the news by given identifier and detaches author and tags.
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.NewsManager#deleteNews(long)
	 */
	@Override
	public void deleteNews(long newsId) throws ServiceException {
		newsService.deleteNews(newsId);
		newsService.detachNewsTags(newsId);
		newsService.detachNewsAuthor(newsId);

		
	}



	/**
	 * The method allows to view news list sorted by comments number and modification date.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.NewsManager#viewNewsList()
	 */
	@Override
	@Transactional
	public List<NewsVO> viewNewsList() throws ServiceException {
		List<NewsVO> newsList = new ArrayList<NewsVO>();
					
			List<NewsTO> list = newsService.findAllNews();
			for (NewsTO news : list) {
				NewsVO cNews = viewSingleNews(news.getId());
				newsList.add(cNews);
			}
			
		return newsList;

	}


	/**
	 *  
	 * The method allows to view single news with author, tags and comments.
	 *
	 * @param newsId the news id
	 * @return the newsVO
	 * @throws ServiceException the service exception
	 * @see by.zhukova.newsapp.service.NewsManager#viewSingleNews(long)
	 */
	@Override
	@Transactional
	public NewsVO viewSingleNews(long newsId) throws ServiceException {
		NewsVO complexNews = new NewsVO();

		NewsTO news = null;
		AuthorTO author = null;
		List<TagTO> tagList = new ArrayList<TagTO>();
		List<CommentTO> commentList = new ArrayList<CommentTO>();

			
			news = newsService.findNewsById(newsId);

			author = authorService.findAuthorByNews(newsId);
			tagList = tagService.findTagsByNews(newsId);
			commentList = commentService.findCommentsByNews(newsId);

			complexNews.setNews(news);
			complexNews.setAuthor(author);
			complexNews.setTagList(tagList);
			complexNews.setCommentList(commentList);
			
		
		return complexNews;
	}
	

}
