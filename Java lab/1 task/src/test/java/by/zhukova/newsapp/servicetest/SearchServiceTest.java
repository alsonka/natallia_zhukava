package by.zhukova.newsapp.servicetest;

import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.zhukova.newsapp.dao.NewsDAOImpl;
import by.zhukova.newsapp.entity.SearchCriteriaVO;
import by.zhukova.newsapp.exception.DAOException;
import by.zhukova.newsapp.exception.ServiceException;
import by.zhukova.newsapp.service.SearchService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
public class SearchServiceTest {
	
	@Mock
	private NewsDAOImpl newsDao;
	@Mock
	private SearchCriteriaVO obj;

	@Autowired
	@InjectMocks
	private SearchService searchService;

	public SearchServiceTest() {
		
	}
	
	@Before
	public void setUp() throws DAOException {
        MockitoAnnotations.initMocks(this);
	}

	@Test
	public void searchByTest() throws DAOException, ServiceException   {
		searchService.searchBy(obj);
		verify(newsDao).findByCriteria(obj);


	}

}
