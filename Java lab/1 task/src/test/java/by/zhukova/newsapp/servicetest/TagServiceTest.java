package by.zhukova.newsapp.servicetest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.zhukova.newsapp.dao.TagDAO;
import by.zhukova.newsapp.entity.TagTO;
import by.zhukova.newsapp.exception.DAOException;
import by.zhukova.newsapp.exception.ServiceException;
import by.zhukova.newsapp.service.TagService;

import static org.mockito.Mockito.*;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
public class TagServiceTest {

	@Mock
	private TagDAO tagDao;
	
	@Autowired
	@InjectMocks
	private TagService tagService;
	
	public TagServiceTest() {
	}
	
	@Before
	public void setUp() throws DAOException {
		 MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testFindAllTags() throws DAOException, ServiceException  {
		tagService.findAllTags();
		verify(tagDao).findAll();
	}
	
	@Test 
	public void testFindTag() throws DAOException, ServiceException {
		when (tagDao.findById(2)).thenReturn(new TagTO(2, "tag2"));
		
		TagTO expected = new TagTO(2, "tag2");
		TagTO actual = tagService.findTagById(2);
		verify(tagDao).findById(2);	
		assertEquals(expected, actual);
	}
	
	@Test
	public void testSaveTag() throws DAOException, ServiceException {
		TagTO tag = new TagTO("tag");
		when (tagDao.save(tag)).thenReturn(1L);
		
		long expected = 1;
		
		long actual = tagService.saveTag(tag).getId();
		verify(tagDao).save(tag);
		assertEquals(expected, actual);	
	}
	
	@Test
	public void testEditTag() throws DAOException, ServiceException {
		
		TagTO tag = new TagTO(1, "tag");
		tagService.editTag(tag);
		verify(tagDao).update(tag);
		
	}
	
	@Test
	public void testDeleteTag() throws DAOException, ServiceException {
		long id = 1L;
		tagService.deleteTag(id);
		verify(tagDao).delete(id);
		verify(tagDao).detachTagNews(id);
		
	}
	
	@Test
	public void testFindTagsByNews() throws DAOException, ServiceException {
		tagService.findTagsByNews(1);
		verify(tagDao).findByNews(1);
	}

}