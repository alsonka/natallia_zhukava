package by.zhukova.newsapp.dbtest;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;

import java.util.LinkedList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;

import by.zhukova.newsapp.dao.AuthorDAOImpl;
import by.zhukova.newsapp.dao.NewsDAO;
import by.zhukova.newsapp.dao.TagDAO;
import by.zhukova.newsapp.entity.AuthorTO;
import by.zhukova.newsapp.entity.NewsTO;
import by.zhukova.newsapp.entity.SearchCriteriaVO;
import by.zhukova.newsapp.entity.TagTO;
import by.zhukova.newsapp.exception.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class NewsDAOTest extends AbstractTest {

	@Autowired
	private NewsDAO newsDAO;
	@Autowired
	private AuthorDAOImpl authorDAO;
	@Autowired
	private TagDAO tagDAO;

	public NewsDAOTest() {

	}

	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news-data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/comment-data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/tag-data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/author-data.xml"))

		};
		return new CompositeDataSet(datasets);
	}

	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());

	}

	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void testFindAll() throws DAOException {

		List<NewsTO> comments = newsDAO.findAll();
		assertThat(comments.size(), is(6));
	}

	@Test
	public void testFindById() throws DAOException, ParseException {
		long id = 3;
		NewsTO expected = new NewsTO(id, "News1", "ShortText", "Full text", Timestamp.valueOf("2015-08-14 09:28:29.66"),
				Date.valueOf("2015-08-14"));
		NewsTO actual = newsDAO.findById(id);
		assertEquals(expected, actual);
	}

	@Test
	public void testFindByTag() throws DAOException {
		List<NewsTO> expected = new LinkedList<NewsTO>();
		expected.add(newsDAO.findById(1));
		expected.add(newsDAO.findById(4));
		Collections.sort(expected, new ListNewsComparator());

		List<NewsTO> actual = newsDAO.findByTag(5);
		Collections.sort(actual, new ListNewsComparator());
		assertEquals(expected, actual);
	}

	@Test
	public void testFindByAuthor() throws DAOException {
		List<NewsTO> expected = new LinkedList<NewsTO>();
		expected.add(newsDAO.findById(1));
		expected.add(newsDAO.findById(3));
		Collections.sort(expected, new ListNewsComparator());
		
		List<NewsTO> actual = newsDAO.findByAuthor(2);
		Collections.sort(actual, new ListNewsComparator());
		assertEquals(expected, actual);
	}

	@Test
	public void testFindByCriteria() throws DAOException {
		SearchCriteriaVO obj = new SearchCriteriaVO();
		obj.setAuthor(new AuthorTO(2, "Alex Barchuk", null));
		List<TagTO> tags = new LinkedList<TagTO>();
		tags.add(new TagTO(2, "Apple"));
		tags.add(new TagTO(5, "iOS"));
		obj.setTagList(tags);

		NewsTO n1 = newsDAO.findById(1);

		List<NewsTO> expected = new LinkedList<NewsTO>();
		expected.add(n1);

		List<NewsTO> actual = newsDAO.findByCriteria(obj);

		assertEquals(expected, actual);
	}

	@Test
	public void testFindBySearchCriteriaWithTagsOnly() throws DAOException {
		SearchCriteriaVO obj = new SearchCriteriaVO();

		List<TagTO> tags = new LinkedList<TagTO>();
		tags.add(new TagTO(2, "Apple"));
		tags.add(new TagTO(5, "iOS"));
		obj.setTagList(tags);

		NewsTO n1 = newsDAO.findById(1);

		List<NewsTO> expected = new LinkedList<NewsTO>();
		expected.add(n1);

		List<NewsTO> actual = newsDAO.findByCriteria(obj);

		assertEquals(expected, actual);
	}

	@Test
	public void testFindBySearchCriteriaWithAuthorOnly() throws DAOException {
		SearchCriteriaVO obj = new SearchCriteriaVO();
		obj.setAuthor(new AuthorTO(2, "Alex Barchuk", null));

		NewsTO n1 = newsDAO.findById(1);
		NewsTO n2 = newsDAO.findById(3);

		List<NewsTO> expected = new LinkedList<NewsTO>();
		expected.add(n1);
		expected.add(n2);
		Collections.sort(expected, new ListNewsComparator());

		List<NewsTO> actual = newsDAO.findByCriteria(obj);
		
		Collections.sort(actual, new ListNewsComparator());
		assertEquals(expected, actual);
	}

	@Test
	public void testSaveNews() throws DAOException {
		Calendar calendar = Calendar.getInstance();
		java.util.Date now = calendar.getTime();
		Timestamp currentTimestamp = new Timestamp(now.getTime());
		// SQL date sets time to 0:00:00, so if we want to compare them, we
		// should get object of Date with such value of time
		calendar.setTime(now);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		// object for comparing
		NewsTO expected = new NewsTO("News6", "ShortText", "Full text", currentTimestamp,
				new Date(calendar.getTime().getTime()));

		long id = newsDAO.save(expected);
		expected.setId(id);
		NewsTO actual = newsDAO.findById(id);

		assertEquals(expected, actual);
	}

	@Test
	public void testAttachAuthorToNews() throws DAOException {
		Calendar calendar = Calendar.getInstance();
		java.util.Date now = calendar.getTime();
		Timestamp currentTimestamp = new Timestamp(now.getTime());

		NewsTO news = new NewsTO("News7", "ShortText", "Full text", currentTimestamp, new Date(now.getTime()));
		long newsId = newsDAO.save(news);

		newsDAO.attachAuthorToNews(newsId, 5);
		AuthorTO expected = new AuthorTO(5, "Julia Ryabuhina", null);
		AuthorTO actual = authorDAO.findByNews(newsId);

		assertEquals(expected, actual);
	}

	@Test
	public void testAttachTagToNews() throws DAOException {
		NewsTO news = newsDAO.findById(5);
		TagTO tag = tagDAO.findById(1);
		boolean expected = true;
		newsDAO.attachTagToNews(news.getId(), 1);

		List<TagTO> tags = tagDAO.findByNews(news.getId());
		boolean actual = tags.contains(tag);

		assertEquals(expected, actual);
	}

	@Test
	public void testUpdateAuthorNews() throws DAOException {
		NewsTO news = newsDAO.findById(2);
		AuthorTO expected = new AuthorTO(5, "Julia Ryabuhina", null);
		newsDAO.updateNewsAuthor(news.getId(), 5);
		AuthorTO actual = authorDAO.findByNews(news.getId());

		assertEquals(expected, actual);

	}

	@Test
	public void testUpdateNews() throws DAOException {

		NewsTO expected = new NewsTO(2, "News2", "ShortText", "Full text", Timestamp.valueOf("2015-08-14 09:28:29.66"),
				Date.valueOf("2015-08-14"));
		NewsTO news = newsDAO.findById(2);
		news.setTitle("News2");
		newsDAO.update(news);
		NewsTO actual = newsDAO.findById(2);
		assertEquals(expected, actual);
	}

	@Test
	public void testDelete() throws DAOException {

		NewsTO expected = newsDAO.findById(4);
		newsDAO.delete(4);
		NewsTO actual = newsDAO.findById(4);
		assertNotEquals(expected, actual);
	}

	@Test
	public void testAddNewsTags() throws DAOException {
		List<TagTO> expected = new LinkedList<TagTO>();
		expected.add(tagDAO.findById(4));
		expected.add(tagDAO.findById(7));
		Collections.sort(expected, new ListTagComparator());
		Calendar calendar = Calendar.getInstance();
		java.util.Date now = calendar.getTime();
		Timestamp currentTimestamp = new Timestamp(now.getTime());

		NewsTO news = new NewsTO("News7", "ShortText", "Full text", currentTimestamp, new Date(now.getTime()));
		long newsId=newsDAO.save(news);

		newsDAO.attachTagsToNews(newsId, expected);

		List<TagTO> actual = tagDAO.findByNews(newsId);
		Collections.sort(expected, new ListTagComparator());
		
		assertEquals(expected, actual);
	}

	@Test
	public void testDeleteNewsTags() throws DAOException {
		newsDAO.detachNewsTags(4);
		List<TagTO> list = tagDAO.findByNews(4);
		assertThat(list.size(), is(0));
	}
	
	@Test
	public void testDeleteNewsAuthor() throws DAOException {
		AuthorTO expected = new AuthorTO(7, "Nastassia Demidova", null);
		newsDAO.detachNewsAuthor(6);
		AuthorTO actual = authorDAO.findByNews(6);
		
		assertNotEquals(expected, actual);
	}
	
	@Test
	public void testCountNews() throws DAOException {
		long expected = 6;
		long actual = newsDAO.countNews();
		assertEquals(expected, actual);
	}
	
	private class ListTagComparator implements Comparator<TagTO> {

		public int compare(TagTO o1, TagTO o2) {
			if (o1.getId() > o2.getId()) {
				return 1;
			} else if (o1.getId() < o2.getId()) {
				return -1;
			} else {
				return 0;
			}
		}
	}
	
	private class ListNewsComparator implements Comparator<NewsTO> {

		public int compare(NewsTO o1, NewsTO o2) {
			if (o1.getId() > o2.getId()) {
				return 1;
			} else if (o1.getId() < o2.getId()) {
				return -1;
			} else {
				return 0;
			}
		}
	}



}
