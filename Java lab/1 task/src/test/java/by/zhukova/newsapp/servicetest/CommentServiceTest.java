package by.zhukova.newsapp.servicetest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.zhukova.newsapp.dao.CommentDAOImpl;
import by.zhukova.newsapp.entity.CommentTO;
import by.zhukova.newsapp.exception.DAOException;
import by.zhukova.newsapp.exception.ServiceException;
import by.zhukova.newsapp.service.CommentService;

import static org.mockito.Mockito.*;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
public class CommentServiceTest {

	@Mock
	private CommentDAOImpl commentDao;
	@Mock
	CommentTO comment;
	
	@Autowired
	@InjectMocks
	private CommentService commentService;
	
	public CommentServiceTest() {		
	}
	
	@Before
	public void setUp() throws DAOException {
        MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testFindAllComments() throws DAOException, ServiceException  {
		commentService.findAllComments();
		verify(commentDao).findAll();
	}
	
	@Test 
	public void testFindCommentById() throws DAOException, ServiceException {
		int id = 2;
		commentService.findCommentById(id);
		verify(commentDao).findById(id);
	}
	
	@Test 
	public void testFindCommentByNews() throws DAOException, ServiceException {
		int id = 2;
		commentService.findCommentsByNews(id);
		verify(commentDao).findByNewsId(id);
	}
	
	@Test
	public void testSaveComment() throws DAOException, ServiceException {
		CommentTO comment = new CommentTO(2, "comment", null);
		when (commentDao.save(comment)).thenReturn(1L);
		
		long expected = 1L;
		long actual = commentService.saveComment(comment).getId();
		verify(commentDao).save(comment);
		assertEquals(expected, actual);	
	}
	
	@Test
	public void testEditComment() throws DAOException, ServiceException {
		
		commentService.editComment(comment);
		verify(commentDao).update(comment);
		
	}
	
	@Test
	public void testDeleteComment() throws DAOException, ServiceException {
		long id = 2L;
		commentService.deleteComment(id);
		verify(commentDao).delete(id);
		
	}
	


}
