package by.zhukova.newsapp.servicetest;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.zhukova.newsapp.dao.AuthorDAOImpl;
import by.zhukova.newsapp.entity.AuthorTO;
import by.zhukova.newsapp.exception.DAOException;
import by.zhukova.newsapp.exception.ServiceException;
import by.zhukova.newsapp.service.AuthorService;


import static org.mockito.Mockito.*;

import java.sql.Timestamp;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
public class AuthorServiceTest {

	@Mock
	private AuthorDAOImpl authorDao;
	
	@Autowired
	@InjectMocks
	private AuthorService authorService;
	
	public AuthorServiceTest() {		
	}
	
	@Before
	public void setUp() throws DAOException {
        MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testFindAllAuthors() throws DAOException, ServiceException  {
		authorService.findAllAuthors();
		verify(authorDao).findAll();
	}
	
	@Test 
	public void testFindAuthorById() throws DAOException, ServiceException {
		when (authorDao.findById(2)).thenReturn(new AuthorTO(2, "author", null));
		
		AuthorTO expected = new AuthorTO(2, "author", null);
		AuthorTO actual = authorService.findAuthorById(2);
		verify(authorDao).findById(2);
		assertEquals(expected, actual);
	}
	
	@Test 
	public void testFindAuthorByNews() throws DAOException, ServiceException {
		when (authorDao.findByNews(2)).thenReturn(new AuthorTO (2, "second author", null));
		
		AuthorTO expected = new AuthorTO (2, "second author", null);
		AuthorTO actual  = authorService.findAuthorByNews(2);
		verify(authorDao).findByNews(2);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testSaveAuthor() throws DAOException, ServiceException {
		AuthorTO author = new AuthorTO("author", null);
		when (authorDao.save(author)).thenReturn(1L);
		
		long expected = 1;
		long actual = authorService.saveAuthor(author).getId();
		verify(authorDao).save(author);
		assertEquals(expected, actual);	
	}
	
	@Test
	public void testEditAuthor() throws DAOException, ServiceException {
		AuthorTO author = new AuthorTO(1, "author", null);
		authorService.editAuthor(author);
		verify(authorDao).update(author);
		
	}
	
	@Test
	public void testDeleteAuthor() throws DAOException, ServiceException {
		
		long id = 1L;
		authorService.deleteAuthor(id);
		verify(authorDao).delete(id);
	}
	
	@Test
	public void testMakeExpired() throws ServiceException, DAOException {
		Timestamp expected = null;
		AuthorTO author = new AuthorTO(1, "author", null);
		authorService.makeExpired(author);
		Timestamp actual = author.getExpiredDate();
		verify(authorDao).update(author);
		assertNotEquals(expected, actual);
	}

}
