package by.zhukova.newsapp.servicetest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.zhukova.newsapp.entity.AuthorTO;
import by.zhukova.newsapp.entity.NewsVO;
import by.zhukova.newsapp.entity.NewsTO;
import by.zhukova.newsapp.entity.TagTO;
import by.zhukova.newsapp.exception.DAOException;
import by.zhukova.newsapp.exception.ServiceException;
import by.zhukova.newsapp.service.AuthorService;
import by.zhukova.newsapp.service.CommentService;
import by.zhukova.newsapp.service.NewsManager;
import by.zhukova.newsapp.service.NewsService;
import by.zhukova.newsapp.service.TagService;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
public class NewsManagerTest {

	@Mock
	private NewsService newsService;
	@Mock
	private AuthorService authorService;
	@Mock
	private TagService tagService;
	@Mock
	private CommentService commentService;
	
	@Autowired
	@InjectMocks
	private NewsManager newsManager;
	
	private NewsVO complexNews;
	
	public NewsManagerTest() {
		complexNews = new NewsVO();
		AuthorTO author = new AuthorTO(2, "author", null);
		complexNews.setAuthor(author);
		List<TagTO> tags = new ArrayList<TagTO>();
		tags.add(new TagTO(1, "tag1"));
		tags.add(new TagTO(2, "tag2"));
		complexNews.setTagList(tags);
	}
	
	@Before
	public void setUp() throws DAOException, ServiceException {
		MockitoAnnotations.initMocks(this);		
	}
	

	
	@Test
	public void viewNewsListTest() throws ServiceException, DAOException {
		
		newsManager.viewNewsList();
		verify(newsService).findAllNews();
	}
	
	@Test
	public void viewSingleNewsTest() throws DAOException, ServiceException {
		NewsTO expected = new NewsTO();
		expected.setId(2);
		when (newsService.findNewsById(2)).thenReturn(expected);
		NewsTO actual = newsManager.viewSingleNews(2).getNews();
		verify(newsService).findNewsById(2);
		verify(authorService).findAuthorByNews(2);
		verify(tagService).findTagsByNews(2);
		verify(commentService).findCommentsByNews(2);
		assertEquals(expected, actual);
	}
	
	@Test
	public void createNewsTest() throws DAOException, ServiceException {
		long expected = 1;
		NewsTO news = new NewsTO();
		news.setId(expected);
		complexNews.setNews(news);
		long actual = newsManager.saveNews(complexNews).getId();
		verify(newsService).saveNews(news);
		verify(newsService).attachNewsAuthor(news.getId(), complexNews.getAuthor().getId());
		verify(newsService).attachNewsTags(news.getId(), complexNews.getTagList());
		assertEquals(expected, actual);	
	}
	
	@Test
	public void editNewsTest() throws DAOException, ServiceException {
		
		NewsTO news = new NewsTO();
		complexNews.setNews(news);
		newsManager.editNews(complexNews);
		verify(newsService).editNews(news);
		
	}
	
	@Test
	public void deleteNewsTest() throws DAOException, ServiceException {
		
		long id = 1L;
		newsManager.deleteNews(id);
		verify(newsService).deleteNews(id);
		verify(newsService).detachNewsAuthor(id);
		verify(newsService).detachNewsTags(id);
		
	}
	
	

}