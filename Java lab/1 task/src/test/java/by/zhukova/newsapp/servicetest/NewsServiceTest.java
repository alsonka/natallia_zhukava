package by.zhukova.newsapp.servicetest;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import by.zhukova.newsapp.dao.NewsDAO;
import by.zhukova.newsapp.entity.NewsTO;
import by.zhukova.newsapp.entity.TagTO;
import by.zhukova.newsapp.exception.DAOException;
import by.zhukova.newsapp.exception.ServiceException;
import by.zhukova.newsapp.service.NewsService;

import static org.mockito.Mockito.*;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
public class NewsServiceTest {

	@Mock
	private NewsDAO newsDao;
	
	@Autowired
	@InjectMocks
	private NewsService newsService;
	
	public NewsServiceTest() {		
	}
	
	@Before
	public void setUp() throws DAOException {
        MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testFindAllNews() throws DAOException, ServiceException  {
		newsService.findAllNews();
		verify(newsDao).findAll();
	}
	
	@Test 
	public void testFindNewsById() throws DAOException, ServiceException {
		when (newsDao.findById(2)).thenReturn(new NewsTO(2, "title", "short_text", "full_text", null, null));
		
		NewsTO expected =  new NewsTO(2, "title", "short_text", "full_text", null, null);
		NewsTO actual = newsService.findNewsById(2);
		verify(newsDao).findById(2);
		assertEquals(expected, actual);
	}
	
	
	@Test
	public void testSaveNews() throws DAOException, ServiceException {
		NewsTO news = new NewsTO("title", "short_text", "full_text", null, null);
		when (newsDao.save(news)).thenReturn(1L);
		
		long expected = 1;
		long actual = newsService.saveNews(news).getId();
		verify(newsDao).save(news);
		assertEquals(expected, actual);	
	}
	
	@Test
	public void testEditNews() throws DAOException, ServiceException {
		NewsTO news = new NewsTO("title", "short_text", "full_text", null, null);
		newsService.editNews(news);
		verify(newsDao).update(news);
		
	}
	
	@Test
	public void testDeleteNews() throws DAOException, ServiceException {
		long id = 1L;
		newsService.deleteNews(id);
		verify(newsDao).delete(id);
	}
	
	@Test
	public void testFindNewsByAuthor() throws DAOException, ServiceException {
		long authorId=4L;
		newsService.findNewsByAuthor(authorId);
		verify(newsDao).findByAuthor(authorId);
	}
	
	@Test
	public void testCountNews() throws DAOException, ServiceException {
		when (newsDao.countNews()).thenReturn(15L);
		
		long expected = 15L;
		long actual = newsService.countAllNews();
		verify(newsDao).countNews();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testSaveNewsTags() throws ServiceException, DAOException {
		long newsId = 1L;
		List<TagTO> list = new LinkedList<TagTO>();
		newsService.attachNewsTags(newsId, list);
		verify(newsDao).attachTagsToNews(newsId, list);
	}
	
	@Test
	public void testSaveNewsAuthor() throws ServiceException, DAOException {
		long newsId = 1L;
		long authorId = 2L;
		newsService.attachNewsAuthor(newsId, authorId);
		verify(newsDao).attachAuthorToNews(newsId, authorId);
	}
	
	
	@Test
	public void testDeleteNewsTags() throws ServiceException, DAOException {
		long newsId = 1L;
		
		newsService.detachNewsTags(newsId);
		verify(newsDao).detachNewsTags(newsId);
	}
	
	@Test
	public void testDeleteNewsAuthor() throws ServiceException, DAOException {
		long newsId = 1L;
		
		newsService.detachNewsAuthor(newsId);
		verify(newsDao).detachNewsAuthor(newsId);
	}
	
	

}
