package by.zhukova.newsapp.dbtest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.Calendar;

import java.util.Date;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;

import by.zhukova.newsapp.dao.CommentDAO;
import by.zhukova.newsapp.entity.CommentTO;
import by.zhukova.newsapp.exception.DAOException;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class CommentDAOTest extends AbstractTest {

	@Autowired
	private CommentDAO commentDAO;

	public CommentDAOTest() {

	}

	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news-data.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/comment-data.xml"))
				
				};
		return new CompositeDataSet(datasets);
	}

	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());
		
	}

	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void testFindAll() throws DAOException {

		List<CommentTO> comments = commentDAO.findAll();
		assertThat(comments.size(), is(11));
	}

	@Test
	public void testFindById() throws DAOException {
		CommentTO expected = new CommentTO(3, 2, "Third",  Timestamp.valueOf("2015-08-14 10:28:29.66"));
		CommentTO actual = commentDAO.findById(3);
		assertEquals(expected, actual);
	}

	@Test
	public void testSave() throws DAOException {
		Calendar calendar = Calendar.getInstance();
		Date now = calendar.getTime();
		Timestamp currentTimestamp = new Timestamp(now.getTime());
		
		CommentTO expected = new CommentTO(4, "New Comment", currentTimestamp);
		long id = commentDAO.save(expected);
		expected.setId(id);
		CommentTO actual = commentDAO.findById(id);
		assertEquals(expected, actual);
	}

	@Test
	public void testUpdate() throws DAOException {

		CommentTO expected = new CommentTO(6, 3,  "Changed", Timestamp.valueOf("2015-08-14 11:28:29.66"));
		CommentTO comment = commentDAO.findById(6);
		comment.setText("Changed");
		commentDAO.update(comment);
		CommentTO actual = commentDAO.findById(6);
		assertEquals(expected, actual);
	}

	@Test
	public void testDelete() throws DAOException {

		CommentTO expected = commentDAO.findById(8);
		commentDAO.delete(8);
		CommentTO actual = commentDAO.findById(8);
		assertNotEquals(expected, actual);
	}

	@Test
	public void testFindByNews() throws DAOException {
		List<CommentTO> list = new LinkedList<CommentTO>();
		list.add(commentDAO.findById(4));
		list.add(commentDAO.findById(6));
		Object[] expected = list.toArray();
		List<CommentTO> list2 = commentDAO.findByNewsId(3);
		Object[] actual = list2.toArray();
		assertArrayEquals(expected, actual);
	}
	
	

	


}
