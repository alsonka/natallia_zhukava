package by.zhukova.tariffs.creation;
import by.zhukova.tariffs.tariff.BasicTariff;

public interface AbstractFactory {
	
	public BasicTariff createTariff();
	

}
